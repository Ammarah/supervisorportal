/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.anandm.apps;

import com.apollo.dao.impl.AgentGatewayServiceConsumer;
import Model.LoginAjaxParams;
import Model.LoginParams;
import Model.LoginResponse;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author Abdul Rehman
 */
@Controller
public class LoginController {

    private static final Logger logger = LoggerFactory.getLogger(LoginController.class);
    @Autowired
    @Qualifier("loginAuthenticationManager")
    AuthenticationManager authenticationManager;

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String SignIn(Locale locale, Model model) {

        //logger.info("Welcome home! The client locale is {}.", locale);
        return "login";
    }

    @RequestMapping(value = "/loginrequest", method = RequestMethod.POST)
    public @ResponseBody
    LoginResponse LoginPOSTRequest(@RequestBody LoginAjaxParams param) throws URISyntaxException {

        LoginResponse response = new LoginResponse();
        AgentGatewayServiceConsumer service = new AgentGatewayServiceConsumer();
        LoginParams loginParams = new LoginParams();
        loginParams.setAgenttype(4);
        loginParams.setPassword(param.getPassword());
        loginParams.setPhonenum(param.getPhonenum());
        loginParams.setStatus(4);
        //this is login request
        response = service.forceLogin(loginParams, param.getWorkId());
        System.out.println("Login Response" + response.getRetcode() + "\t Message: \t" + response.getMessage() + "\t Result" + response.getResult());
        // check response if retcode is zero
        if (response.getRetcode().equalsIgnoreCase("0")) {
            if (authenticationManager != null) {
                UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(param.getWorkId(), param.getPassword());
                List<GrantedAuthority> grantedAuths = new ArrayList<GrantedAuthority>();
                grantedAuths.add(new SimpleGrantedAuthority("ROLE_USER"));
                User details = new User(param.getWorkId(), param.getPassword(), grantedAuths);
                token.setDetails(details);
                try {
                    Authentication auth = authenticationManager.authenticate(token);
                    SecurityContextHolder.getContext().setAuthentication(auth);
                    //return new LoginStatus(auth.isAuthenticated(), auth.getName());
                } catch (Exception e) {
                    //return new LoginStatus(false, null);
                    System.out.println("" + e.getMessage());
                    response.setMessage(e.getMessage());
                }
            }

        } else {
            //response.setRetcode("100-001");
            //response.setMessage("Invlid workid or password");
        }

        return response;
        // your logic next
    }

    @RequestMapping(value = "/redirect_login", method = RequestMethod.GET)
    public String RedirectToLogin(HttpServletRequest request, HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null && auth.isAuthenticated()) {
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }
        return "login";
    }

}
