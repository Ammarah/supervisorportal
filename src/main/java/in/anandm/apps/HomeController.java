package in.anandm.apps;

import Model.AgentInfoOfSpecifiedVDN;
import Model.AgentNameSkillsWrapper;
import com.apollo.dao.impl.AgentGatewayServiceConsumer;
import Model.AllAgentSkills;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * Handles requests for the application home page.
 */
@Controller
public class HomeController {

    private static final Logger logger = LoggerFactory.getLogger(HomeController.class);

    @RequestMapping(value = "/agent/dashboard", method = RequestMethod.GET)
    public String dashboard(Model mv) {
        AllAgentSkills agentSkillsResponse = new AllAgentSkills();
        AgentInfoOfSpecifiedVDN resultWithAgentName = new AgentInfoOfSpecifiedVDN();
        AgentNameSkillsWrapper response = new AgentNameSkillsWrapper();
        List<AgentNameSkillsWrapper.Result> rsList = new ArrayList<AgentNameSkillsWrapper.Result>();
        Integer agentNo = 0;
        try {
            AgentGatewayServiceConsumer service = new AgentGatewayServiceConsumer();       
            agentSkillsResponse = service.getAllAgentStatusInfo();
            resultWithAgentName = service.getAgentNames();
            if (agentSkillsResponse.getRetcode().equalsIgnoreCase("0") && resultWithAgentName.getRetcode().equalsIgnoreCase("0")) {
                for (int i = 0; i < agentSkillsResponse.getResult().size(); i++) {
                    for (int j = 0; j < resultWithAgentName.getResult().size(); j++) {
                        agentNo = Integer.parseInt(resultWithAgentName.getResult().get(j).getAgentNo());
                        if (agentSkillsResponse.getResult().get(i).getAgentId() == agentNo) {
                            AgentNameSkillsWrapper.Result rsltObj = new AgentNameSkillsWrapper.Result();
                            rsltObj.setAgentId(agentSkillsResponse.getResult().get(i).getAgentId());
                            rsltObj.setAgentName(resultWithAgentName.getResult().get(j).getAgentName());
                            rsltObj.setAgentStatusTime(agentSkillsResponse.getResult().get(i).getAgentStatusTime());
                            rsltObj.setCurrentPrivateCallNum(agentSkillsResponse.getResult().get(i).getCurrentPrivateCallNum());
                            rsltObj.setCurrentState(agentSkillsResponse.getResult().get(i).getCurrentState());
                            rsltObj.setCurrentStateTime(agentSkillsResponse.getResult().get(i).getCurrentStateTime());
                            rsltObj.setInMultimediaConf(agentSkillsResponse.getResult().get(i).getInMultimediaConf());
                            rsltObj.setLocationId(agentSkillsResponse.getResult().get(i).getLocationId());
                            rsltObj.setMonitor(agentSkillsResponse.getResult().get(i).getMonitor());
                            rsltObj.setRecord(agentSkillsResponse.getResult().get(i).getRecord());
                            rsltObj.setStatus(agentSkillsResponse.getResult().get(i).getStatus());
                            rsltObj.setSupers(agentSkillsResponse.getResult().get(i).getSupers());
                            rsltObj.setWorkGroupId(agentSkillsResponse.getResult().get(i).getWorkGroupId());
                            rsList.add(rsltObj);
                            resultWithAgentName.getResult().remove(j);
                            break;
                        }
                    }
                }
                response.setMessage(agentSkillsResponse.getMessage());
                response.setRetcode(agentSkillsResponse.getRetcode());
                response.setResult(rsList);
            }

            if (response.getRetcode().equalsIgnoreCase("0")) {
                for (int i = 0; i < response.getResult().size(); i++) {
                    String id = String.valueOf(response.getResult().get(i).getAgentId());
                    if (id.startsWith("6")) {
                        //6 is agent id
                        response.getResult().remove(i);
                        i--;
                    }
                    if (id.startsWith("8")) {
                        //8 is supervisor id
                        response.getResult().remove(i);
                        i--;
                    }
                    
                }

                // ye samj ay ke list hainhan
                mv.addAttribute("agentList", response.getResult());
                //mv.addAttribute("name", "hello");
            }
        } catch (Exception ex) {
            System.out.println("" + ex.getMessage());
        }

        //mv.addObject("carList", carManager.getCarList());
        return "agent/dashboard";
    }

 @RequestMapping(value = "/agent/dashboard_v2", method = RequestMethod.GET)
    public String dashboard_v2(Model mv) {
        try {
            AgentGatewayServiceConsumer service = new AgentGatewayServiceConsumer();
            AllAgentSkills response = service.getAllAgentStatusInfo();

            if (response.getRetcode().equalsIgnoreCase("0")) {
                for (int i = 0; i < response.getResult().size(); i++) {
                    String id = String.valueOf(response.getResult().get(i).getAgentId());
                    if (id.startsWith("6")) {
                        //6 is agent id
                        response.getResult().remove(i);
                        i--;
                    }
                    if (id.startsWith("8")) {
                        //8 is supervisor id
                        response.getResult().remove(i);
                        i--;
                    }
                }

                // ye samj ay ke list hainhan
                mv.addAttribute("agentList", response.getResult());
                //mv.addAttribute("name", "hello");
            }
        } catch (Exception ex) {
            System.out.println("" + ex.getMessage());
        }

        //mv.addObject("carList", carManager.getCarList());
        return "agent/dashboard_v2";
    }
    
    @RequestMapping(value = "/CallAudioRecord/AgentStatisticsReport", method = RequestMethod.GET)
    public String callrecords(Model mv) {
        return "Call_Records/Call-Records";
    }

    @RequestMapping(value = "/skill/agent-info", method = RequestMethod.GET)
    public String Skillagentinfo(Model mv) {
        try {
            AgentGatewayServiceConsumer service = new AgentGatewayServiceConsumer();
            AllAgentSkills response = service.getAllAgentStatusInfo();

            if (response.getRetcode().equalsIgnoreCase("0")) {
                for (int i = 0; i < response.getResult().size(); i++) {
                    String id = String.valueOf(response.getResult().get(i).getAgentId());
                    if (id.startsWith("6")) {
                        response.getResult().remove(i);
                        i--;
                    }
                    if (id.startsWith("8")) {
                        response.getResult().remove(i);
                        i--;
                    }
                }
                // ye samj ay ke list hainhan
                mv.addAttribute("agentList", response.getResult());
            }
        } catch (Exception ex) {
            System.out.println("" + ex.getMessage());
        }

        //mv.addObject("carList", carManager.getCarList());
        return "Call_Records/Call-Records";
    }

    @RequestMapping(value = "/pages/{type}/{page}", method = RequestMethod.GET)
    public String pagesOfTypes(Locale locale, Model model, @PathVariable(value = "type") String type, @PathVariable(value = "page") String page) {
        logger.info("Welcome page! The client locale is {}.", locale);

        Date date = new Date();
        DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);

        String formattedDate = dateFormat.format(date);

        model.addAttribute("serverTime", formattedDate);

        return "pages/" + type + "/" + page;
    }

    @RequestMapping(value = "/pages/{page}", method = RequestMethod.GET)
    public String pages(Locale locale, Model model, @PathVariable(value = "page") String page) {
        logger.info("Welcome page! The client locale is {}.", locale);

        Date date = new Date();
        DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);

        String formattedDate = dateFormat.format(date);

        model.addAttribute("serverTime", formattedDate);

        return "pages/" + page;
    }
}
