package in.anandm.apps;

import com.apollo.dao.impl.AgentGatewayServiceConsumer;
import Model.AgenInfoResponse;
import Model.AgentBreaks;
import Model.AgentDurationResponse;
import Model.AgentInfoOfSpecifiedVDN;
import Model.AgentNameSkillsWrapper;
import Model.AgentPoolResponse;
import Model.AgentSkillAdjustData;
import Model.AgentSkillStatisticsDto;
import Model.AgentSkillsByWorkno;
import Model.AgentStatisticsDto;
import Model.AllAgentSkills;
import Model.AgentNameSkillsWrapper.Result;
import Model.CMSAgentHistoryResponse;
import Model.CheckAgentStatus;
import Model.DefaultResponse;
import Model.SpecificAgentDetailResponse;
import Model.VDNSkillsResponse;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Handles requests for the application home page.
 */
@Controller
public class AjaxController {

    private static final Logger logger = LoggerFactory.getLogger(AjaxController.class);

    @RequestMapping(value = "/agentpoolingstatus", method = RequestMethod.GET)
    public @ResponseBody
    AgentPoolResponse AgentPool() throws URISyntaxException, URISyntaxException {

        AgentGatewayServiceConsumer service = new AgentGatewayServiceConsumer();
        AgentPoolResponse res = service.agentPoolRequest();
        return res;
    }
    
       public AgentNameSkillsWrapper getAllAgents() {
        AllAgentSkills agentSkillsResponse = new AllAgentSkills();
        AgentInfoOfSpecifiedVDN resultWithAgentName = new AgentInfoOfSpecifiedVDN();
        AgentNameSkillsWrapper response = new AgentNameSkillsWrapper();
        List<Result> rsList = new ArrayList<Result>();
        Integer agentNo = 0;
        Integer agentId= 0;
        try {
            AgentGatewayServiceConsumer service = new AgentGatewayServiceConsumer();
            agentSkillsResponse = service.getAllAgentStatusInfo();
            resultWithAgentName = service.getAgentNames();
            if (agentSkillsResponse.getRetcode().equalsIgnoreCase("0") && resultWithAgentName.getRetcode().equalsIgnoreCase("0")) {
                for (int i = 0; i < agentSkillsResponse.getResult().size(); i++) {
                    for (int j = 0; j < resultWithAgentName.getResult().size(); j++) {
                        agentNo = Integer.parseInt(resultWithAgentName.getResult().get(j).getAgentNo());
                        agentId = agentSkillsResponse.getResult().get(i).getAgentId();
                        System.out.println("Agent no  : " + agentNo + "agenId  : " + agentId);
                        if (Integer.compare(agentId, agentNo) == 0){
                            Result rsltObj = new Result();
                            rsltObj.setAgentId(agentSkillsResponse.getResult().get(i).getAgentId());
                            rsltObj.setAgentName(resultWithAgentName.getResult().get(j).getAgentName());
                            rsltObj.setAgentStatusTime(agentSkillsResponse.getResult().get(i).getAgentStatusTime());
                            rsltObj.setCurrentPrivateCallNum(agentSkillsResponse.getResult().get(i).getCurrentPrivateCallNum());
                            rsltObj.setCurrentState(agentSkillsResponse.getResult().get(i).getCurrentState());
                            rsltObj.setCurrentStateTime(agentSkillsResponse.getResult().get(i).getCurrentStateTime());
                            rsltObj.setInMultimediaConf(agentSkillsResponse.getResult().get(i).getInMultimediaConf());
                            rsltObj.setLocationId(agentSkillsResponse.getResult().get(i).getLocationId());
                            rsltObj.setMonitor(agentSkillsResponse.getResult().get(i).getMonitor());
                            rsltObj.setRecord(agentSkillsResponse.getResult().get(i).getRecord());
                            rsltObj.setStatus(agentSkillsResponse.getResult().get(i).getStatus());
                            rsltObj.setSupers(agentSkillsResponse.getResult().get(i).getSupers());
                            rsltObj.setWorkGroupId(agentSkillsResponse.getResult().get(i).getWorkGroupId());
                            rsList.add(rsltObj);
                            resultWithAgentName.getResult().remove(j);
                            break;
                        }
                    }
                }
                response.setMessage(agentSkillsResponse.getMessage());
                response.setRetcode(agentSkillsResponse.getRetcode());
                response.setResult(rsList);
            }
            if (response.getRetcode().equalsIgnoreCase("0")) {
                for (int i = 0; i < response.getResult().size(); i++) {
                    String id = String.valueOf(response.getResult().get(i).getAgentId());
                    if (id.startsWith("6")) {
                        response.getResult().remove(i);
                        i--;
                    }
                    if (id.startsWith("8")) {
                        response.getResult().remove(i);
                        i--;
                    }
                }
            }
        } catch (Exception ex) {
            System.out.println("" + ex.getMessage());
        }
        return response;
    }

    //dashboard v2
    @RequestMapping(value = "/agent/GetAllAgentsDashboardV2", method = RequestMethod.GET)
    @ResponseBody
    public String GetAllAgentsDashboardV2() {
        AgentNameSkillsWrapper response = null;
        Gson gson = new Gson();
        JsonObject jsonResponse = new JsonObject();
        try {
            response = getAllAgents();
            jsonResponse.add("data", gson.toJsonTree(response));
        } catch (Exception ex) {
            System.out.println("" + ex.getMessage());
        }
        return jsonResponse.toString();
    }
    //dashboard v2
    @RequestMapping(value = "/agent/filterAgentsDashboardV2/{searchType}/{searchValue}", method = RequestMethod.GET)
    @ResponseBody
    public String filterAgentsDashboardV2(@PathVariable String searchType, @PathVariable String searchValue) {
        AgentNameSkillsWrapper response = null;
        Gson gson = new Gson();
        JsonObject jsonResponse = new JsonObject();
        try {
            response = getAllAgents();
            //for status serach
            if (searchType.equals("AgentStatus")) {
                int status = Integer.parseInt(searchValue);
                if (status == -1) {
                    jsonResponse.add("data", gson.toJsonTree(response));
                    return jsonResponse.toString();
                }
                for (int i = 0; i < response.getResult().size(); i++) {
                    int ret_status = response.getResult().get(i).getStatus();
                    System.out.println("status result:" + ret_status);
                    if (ret_status != status) {
                        response.getResult().remove(i);
                        i--;
                    }
                }
            } //for agent name search
            else if (searchType.equals("AgentName")) {
                for (int i = 0; i < response.getResult().size(); i++) {
                    String AgentName = response.getResult().get(i).getAgentName();
                    if (!Pattern.compile(Pattern.quote(searchValue), Pattern.CASE_INSENSITIVE).matcher(AgentName).find()) {
                        response.getResult().remove(i);
                        i--;
                    }
                }
            }//for agent id search
            else {
                for (int i = 0; i < response.getResult().size(); i++) {
                    int AgentId = response.getResult().get(i).getAgentId();
                    int agent = Integer.parseInt(searchValue);
                    if (AgentId != agent) {
                        response.getResult().remove(i);
                        i--;
                    }
                }
            }
            jsonResponse.add("data", gson.toJsonTree(response));
        } catch (Exception ex) {
            System.out.println("" + ex.getMessage());
        }
        return jsonResponse.toString();
    }

    //Dashboard v1
    @RequestMapping(value = "/agent/GetAllAgentsDashboardV1", method = RequestMethod.GET)
    @ResponseBody
    public AgentNameSkillsWrapper GetAllAgentsDashboardV1() {
        AgentNameSkillsWrapper response = null;
        try {
            response = getAllAgents();
        } catch (Exception ex) {
            System.out.println("" + ex.getMessage());
        }
        return response;
    }

    @RequestMapping(value = "/agent/filterAgentsDashboardV1/{searchType}/{searchValue}", method = RequestMethod.GET)
    @ResponseBody
    public AgentNameSkillsWrapper filterAgentsDashboardV1(@PathVariable String searchType, @PathVariable String searchValue) {
        AgentNameSkillsWrapper response = null;
        try {
            response = getAllAgents();
            //for status serach
            if (searchType.equals("AgentStatus")) {
                int status = Integer.parseInt(searchValue);
                if (status == -1) {
                    return response;
                }
                for (int i = 0; i < response.getResult().size(); i++) {
                    int ret_status = response.getResult().get(i).getStatus();
                    if (ret_status != status) {
                        response.getResult().remove(i);
                        i--;
                    }
                }
            } //for agent name search
            else if (searchType.equals("AgentName")) {
                for (int i = 0; i < response.getResult().size(); i++) {
                    String AgentName = response.getResult().get(i).getAgentName();
                    if (!Pattern.compile(Pattern.quote(searchValue), Pattern.CASE_INSENSITIVE).matcher(AgentName).find()) {
                        response.getResult().remove(i);
                        i--;
                    }
                }
            }//for agent id search
            else {
                for (int i = 0; i < response.getResult().size(); i++) {
                    int AgentId = response.getResult().get(i).getAgentId();
                    int agent = Integer.parseInt(searchValue);
                    if (AgentId != agent) {
                        response.getResult().remove(i);
                        i--;
                    }
                }
            }
        } catch (Exception ex) {
            System.out.println("" + ex.getMessage());
        }
        return response;

    }

    @RequestMapping(value = {"/agent_logout", "/agent/agent_logout", "/agent/agent_logout"}, method = RequestMethod.GET)
    @ResponseBody
    public DefaultResponse AgentLogout(HttpServletRequest request, HttpServletResponse response) throws URISyntaxException {

        DefaultResponse res = new DefaultResponse();
        AgentGatewayServiceConsumer service = new AgentGatewayServiceConsumer();
        res = service.logout();
        System.out.println("res.get mesage:" + res.getMessage());
        System.out.println("res. get retcod: " + res.getRetcode());
        if(res.getMessage().contains("Logout Failed") || !res.getRetcode().equalsIgnoreCase("0") ){
              System.out.println("Already logged out");       
        }
        if (res.getRetcode().equalsIgnoreCase("0")) {
            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            if (auth != null && auth.isAuthenticated()) {
                new SecurityContextLogoutHandler().logout(request, response, auth);
            }
            request.getSession().invalidate();
        } else {
        }

        return res;
    }

    @RequestMapping(value = "/agent/agentsetidle", method = RequestMethod.GET)
    @ResponseBody
    public DefaultResponse AgentSetIdle(String agentId) throws URISyntaxException {
        DefaultResponse res = new DefaultResponse();
        AgentGatewayServiceConsumer service = new AgentGatewayServiceConsumer();
        res = service.setAgentIdle(agentId);
        if (res.getRetcode().equalsIgnoreCase("0")) {
            res.setMessage("Set Agent Idle Successfully");
        } else {
            res.setMessage("Set Agent Idle Failed");
        }

        return res;
    }

    @RequestMapping(value = "/agent/agentsetnotready", method = RequestMethod.GET)
    @ResponseBody
    public DefaultResponse AgentSetNotReady(String agentId) throws URISyntaxException {
        DefaultResponse res = new DefaultResponse();
        AgentGatewayServiceConsumer service = new AgentGatewayServiceConsumer();
        res = service.setAgentBusy(agentId);
        if (res.getRetcode().equalsIgnoreCase("0")) {
            res.setMessage("Set Agent Not Ready Successfully");
        } else {
            res.setMessage("Set Agent Not Ready Failed");
        }

        return res;
    }

    @RequestMapping(value = "/agent/agentsetbreak", method = RequestMethod.GET)
    @ResponseBody
    public DefaultResponse AgentSetBreak(String agentId, String time, String ReasonId) throws URISyntaxException {
        DefaultResponse res = new DefaultResponse();
        AgentGatewayServiceConsumer service = new AgentGatewayServiceConsumer();
        res = service.setAgentRest(agentId, time, ReasonId);
        if (res.getRetcode().equalsIgnoreCase("0")) {
            res.setMessage("Set Agent Break Successfully");
        } else {
            res.setMessage("Set Agent Break Failed");
        }

        return res;
    }

    @RequestMapping(value = "/agent/agentgetbreaks", method = RequestMethod.GET)
    @ResponseBody
    public AgentBreaks AgentGetBreak() throws URISyntaxException {
        AgentBreaks res = new AgentBreaks();
        AgentGatewayServiceConsumer service = new AgentGatewayServiceConsumer();
        res = service.AgentGetBreak();
        if (res.getRetcode().equalsIgnoreCase("0")) {
            res.setMessage("Set Agent Break Successfully");
        } else {
            res.setMessage("Set Agent Break Failed");
        }

        return res;
    }

    @RequestMapping(value = "/agent/agentsetinsert", method = RequestMethod.GET)
    @ResponseBody
    public DefaultResponse AgentSetInsert(String agentId) throws URISyntaxException {
        DefaultResponse res = new DefaultResponse();
        AgentGatewayServiceConsumer service = new AgentGatewayServiceConsumer();
        res = service.setAgentInsert(agentId);
        if (res.getRetcode().equalsIgnoreCase("0")) {
            res.setMessage("Set Agent Whisper Successfully");
        } else {
            res.setMessage("Set Agent Whisper Failed");
        }

        return res;
    }

    @RequestMapping(value = "/agent/agentsetlisten", method = RequestMethod.GET)
    @ResponseBody
    public DefaultResponse AgentSetListen(String agentId) throws URISyntaxException {

        DefaultResponse res = new DefaultResponse();
        AgentGatewayServiceConsumer service = new AgentGatewayServiceConsumer();
        res = service.setAgentListeningOn(agentId);
        if (res.getRetcode().equalsIgnoreCase("0")) {
            res.setMessage("Set Agent Listen Successfully");
        } else {
            res.setMessage("Set Agent Listen Failed");
        }

        return res;
    }

    @RequestMapping(value = "/agent/agentstoplisteninsert", method = RequestMethod.GET)
    @ResponseBody
    public DefaultResponse AgentStopListenInsert(String agentId) throws URISyntaxException {

        DefaultResponse res = new DefaultResponse();
        AgentGatewayServiceConsumer service = new AgentGatewayServiceConsumer();
        res = service.setAgentStopInsertListeningOn(agentId);
        if (res.getRetcode().equalsIgnoreCase("0")) {
            res.setMessage("Set Agent Listen/Whisper Successfully");
        } else {
            res.setMessage("Set Agent Listen/Whisper Failed");
        }

        return res;
    }

    @RequestMapping(value = "/agent/agentsetforcelogout", method = RequestMethod.GET)
    @ResponseBody
    public DefaultResponse AgentSetForceLogout(String agentId) throws URISyntaxException {

        DefaultResponse res = new DefaultResponse();
        AgentGatewayServiceConsumer service = new AgentGatewayServiceConsumer();
        res = service.setAgentForceLogout(agentId);
        if (res.getRetcode().equalsIgnoreCase("0")) {
            res.setMessage("Set Agent Logout Successfully");
        } else {
            res.setMessage("Set Agent Logout Failed");
        }

        return res;
    }

//    @RequestMapping(value = "/agent/agentgetstatistics", method = RequestMethod.GET)
//    @ResponseBody
//    public CMSAgentHistoryResponse AgentGetStatistics(String agentId) {
//        CMSAgentHistoryResponse res = new CMSAgentHistoryResponse();
//        SpecificAgentDetailResponse agentbyworkno = new SpecificAgentDetailResponse();
//        AgentGatewayServiceConsumer service = new AgentGatewayServiceConsumer();
//        res = service.getAgentStatistics(agentId);
//        agentbyworkno = service.getSpecificAgentDetail(agentId);
//        if (res.getRetcode().equalsIgnoreCase("0")) {
//
//        } else {
//
//        }
//
//        return res;
//    }
    @RequestMapping(value = "/agent/agentgetstatistics", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public AgentStatisticsDto AgentGetStatistics(String agentId) throws JsonProcessingException, ParseException, URISyntaxException {
        ObjectMapper objectMapper = new ObjectMapper();
        CMSAgentHistoryResponse StatList = new CMSAgentHistoryResponse();
        SpecificAgentDetailResponse AgentDetailData = new SpecificAgentDetailResponse();
        AgentDurationResponse Agentdurationresp = new AgentDurationResponse();
        AgentGatewayServiceConsumer service = new AgentGatewayServiceConsumer();
        StatList = service.getAgentStatistics(agentId);
        AgentDetailData = service.getSpecificAgentDetail(agentId);
        Agentdurationresp = service.getAgentDuration();
        AgentStatisticsDto dto = new AgentStatisticsDto(StatList, AgentDetailData, Agentdurationresp);
        objectMapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
        String CumlativeResponse = objectMapper.writeValueAsString(dto);
        if (StatList.getRetcode().equalsIgnoreCase("0")) {
        } else {

        }
        return dto;
    }

    @RequestMapping(value = "/agent/agentadjustskill", method = RequestMethod.POST)
    @ResponseBody
    public DefaultResponse AgentAdjustSkill(@RequestBody AgentSkillAdjustData data) throws URISyntaxException {

        DefaultResponse res = new DefaultResponse();
        AgentGatewayServiceConsumer service = new AgentGatewayServiceConsumer();
        res = service.adjustAgentSkill(data);
        if (res.getRetcode().equalsIgnoreCase("0")) {

        } else {

        }

        return res;
    }
//    @RequestMapping(value = "/agent/getallskills", method = RequestMethod.GET)
//    @ResponseBody
//    public VDNSkillsResponse GetAllSkills(String agentId) throws URISyntaxException {
//
//        VDNSkillsResponse res = new VDNSkillsResponse();
//        AgentSkillsByWorkno AgentSkillsresult = new AgentSkillsByWorkno();
//        AgentGatewayServiceConsumer service = new AgentGatewayServiceConsumer();
//        AgentSkillsresult = service.agentskillsbyworkno(agentId);
//        res = service.getAllVDNSkills();
//        if (res.getRetcode().equalsIgnoreCase("0")) {
//
//        } else {
//
//        }
//
//        return res;
//    }

    @RequestMapping(value = "/agent/getallskills", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public AgentSkillStatisticsDto GetAllSkills(String agentId) throws JsonProcessingException, ParseException, URISyntaxException {
        ObjectMapper objectMapper = new ObjectMapper();
        VDNSkillsResponse res = new VDNSkillsResponse();
        AgentSkillsByWorkno resUniskill = new AgentSkillsByWorkno();
        AgentGatewayServiceConsumer service = new AgentGatewayServiceConsumer();
        res = service.getAllVDNSkills();
        resUniskill = service.agentskillsbyworkno(agentId);
        AgentSkillStatisticsDto dto = new AgentSkillStatisticsDto(res, resUniskill);
        objectMapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
        String CumlativeResponse = objectMapper.writeValueAsString(dto);
        if (res.getRetcode().equalsIgnoreCase("0")) {
        } else {

        }
        return dto;
    }

    @RequestMapping(value = "/agent/checkAgentStatus", method = RequestMethod.GET)
    @ResponseBody
    public CheckAgentStatus checkAgentStatus(HttpServletRequest request, HttpServletResponse response) throws URISyntaxException {
        CheckAgentStatus res = new CheckAgentStatus();
        AgentGatewayServiceConsumer service = new AgentGatewayServiceConsumer();
        res = service.CheckAgentStatus();
        if (res == null) {
            return res;
        } else {
            if (res.getEvent() != null) {
                res.getEvent().getEventType();
                return res;
            } else {
                res.getRetcode();
                return res;
            }
        }
    }

}
