/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.List;

/**
 *
 * @author Abdul Rehman
 */
public class CMSAgentHistoryParams {

    private String ccId;
    private String vdnId;
    private List<Integer> objectIds = null;
    private List<String> indexIds = null;

    public String getCcId() {
        return ccId;
    }

    public void setCcId(String ccId) {
        this.ccId = ccId;
    }

    public String getVdnId() {
        return vdnId;
    }

    public void setVdnId(String vdnId) {
        this.vdnId = vdnId;
    }

    public List<Integer> getObjectIds() {
        return objectIds;
    }

    public void setObjectIds(List<Integer> objectIds) {
        this.objectIds = objectIds;
    }

    public List<String> getIndexIds() {
        return indexIds;
    }

    public void setIndexIds(List<String> indexIds) {
        this.indexIds = indexIds;
    }

}
