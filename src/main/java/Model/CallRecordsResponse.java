/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.List;

/**
 *
 * @author Iceman
 */
public class CallRecordsResponse {

//    private List<Result> result = null;
//
//    public List<Result> getResult() {
//        return result;
//    }
//
//    public void setResult(List<Result> result) {
//        this.result = result;
//    }
//
//    public static class Result {
    private String CALLID;
    private String AGENTID;
    private String CALLEENO;
    private String CALLERNO;
    private String BEGINTIME;
    private String ENDTIME;
    private String FILENAME;

    public String getCALLID() {
        return CALLID;
    }

    public void setCALLID(String CALLID) {
        this.CALLID = CALLID;
    }

    public String getAGENTID() {
        return AGENTID;
    }

    public void setAGENTID(String AGENTID) {
        this.AGENTID = AGENTID;
    }

    public String getCALLEENO() {
        return CALLEENO;
    }

    public void setCALLEENO(String CALLEENO) {
        this.CALLEENO = CALLEENO;
    }

    public String getCALLERNO() {
        return CALLERNO;
    }

    public void setCALLERNO(String CALLERNO) {
        this.CALLERNO = CALLERNO;
    }

    public String getBEGINTIME() {
        return BEGINTIME;
    }

    public void setBEGINTIME(String BEGINTIME) {
        this.BEGINTIME = BEGINTIME;
    }

    public String getENDTIME() {
        return ENDTIME;
    }

    public void setENDTIME(String ENDTIME) {
        this.ENDTIME = ENDTIME;
    }

    public String getFILENAME() {
        return FILENAME;
    }

    public void setFILENAME(String FILENAME) {
        this.FILENAME = FILENAME;
    }
//    }

}
