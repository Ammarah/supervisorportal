/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author Abdul Rehman
 */
import java.io.Serializable;
import java.util.List;

public class AllAgentSkills implements Serializable{

    private String message;
    private String retcode;
    private List<Result> result = null;

    public AllAgentSkills()
    {
        
    }
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getRetcode() {
        return retcode;
    }

    public void setRetcode(String retcode) {
        this.retcode = retcode;
    }

    public List<Result> getResult() {
        return result;
    }

    public void setResult(List<Result> result) {
        this.result = result;
    }

    public static class Result {

        private Integer agentId;
        private Integer workGroupId;
        private Integer record;
        private Integer supers;
        private Integer monitor;
        private Integer status;
        private Integer agentStatusTime;
        private Integer currentState;
        private Integer currentStateTime;
        private Integer locationId;
        private Integer currentPrivateCallNum;
        private Integer inMultimediaConf;

        public Result()
        {
        }
        public Integer getAgentId() {
            return agentId;
        }

        public void setAgentId(Integer agentId) {
            this.agentId = agentId;
        }

        public Integer getWorkGroupId() {
            return workGroupId;
        }

        public void setWorkGroupId(Integer workGroupId) {
            this.workGroupId = workGroupId;
        }

        public Integer getRecord() {
            return record;
        }

        public void setRecord(Integer record) {
            this.record = record;
        }

        public Integer getSupers() {
            return supers;
        }

        public void setSupers(Integer supers) {
            this.supers = supers;
        }

        public Integer getMonitor() {
            return monitor;
        }

        public void setMonitor(Integer monitor) {
            this.monitor = monitor;
        }

        public Integer getStatus() {
            return status;
        }

        public void setStatus(Integer status) {
            this.status = status;
        }

        public Integer getAgentStatusTime() {
            return agentStatusTime;
        }

        public void setAgentStatusTime(Integer agentStatusTime) {
            this.agentStatusTime = agentStatusTime;
        }

        public Integer getCurrentState() {
            return currentState;
        }

        public void setCurrentState(Integer currentState) {
            this.currentState = currentState;
        }

        public Integer getCurrentStateTime() {
            return currentStateTime;
        }

        public void setCurrentStateTime(Integer currentStateTime) {
            this.currentStateTime = currentStateTime;
        }

        public Integer getLocationId() {
            return locationId;
        }

        public void setLocationId(Integer locationId) {
            this.locationId = locationId;
        }

        public Integer getCurrentPrivateCallNum() {
            return currentPrivateCallNum;
        }

        public void setCurrentPrivateCallNum(Integer currentPrivateCallNum) {
            this.currentPrivateCallNum = currentPrivateCallNum;
        }

        public Integer getInMultimediaConf() {
            return inMultimediaConf;
        }

        public void setInMultimediaConf(Integer inMultimediaConf) {
            this.inMultimediaConf = inMultimediaConf;
        }

    }
}
