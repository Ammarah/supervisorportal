/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.List;

/**
 *
 * @author Iceman
 */
public class AgentBreaks {

    private String message;
    private String retcode;
    private List<Result> result = null;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getRetcode() {
        return retcode;
    }

    public void setRetcode(String retcode) {
        this.retcode = retcode;
    }

    public List<Result> getResult() {
        return result;
    }

    public void setResult(List<Result> result) {
        this.result = result;
    }

    public static class Result {

        private Integer restReasonId;
        private String restReason;

        public Integer getRestReasonId() {
            return restReasonId;
        }

        public void setRestReasonId(Integer restReasonId) {
            this.restReasonId = restReasonId;
        }

        public String getRestReason() {
            return restReason;
        }

        public void setRestReason(String restReason) {
            this.restReason = restReason;
        }

    }

}
