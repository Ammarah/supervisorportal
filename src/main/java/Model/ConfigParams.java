package Model;
import org.springframework.beans.factory.annotation.Value;

public class ConfigParams {

    @Value("${conf.AgentGatewayURL}")
    public String AgentGatewayURL;
    @Value("${conf.CMSGatewayURL}")
    public String CMSGatewayURL;
    @Value("${conf.VDNID}")
    public String VDNID;
    @Value("${conf.CCID}")
    public String CCID;
    @Value("${conf.DefaultSip}")
    public String DefaultSip;
    @Value("${conf.DefaultBreakTime}")
    public String DefaultBreakTime;

    public ConfigParams() {
    }

    public ConfigParams(String AgentGatewayURL, String CMSGatewayURL, String VDNID, String CCID, String DefaultSip, String DefaultBreakTime) {
        this.AgentGatewayURL = AgentGatewayURL;
        this.CMSGatewayURL = CMSGatewayURL;
        this.VDNID = VDNID;
        this.CCID = CCID;
        this.DefaultSip = DefaultSip;
        this.DefaultBreakTime = DefaultBreakTime;
    }

    public String getAgentGatewayURL() {
        return AgentGatewayURL;
    }

    public void setAgentGatewayURL(String AgentGatewayURL) {
        this.AgentGatewayURL = AgentGatewayURL;
    }

    public String getCMSGatewayURL() {
        return CMSGatewayURL;
    }

    public void setCMSGatewayURL(String CMSGatewayURL) {
        this.CMSGatewayURL = CMSGatewayURL;
    }

    public String getVDNID() {
        return VDNID;
    }

    public void setVDNID(String VDNID) {
        this.VDNID = VDNID;
    }

    public String getCCID() {
        return CCID;
    }

    public void setCCID(String CCID) {
        this.CCID = CCID;
    }

    public String getDefaultSip() {
        return DefaultSip;
    }

    public void setDefaultSip(String DefaultSip) {
        this.DefaultSip = DefaultSip;
    }

    public String getDefaultBreakTime() {
        return DefaultBreakTime;
    }

    public void setDefaultBreakTime(String DefaultBreakTime) {
        this.DefaultBreakTime = DefaultBreakTime;
    }

}
