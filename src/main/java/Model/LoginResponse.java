/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author AbdulRehman
 */
public class LoginResponse {

    private String message;
    private String retcode;
    private Result result;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getRetcode() {
        return retcode;
    }

    public void setRetcode(String retcode) {
        this.retcode = retcode;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public class Result {

        private Integer vdnid;
        private String workno;
        private String mediatype;
        private String loginTime;
        private String isForceChange;

        public Integer getVdnid() {
            return vdnid;
        }

        public void setVdnid(Integer vdnid) {
            this.vdnid = vdnid;
        }

        public String getWorkno() {
            return workno;
        }

        public void setWorkno(String workno) {
            this.workno = workno;
        }

        public String getMediatype() {
            return mediatype;
        }

        public void setMediatype(String mediatype) {
            this.mediatype = mediatype;
        }

        public String getLoginTime() {
            return loginTime;
        }

        public void setLoginTime(String loginTime) {
            this.loginTime = loginTime;
        }

        public String getIsForceChange() {
            return isForceChange;
        }

        public void setIsForceChange(String isForceChange) {
            this.isForceChange = isForceChange;
        }

    }
}
