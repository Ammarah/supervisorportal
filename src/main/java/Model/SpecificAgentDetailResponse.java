/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.List;

/**
 *
 * @author Abdul Rehman
 */
public class SpecificAgentDetailResponse {

    private String message;
    private String retcode;
    private AgentDetail result;

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * @return the retcode
     */
    public String getRetcode() {
        return retcode;
    }

    /**
     * @param retcode the retcode to set
     */
    public void setRetcode(String retcode) {
        this.retcode = retcode;
    }

    /**
     * @return the result
     */
    public AgentDetail getResult() {
        return result;
    }

    /**
     * @param result the result to set
     */
    public void setResult(AgentDetail result) {
        this.result = result;
    }

    public static class Skilllist {

        private String name;
        private int id;
        private int mediatype;

        /**
         * @return the name
         */
        public String getName() {
            return name;
        }

        /**
         * @param name the name to set
         */
        public void setName(String name) {
            this.name = name;
        }

        /**
         * @return the id
         */
        public int getId() {
            return id;
        }

        /**
         * @param id the id to set
         */
        public void setId(int id) {
            this.id = id;
        }

        /**
         * @return the mediatype
         */
        public int getMediatype() {
            return mediatype;
        }

        /**
         * @param mediatype the mediatype to set
         */
        public void setMediatype(int mediatype) {
            this.mediatype = mediatype;
        }
    }

    public static class AgentDetail {

        private String workno;
        private String name;
        private String status;
        private String ctiStatus;
        private int groupid;
        private String groupname;
        private List<Skilllist> skilllist;
        private String mediatype;
        private int vdnid;
        private String phonenumber;
        private int currentstatetime;
        private long logindate;
        private List<Object> callids;
        private int inMultimediaConf;
        private long currentStateReason;

        /**
         * @return the workno
         */
        public String getWorkno() {
            return workno;
        }

        /**
         * @param workno the workno to set
         */
        public void setWorkno(String workno) {
            this.workno = workno;
        }

        /**
         * @return the name
         */
        public String getName() {
            return name;
        }

        /**
         * @param name the name to set
         */
        public void setName(String name) {
            this.name = name;
        }

        /**
         * @return the status
         */
        public String getStatus() {
            return status;
        }

        /**
         * @param status the status to set
         */
        public void setStatus(String status) {
            this.status = status;
        }

        public String getCtiStatus() {
            return ctiStatus;
        }

        public void setCtiStatus(String ctiStatus) {
            this.ctiStatus = ctiStatus;
        }

        /**
         * @return the groupid
         */
        public int getGroupid() {
            return groupid;
        }

        /**
         * @param groupid the groupid to set
         */
        public void setGroupid(int groupid) {
            this.groupid = groupid;
        }

        /**
         * @return the groupname
         */
        public String getGroupname() {
            return groupname;
        }

        /**
         * @param groupname the groupname to set
         */
        public void setGroupname(String groupname) {
            this.groupname = groupname;
        }

        /**
         * @return the skilllist
         */
        public List<Skilllist> getSkilllist() {
            return skilllist;
        }

        /**
         * @param skilllist the skilllist to set
         */
        public void setSkilllist(List<Skilllist> skilllist) {
            this.skilllist = skilllist;
        }

        /**
         * @return the mediatype
         */
        public String getMediatype() {
            return mediatype;
        }

        /**
         * @param mediatype the mediatype to set
         */
        public void setMediatype(String mediatype) {
            this.mediatype = mediatype;
        }

        /**
         * @return the vdnid
         */
        public int getVdnid() {
            return vdnid;
        }

        /**
         * @param vdnid the vdnid to set
         */
        public void setVdnid(int vdnid) {
            this.vdnid = vdnid;
        }

        /**
         * @return the phonenumber
         */
        public String getPhonenumber() {
            return phonenumber;
        }

        /**
         * @param phonenumber the phonenumber to set
         */
        public void setPhonenumber(String phonenumber) {
            this.phonenumber = phonenumber;
        }

        /**
         * @return the currentstatetime
         */
        public int getCurrentstatetime() {
            return currentstatetime;
        }

        /**
         * @param currentstatetime the currentstatetime to set
         */
        public void setCurrentstatetime(int currentstatetime) {
            this.currentstatetime = currentstatetime;
        }

        /**
         * @return the logindate
         */
        public long getLogindate() {
            return logindate;
        }

        /**
         * @param logindate the logindate to set
         */
        public void setLogindate(long logindate) {
            this.logindate = logindate;
        }

        public List<Object> getCallids() {
            return callids;
        }

        public void setCallids(List<Object> callids) {
            this.callids = callids;
        }

        /**
         * @return the inMultimediaConf
         */
        public int getInMultimediaConf() {
            return inMultimediaConf;
        }

        /**
         * @param inMultimediaConf the inMultimediaConf to set
         */
        public void setInMultimediaConf(int inMultimediaConf) {
            this.inMultimediaConf = inMultimediaConf;
        }

        public long getCurrentStateReason() {
            return currentStateReason;
        }

        public void setCurrentStateReason(long currentStateReason) {
            this.currentStateReason = currentStateReason;
        }
    }
}
