/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.io.Serializable;

/**
 *
 * @author Iceman
 */
public class AgentSkillStatisticsDto implements Serializable {

    private VDNSkillsResponse res;
    private AgentSkillsByWorkno resUniskill;

    public VDNSkillsResponse getRes() {
        return res;
    }

    public void setRes(VDNSkillsResponse res) {
        this.res = res;
    }

    public AgentSkillsByWorkno getResUniskill() {
        return resUniskill;
    }

    public void setResUniskill(AgentSkillsByWorkno resUniskill) {
        this.resUniskill = resUniskill;
    }

    public AgentSkillStatisticsDto(VDNSkillsResponse res, AgentSkillsByWorkno resUniskill) {
        this.res = res;
        this.resUniskill = resUniskill;
    }

}
