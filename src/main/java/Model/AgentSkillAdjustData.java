/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author Abdul Rehman
 */
public class AgentSkillAdjustData {
        private String workNo ;
        private String[] skills ;

    /**
     * @return the workNo
     */
    public String getWorkNo() {
        return workNo;
    }

    /**
     * @param workNo the workNo to set
     */
    public void setWorkNo(String workNo) {
        this.workNo = workNo;
    }

    /**
     * @return the skills
     */
    public String[] getSkills() {
        return skills;
    }

    /**
     * @param skills the skills to set
     */
    public void setSkills(String[] skills) {
        this.skills = skills;
    }
        
}
