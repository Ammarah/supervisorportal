/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.List;

/**
 *
 * @author AbdulRehman
 */
public class AgentInfo {

    private String workID;
    private String guid;
    private int vdnID;
    private int ccID;
    private String phoneNum;
    private List<SkillSet> skills;
    private static AgentInfo instance;

    private AgentInfo() {

    }

    public static AgentInfo getInstance() {
        if (instance == null) {
            instance = new AgentInfo();
        }
        return instance;
    }

    /**
     * @return the workID
     */
    public String getWorkID() {
        return workID;
    }

    /**
     * @param workID the workID to set
     */
    public void setWorkID(String workID) {
        this.workID = workID;
    }

    /**
     * @return the guid
     */
    public String getGuid() {
        return guid;
    }

    /**
     * @param guid the guid to set
     */
    public void setGuid(String guid) {
        this.guid = guid;
    }

    /**
     * @return the vdnID
     */
    public int getVdnID() {
        return vdnID;
    }

    /**
     * @param vdnID the vdnID to set
     */
    public void setVdnID(int vdnID) {
        this.vdnID = vdnID;
    }

    /**
     * @return the ccID
     */
    public int getCcID() {
        return ccID;
    }

    /**
     * @param ccID the ccID to set
     */
    public void setCcID(int ccID) {
        this.ccID = ccID;
    }

    /**
     * @return the phoneNum
     */
    public String getPhoneNum() {
        return phoneNum;
    }

    /**
     * @param phoneNum the phoneNum to set
     */
    public void setPhoneNum(String phoneNum) {
        this.phoneNum = phoneNum;
    }

    /**
     * @return the skills
     */
    public List<SkillSet> getSkills() {
        return skills;
    }

    /**
     * @param skills the skills to set
     */
    public void setSkills(List<SkillSet> skills) {
        this.skills = skills;
    }

    public static class SkillSet {

        private String name;
        private Integer id;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }
    }
}
