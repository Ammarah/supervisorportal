/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Apollo
 */
public class AgentInfoOfSpecifiedVDN {
    
    private String message;
    private String retcode;
    private List<Result> result = null;

    public AgentInfoOfSpecifiedVDN()
    {
        
    }
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getRetcode() {
        return retcode;
    }

    public void setRetcode(String retcode) {
        this.retcode = retcode;
    }

    public List<Result> getResult() {
        return result;
    }

    public void setResult(List<Result> result) {
        this.result = result;
    }

    public static class Result {

        public Integer getCcId() {
            return ccId;
        }

        public void setCcId(Integer ccId) {
            this.ccId = ccId;
        }

        public Integer getVdnId() {
            return vdnId;
        }

        public void setVdnId(Integer vdnId) {
            this.vdnId = vdnId;
        }

        public String getAgentNo() {
            return agentNo;
        }

        public void setAgentNo(String agentNo) {
            this.agentNo = agentNo;
        }

        public String getAgentName() {
            return agentName;
        }

        public void setAgentName(String agentName) {
            this.agentName = agentName;
        }

        public String getSkillGroupId() {
            return skillGroupId;
        }

        public void setSkillGroupId(String skillGroupId) {
            this.skillGroupId = skillGroupId;
        }

        public String getSkillId() {
            return skillId;
        }

        public void setSkillId(String skillId) {
            this.skillId = skillId;
        }

        public String getGroupId() {
            return groupId;
        }

        public void setGroupId(String groupId) {
            this.groupId = groupId;
        }

        public String getRoleId() {
            return roleId;
        }

        public void setRoleId(String roleId) {
            this.roleId = roleId;
        }

        private Integer ccId;
        private Integer vdnId;
        private String agentNo;
        private String agentName;
        private String skillGroupId;
        private String skillId;
        private String groupId;
        private String roleId;
       

        public Result()
        {
        }
       
    }
}
