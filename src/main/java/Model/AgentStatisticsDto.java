/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.io.Serializable;

/**
 *
 * @author Iceman
 */
public class AgentStatisticsDto implements Serializable {

    private CMSAgentHistoryResponse StatList;
    private SpecificAgentDetailResponse AgentDetailData;
    private AgentDurationResponse Agentdurationresp;

    public CMSAgentHistoryResponse getRes() {
        return StatList;
    }

    public void setRes(CMSAgentHistoryResponse StatList) {
        this.StatList = StatList;
    }

    public SpecificAgentDetailResponse getAgentbyworkno() {
        return AgentDetailData;
    }

    public void setAgentbyworkno(SpecificAgentDetailResponse AgentDetailData) {
        this.AgentDetailData = AgentDetailData;
    }

    public AgentDurationResponse getAgentdurationresp() {
        return Agentdurationresp;
    }

    public void setAgentdurationresp(AgentDurationResponse Agentdurationresp) {
        this.Agentdurationresp = Agentdurationresp;
    }

    public AgentStatisticsDto(CMSAgentHistoryResponse StatList, SpecificAgentDetailResponse AgentDetailData, AgentDurationResponse Agentdurationresp) {
        this.StatList = StatList;
        this.AgentDetailData = AgentDetailData;
        this.Agentdurationresp = Agentdurationresp;
    }

}
