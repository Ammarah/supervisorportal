/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author Abdul Rehman
 */
public class VDNParams {

    private String ccId;
    private String vdnId;
    private String wasIp;
    private String wasPort;
    private Boolean isSSL;
    private String wasPath;
    private String workNo;
    private String password;

    public String getCcId() {
        return ccId;
    }

    public void setCcId(String ccId) {
        this.ccId = ccId;
    }

    public String getVdnId() {
        return vdnId;
    }

    public void setVdnId(String vdnId) {
        this.vdnId = vdnId;
    }

    public String getWasIp() {
        return wasIp;
    }

    public void setWasIp(String wasIp) {
        this.wasIp = wasIp;
    }

    public String getWasPort() {
        return wasPort;
    }

    public void setWasPort(String wasPort) {
        this.wasPort = wasPort;
    }

    public Boolean getIsSSL() {
        return isSSL;
    }

    public void setIsSSL(Boolean isSSL) {
        this.isSSL = isSSL;
    }

    public String getWasPath() {
        return wasPath;
    }

    public void setWasPath(String wasPath) {
        this.wasPath = wasPath;
    }

    public String getWorkNo() {
        return workNo;
    }

    public void setWorkNo(String workNo) {
        this.workNo = workNo;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
