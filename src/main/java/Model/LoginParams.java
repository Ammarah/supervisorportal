/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author Abdul Rehman
 */
public class LoginParams {

    private String password;
    private String phonenum;
    private Integer status;
    private Integer agenttype;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhonenum() {
        return phonenum;
    }

    public void setPhonenum(String phonenum) {
        this.phonenum = phonenum;
    }

    /**
     * @return the status
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * @return the agenttype
     */
    public Integer getAgenttype() {
        return agenttype;
    }

    /**
     * @param agenttype the agenttype to set
     */
    public void setAgenttype(Integer agenttype) {
        this.agenttype = agenttype;
    }

   

}
