/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import com.apollo.AgentGateway.CMSMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/**
 *
 * @author Abdul Rehman
 */
public class CMSAgentHistoryResponse {

    private String message;
    private String retcode;
    private List<Result> result = null;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getRetcode() {
        return retcode;
    }

    public void setRetcode(String retcode) {
        this.retcode = retcode;
    }

    public List<Result> getResult() {
        return result;
    }

    public void setResult(List<Result> result) {
        this.result = result;
    }

    public static class Idx {

        private String id;
        private Object val;
        Map<String, String> agentMap = CMSMap.agentMap;
        Map<String, String> skillMap = CMSMap.skillMap;
        Map<String, String> vdnMap = CMSMap.vdnMap;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            String Key = "";
            if (id.startsWith("IDX_03_")) {
                for (Entry<String, String> entry : skillMap.entrySet()) {
                    if (entry.getValue().equals(id)) {
                        Key = entry.getKey();
                        break;
                    }
                }
            } else if (id.startsWith("IDX_01_")) {
                for (Entry<String, String> entry : vdnMap.entrySet()) {
                    if (entry.getValue().equals(id)) {
                        Key = entry.getKey();
                        break;
                    }
                }
            } else {
                for (Entry<String, String> entry : agentMap.entrySet()) {
                    if (entry.getValue().equals(id)) {
                        Key = entry.getKey();
                        break;
                    }
                }
            }
            this.id = Key;
        }

        public Object getVal() {
            return val;
        }

        public void setVal(Object val) {
            if (val != null) {
                this.val = val;
            } else {
                this.val = 0;
            }
        }
    }

    public static class Result {

        private String id;
        private List<Idx> idxs = null;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public List<Idx> getIdxs() {
            return idxs;
        }

        public void setIdxs(List<Idx> idxs) {
            this.idxs = idxs;
        }

    }

}
