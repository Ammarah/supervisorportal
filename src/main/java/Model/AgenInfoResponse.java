/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author Abdul Rehman
 */

import java.util.List;

public class AgenInfoResponse {

    private String message;
    private String retcode;
    private List<Result> result = null;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getRetcode() {
        return retcode;
    }

    public void setRetcode(String retcode) {
        this.retcode = retcode;
    }

    public List<Result> getResult() {
        return result;
    }

    public void setResult(List<Result> result) {
        this.result = result;
    }

    public static class Result {

        private String ccId;
        private String vdnId;
        private String agentNo;
        private String agentName;
        private Object skillGroupId;
        private Object skillId;
        private Object groupId;
        private String roleId;
        public Result()
        {
            
        }
        public String getCcId() {
            return ccId;
        }

        public void setCcId(String ccId) {
            this.ccId = ccId;
        }

        public String getVdnId() {
            return vdnId;
        }

        public void setVdnId(String vdnId) {
            this.vdnId = vdnId;
        }

        public String getAgentNo() {
            return agentNo;
        }

        public void setAgentNo(String agentNo) {
            this.agentNo = agentNo;
        }

        public String getAgentName() {
            return agentName;
        }

        public void setAgentName(String agentName) {
            this.agentName = agentName;
        }

        public Object getSkillGroupId() {
            return skillGroupId;
        }

        public void setSkillGroupId(Object skillGroupId) {
            this.skillGroupId = skillGroupId;
        }

        public Object getSkillId() {
            return skillId;
        }

        public void setSkillId(Object skillId) {
            this.skillId = skillId;
        }

        public Object getGroupId() {
            return groupId;
        }

        public void setGroupId(Object groupId) {
            this.groupId = groupId;
        }

        public String getRoleId() {
            return roleId;
        }

        public void setRoleId(String roleId) {
            this.roleId = roleId;
        }

    }
}
