/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author Abdul Rehman
 */
public class DefaultResponse {

private String message;
private String retcode;

public String getMessage() {
return message;
}

public void setMessage(String message) {
this.message = message;
}

public String getRetcode() {
return retcode;
}

public void setRetcode(String retcode) {
this.retcode = retcode;
}

}
