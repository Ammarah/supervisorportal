/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apollo.service;

import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author Ammarah
 */
public interface WallboardNotificationService {
        public String getNotifications(HttpServletRequest request);
      //  public void saveNotification(String FromDate, String ToDate, HttpServletRequest request);
        public void updateNotification(Integer id,HttpServletRequest request);
        public void deleteNotification(Integer id,HttpServletRequest request);

        

    
}
