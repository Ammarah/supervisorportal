/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apollo.service;

import Model.CMSAgentHistoryResponse;
import Model.VDNSkillsResponse;

/**
 *
 * @author Iceman
 */
public interface SkillAgentService {

    public CMSAgentHistoryResponse skillOfagentinfo(String SkillId);

    public CMSAgentHistoryResponse AgentSkillinformation(String SkillId);

    public VDNSkillsResponse skillagentinfo();

    public CMSAgentHistoryResponse skillOfcallstat(String skillID);

    public CMSAgentHistoryResponse skillOfcallavg(String skillID);

    public CMSAgentHistoryResponse skillOfcallingrate(String skillID);

}
