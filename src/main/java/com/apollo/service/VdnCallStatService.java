/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apollo.service;

import Model.CMSAgentHistoryResponse;

/**
 *
 * @author Iceman
 */
public interface VdnCallStatService {

    public CMSAgentHistoryResponse HistoricalVDNDropRate();

    public CMSAgentHistoryResponse HistoricalVDNIncomingCallStats();

    public CMSAgentHistoryResponse HistoricalVDNOutGoingCallStats();

    public CMSAgentHistoryResponse RealTimeVdnStats();

}
