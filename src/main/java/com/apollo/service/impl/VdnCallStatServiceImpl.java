/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apollo.service.impl;

import Model.CMSAgentHistoryResponse;
import com.apollo.dao.VdnCallStatDao;
import com.apollo.service.VdnCallStatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Iceman
 */
@Service
public class VdnCallStatServiceImpl implements VdnCallStatService {

    @Autowired
    private VdnCallStatDao vdncallstatdao;

    @Override
    public CMSAgentHistoryResponse HistoricalVDNDropRate() {
        return vdncallstatdao.HistoricalVDNDropRate();
    }

    @Override
    public CMSAgentHistoryResponse HistoricalVDNIncomingCallStats() {
        return vdncallstatdao.HistoricalVDNIncomingCallStats();
    }

    @Override
    public CMSAgentHistoryResponse HistoricalVDNOutGoingCallStats() {
        return vdncallstatdao.HistoricalVDNOutGoingCallStats();
    }

    @Override
    public CMSAgentHistoryResponse RealTimeVdnStats() {
        return vdncallstatdao.RealTimeVdnStats();
    }

}
