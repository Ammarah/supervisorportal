/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apollo.service.impl;

import Model.CallRecordsResponse;
import com.apollo.dao.CallRecordsDao;
import com.apollo.service.CallRecordsService;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Iceman
 */
@Service
public class CallRecordsServiceImpl implements CallRecordsService {

    @Autowired
    private CallRecordsDao callrecordsDao;

    @Override
    public String getCallData(String FromDate, String ToDate, HttpServletRequest request) {
        return callrecordsDao.getCallData(FromDate, ToDate, request);
    }

}
