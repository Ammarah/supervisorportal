/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apollo.service.impl;

import Model.CMSAgentHistoryResponse;
import Model.VDNSkillsResponse;
import com.apollo.dao.SkillAgentDao;
import com.apollo.service.SkillAgentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Iceman
 */
@Service
public class SkillAgentServiceImpl implements SkillAgentService {

    @Autowired
    private SkillAgentDao skillagentdao;

    @Override
    public CMSAgentHistoryResponse skillOfagentinfo(String SkillId) {
        return skillagentdao.skillOfagentinfo(SkillId);
    }

    @Override
    public CMSAgentHistoryResponse AgentSkillinformation(String SkillId) {
        return skillagentdao.AgentSkillinformation(SkillId);
    }

    @Override
    public VDNSkillsResponse skillagentinfo() {
        return skillagentdao.skillagentinfo();
    }

    @Override
    public CMSAgentHistoryResponse skillOfcallstat(String skillID) {
        return skillagentdao.skillOfcallstat(skillID);
    }

    @Override
    public CMSAgentHistoryResponse skillOfcallavg(String skillID) {
        return skillagentdao.skillOfcallavg(skillID);
    }

    @Override
    public CMSAgentHistoryResponse skillOfcallingrate(String skillID) {
        return skillagentdao.skillOfcallingrate(skillID);
    }
}
