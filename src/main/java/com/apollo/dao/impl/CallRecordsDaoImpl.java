/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apollo.dao.impl;

import Model.CallRecordsResponse;
import com.apollo.dao.CallRecordsDao;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.sql.DataSource;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Iceman
 */
@Repository
public class CallRecordsDaoImpl implements CallRecordsDao {

    private SimpleJdbcCall simpleJdbcCall;
    private DataSource dataSource;
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
        namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    @Override
    public String getCallData(String FromDate, String ToDate, HttpServletRequest request) {
        JSONArray jAry = new JSONArray();
        JSONObject obj = new JSONObject();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date FromDateDate = null;
        Date ToDateDate = null;
        try {
            FromDateDate = formatter.parse(FromDate); //2018-01-17
            ToDateDate = formatter.parse(ToDate);
        } catch (ParseException ex) {
            Logger.getLogger(CallRecordsDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        SimpleDateFormat formatter1 = new SimpleDateFormat("dd-MMM-yy");
        String FromDatejavastr = formatter1.format(FromDateDate); // 09-May-18
        String ToDatejavastr = formatter1.format(ToDateDate);
        System.out.println("From Date: \t" + FromDatejavastr + "\t End Date: \t" + ToDatejavastr);
//        List<CallRecordsResponse> customers = null;
        String JsonData = "[{}]";
        try {
            String SQL = "Select CALLID,AGENTID,CALLEENO,CALLERNO,CURRENTSKILLID,FILENAME,BEGINTIME,ENDTIME from (SELECT CALLID,AGENTID,CALLEENO,CALLERNO,CURRENTSKILLID,FILENAME,BEGINTIME,ENDTIME FROM TRECORDINFO1 UNION SELECT CALLID,AGENTID,CALLEENO,CALLERNO,CURRENTSKILLID,FILENAME,BEGINTIME,ENDTIME FROM TRECORDINFO2 UNION SELECT CALLID,AGENTID,CALLEENO,CALLERNO,CURRENTSKILLID,FILENAME,BEGINTIME,ENDTIME FROM TRECORDINFO3 UNION SELECT CALLID,AGENTID,CALLEENO,CALLERNO,CURRENTSKILLID,FILENAME,BEGINTIME,ENDTIME FROM TRECORDINFO4 UNION SELECT CALLID,AGENTID,CALLEENO,CALLERNO,CURRENTSKILLID,FILENAME,BEGINTIME,ENDTIME FROM TRECORDINFO5 UNION SELECT CALLID,AGENTID,CALLEENO,CALLERNO,CURRENTSKILLID,FILENAME,BEGINTIME,ENDTIME FROM TRECORDINFO6 UNION SELECT CALLID,AGENTID,CALLEENO,CALLERNO,CURRENTSKILLID,FILENAME,BEGINTIME,ENDTIME FROM TRECORDINFO7 UNION SELECT CALLID,AGENTID,CALLEENO,CALLERNO,CURRENTSKILLID,FILENAME,BEGINTIME,ENDTIME FROM TRECORDINFO8 UNION SELECT CALLID,AGENTID,CALLEENO,CALLERNO,CURRENTSKILLID,FILENAME,BEGINTIME,ENDTIME FROM TRECORDINFO9 UNION SELECT CALLID,AGENTID,CALLEENO,CALLERNO,CURRENTSKILLID,FILENAME,BEGINTIME,ENDTIME FROM TRECORDINFO10  UNION SELECT CALLID,AGENTID,CALLEENO,CALLERNO,CURRENTSKILLID,FILENAME,BEGINTIME,ENDTIME FROM TRECORDINFO11 UNION SELECT CALLID,AGENTID,CALLEENO,CALLERNO,CURRENTSKILLID,FILENAME,BEGINTIME,ENDTIME FROM TRECORDINFO12) WHERE to_timestamp( TO_CHAR(begintime , 'DD/MM/YYYY HH24:MI:SS'),'DD/MM/YYYY HH24:MI:SS') between '" + FromDatejavastr + " 12.00.00.000000000 AM' and '" + ToDatejavastr + " 11.59.59.000000000 PM'";
            SqlParameterSource namedParameters = new MapSqlParameterSource();
//            customers = namedParameterJdbcTemplate.query(SQL,
//                    new BeanPropertyRowMapper(CallRecordsResponse.class));
            List<Map<String, Object>> outputMap = namedParameterJdbcTemplate.queryForList(SQL, namedParameters);
//            System.out.println("OffLine Message Message Get:" + outputMap);
            if (!outputMap.isEmpty()) {
                try {
                    JSONParser parser = new JSONParser();
                    JsonData = new ObjectMapper().writeValueAsString(outputMap);
                } catch (IOException ex) {
                    obj.put("error", true);
                    return "[{}]";
                }
            }
        } catch (DataAccessException e) {
            obj.put("error", true);
            return "[{}]";
        }
        return JsonData;
    }

}
