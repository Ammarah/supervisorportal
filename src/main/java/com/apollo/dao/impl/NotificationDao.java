package com.apollo.dao.impl;

import Model.Notification;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author Ammarah
 */
public class NotificationDao {

    JdbcTemplate template;

    public void setTemplate(JdbcTemplate template) {
        this.template = template;
    }

    public int save(Notification n) {
        String sql = "insert into notifications(notificationText,startDateTime,endDateTime,priority,type,status,location,skill_id) values('" + n.getNotificationText() + "','" + n.getStartDateTime() + "','" + n.getEndDateTime() + "','" + n.getPriority() + "','" + n.getType() + "','Active'," + n.getLocation() + ",'" + n.getSkillId() + "')";
        System.out.println("query insert------------------------insert into notifications(notificationText,startDateTime,endDateTime,priority,type,status,location,skill_id) values('" + n.getNotificationText() + "'," + n.getStartDateTime() + "," + n.getEndDateTime() + ",'" + n.getPriority() + "','" + n.getType() + "','Active'," + n.getLocation() + ",'" + n.getSkillId() + "')");
        return template.update(sql);
    }

    public int update(Notification n) {
        String sql = "update notifications set notificationText='" + n.getNotificationText() + "', startDateTime='" + n.getStartDateTime() + "', endDateTime='" + n.getEndDateTime() + "', priority='" + n.getPriority() + "',type='" + n.getType() + "',status='" + n.getStatus() + "',location=" + n.getLocation() + ",skill_id='" + n.getSkillId() + "' where notificationID=" + n.getNotificationID() + "";
        return template.update(sql);
    }

    public int delete(int id) {
        String sql = "delete from notifications where notificationID=" + id + "";
        return template.update(sql);
    }

    public Notification getNotificationById(int notificationID) {
        String sql = "select * from notifications where notificationID=?";
        return template.queryForObject(sql, new Object[]{notificationID}, new BeanPropertyRowMapper<Notification>(Notification.class));
    }

    public List<Notification> getNotifications() {
        return template.query("select * from notifications", new RowMapper<Notification>() {
            public Notification mapRow(ResultSet rs, int row) throws SQLException {
                Notification n = new Notification();
                n.setNotificationID(rs.getInt(1));
                n.setNotificationText(rs.getString(2));
                n.setStartDateTime(rs.getTimestamp(3));
                n.setEndDateTime(rs.getTimestamp(4));
                n.setPriority(rs.getString(5));
                n.setType(rs.getString(6));
                n.setStatus(rs.getString(7));
                n.setLocation(rs.getInt(8));
                n.setSkillId(rs.getString(9));
                return n;
            }
        });
    }

    public List<Notification> getActiveNotificationsSkillBased(String skillId) {
        return template.query("SELECT * FROM notifications WHERE status=\"Active\" And (NOW() between startDateTime AND endDateTime) And skill_id=\"" + skillId + "\" limit 3", new RowMapper<Notification>() {
            public Notification mapRow(ResultSet rs, int row) throws SQLException {
                Notification n = new Notification();
                n.setNotificationID(rs.getInt(1));
                n.setNotificationText(rs.getString(2));
                n.setStartDateTime(rs.getTimestamp(3));
                n.setEndDateTime(rs.getTimestamp(4));
                n.setPriority(rs.getString(5));
                n.setType(rs.getString(6));
                n.setStatus(rs.getString(7));
                n.setLocation(rs.getInt(8));
                n.setSkillId(rs.getString(9));
                return n;
            }
        });
    }

    public List<Notification> getActiveNotifications(int location) {
        return template.query("SELECT * FROM notifications WHERE status=\"Active\" And (NOW() between startDateTime AND endDateTime) And location=" + location + " limit 3", new RowMapper<Notification>() {
            public Notification mapRow(ResultSet rs, int row) throws SQLException {
                Notification n = new Notification();
                n.setNotificationID(rs.getInt(1));
                n.setNotificationText(rs.getString(2));
                n.setStartDateTime(rs.getTimestamp(3));
                n.setEndDateTime(rs.getTimestamp(4));
                n.setPriority(rs.getString(5));
                n.setType(rs.getString(6));
                n.setStatus(rs.getString(7));
                n.setLocation(rs.getInt(8));
                n.setSkillId(rs.getString(9));

                return n;
            }
        });
    }

    public List<Notification> getActiveNotifications() {
        return template.query("SELECT * FROM notifications WHERE status=\"Active\" And (NOW() between startDateTime AND endDateTime) limit 3", new RowMapper<Notification>() {
            public Notification mapRow(ResultSet rs, int row) throws SQLException {
                Notification n = new Notification();
                n.setNotificationID(rs.getInt(1));
                n.setNotificationText(rs.getString(2));
                n.setStartDateTime(rs.getTimestamp(3));
                n.setEndDateTime(rs.getTimestamp(4));
                n.setPriority(rs.getString(5));
                n.setType(rs.getString(6));
                n.setStatus(rs.getString(7));
                n.setLocation(rs.getInt(8));
                n.setSkillId(rs.getString(9));

                return n;
            }
        });
    }
}
