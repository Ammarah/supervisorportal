/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apollo.dao.impl;

import com.apollo.AgentGateway.CMSMap;
import Model.AgentInfo;
import Model.CMSAgentHistoryResponse;
import Model.CMSVdnHistoryParams;
import com.apollo.dao.VdnCallStatDao;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.ObjectMapper;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Iceman
 */
@Repository
public class VdnCallStatDaoImpl implements VdnCallStatDao {

    public String AgentGatewayURL = "";
    public String CMSGatewayURL = "";
    public AgentInfo agent;
    public String VDNID;
    public String CCID;
    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(VdnCallStatDaoImpl.class);

    public VdnCallStatDaoImpl() throws URISyntaxException {
        agent = AgentInfo.getInstance();
        String input = getClass().getProtectionDomain().getCodeSource().getLocation().toURI().getPath();
        try {
            InputStream stream = new FileInputStream(input + "props//Config.properties");
            Properties prop = new Properties();
            prop.load(stream);
            AgentGatewayURL = prop.getProperty("AgentGatewayURL");
            CMSGatewayURL = prop.getProperty("CMSGatewayURL");
            VDNID = prop.getProperty("VDNID");
            CCID = prop.getProperty("CCID");
            stream.close();
        } catch (Exception ex) {
            Logger.getLogger(AgentGatewayServiceConsumer.class.getName()).log(Level.SEVERE, null, ex);
        }

        Unirest.setObjectMapper(new ObjectMapper() {
            private com.fasterxml.jackson.databind.ObjectMapper jacksonObjectMapper
                    = new com.fasterxml.jackson.databind.ObjectMapper();

            @Override
            public <T> T readValue(String value, Class<T> valueType) {
                try {
                    return jacksonObjectMapper.readValue(value, valueType);
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }

            public String writeValue(Object value) {
                try {
                    return jacksonObjectMapper.writeValueAsString(value);
                } catch (JsonProcessingException e) {
                    throw new RuntimeException(e);
                }
            }
        });
    }

    @Override
    public CMSAgentHistoryResponse HistoricalVDNDropRate() {
        CMSAgentHistoryResponse result = new CMSAgentHistoryResponse();
        HttpResponse<CMSAgentHistoryResponse> response = null;
        CMSVdnHistoryParams params = new CMSVdnHistoryParams();
        params.setCcId(CCID);
        List<Integer> objList = new ArrayList<Integer>();
        objList.add(Integer.parseInt(VDNID));
        params.setObjectIds(objList);
        List<String> indexIds = new ArrayList<String>();
        Map<String, String> vdnMap = CMSMap.vdnMap;
        if (vdnMap.size() > 0) {
            indexIds.add(vdnMap.get("Drop Rate of Manual Incoming Calls"));
            indexIds.add(vdnMap.get("Drop Rate of Incoming Calls"));
        }
        params.setIndexIds(indexIds);
        try {
            response = Unirest.post(CMSGatewayURL + "hindex/vdn").header("Content-Type", "application/json").body(params).asObject(CMSAgentHistoryResponse.class);
            int status = response.getStatus();
            result = response.getBody();
            if (result.getRetcode().equalsIgnoreCase("0")) {
            } else {
            }
        } catch (UnirestException ex) {
            result.setRetcode("100-001");
            logger.error("HistoricalVDNDropRate Error:  " + ex.getMessage());
        }
        return result;
    }

    @Override
    public CMSAgentHistoryResponse HistoricalVDNIncomingCallStats() {
        CMSAgentHistoryResponse result = new CMSAgentHistoryResponse();
        HttpResponse<CMSAgentHistoryResponse> response = null;
        CMSVdnHistoryParams params = new CMSVdnHistoryParams();
        params.setCcId(CCID);
        List<Integer> objList = new ArrayList<Integer>();
        objList.add(Integer.parseInt(VDNID));
        params.setObjectIds(objList);
        List<String> indexIds = new ArrayList<String>();
        Map<String, String> vdnMap = CMSMap.vdnMap;
        if (vdnMap.size() > 0) {
            indexIds.add(vdnMap.get("Count"));
            indexIds.add(vdnMap.get("Connected<br>Count"));
            indexIds.add(vdnMap.get("Completion<br>Rate<br>of Calls"));
            indexIds.add(vdnMap.get("Incoming<br>IVR Count"));
            indexIds.add(vdnMap.get("Connected IVR<br>Incoming<br>Count"));
            indexIds.add(vdnMap.get("Completion<br>Rate of<br>IVR Calls"));
            indexIds.add(vdnMap.get("Manual<br>Count"));
            indexIds.add(vdnMap.get("Connected<br>Manual Count"));
            indexIds.add(vdnMap.get("Completion<br>Rate<br>of Manual Calls"));
            indexIds.add(vdnMap.get("Average<br>Duration<br>of Occupied"));
            indexIds.add(vdnMap.get("Dropped<br>Count"));
            indexIds.add(vdnMap.get("Average<br>Duration Call"));
            indexIds.add(vdnMap.get("Average<br>Waiting Time"));
        }
        params.setIndexIds(indexIds);
        try {
            response = Unirest.post(CMSGatewayURL + "hindex/vdn").header("Content-Type", "application/json").body(params).asObject(CMSAgentHistoryResponse.class);
            int status = response.getStatus();
            result = response.getBody();
            if (result.getRetcode().equalsIgnoreCase("0")) {
            } else {
            }
        } catch (UnirestException ex) {
            result.setRetcode("100-001");
            logger.error("HistoricalVDNIncomingCallStats Error:  " + ex.getMessage());
        }
        return result;
    }

    @Override
    public CMSAgentHistoryResponse HistoricalVDNOutGoingCallStats() {
        CMSAgentHistoryResponse result = new CMSAgentHistoryResponse();
        HttpResponse<CMSAgentHistoryResponse> response = null;
        CMSVdnHistoryParams params = new CMSVdnHistoryParams();
        params.setCcId(CCID);
        List<Integer> objList = new ArrayList<Integer>();
        objList.add(Integer.parseInt(VDNID));
        params.setObjectIds(objList);
        List<String> indexIds = new ArrayList<String>();
        Map<String, String> vdnMap = CMSMap.vdnMap;
        if (vdnMap.size() > 0) {
            indexIds.add(vdnMap.get("Outgoing<br>Calls"));
            indexIds.add(vdnMap.get("Outgoing<br>Connected Calls"));
            indexIds.add(vdnMap.get("Completion Rate<br>of Outgoing Calls"));
            indexIds.add(vdnMap.get("Outgoing<br>Connected<br>IVR Calls"));
            indexIds.add(vdnMap.get("Outgoing<br>Manual<br>Calls"));
            indexIds.add(vdnMap.get("Average<br>Duration<br>of Occupied<br>Calls"));
        }
        params.setIndexIds(indexIds);
        try {
            response = Unirest.post(CMSGatewayURL + "hindex/vdn").header("Content-Type", "application/json").body(params).asObject(CMSAgentHistoryResponse.class);
            int status = response.getStatus();
            result = response.getBody();
            if (result.getRetcode().equalsIgnoreCase("0")) {
            } else {
            }
        } catch (UnirestException ex) {
            result.setRetcode("100-001");
            logger.error("HistoricalVDNOutGoingCallStats Error:  " + ex.getMessage());
        }
        return result;
    }

    @Override
    public CMSAgentHistoryResponse RealTimeVdnStats() {
        CMSAgentHistoryResponse result = new CMSAgentHistoryResponse();
        HttpResponse<CMSAgentHistoryResponse> response = null;
        CMSVdnHistoryParams params = new CMSVdnHistoryParams();
        params.setCcId(CCID);
        List<Integer> objList = new ArrayList<Integer>();
        objList.add(Integer.parseInt(VDNID));
        params.setObjectIds(objList);
        List<String> indexIds = new ArrayList<String>();
        Map<String, String> vdnMap = CMSMap.vdnMap;
        if (vdnMap.size() > 0) {
            indexIds.add(vdnMap.get("Call Count"));
            indexIds.add(vdnMap.get("Incoming Calls"));
            indexIds.add(vdnMap.get("IVR Count"));
            indexIds.add(vdnMap.get("Queueing Calls"));
            indexIds.add(vdnMap.get("Login Agents"));
            indexIds.add(vdnMap.get("Ready Agents"));
            indexIds.add(vdnMap.get("Not Ready Agents"));
            indexIds.add(vdnMap.get("Talking Agents"));
            indexIds.add(vdnMap.get("Study Agents"));
            indexIds.add(vdnMap.get("Work Agents"));
            indexIds.add(vdnMap.get("Answering Agents"));
            indexIds.add(vdnMap.get("Preoccupied Agents"));
            indexIds.add(vdnMap.get("Occupied Agents"));
            indexIds.add(vdnMap.get("Not Login Agents"));
            indexIds.add(vdnMap.get("Break Agents"));
        }
        params.setIndexIds(indexIds);
        try {
            response = Unirest.post(CMSGatewayURL + "rindex/vdn").header("Content-Type", "application/json").body(params).asObject(CMSAgentHistoryResponse.class);
            int status = response.getStatus();
            result = response.getBody();
            if (result.getRetcode().equalsIgnoreCase("0")) {
            } else {
            }
        } catch (UnirestException ex) {
            result.setRetcode("100-001");
            logger.error("RealTimeVdnStats Error:  " + ex.getMessage());
        }
        return result;
    }

}
