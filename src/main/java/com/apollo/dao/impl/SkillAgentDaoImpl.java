/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apollo.dao.impl;

import com.apollo.AgentGateway.CMSMap;
import Model.AgentInfo;
import Model.CMSAgentHistoryParams;
import Model.CMSAgentHistoryResponse;
import Model.VDNSkillsResponse;
import com.apollo.dao.SkillAgentDao;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.ObjectMapper;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Iceman
 */
@Repository
public class SkillAgentDaoImpl implements SkillAgentDao {

    public String AgentGatewayURL = "";
    public String CMSGatewayURL = "";
    public AgentInfo agent;
    public String VDNID;
    public String CCID;
    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(SkillAgentDaoImpl.class);

    public SkillAgentDaoImpl() throws URISyntaxException {
        agent = AgentInfo.getInstance();
        String input = getClass().getProtectionDomain().getCodeSource().getLocation().toURI().getPath();
        try {
            InputStream stream = new FileInputStream(input + "props//Config.properties");
            Properties prop = new Properties();
            prop.load(stream);
            AgentGatewayURL = prop.getProperty("AgentGatewayURL");
            CMSGatewayURL = prop.getProperty("CMSGatewayURL");
            VDNID = prop.getProperty("VDNID");
            CCID = prop.getProperty("CCID");
            stream.close();
        } catch (Exception ex) {
            Logger.getLogger(AgentGatewayServiceConsumer.class.getName()).log(Level.SEVERE, null, ex);
        }

        Unirest.setObjectMapper(new ObjectMapper() {
            private com.fasterxml.jackson.databind.ObjectMapper jacksonObjectMapper
                    = new com.fasterxml.jackson.databind.ObjectMapper();

            @Override
            public <T> T readValue(String value, Class<T> valueType) {
                try {
                    return jacksonObjectMapper.readValue(value, valueType);
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }

            public String writeValue(Object value) {
                try {
                    return jacksonObjectMapper.writeValueAsString(value);
                } catch (JsonProcessingException e) {
                    throw new RuntimeException(e);
                }
            }
        });
    }

    @Override
    public CMSAgentHistoryResponse skillOfagentinfo(String SkillId) {
        CMSAgentHistoryResponse result = new CMSAgentHistoryResponse();
        HttpResponse<CMSAgentHistoryResponse> response = null;
        CMSAgentHistoryParams params = new CMSAgentHistoryParams();
        params.setCcId(CCID);
        params.setVdnId(VDNID);
        List<Integer> objList = new ArrayList<Integer>();
        objList.add(Integer.parseInt(SkillId));
        params.setObjectIds(objList);
        List<String> indexIds = new ArrayList<String>();
        Map<String, String> skillMap = CMSMap.skillMap;
        if (skillMap.size() > 0) {
            indexIds.add(skillMap.get("No of queueing calls"));
            indexIds.add(skillMap.get("No of ongoing calls"));
            indexIds.add(skillMap.get("Max waiting time for queue"));
            indexIds.add(skillMap.get("No of connected calls in last 5 minutes"));
        }
        params.setIndexIds(indexIds);
        try {
            response = Unirest.post(CMSGatewayURL + "rindex/skill").header("Content-Type", "application/json").body(params).asObject(CMSAgentHistoryResponse.class);
            int status = response.getStatus();
            result = response.getBody();
            if (result.getRetcode().equalsIgnoreCase("0")) {
            } else {

            }

        } catch (Exception ex) {
            result.setRetcode("100-001");
            logger.error("skillOfagentinfo Error: against skillID" + SkillId + "" + ex.getMessage());
        }
        return result;
    }

    @Override
    public CMSAgentHistoryResponse AgentSkillinformation(String SkillId) {
        CMSAgentHistoryResponse result = new CMSAgentHistoryResponse();
        HttpResponse<CMSAgentHistoryResponse> response = null;
        CMSAgentHistoryParams params = new CMSAgentHistoryParams();
        params.setCcId(CCID);
        params.setVdnId(VDNID);
        List<Integer> objList = new ArrayList<Integer>();
        objList.add(Integer.parseInt(SkillId));
        params.setObjectIds(objList);
        List<String> indexIds = new ArrayList<String>();
        Map<String, String> skillMap = CMSMap.skillMap;
        if (skillMap.size() > 0) {
            indexIds.add(skillMap.get("Agents Count"));
            indexIds.add(skillMap.get("Talking Agents"));
            indexIds.add(skillMap.get("Ready Agents"));
            indexIds.add(skillMap.get("Available Agents"));
            indexIds.add(skillMap.get("Not Ready Agents"));
            indexIds.add(skillMap.get("Work Agents"));
            indexIds.add(skillMap.get("Break Agents"));
            indexIds.add(skillMap.get("Study Agents"));
        }
        params.setIndexIds(indexIds);
        try {
            response = Unirest.post(CMSGatewayURL + "rindex/skill").header("Content-Type", "application/json").body(params).asObject(CMSAgentHistoryResponse.class);
            int status = response.getStatus();
            result = response.getBody();
            if (result.getRetcode().equalsIgnoreCase("0")) {
            } else {

            }

        } catch (Exception ex) {
            result.setRetcode("100-001");
            logger.error("skillOfagentinfo Error: against skillID" + SkillId + "" + ex.getMessage());
        }
        return result;
    }

    @Override
    public VDNSkillsResponse skillagentinfo() {
        VDNSkillsResponse result = new VDNSkillsResponse();
        HttpResponse<VDNSkillsResponse> response = null;
        try {
            response = Unirest.get(CMSGatewayURL + "synwas/skills/" + CCID + "/" + VDNID).header("Content-Type", "application/json").asObject(VDNSkillsResponse.class);
            int status = response.getStatus();
            result = response.getBody();
            if (result.getRetcode().equalsIgnoreCase("0")) {
                result.setMessage(agent.getWorkID() + "|" + agent.getPhoneNum());
            } else {
            }
        } catch (Exception ex) {
            result.setRetcode("100-001");
            logger.error("Skill List OF Agent: " + ex.getMessage());
        }
        return result;
    }

    @Override
    public CMSAgentHistoryResponse skillOfcallstat(String skillID) {
        CMSAgentHistoryResponse result = new CMSAgentHistoryResponse();
        HttpResponse<CMSAgentHistoryResponse> response = null;
        CMSAgentHistoryParams params = new CMSAgentHistoryParams();
        params.setCcId(CCID);
        params.setVdnId(VDNID);
        List<Integer> objList = new ArrayList<Integer>();
        objList.add(Integer.parseInt(skillID));
        params.setObjectIds(objList);
        List<String> indexIds = new ArrayList<String>();
        Map<String, String> skillMap = CMSMap.skillMap;
        if (skillMap.size() > 0) {
            indexIds.add(skillMap.get("In Queue"));
            indexIds.add(skillMap.get("Count"));
            indexIds.add(skillMap.get("Lost"));
            indexIds.add(skillMap.get("Disconnected<br>Queues"));
            indexIds.add(skillMap.get("Hung<br>up"));
            indexIds.add(skillMap.get("Dropped"));
            indexIds.add(skillMap.get("Interrupted"));
            indexIds.add(skillMap.get("Not<br>Answered"));
            indexIds.add(skillMap.get("Tranfered <br>to<br>Queue"));
            indexIds.add(skillMap.get("Tranfered <br>to<br>Agent"));

        }
        params.setIndexIds(indexIds);
        try {
            response = Unirest.post(CMSGatewayURL + "hindex/skill").header("Content-Type", "application/json").body(params).asObject(CMSAgentHistoryResponse.class);
            int status = response.getStatus();
            result = response.getBody();
            if (result.getRetcode().equalsIgnoreCase("0")) {
            } else {
            }
        } catch (UnirestException ex) {
            result.setRetcode("100-001");
            logger.error("skillOfcallstat Error: against skillID" + skillID + "" + ex.getMessage());
        }
        return result;
    }

    @Override
    public CMSAgentHistoryResponse skillOfcallavg(String skillID) {
        CMSAgentHistoryResponse result = new CMSAgentHistoryResponse();
        HttpResponse<CMSAgentHistoryResponse> response = null;
        CMSAgentHistoryParams params = new CMSAgentHistoryParams();
        params.setCcId(CCID);
        params.setVdnId(VDNID);
        List<Integer> objList = new ArrayList<Integer>();
        objList.add(Integer.parseInt(skillID));
        params.setObjectIds(objList);
        List<String> indexIds = new ArrayList<String>();
        Map<String, String> skillMap = CMSMap.skillMap;
        if (skillMap.size() > 0) {
            indexIds.add(skillMap.get("Average waiting time of queuing calls"));
            indexIds.add(skillMap.get("Average waiting time of queuing lost calls"));
            indexIds.add(skillMap.get("Average waiting time before calls answered"));
            indexIds.add(skillMap.get("Average waiting time before lost calls are answered"));
            indexIds.add(skillMap.get("Average Call Duration"));
            indexIds.add(skillMap.get("Average Waiting Time"));
            indexIds.add(skillMap.get("Longest Waiting Time"));
            indexIds.add(skillMap.get("Shortest Waiting Time"));
        }
        params.setIndexIds(indexIds);
        try {
            response = Unirest.post(CMSGatewayURL + "hindex/skill").header("Content-Type", "application/json").body(params).asObject(CMSAgentHistoryResponse.class);
            int status = response.getStatus();
            result = response.getBody();
            if (result.getRetcode().equalsIgnoreCase("0")) {
            } else {
            }
        } catch (UnirestException ex) {
            result.setRetcode("100-001");
            logger.error("skillOfcallstat Error: against skillID" + skillID + "" + ex.getMessage());
        }
        return result;
    }

    @Override
    public CMSAgentHistoryResponse skillOfcallingrate(String skillID) {
        CMSAgentHistoryResponse result = new CMSAgentHistoryResponse();
        HttpResponse<CMSAgentHistoryResponse> response = null;
        CMSAgentHistoryParams params = new CMSAgentHistoryParams();
        params.setCcId(CCID);
        params.setVdnId(VDNID);
        List<Integer> objList = new ArrayList<Integer>();
        objList.add(Integer.parseInt(skillID));
        params.setObjectIds(objList);
        List<String> indexIds = new ArrayList<String>();
        Map<String, String> skillMap = CMSMap.skillMap;
        if (skillMap.size() > 0) {
            indexIds.add(skillMap.get("Call Completion Rate"));
            indexIds.add(skillMap.get("Call Loss Rate"));
            indexIds.add(skillMap.get("Call Reached to Agents Rate"));
        }
        params.setIndexIds(indexIds);
        try {
            response = Unirest.post(CMSGatewayURL + "hindex/skill").header("Content-Type", "application/json").body(params).asObject(CMSAgentHistoryResponse.class);
            int status = response.getStatus();
            result = response.getBody();
            if (result.getRetcode().equalsIgnoreCase("0")) {
            } else {
            }
        } catch (UnirestException ex) {
            result.setRetcode("100-001");
            logger.error("skillOfcallstat Error: against skillID" + skillID + "" + ex.getMessage());
        }
        return result;
    }

}
