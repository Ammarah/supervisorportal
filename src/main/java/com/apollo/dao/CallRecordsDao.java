/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apollo.dao;

import Model.CallRecordsResponse;
import java.util.List;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author Iceman
 */
public interface CallRecordsDao {

    public String getCallData(String FromDate, String ToDate, HttpServletRequest request);

}
