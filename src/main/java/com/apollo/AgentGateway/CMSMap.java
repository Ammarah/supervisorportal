/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apollo.AgentGateway;

import java.util.Map;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Abdul Rehman
 */
public class CMSMap {

    public static Map<Integer, String> agentCMSStates = new HashMap<Integer, String>();
    public static Map<String, String> agentMap = new HashMap<String, String>();
    public static Map<String, String> skillMap = new HashMap<String, String>();
    public static Map<String, String> ivrMap = new HashMap<String, String>();
    public static Map<String, String> vdnMap = new HashMap<String, String>();
    private static CMSMap instance;

    private CMSMap() {

    }

    public static CMSMap getInstance() {
        if (instance == null) {
            instance = new CMSMap();
            loadMaps();
        }
        return instance;
    }

    public static void Clear() {
        if (instance != null) {
            instance = null;
            agentMap.clear();
            skillMap.clear();
            vdnMap.clear();
            ivrMap.clear();
            agentCMSStates.clear();
        }
    }

    private static void loadMaps() {
        //Agent Status
        agentCMSStates.put(0, "Not sign-in");
        agentCMSStates.put(1, "Ready");
        agentCMSStates.put(2, "Preoccupied");
        agentCMSStates.put(3, "Occupied");
        agentCMSStates.put(4, "Answering");
        agentCMSStates.put(5, "Talk");
        agentCMSStates.put(6, "Work");
        agentCMSStates.put(7, "Busy");
        agentCMSStates.put(8, "Rest");
        agentCMSStates.put(9, "Study");
        //Agent
        //#region
        // har idx value ka maine name diya hai doc daikhte hue
        agentMap.put("Agent Sign In Times", "IDX_04_02_001");
        agentMap.put("Agent Sign In Duration", "IDX_04_02_002");
        agentMap.put("Agent No Of Incoming Calls", "IDX_04_02_003");
        agentMap.put("Agent Duration Of Incoming Calls", "IDX_04_02_004");
        agentMap.put("Agent Average Duration Of Incoming Calls", "IDX_04_02_005");
        agentMap.put("Agent No Of Outgoing Calls", "IDX_04_02_006");
        agentMap.put("Agent Duration Of Outgoing Calls", "IDX_04_02_007");
        agentMap.put("Agent Average Duration Of Outgoing Calls", "IDX_04_02_008");
        agentMap.put("Agent WrapUp Times", "IDX_04_02_009");
        agentMap.put("Agent WrapUp Duration", "IDX_04_02_010");
        agentMap.put("Agent WrapUp Average Duration", "IDX_04_02_011");
        agentMap.put("Agent Rest Times", "IDX_04_02_012");
        agentMap.put("Agent Rest Duration", "IDX_04_02_013");
        agentMap.put("Agent Rest Average Duration", "IDX_04_02_014");
        agentMap.put("Agent HoldOn Times", "IDX_04_02_015");
        agentMap.put("Agent HoldOn Duration", "IDX_04_02_016");
        agentMap.put("Agent HoldOn Average Duration", "IDX_04_02_017");
        agentMap.put("Agent Busy Times", "IDX_04_02_018");
        agentMap.put("Agent Busy Duration", "IDX_04_02_019");
        agentMap.put("Agent Busy Average Duration", "IDX_04_02_020");
        agentMap.put("Agent Answer Call Duration", "IDX_04_02_021");
        agentMap.put("Agent Idle Duration", "IDX_04_02_022");
        agentMap.put("Agent Internal Transfer In Time", "IDX_04_02_023");
        agentMap.put("Agent Internal Transfer Out Time", "IDX_04_02_024");
        agentMap.put("Agent No Of Blocked Or IVR Transfer Call Time", "IDX_04_02_025");
        agentMap.put("Agent No Of Third Party Calls", "IDX_04_02_026");
        agentMap.put("Agent No Of Internal Calls", "IDX_04_02_027");
        agentMap.put("Agent No Of Internal Help", "IDX_04_02_028");
        agentMap.put("Agent SignIn SignOut Stats", "IDX_04_03_001");
        agentMap.put("Agent No Of Request", "IDX_04_04_001");
        agentMap.put("Agent No Of Calls", "IDX_04_04_002");
        agentMap.put("Agent No Of Call Losses", "IDX_04_04_003");
        agentMap.put("Agent Call Completion Rate", "IDX_04_04_004");
        agentMap.put("Agent No Of Call Ring And Wait To Answered", "IDX_04_04_005");
        agentMap.put("Agent No Of Call Ring And Not Answered", "IDX_04_04_006");
        agentMap.put("Agent No Of Call Wait For Long But Not Answered", "IDX_04_04_007");
        agentMap.put("Agent No Of Call HangUp After Answered", "IDX_04_04_008");
        agentMap.put("Agent Average Duration Of Answered Calls", "IDX_04_04_009");
        agentMap.put("Agent Average Call Duration", "IDX_04_04_010");
        agentMap.put("Agent Longest Call Duration", "IDX_04_04_011");
        agentMap.put("Agent Shortest Call Duration", "IDX_04_04_012");
        agentMap.put("Agent No Of Request Multi", "IDX_04_07_001");
        agentMap.put("Agent No Of Successfull Multi", "IDX_04_07_002");
        agentMap.put("Agent Call Duration Multi", "IDX_04_07_003");
        //#endregion
        //Skill
        //#region
        skillMap.put("Agents Count", "IDX_03_02_001");
        skillMap.put("Talking Agents", "IDX_03_02_002");
        skillMap.put("Ready Agents", "IDX_03_02_003");
        skillMap.put("Available Agents", "IDX_03_02_004");
        skillMap.put("Not Ready Agents", "IDX_03_02_005");
        skillMap.put("Work Agents", "IDX_03_02_006");
        skillMap.put("Break Agents", "IDX_03_02_007");
        skillMap.put("Study Agents", "IDX_03_02_010");

        skillMap.put("No of queueing calls", "IDX_03_01_002");
        skillMap.put("No of ongoing calls", "IDX_03_01_003");
        skillMap.put("Max waiting time for queue", "IDX_03_01_004");
        skillMap.put("No of connected calls in last 5 minutes", "IDX_03_01_005");

        skillMap.put("In Queue", "IDX_03_04_001");
        skillMap.put("Count", "IDX_03_04_002");
        skillMap.put("Lost", "IDX_03_04_003");
        skillMap.put("Disconnected<br>Queues", "IDX_03_04_004");
        skillMap.put("Hung<br>up", "IDX_03_04_005");
        skillMap.put("Dropped", "IDX_03_04_006");
        skillMap.put("Interrupted", "IDX_03_04_007");
        skillMap.put("Not<br>Answered", "IDX_03_04_008");
        skillMap.put("Tranfered <br>to<br>Queue", "IDX_03_04_009");
        skillMap.put("Tranfered <br>to<br>Agent", "IDX_03_04_010");

        skillMap.put("Call Completion Rate", "IDX_03_05_001");
        skillMap.put("Call Loss Rate", "IDX_03_05_002");
        skillMap.put("Call Reached to Agents Rate", "IDX_03_05_003");

        skillMap.put("Average waiting time of queuing calls", "IDX_03_06_001");
        skillMap.put("Average waiting time of queuing lost calls", "IDX_03_06_002");
        skillMap.put("Average waiting time before calls answered", "IDX_03_06_003");
        skillMap.put("Average waiting time before lost calls are answered", "IDX_03_06_004");
        skillMap.put("Average Call Duration", "IDX_03_06_005");
        skillMap.put("Average Waiting Time", "IDX_03_06_006");
        skillMap.put("Longest Waiting Time", "IDX_03_06_007");
        skillMap.put("Shortest Waiting Time", "IDX_03_06_008");

        //60 min interval
        //skillMap.put("IDX_03_12_001", "In Queues");
        //skillMap.put("IDX_03_12_002", "Count");
        //skillMap.put("IDX_03_12_003", "Lost");
        //skillMap.put("IDX_03_12_004", "Queues Disconnected");
        //skillMap.put("IDX_03_12_005", "Hung Up");
        //skillMap.put("IDX_03_12_006", "Dropped");
        //skillMap.put("IDX_03_12_007", "Interrupted");
        //skillMap.put("IDX_03_12_008", "Shortest Waiting Time");
        //skillMap.put("IDX_03_12_009", "Shortest Waiting Time");
        //skillMap.put("IDX_03_12_010", "Shortest Waiting Time");
        //skillMap.put("IDX_03_12_011", "Shortest Waiting Time");
        //skillMap.put("IDX_03_12_012", "Shortest Waiting Time");
        //skillMap.put("IDX_03_12_013", "Shortest Waiting Time");
        //skillMap.put("IDX_03_12_014", "Shortest Waiting Time");
        //skillMap.put("IDX_03_12_015", "Shortest Waiting Time");
        //skillMap.put("IDX_03_12_016", "Shortest Waiting Time");
        //skillMap.put("IDX_03_12_017", "Shortest Waiting Time");
        //skillMap.put("IDX_03_12_018", "Shortest Waiting Time");
        //skillMap.put("IDX_03_12_019", "Shortest Waiting Time");
        //skillMap.put("IDX_03_12_020", "Shortest Waiting Time");
        //skillMap.put("IDX_03_12_021", "Shortest Waiting Time");
        //skillMap.put("IDX_03_12_022", "Shortest Waiting Time");
        //skillMap.put("IDX_03_12_023", "Shortest Waiting Time");
        //skillMap.put("IDX_03_12_024", "Shortest Waiting Time");
        //skillMap.put("IDX_03_12_025", "Shortest Waiting Time");
        // #endregion
        //IVR
        //ivrMap.put("", "");
        //VDN Real Time
        vdnMap.put("Call Count", "IDX_01_04_001");
        vdnMap.put("Incoming Calls", "IDX_01_04_009");
        vdnMap.put("IVR Count", "IDX_01_04_002");
        vdnMap.put("Queueing Calls", "IDX_01_04_004");
        vdnMap.put("Login Agents", "IDX_01_04_005");
        vdnMap.put("Ready Agents", "IDX_01_04_006");
        vdnMap.put("Not Ready Agents", "IDX_01_04_007");
        vdnMap.put("Talking Agents", "IDX_01_04_008");
        vdnMap.put("Study Agents", "IDX_01_04_010");
        vdnMap.put("Work Agents", "IDX_01_04_011");
        vdnMap.put("Answering Agents", "IDX_01_04_012");
        vdnMap.put("Preoccupied Agents", "IDX_01_04_013");
        vdnMap.put("Occupied Agents", "IDX_01_04_014");
        vdnMap.put("Not Login Agents", "IDX_01_04_015");
        vdnMap.put("Break Agents", "IDX_01_04_016");

        //Historical VDN
        vdnMap.put("Count", "IDX_01_02_001");
        vdnMap.put("Connected<br>Count", "IDX_01_02_002");
        vdnMap.put("Completion<br>Rate<br>of Calls", "IDX_01_02_003");
        vdnMap.put("Incoming<br>IVR Count", "IDX_01_02_004");
        vdnMap.put("Connected IVR<br>Incoming<br>Count", "IDX_01_02_005");
        vdnMap.put("Completion<br>Rate of<br>IVR Calls", "IDX_01_02_006");
        vdnMap.put("Manual<br>Count", "IDX_01_02_007");
        vdnMap.put("Connected<br>Manual Count", "IDX_01_02_008");
        vdnMap.put("Completion<br>Rate<br>of Manual Calls", "IDX_01_02_009");
        vdnMap.put("Average<br>Duration<br>of Occupied", "IDX_01_02_010");
        vdnMap.put("Dropped<br>Count", "IDX_01_02_011");
        vdnMap.put("Average<br>Duration Call", "IDX_01_02_012");
        vdnMap.put("Average<br>Waiting Time", "IDX_01_02_013");

        //vdnMap.put("IDX_01_01_007", "Calls in In 1 Hour");
        //vdnMap.put("IDX_01_01_008", "Connected Calls in In 1 Hour");
        //vdnMap.put("IDX_01_01_009", "Lost Calls in In 1 Hour");
        vdnMap.put("Outgoing<br>Calls", "IDX_01_03_001");
        vdnMap.put("Outgoing<br>Connected Calls", "IDX_01_03_002");
        vdnMap.put("Completion Rate<br>of Outgoing Calls", "IDX_01_03_003");
        vdnMap.put("Outgoing<br>Connected<br>IVR Calls", "IDX_01_03_004");
        vdnMap.put("Outgoing<br>Manual<br>Calls", "IDX_01_03_005");
        vdnMap.put("Average<br>Duration<br>of Occupied<br>Calls", "IDX_01_03_006");

        vdnMap.put("Drop Rate of Manual Incoming Calls", "IDX_01_05_001");
        vdnMap.put("Drop Rate of Incoming Calls", "IDX_01_05_002");

        //multimedia calls
        vdnMap.put("Request Count", "IDX_01_11_001");
        vdnMap.put("Successfull Count", "IDX_01_11_002");
        vdnMap.put("Duration", "IDX_01_11_003");
        vdnMap.put("Waiting Time", "IDX_01_11_004");
        vdnMap.put("Queue Failure", "IDX_01_11_005");
        vdnMap.put("Queue Drops", "IDX_01_11_006");
        vdnMap.put("Answered Calls", "IDX_01_11_007");
    }
}
