/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apollo.controller;

import Model.AgentInfo;
import Model.CMSAgentHistoryResponse;
import Model.VDNSkillsResponse;
import com.apollo.service.VdnCallStatService;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Iceman
 */
@Controller
@RequestMapping(value = "/vdn")
public class VdnCallStat {

    @Autowired
    private VdnCallStatService vdncallstatservice;

    public VdnCallStat() {

    }

    @RequestMapping(value = "/incoming", method = RequestMethod.GET)
    public ModelAndView vdnincoming(Model mv) {
        ModelAndView model = new ModelAndView("VDN/Incoming");
        return model;
    }

    @RequestMapping(value = "/outgoing", method = RequestMethod.GET)
    public ModelAndView vdnoutgoing(Model mv) {
        ModelAndView model = new ModelAndView("VDN/Outgoing");
        return model;
    }

    @RequestMapping(value = "/realtime", method = RequestMethod.GET)
    public ModelAndView vdnrealtime(Model mv) {
        ModelAndView model = new ModelAndView("VDN/Real-Time");
        return model;
    }

    @RequestMapping(value = "/HistoricalVDNDropRate", method = RequestMethod.GET)
    @ResponseBody
    public CMSAgentHistoryResponse HistoricalVDNDropRate(Model mv) {
        CMSAgentHistoryResponse response = new CMSAgentHistoryResponse();
        response = vdncallstatservice.HistoricalVDNDropRate();
        return response;
    }

    @RequestMapping(value = "/HistoricalVDNIncomingCallStats", method = RequestMethod.GET)
    @ResponseBody
    public CMSAgentHistoryResponse HistoricalVDNIncomingCallStats(Model mv) {
        CMSAgentHistoryResponse response = new CMSAgentHistoryResponse();
        response = vdncallstatservice.HistoricalVDNIncomingCallStats();
        return response;
    }

    @RequestMapping(value = "/HistoricalVDNOutGoingCallStats", method = RequestMethod.GET)
    @ResponseBody
    public CMSAgentHistoryResponse HistoricalVDNOutGoingCallStats(Model mv) {
        CMSAgentHistoryResponse response = new CMSAgentHistoryResponse();
        response = vdncallstatservice.HistoricalVDNOutGoingCallStats();
        return response;
    }

    @RequestMapping(value = "/RealTimeVdnStats", method = RequestMethod.GET)
    @ResponseBody
    public CMSAgentHistoryResponse RealTimeVdnStats(Model mv) {
        CMSAgentHistoryResponse response = new CMSAgentHistoryResponse();
        response = vdncallstatservice.RealTimeVdnStats();
        return response;
    }

}
