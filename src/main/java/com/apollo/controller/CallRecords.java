/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apollo.controller;

import Model.V3ToWav;
import com.apollo.service.CallRecordsService;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.io.FilenameUtils;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.support.RequestContextUtils;

/**
 *
 * @author Iceman
 */
@Controller
@RequestMapping(value = "/CallAudioRecord")
public class CallRecords {

    @Autowired
    private CallRecordsService callrecordsService;
    private String sftpIp;
    private String sftpUserName;
    private String sftpPassword;
    String webapps = "webapps";
    String TempDir = "SupervisorCallAudio";
    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(CallRecords.class);

    public CallRecords() throws URISyntaxException {
        String input = getClass().getProtectionDomain().getCodeSource().getLocation().toURI().getPath();
        try {
            InputStream stream = new FileInputStream(input + "props//sftpConfig.properties");
            Properties prop = new Properties();
            prop.load(stream);
            sftpIp = prop.getProperty("sftp.Ip");
            sftpUserName = prop.getProperty("sftp.Username");
            sftpPassword = prop.getProperty("sftp.Password");
            stream.close();

        } catch (Exception ex) {
            Logger.getLogger(CallRecords.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @RequestMapping(value = "/AjaxGetDataFromIPCC", method = RequestMethod.POST)
    public ModelAndView GetCallData(RedirectAttributes redirectAttributes, HttpServletRequest request) {
        ModelAndView model = null;
        String FromDate = request.getParameter("StartDateFilter");
        String ToDate = request.getParameter("EndDateFilter");
        model = new ModelAndView("redirect:/CallAudioRecord/AgentReport");
        String Calldata = callrecordsService.getCallData(FromDate, ToDate, request);
        redirectAttributes.addFlashAttribute("Calldata", Calldata);
        redirectAttributes.addFlashAttribute("FromDate", FromDate);
        redirectAttributes.addFlashAttribute("ToDate", ToDate);
        return model;
    }

    @RequestMapping(value = "/AgentReport", method = RequestMethod.GET)
    public ModelAndView AgentStatisticsReport(Model models, HttpServletRequest request, HttpServletResponse response) throws ParseException {
        logger.info("This is my first log4j's statement");
        ModelAndView model = new ModelAndView("Call_Records/Call-Records");
        String fileUrl = request.getParameter("fileUrl");
        Object objUser = new Object();
        JSONParser parser = new JSONParser();
        Map<String, ?> inputFlashMap = RequestContextUtils.getInputFlashMap(request);
        if (inputFlashMap != null) {
            String Calldata = (String) inputFlashMap.get("Calldata");
            String FromDate = (String) inputFlashMap.get("FromDate");
            String ToDate = (String) inputFlashMap.get("ToDate");
            Object CalldataObj = new Object();
            CalldataObj = parser.parse(Calldata);
            model.addObject("agentStats", CalldataObj);
            model.addObject("FromDate", FromDate);
            model.addObject("ToDate", ToDate);
            model.addObject("userList", objUser);
            return model;
        }
        return model;
    }

    @RequestMapping(value = "/DownloadWidgetDataFile", method = RequestMethod.POST)
    @ResponseBody
    public String DownloadWidgetDataFile(HttpServletRequest request, HttpServletResponse response) throws ParseException, MalformedURLException, UnknownHostException {
        logger.info("Ajax Call is received For Download");
        ModelAndView model = new ModelAndView("Call_Records/Call-Records");
        String SaveWavDir = null;
        String DownloadWavDir = null;
        String fileUrl = request.getParameter("fileUrl");
        logger.info("fileUrl From DB: \t" + fileUrl);
        File myFile = new File(fileUrl);
        URL myUrl = myFile.toURI().toURL();
        V3ToWav v3towav = new V3ToWav();
        logger.info("myUrl:  :\t" + myUrl);
        String basename = FilenameUtils.getBaseName(fileUrl);
        String extension = FilenameUtils.getExtension(fileUrl);
        JSch jsch = new JSch();
        Session session = null;
        String myUrl1 = "file:/" + fileUrl.replace("\\", "/");
        logger.info("Concatinate MyurlO:  :\t" + myUrl1);
//        String[] GetPath = myUrl.toString().split(":");
        String[] GetPath = myUrl1.split(":");
        String WorkingDir = GetPath[2];
        logger.info("WorkingDir  :\t" + WorkingDir);
        try {
            session = jsch.getSession(sftpUserName, sftpIp, 22);
            session.setConfig("StrictHostKeyChecking", "no");
            session.setPassword(sftpPassword);
            session.connect();
            System.out.println("sftp connected successfully");
            Channel channel = session.openChannel("sftp");
            channel.connect();
            ChannelSftp sftpChannel = (ChannelSftp) channel;
            sftpChannel.cd("..");
            sftpChannel.cd("..");
            String currentDirectory = sftpChannel.pwd();
            logger.info("sftp currentDirectory" + currentDirectory);
            sftpChannel.cd(currentDirectory);
            logger.info("sftp Change Dir Successfull");
            try {
                String SaveDir = System.getProperty("catalina.base") + File.separator + webapps + File.separator;
            } catch (Exception e) {
                e.printStackTrace();
            }
//            String SaveDir = System.getProperty("catalina.base") + "/" + webapps + "/";
            String SaveDir = System.getProperty("catalina.base") + File.separator + webapps + File.separator;
            System.out.println("Save Dir \t" + SaveDir);

            File dir = new File(SaveDir + TempDir);
            if (!dir.exists()) {
                dir.mkdirs();
                logger.info("Dir Created \t");
            }
            currentDirectory = currentDirectory + WorkingDir;

            SaveDir = SaveDir + TempDir + File.separator + basename + "." + extension;

            sftpChannel.get(currentDirectory, SaveDir);
//            SaveWavDir = System.getProperty("catalina.base") + "/" + webapps + "/" + TempDir + "/" + basename + ".wav";
            SaveWavDir = System.getProperty("catalina.base") + File.separator + webapps + File.separator + TempDir + File.separator + basename + ".wav";
            logger.info("Save SaveWavDir: \t" + SaveDir);
//            String[] DownloadWavDir = (request.s + File.separator + TempDir).split(SaveDir, 0
            DownloadWavDir = request.getServerName() + ":" + request.getServerPort() + File.separator + TempDir + File.separator + basename + ".wav";

            logger.info("Download Wav File Dir: \t" + DownloadWavDir);

            try {
                v3towav.voxConvert(SaveDir, SaveWavDir, 1, 6000, 1);
            } catch (Exception e) {
                e.printStackTrace();
            }
            File file = new File(SaveDir);
            if (file.delete()) {
                logger.info(file.getName() + " is deleted!");
            } else {
                logger.info("Delete operation is failed.");
            }
//            convertULawFileToWav(SaveDir, basename);
            sftpChannel.exit();
            session.disconnect();
        } catch (JSchException e) {
            e.printStackTrace();
        } catch (SftpException e) {
            e.printStackTrace();
        }
        return DownloadWavDir;
    }

//    public void convertULawFileToWav(String filepath, String basename) {
//        String SaveDir = System.getProperty("catalina.base") + File.separator + webapps + File.separator + TempDir + File.separator;
//        File file = new File(filepath);
//        if (!file.exists()) {
//            return;
//        }
//        if (file.delete()) {
//            System.out.println(file.getName() + " is deleted!");
//        } else {
//            System.out.println("Failed to delete the file");
//        }
//        try {
//            long fileSize = file.length();
//            int frameSize = 160;
//            long numFrames = fileSize / frameSize;
//            AudioFormat audioFormat = new AudioFormat(AudioFormat.Encoding.ULAW, 8000, 8, 1, frameSize, 50, true);
//            AudioInputStream audioInputStream = new AudioInputStream(new FileInputStream(file), audioFormat, numFrames);
//            AudioSystem.write(audioInputStream, AudioFileFormat.Type.WAVE, new File(SaveDir + File.separator + basename + ".wav"));
//
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }
}
