/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apollo.controller;

import Model.AgentInfo;
import Model.CMSAgentHistoryResponse;
import Model.VDNSkillsResponse;
import com.apollo.service.SkillAgentService;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Iceman
 */
@Controller
@RequestMapping(value = "/skill")
public class SkillAgent {

    public String AgentGatewayURL = "";
    public String CMSGatewayURL = "";
    public AgentInfo agent;
    public String VDNID;
    public String CCID;
    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(SkillAgent.class);

    @Autowired
    private SkillAgentService skillagentservice;

    public SkillAgent() {
    }

    @RequestMapping(value = "/agentinfo", method = RequestMethod.GET)
    public ModelAndView skillagentinfo(Model mv) {
        ModelAndView model = new ModelAndView("skill/agent-info");
        VDNSkillsResponse result = new VDNSkillsResponse();
        result = skillagentservice.skillagentinfo();
        model.addObject("SkillList", result);
        return model;
    }

    @RequestMapping(value = "/callstats", method = RequestMethod.GET)
    public ModelAndView callstatsInfo(Model mv) {
        ModelAndView model = new ModelAndView("skill/call-stats");
        VDNSkillsResponse result = new VDNSkillsResponse();
        result = skillagentservice.skillagentinfo();
        model.addObject("SkillList", result);
        return model;
    }

    @RequestMapping(value = "/skillOfagentinfo", method = RequestMethod.GET)
    @ResponseBody
    public CMSAgentHistoryResponse skillOfagentinfo(Model mv, String skillID) {
        CMSAgentHistoryResponse result = new CMSAgentHistoryResponse();
        result = skillagentservice.skillOfagentinfo(skillID);
        return result;
    }

    @RequestMapping(value = "/AgentSkillinformation", method = RequestMethod.GET)
    @ResponseBody
    public CMSAgentHistoryResponse AgentSkillinformation(Model mv, String skillID) {
        CMSAgentHistoryResponse result = new CMSAgentHistoryResponse();
        result = skillagentservice.AgentSkillinformation(skillID);
        return result;
    }

    @RequestMapping(value = "/skillOfcallstat", method = RequestMethod.GET)
    @ResponseBody
    public CMSAgentHistoryResponse skillOfcallstat(Model mv, String skillID) {
        CMSAgentHistoryResponse response = new CMSAgentHistoryResponse();
        response = skillagentservice.skillOfcallstat(skillID);
        return response;
    }

    @RequestMapping(value = "/calldetails", method = RequestMethod.GET)
    public ModelAndView skillOfcalldetails(Model mv) {
        ModelAndView model = new ModelAndView("skill/call-details");
        VDNSkillsResponse result = new VDNSkillsResponse();
        result = skillagentservice.skillagentinfo();
        model.addObject("SkillList", result);
        return model;
    }

    @RequestMapping(value = "/skillOfcallavg", method = RequestMethod.GET)
    @ResponseBody
    public CMSAgentHistoryResponse skillOfcallavg(Model mv, String skillID) {
        CMSAgentHistoryResponse response = new CMSAgentHistoryResponse();
        response = skillagentservice.skillOfcallavg(skillID);
        return response;
    }

    @RequestMapping(value = "/skillOfcallingrate", method = RequestMethod.GET)
    @ResponseBody
    public CMSAgentHistoryResponse skillOfcallingrate(Model mv, String skillID) {
        CMSAgentHistoryResponse response = new CMSAgentHistoryResponse();
        response = skillagentservice.skillOfcallingrate(skillID);
        return response;
    }

}
