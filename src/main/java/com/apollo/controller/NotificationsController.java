package com.apollo.controller;

/**
 *
 * @author Ammarah
 */
import Model.Notification;
import com.apollo.dao.impl.NotificationDao;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value = "/NotificationsController")
public class NotificationsController {

    @Autowired
    NotificationDao dao;
    Timestamp startTimestamp = null;
    Timestamp endTimestamp = null;

    @RequestMapping(value = "/notification", method = RequestMethod.GET)
    public ModelAndView notification(Model models, HttpServletRequest request, HttpServletResponse response) throws ParseException {
        ModelAndView model = new ModelAndView("Notification/Wallboard-Notification");
        return model;
    }

    @RequestMapping(value = "/addNotification", method = RequestMethod.GET)
    public ModelAndView addNotification(Model models, HttpServletRequest request, HttpServletResponse response) throws ParseException {
        ModelAndView model = new ModelAndView("Notification/addNotification");
        return model;
    }

    @RequestMapping(value = "/saveNotification", method = RequestMethod.POST)
    public String saveNotification(Model models, HttpServletRequest request, HttpServletResponse response) throws ParseException {
        System.out.println("lets save it ");
        String text = request.getParameter("text_info");
        String startDate = request.getParameter("startDateTime");
        String endDate = request.getParameter("endDateTime");
        Integer location = Integer.parseInt(request.getParameter("location"));
        String skillId = request.getParameter("skillId");
        String priority = request.getParameter("priority");
        String type = request.getParameter("type");
        Notification n = new Notification();
        n.setNotificationText(text);
        n.setPriority(priority);
        n.setType(type);
        n.setLocation(location);
        n.setSkillId(skillId);

        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MMMM-dd HH:mm");
            Date startParsedDate = dateFormat.parse(startDate);
            System.out.println(startParsedDate.toString());
            startTimestamp = new java.sql.Timestamp(startParsedDate.getTime());
            System.out.println(startTimestamp.toString());
            Date endParsedDate = dateFormat.parse(endDate);
            System.out.println(endParsedDate.toString());
            endTimestamp = new java.sql.Timestamp(endParsedDate.getTime());
            System.out.println(endTimestamp.toString());
            n.setStartDateTime(startTimestamp);
            n.setEndDateTime(endTimestamp);
            dao.save(n);
        } catch (Exception e) {
            System.out.println(e);
        }

        ModelAndView model = new ModelAndView("Notification/Wallboard-Notification");
        return "redirect:/NotificationsController/notification";
    }

    @RequestMapping(value = "/getNotifications", method = RequestMethod.GET)
    @ResponseBody
    public String getNotifications() {
        System.out.println("get notifications is called");
        List<Notification> response = null;
        Gson gson = new Gson();
        JsonObject jsonResponse = new JsonObject();
        try {
            response = dao.getNotifications();
            jsonResponse.add("data", gson.toJsonTree(response));
        } catch (Exception ex) {
            System.out.println("" + ex.getMessage());
        }
        return jsonResponse.toString();
    }

    @RequestMapping(value = "/editNotification/{id}", method = RequestMethod.GET)
    @ResponseBody
    public ModelAndView editNotification(@PathVariable int id, Model models, HttpServletRequest request, HttpServletResponse response) throws ParseException {
        Notification notification = dao.getNotificationById(id);
        Timestamp now = new Timestamp(new Date().getTime());
        boolean editable = false;
//        if(now.after(notification.getStartDateTime()) && now.before(notification.getEndDateTime())){
//            editable=true;
//        } 
        if (now.before(notification.getEndDateTime())) {
            editable = true;
        } else {
            editable = false;
        }
        ModelAndView model = new ModelAndView("Notification/editNotification");
        model.addObject("notification", notification);
        model.addObject("editable", editable);
        return model;
    }

    @RequestMapping(value = "/deleteNotification/{id}", method = RequestMethod.GET)
    public String delete(@PathVariable int id) {
        dao.delete(id);
        return "redirect:/NotificationsController/notification";
    }

    @RequestMapping(value = "/updateNotification", method = RequestMethod.POST)
    public String updateNotification(Model models, HttpServletRequest request, HttpServletResponse response) throws ParseException {
        System.out.println("lets update ");
        Integer id = Integer.parseInt(request.getParameter("notificationID"));
        String text = request.getParameter("text_info");
        String startDate = request.getParameter("startDateTime");
        String endDate = request.getParameter("endDateTime");
        Integer location = Integer.parseInt(request.getParameter("location"));
        String skillId = request.getParameter("skillId");
        String priority = request.getParameter("priority");
        String type = request.getParameter("type");
        String status = request.getParameter("status");
        Notification n = new Notification();
        n.setNotificationID(id);
        n.setNotificationText(text);
        n.setPriority(priority);
        n.setType(type);
        n.setStatus(status);
        n.setLocation(location);
        n.setSkillId(skillId);

        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MMMM-dd HH:mm");
            Date startParsedDate = dateFormat.parse(startDate);
            System.out.println(startParsedDate.toString());
            startTimestamp = new java.sql.Timestamp(startParsedDate.getTime());
            System.out.println(startTimestamp.toString());
            Date endParsedDate = dateFormat.parse(endDate);
            System.out.println(endParsedDate.toString());
            endTimestamp = new java.sql.Timestamp(endParsedDate.getTime());
            System.out.println(endTimestamp.toString());
            n.setStartDateTime(startTimestamp);
            n.setEndDateTime(endTimestamp);
            dao.update(n);
        } catch (Exception e) {
            System.out.println(e);
        }

        ModelAndView model = new ModelAndView("Notification/Wallboard-Notification");
        return "redirect:/NotificationsController/notification";
    }

}
