package com.apollo.controller;

import Model.Notification;
import com.apollo.dao.impl.NotificationDao;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Ammarah
 */
@RestController
@RequestMapping("api/notification")
public class NotificationApi {

    @Autowired
    NotificationDao dao;

    @RequestMapping("")
    public String welcome() {
        return "Welcome to Notifications API.";
    }

    @RequestMapping(value = "/getAllNotifications", method = RequestMethod.GET)
    @ResponseBody
    public List getAllNotifications() {
        List<Notification> response = null;
        try {
            response = dao.getNotifications();
        } catch (Exception ex) {
            System.out.println("" + ex.getMessage());
        }
        return response;

    }

    @RequestMapping(value = "/getActiveNotifications/{location}", method = RequestMethod.GET)
    @ResponseBody
    public List getActiveNotifications(@PathVariable int location) {
        List<Notification> response = null;
        try {
            response = dao.getActiveNotifications(location);
        } catch (Exception ex) {
            System.out.println("" + ex.getMessage());
        }
        return response;
    }

        @RequestMapping(value = "/getActiveNotificationsSkillBased/{skillId}", method = RequestMethod.GET)
    @ResponseBody
    public List getActiveNotificationsSkillBased(@PathVariable String skillId) {
        List<Notification> response = null;
        try {
            response = dao.getActiveNotificationsSkillBased(skillId);
        } catch (Exception ex) {
            System.out.println("" + ex.getMessage());
        }
        return response;
    }
    @RequestMapping(value = "/getActiveNotifications", method = RequestMethod.GET)
    @ResponseBody
    public List getActiveNotifications() {
        List<Notification> response = null;
        try {
            response = dao.getActiveNotifications();
        } catch (Exception ex) {
            System.out.println("" + ex.getMessage());
        }
        return response;
    }
}
