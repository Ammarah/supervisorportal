/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AgentGateway;

import Model.AgenInfoResponse;
import Model.AgentBreaks;
import Model.AgentDurationResponse;
import Model.AgentInfo;
import Model.AgentPhoneListResponse;
import Model.AgentPoolResponse;
import Model.AgentSkillAdjustData;
import Model.AgentSkillsByWorkno;
import Model.AllAgentSkills;
import Model.CMSAgentHistoryParams;
import Model.CMSAgentHistoryResponse;
import Model.CTIParams;
import Model.CheckAgentStatus;
import Model.DefaultResponse;
import Model.LoginAgentSkills;
import Model.LoginParams;
import Model.LoginResponse;
import Model.SpecificAgentDetailResponse;
import Model.VDNParams;
import Model.VDNSkillsResponse;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.mashape.unirest.http.Headers;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.ObjectMapper;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Abdul Rehman
 */
public class AgentGatewayServiceConsumer {

    public String AgentGatewayURL = "";
    public String CMSGatewayURL = "";
    public AgentInfo agent;
    public String VDNID;
    public String CCID;
    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(AgentGatewayServiceConsumer.class);

    public AgentGatewayServiceConsumer() throws URISyntaxException {
        InputStream stream = null;
        agent = AgentInfo.getInstance();
        //live config BAFL
//        AgentGatewayURL = "http://172.21.31.175:8000/agentgateway/resource/";//"http://10.77.194.82:8000/agentgateway/resource/";//"http://172.21.31.175:8000/agentgateway/resource/";
//        CMSGatewayURL = "http://172.21.31.177:9068/cmsgateway/resource/";//"http://10.77.194.86:9068/cmsgateway/resource/";//http://172.21.31.177:9068/cmsgateway/resource/";
//test config
        AgentGatewayURL = "http://10.77.194.82:8000/agentgateway/resource/";//"http://172.21.31.175:8000/agentgateway/resource/";
        CMSGatewayURL = "http://10.77.194.86:9068/cmsgateway/resource/";//http://172.21.31.177:9068/cmsgateway/resource/";
        VDNID = "1";
        CCID = "1";

        //live configs CDNS
//        AgentGatewayURL = "http://172.16.0.25:8000/agentgateway/resource/";
//        CMSGatewayURL = "http://172.16.0.29:9068/cmsgateway/resource/";
//        VDNID = "2";
//        CCID = "1";
        
        
 //        try {
                //            stream = new FileInputStream(input + "props//Config.properties");
                //            Properties prop = new Properties();
                //            prop.load(stream);
                //            AgentGatewayURL = prop.getProperty("AgentGatewayURL");
                //            CMSGatewayURL = prop.getProperty("CMSGatewayURL");
                //            VDNID = prop.getProperty("VDNID");
                //            CCID = prop.getProperty("CCID");
                ////            System.out.println("AgentGatewayURL: \t" + AgentGatewayURL);
                ////            System.out.println("CMSGatewayURL: \t" + CMSGatewayURL);
                ////            System.out.println("VDNID: \t" + VDNID + "\t CCID:" + CCID);
                ////            stream.close();
                //        } catch (FileNotFoundException e) {
                //            logger.error("FileNotFound Config.properties " + e.getMessage());
                //            Logger.getLogger(AgentGatewayServiceConsumer.class.getName()).log(Level.SEVERE, null, e);
                //        } catch (IOException e) {
                //            logger.error("IOEXCeption Config.properties" + e.getMessage());
                //            Logger.getLogger(AgentGatewayServiceConsumer.class.getName()).log(Level.SEVERE, null, e);
                //        } finally {
                //            if (null != stream) {
                //                try {
                //                    stream.close();
                //                    logger.info("Config.properties closed \n");
                //                } catch (Exception e) {
                //                    logger.error("Config.properties not closing in finally block \n" + e.getMessage());
                ////                    System.out.println("Config.properties not closing in finally block \n" + e.getMessage());
                //                    e.printStackTrace();
                //                }
                //            }
                //        }
        Unirest.setObjectMapper(new ObjectMapper() {
            private com.fasterxml.jackson.databind.ObjectMapper jacksonObjectMapper
                    = new com.fasterxml.jackson.databind.ObjectMapper();

            @Override
            public <T> T readValue(String value, Class<T> valueType) {
                try {
                    return jacksonObjectMapper.readValue(value, valueType);
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }

            public String writeValue(Object value) {
                try {
                    return jacksonObjectMapper.writeValueAsString(value);
                } catch (JsonProcessingException e) {
                    throw new RuntimeException(e);
                }
            }
        }
        );
    }

    public void loginRequest(LoginParams params, String workid) {
        HttpResponse<LoginResponse> response = null;
        String guid = "";

        try {
            response = Unirest.put(AgentGatewayURL + "/onlineagent/" + workid).header("Content-Type", "application/json").body(params).asObject(LoginResponse.class
            );
            int status = response.getStatus();
            Headers header = response.getHeaders();
            if (header.containsKey("Set-GUID")) {
                guid = header.get("Set-GUID").toString();
                guid = guid.replace("JSESSIONID=", "");
                guid = guid.replace("[", "");
                guid = guid.replace("]", "");
            }
            System.out.println("Status code: " + status);
            LoginResponse result = response.getBody();
            if (result.getRetcode().equalsIgnoreCase("0")) {
                agent.setGuid(guid);
                agent.setCcID(1);
                agent.setPhoneNum(params.getPhonenum());
                agent.setVdnID(1);
                agent.setWorkID(workid);
                CMSMap.Clear();
                CMSMap.getInstance();
            } else {
                forceLogin(params, workid);
            }

        } catch (Exception ex) {
            logger.error("Login Request Error: " + ex.getMessage());
        }

    }

    public LoginResponse forceLogin(LoginParams params, String workid) {
        LoginResponse result = new LoginResponse();
        boolean agentFlag = checkAgentValidation(workid);
        System.out.println("agentFlag in Login" + agentFlag);
        boolean phoneFlag = checkPhoneNumber(params.getPhonenum());
        System.out.println("phoneFlag in Login" + phoneFlag);
        if (agentFlag == true && phoneFlag == false) {
            try {
                HttpResponse<LoginResponse> response = null;
                String guid = "";
                response
                        = Unirest.put(AgentGatewayURL + "/onlineagent/" + workid + "/forcelogin")
                                .header("Content-Type", "application/json")
                                .body(params).asObject(LoginResponse.class);
                int status = response.getStatus();
                Headers header = response.getHeaders();
                if (header.containsKey("Set-GUID")) {
                    //guid ko save karana after login header main value hoti hai

                    guid = header.get("Set-GUID").toString();
                    guid = guid.replace("JSESSIONID=", "");
                    guid = guid.replace("[", "");
                    guid = guid.replace("]", "");
                }
                System.out.println("Status code: " + status);
                result = response.getBody();
                if (result.getRetcode().equalsIgnoreCase("0")) {
                    //agent ka single instance banta hai us main agent info hai
                    agent.setGuid(guid);
                    agent.setCcID(Integer.parseInt(CCID));
                    agent.setPhoneNum(params.getPhonenum());
                    agent.setVdnID(Integer.parseInt(VDNID));
                    agent.setWorkID(workid);
                    //Ye cms ki value hain jo body main cms ko bhejte hain.yea kon si body
                    // ab samja han smja..gaia yea login against jo agent ki info ati ha yea wo ha
                    CMSMap.Clear();
                    CMSMap.getInstance();
                } else {
                    result.setRetcode("100-000");
                    result.setMessage("Invalid workid or password");
                }

            } catch (UnirestException ex) {
                logger.error("Login Request Error: " + ex.getMessage());
            }
        } else {
            if (agentFlag) {
                result.setMessage("Invalid User");
            }
            if (phoneFlag) {
                result.setMessage("Phone No already in use");
            }
            result.setRetcode("100-000");

        }

        return result;
    }

    public boolean checkAgentValidation(String workID) {
        HttpResponse<AgenInfoResponse> response = null;

        try {
            response = Unirest.get(CMSGatewayURL + "synwas/agents/" + CCID + "/" + VDNID).header("Content-Type", "application/json").asObject(AgenInfoResponse.class);
            int status = response.getStatus();
            AgenInfoResponse result = response.getBody();
            System.out.println("Agent Validation: \t Retcode:" + result.getRetcode() + "\t Message: \t" + result.getMessage());
            if (result.getRetcode().equalsIgnoreCase("0")) {
                for (AgenInfoResponse.Result obj : result.getResult()) {
                    if (obj.getAgentNo().equalsIgnoreCase(workID)) {
                        return true;
                    }
                }
            } else {

            }
        } catch (Exception ex) {
            logger.error("Login Agent Get Skill Request Error: " + ex.getMessage());
        }
        return false;
    }

    public void ctiSync(String workId, String pwd) {
        CTIParams param = new CTIParams();
        param.setCcId(CCID);
        param.setIsSSL(false);
        param.setPassword(pwd);
        param.setWasIp("10.77.194.83");
        param.setWasPath("webconfig");
        param.setWasPort("8443");
        param.setWorkNo(workId);
        HttpResponse<DefaultResponse> response = null;

        try {
            response = Unirest.post(CMSGatewayURL + "synwas/syncti").header("Content-Type", "application/json").body(param).asObject(DefaultResponse.class
            );
            int status = response.getStatus();
            DefaultResponse result = response.getBody();
            if (result.getRetcode().equalsIgnoreCase("0")) {

            } else {

            }

        } catch (Exception ex) {
            logger.error("CTI Sync Request Error: " + ex.getMessage());
        }
    }

    public void vdnSync(String workId, String pwd) {
        VDNParams param = new VDNParams();
        param.setCcId(CCID);
        param.setIsSSL(false);
        param.setPassword(pwd);
        param.setWasIp("10.77.194.83");
        param.setWasPath("webconfig");
        param.setWasPort("8443");
        param.setWorkNo(workId);
        param.setVdnId(VDNID);
        HttpResponse<DefaultResponse> response = null;

        try {
            response = Unirest.post(CMSGatewayURL + "synwas/synvdn").header("Content-Type", "application/json").body(param).asObject(DefaultResponse.class
            );
            int status = response.getStatus();
            DefaultResponse result = response.getBody();
            if (result.getRetcode().equalsIgnoreCase("0")) {

            } else {

            }

        } catch (Exception ex) {
            logger.error("VDN Sync Request Error: " + ex.getMessage());
        }
    }

    public void getLoginAgentSkills() {
        HttpResponse<LoginAgentSkills> response = null;

        try {
            response = Unirest.get(AgentGatewayURL + "onlineagent/" + agent.getWorkID() + "/agentskills").header("Content-Type", "application/json").header("guid", agent.getGuid()).asObject(LoginAgentSkills.class
            );
            int status = response.getStatus();
            LoginAgentSkills result = response.getBody();
            if (result.getRetcode().equalsIgnoreCase("0")) {
                List<AgentInfo.SkillSet> listSkills = new ArrayList<AgentInfo.SkillSet>();
                for (LoginAgentSkills.Result obj : result.getResult()) {
                    AgentInfo.SkillSet skill = new AgentInfo.SkillSet();
                    skill.setId(obj.getId());
                    skill.setName(obj.getName());
                    listSkills.add(skill);
                }
                agent.setSkills(listSkills);
            } else {

            }

        } catch (Exception ex) {
            logger.error("Login Agent Get Skill Request Error: " + ex.getMessage());
        }
    }

    public AllAgentSkills getAllAgentStatusInfo() {
        AllAgentSkills result = new AllAgentSkills();
        HttpResponse<AllAgentSkills> response = null;

        try {
            response = Unirest.post(CMSGatewayURL + "real/agent/totalagentabstractex3").header("Content-Type", "application/json").body("{\"ccId\":" + Integer.parseInt(CCID) + ",\"vdnId\":" + Integer.parseInt(VDNID) + "}").asObject(AllAgentSkills.class
            );
            int status = response.getStatus();
            result = response.getBody();
            if (result.getRetcode().equalsIgnoreCase("0")) {
                //0 means success
                return result;
            } else {
            }

        } catch (Exception ex) {
            logger.error("VDN Sync Request Error: " + ex.getMessage());
        }
        return result;
    }

    public CMSAgentHistoryResponse getAgentStatistics(String agentId) {
        CMSAgentHistoryResponse result = new CMSAgentHistoryResponse();
        HttpResponse<CMSAgentHistoryResponse> response = null;
        CMSAgentHistoryParams params = new CMSAgentHistoryParams();
        params.setCcId(CCID);
        params.setVdnId(VDNID);
        List<Integer> objList = new ArrayList<Integer>();
        objList.add(Integer.parseInt(agentId));
        params.setObjectIds(objList);
        List<String> indexIds = new ArrayList<String>();
        Map<String, String> agentMap = CMSMap.agentMap;
        if (agentMap.size() > 0) {
//            List<String> keys = new ArrayList<String>();
//            for (String key : agentMap.keySet()) {
//                keys.add(key);
//            }

            indexIds.add(agentMap.get("Agent Sign In Times"));
            indexIds.add(agentMap.get("Agent No Of Incoming Calls"));
            indexIds.add(agentMap.get("Agent Duration Of Incoming Calls"));
            indexIds.add(agentMap.get("Agent Average Duration Of Incoming Calls"));
            indexIds.add(agentMap.get("Agent No Of Outgoing Calls"));
            indexIds.add(agentMap.get("Agent Duration Of Outgoing Calls"));
            indexIds.add(agentMap.get("Agent Average Duration Of Outgoing Calls"));
            indexIds.add(agentMap.get("Agent Rest Times"));
            indexIds.add(agentMap.get("Agent Rest Duration"));
            indexIds.add(agentMap.get("Agent Rest Average Duration"));
            indexIds.add(agentMap.get("Agent HoldOn Times"));
            indexIds.add(agentMap.get("Agent HoldOn Duration"));
            indexIds.add(agentMap.get("Agent HoldOn Average Duration"));
            indexIds.add(agentMap.get("Agent Busy Times"));
            indexIds.add(agentMap.get("Agent Busy Duration"));
            indexIds.add(agentMap.get("Agent Busy Average Duration"));
            indexIds.add(agentMap.get("Agent Idle Duration"));
            //indexIds.Add(agentMap.FirstOrDefault(x => x.Value == "Agent Internal Transfer In Time"));
            //indexIds.Add(agentMap.FirstOrDefault(x => x.Value == "Agent Internal Transfer Out Time"));
            //indexIds.Add(agentMap.FirstOrDefault(x => x.Value == "Agent No Of Internal Calls"));
            indexIds.add(agentMap.get("Agent WrapUp Times"));
            indexIds.add(agentMap.get("Agent WrapUp Duration"));
            indexIds.add(agentMap.get("Agent WrapUp Average Duration"));
        }
        params.setIndexIds(indexIds);

        try {
            response = Unirest.post(CMSGatewayURL + "hindex/agent").header("Content-Type", "application/json").body(params).asObject(CMSAgentHistoryResponse.class
            );
            int status = response.getStatus();
            result = response.getBody();
            if (result.getRetcode().equalsIgnoreCase("0")) {
            } else {

            }

        } catch (Exception ex) {
            result.setRetcode("100-001");
            logger.error("VDN Sync Request Error: " + ex.getMessage());
        }
        return result;
    }

    public AgentDurationResponse getAgentDuration() {
        HttpResponse<AgentDurationResponse> response = null;
        CMSAgentHistoryParams params = new CMSAgentHistoryParams();
        AgentDurationResponse result = new AgentDurationResponse();
        params.setCcId(CCID);
        params.setVdnId(VDNID);
        List<Integer> objList = new ArrayList<Integer>();
        objList.add(Integer.parseInt(agent.getWorkID()));
        params.setObjectIds(objList);
        List<String> indexIds = new ArrayList<String>();
        Map<String, String> agentMap = CMSMap.agentMap;
        if (agentMap.size() > 0) {

            indexIds.add(agentMap.get("Agent SignIn SignOut Stats"));

        }
        params.setIndexIds(indexIds);

        try {
            response = Unirest.post(CMSGatewayURL + "hindex/agent").header("Content-Type", "application/json").body(params).asObject(AgentDurationResponse.class
            );
            int status = response.getStatus();
            result = response.getBody();
            if (result.getRetcode().equalsIgnoreCase("0")) {

            } else {

            }

        } catch (Exception ex) {
            logger.error("VDN Sync Request Error: " + ex.getMessage());
        }
        return result;
    }

    public SpecificAgentDetailResponse getSpecificAgentDetail(String agentId) {
        HttpResponse<SpecificAgentDetailResponse> response = null;
        SpecificAgentDetailResponse result = new SpecificAgentDetailResponse();

        try {
            response = Unirest.get(AgentGatewayURL + "agentgroup/{agentid}/agentbyworkno/{workno}").header("Content-Type", "application/json").header("guid", agent.getGuid()).routeParam("agentid", agent.getWorkID()).routeParam("workno", agentId).asObject(SpecificAgentDetailResponse.class
            );
            int status = response.getStatus();
            result = response.getBody();
            if (result.getRetcode().equalsIgnoreCase("0")) {

            } else {

            }

        } catch (Exception ex) {
            logger.error("Login Agent Get Skill Request Error: " + ex.getMessage());
        }
        return result;
    }

    public DefaultResponse logout() {
        DefaultResponse result = new DefaultResponse();
        if (agent != null && agent.getGuid() != null) {
            HttpResponse<DefaultResponse> response = null;

            try {
                response = Unirest.delete(AgentGatewayURL + "onlineagent/" + agent.getWorkID() + "/forcelogout").header("Content-Type", "application/json").header("guid", agent.getGuid()).asObject(DefaultResponse.class
                );
                int status = response.getStatus();
                result = response.getBody();

                if (result.getRetcode().equalsIgnoreCase("0")) {
                    result.setMessage("Logout Successfully");
                } else {
                    result.setMessage("Logout Failed");
                }

            } catch (Exception ex) {
                logger.error("Agent Logout Error: " + ex.getMessage());
                result.setMessage("Logout Failed \n " + ex.getMessage());
            }
        } else {
            result.setMessage("Logout Failed");
        }

        return result;
    }

    public DefaultResponse setAgentIdle(String agentId) {
        DefaultResponse result = new DefaultResponse();
        HttpResponse<DefaultResponse> response = null;

        try {
            response = Unirest.post(AgentGatewayURL + "qualitycontrol/" + agent.getWorkID() + "/forceidle/" + agentId).header("Content-Type", "application/json").header("guid", agent.getGuid()).asObject(DefaultResponse.class
            );
            int status = response.getStatus();
            result = response.getBody();
            if (result.getRetcode().equalsIgnoreCase("0")) {

            } else {

            }

        } catch (Exception ex) {
            result.setRetcode("100-001");
            logger.error("Login Agent Get Skill Request Error: " + ex.getMessage());
        }
        return result;
    }

    public DefaultResponse setAgentBusy(String agentId) {
        DefaultResponse result = new DefaultResponse();
        HttpResponse<DefaultResponse> response = null;

        try {
            response = Unirest.post(AgentGatewayURL + "qualitycontrol/" + agent.getWorkID() + "/forcebusy/" + agentId).header("Content-Type", "application/json").header("guid", agent.getGuid()).asObject(DefaultResponse.class
            );
            int status = response.getStatus();
            result = response.getBody();
            if (result.getRetcode().equalsIgnoreCase("0")) {

            } else {

            }

        } catch (Exception ex) {
            result.setRetcode("100-001");
            logger.error("Login Agent Get Skill Request Error: " + ex.getMessage());
        }
        return result;
    }

    public DefaultResponse setAgentRest(String agentId, String time, String ReasonId) {
        DefaultResponse result = new DefaultResponse();
        HttpResponse<DefaultResponse> response = null;

        try {
            response = Unirest.post(AgentGatewayURL + "qualitycontrol/" + agent.getWorkID() + "/forcerest/" + agentId + "/" + time + "/" + ReasonId).header("Content-Type", "application/json").header("guid", agent.getGuid()).asObject(DefaultResponse.class
            );
            int status = response.getStatus();
            result = response.getBody();
            if (result.getRetcode().equalsIgnoreCase("0")) {

            } else {

            }

        } catch (Exception ex) {
            result.setRetcode("100-001");
            logger.error("Login Agent Get Skill Request Error: " + ex.getMessage());
        }
        return result;
    }

    public AgentBreaks AgentGetBreak() {
        AgentBreaks result = new AgentBreaks();
        HttpResponse<AgentBreaks> response = null;

        try {
            response = Unirest.get(AgentGatewayURL + "agentgroup/" + agent.getWorkID() + "/restreason").header("Content-Type", "application/json").header("guid", agent.getGuid()).asObject(AgentBreaks.class
            );
            int status = response.getStatus();
            result = response.getBody();
            if (result.getRetcode().equalsIgnoreCase("0")) {

            } else {

            }

        } catch (Exception ex) {
            result.setRetcode("100-001");
            logger.error("Login Agent Get Skill Request Error: " + ex.getMessage());
        }
        return result;
    }

    public DefaultResponse setAgentForceLogout(String agentId) {
        DefaultResponse result = new DefaultResponse();
        HttpResponse<DefaultResponse> response = null;

        try {
            response = Unirest.post(AgentGatewayURL + "qualitycontrol/" + agent.getWorkID() + "/forcelogout/" + agentId + "/").header("Content-Type", "application/json").header("guid", agent.getGuid()).asObject(DefaultResponse.class
            );
            int status = response.getStatus();
            result = response.getBody();
            if (result.getRetcode().equalsIgnoreCase("0")) {

            } else {

            }

        } catch (Exception ex) {
            result.setRetcode("100-001");
            logger.error("Login Agent Get Skill Request Error: " + ex.getMessage());
        }
        return result;
    }

    public DefaultResponse setAgentInsert(String agentId) {
        DefaultResponse result = new DefaultResponse();
        HttpResponse<DefaultResponse> response = null;

        try {
            response = Unirest.put(AgentGatewayURL + "qualitycontrol/" + agent.getWorkID() + "/addinsert/{workNo}").header("Content-Type", "application/json").header("guid", agent.getGuid()).routeParam("workNo", agentId).asObject(DefaultResponse.class
            );
            int status = response.getStatus();
            result = response.getBody();
            if (result.getRetcode().equalsIgnoreCase("0")) {

            } else {

            }

        } catch (Exception ex) {
            result.setRetcode("100-001");
            logger.error("Login Agent Get Skill Request Error: " + ex.getMessage());
        }
        return result;
    }

    public DefaultResponse setAgentListeningOn(String agentId) {
        AgentInfo agent;
        agent = AgentInfo.getInstance();
        String AgentID = agent.getWorkID();
        String phonenum = agent.getPhoneNum();
        boolean phoneFlag = checkPhoneNumber(phonenum);
//        System.out.println("affisur" + phoneFlag);
        DefaultResponse result = new DefaultResponse();
        HttpResponse<DefaultResponse> response = null;

        try {
            response = Unirest.put(AgentGatewayURL + "qualitycontrol/" + agent.getWorkID() + "/addsupervise/{workNo}").header("Content-Type", "application/json").header("guid", agent.getGuid()).routeParam("workNo", agentId).asObject(DefaultResponse.class
            );
            int status = response.getStatus();
            result = response.getBody();
            if (result.getRetcode().equalsIgnoreCase("0")) {

            } else {

            }

        } catch (Exception ex) {
            result.setRetcode("100-001");
            logger.error("Login Agent Get Skill Request Error: " + ex.getMessage());
        }
        return result;
    }

    public DefaultResponse setAgentStopInsertListeningOn(String agentId) {
        DefaultResponse result = new DefaultResponse();
        HttpResponse<DefaultResponse> response = null;

        try {
            response = Unirest.delete(AgentGatewayURL + "qualitycontrol/" + agent.getWorkID() + "/{workNo}").header("Content-Type", "application/json").header("guid", agent.getGuid()).routeParam("workNo", agentId).asObject(DefaultResponse.class
            );
            int status = response.getStatus();
            result = response.getBody();
            if (result.getRetcode().equalsIgnoreCase("0")) {

            } else {

            }

        } catch (Exception ex) {
            result.setRetcode("100-001");
            logger.error("Login Agent Get Skill Request Error: " + ex.getMessage());
        }
        return result;
    }

    public DefaultResponse adjustAgentSkill(AgentSkillAdjustData data) {
        DefaultResponse result = new DefaultResponse();
        HttpResponse<DefaultResponse> response = null;

        try {
            response = Unirest.post(AgentGatewayURL + "qualitycontrol/" + agent.getWorkID() + "/adjustskill").header("Content-Type", "application/json").header("guid", agent.getGuid()).body(data).asObject(DefaultResponse.class
            );
            int status = response.getStatus();
            result = response.getBody();
            if (result.getRetcode().equalsIgnoreCase("0")) {

            } else {

            }

        } catch (Exception ex) {
            result.setRetcode("100-001");
            logger.error("Login Agent Get Skill Request Error: " + ex.getMessage());
        }
        return result;
    }

    public AgentPoolResponse agentPoolRequest() {
        AgentPoolResponse result = new AgentPoolResponse();
        HttpResponse<AgentPoolResponse> response = null;

        try {
            response = Unirest.get(AgentGatewayURL + "agentevent/" + agent.getWorkID()).header("Content-Type", "application/json").header("guid", agent.getGuid()).asObject(AgentPoolResponse.class
            );
            int status = response.getStatus();
            result = response.getBody();

        } catch (Exception ex) {
            logger.error("Login Agent Get Skill Request Error: " + ex.getMessage());
        }
        return result;
    }

    public boolean checkPhoneNumber(String phonNum) {
        try {
            HttpResponse<AgentPhoneListResponse> response = Unirest.post(CMSGatewayURL + "real/agent/allagentphones")
                    .header("content-type", "application/json")
                    .body("{\"ccId\":" + Integer.parseInt(CCID) + ",\"vdnId\":" + Integer.parseInt(VDNID) + "}")
                    .asObject(AgentPhoneListResponse.class
                    );
            AgentPhoneListResponse result = response.getBody();

            for (AgentPhoneListResponse.Result obj : result.getResult()) {
                if (obj.getPhone().equalsIgnoreCase(phonNum)) {
                    return true;
                }
            }
        } catch (Exception ex) {

        }
        return false;
    }

    public VDNSkillsResponse getAllVDNSkills() {
        VDNSkillsResponse result = new VDNSkillsResponse();
        HttpResponse<VDNSkillsResponse> response = null;

        try {
            response = Unirest.get(CMSGatewayURL + "synwas/skills/" + CCID + "/" + VDNID).header("Content-Type", "application/json").asObject(VDNSkillsResponse.class
            );
            int status = response.getStatus();
            result = response.getBody();
            if (result.getRetcode().equalsIgnoreCase("0")) {
                result.setMessage(agent.getWorkID() + "|" + agent.getPhoneNum());
            } else {

            }

        } catch (Exception ex) {
            result.setRetcode("100-001");
            logger.error("Login Agent Get Skill Request Error: " + ex.getMessage());
        }
        return result;
    }

    public AgentSkillsByWorkno agentskillsbyworkno(String agentId) {
        AgentSkillsByWorkno AgentSkillsresult = new AgentSkillsByWorkno();
        HttpResponse<AgentSkillsByWorkno> response = null;

        try {
            response = Unirest.get(AgentGatewayURL + "onlineagent/" + agent.getWorkID() + "/agentskillsbyworkno/" + Integer.parseInt(agentId)).header("Content-Type", "application/json").header("guid", agent.getGuid()).asObject(AgentSkillsByWorkno.class
            );
            int status = response.getStatus();
            AgentSkillsresult = response.getBody();
//            if (AgentSkillsresult.getRetcode().equalsIgnoreCase("0")) {
//            } else {
//            }
            if (AgentSkillsresult.getRetcode().equalsIgnoreCase("0")) {
//                List<AgentInfo.SkillSet> listSkills = new ArrayList<AgentInfo.SkillSet>();
//                for (AgentSkillsByWorkno.Result obj : AgentSkillsresult.getResult()) {
//                    AgentInfo.SkillSet skill = new AgentInfo.SkillSet();
//                    skill.setId(obj.getId());
//                    skill.setName(obj.getName());
//                    listSkills.add(skill);
//                }
//                agent.setSkills(listSkills);
            } else {

            }

        } catch (Exception ex) {
            AgentSkillsresult.setRetcode("100-001");
            logger.error("AgentSkillsByWorkno Request Error: " + ex.getMessage());
        }
        return AgentSkillsresult;
    }

    public CheckAgentStatus CheckAgentStatus() {
        CheckAgentStatus result = new CheckAgentStatus();
        HttpResponse<CheckAgentStatus> response = null;
        AgentInfo agent;
        agent = AgentInfo.getInstance();
        String AgentID = agent.getWorkID();
        String phonenum = agent.getPhoneNum();
        boolean phoneFlag = checkPhoneNumber(phonenum);
//        System.out.println("affisur" + phoneFlag);

        try {
            response = Unirest.get(AgentGatewayURL + "agentevent/" + agent.getWorkID()).header("Content-Type", "application/json").header("guid", agent.getGuid()).asObject(CheckAgentStatus.class
            );
            int status = response.getStatus();
            result = response.getBody();
            if (result.getRetcode().equalsIgnoreCase("0")) {
            } else {
            }

        } catch (Exception ex) {
            result.setRetcode("100-001");
            logger.error("CheckAgentStatus Error: " + ex.getMessage());
        }
        return result;
    }
}
