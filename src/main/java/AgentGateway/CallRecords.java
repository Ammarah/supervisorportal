/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AgentGateway;

import Model.AgentInfo;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.JSONObject;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

/**
 *
 * @author Iceman
 */
public class CallRecords {

    JdbcTemplate template;

    public void setTemplate(JdbcTemplate temp) {
        this.template = temp;
    }

    public String JdbcDriver = "";
    public String JdbcUrl = "";
    public String JdbcUserName = "";
    public String JdbcPassword = "";
    public AgentInfo agent;
    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(AgentGatewayServiceConsumer.class);

    public CallRecords() {
        InputStream stream = null;
        agent = AgentInfo.getInstance();
        String input = getClass().getProtectionDomain().getCodeSource().getLocation().getPath();
        try {
            stream = new FileInputStream(input + "props.jdbc//jdbc.properties");
            Properties prop = new Properties();
            prop.load(stream);
            JdbcDriver = prop.getProperty("jdbc.driver");
            JdbcUrl = prop.getProperty("jdbc.url");
            JdbcUserName = prop.getProperty("jdbc.username");
            JdbcPassword = prop.getProperty("jdbc.password");
//            stream.close();
//        } catch (Exception ex) {
//            Logger.getLogger(AgentGatewayServiceConsumer.class.getName()).log(Level.SEVERE, null, ex);
//        }
        } catch (FileNotFoundException e) {
            logger.error("FileNotFound jdbc.properties " + e.getMessage());
            Logger.getLogger(CallRecords.class.getName()).log(Level.SEVERE, null, e);
        } catch (IOException e) {
            logger.error("IOEXCeption jdbc.properties" + e.getMessage());
            Logger.getLogger(CallRecords.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            if (null != stream) {
                try {
                    stream.close();
                    logger.info("jdbc.properties is closed \n");
                } catch (Exception e) {
                    System.out.println("jdbc.properties not closing in finally block \n" + e.getMessage());
                    e.printStackTrace();
                }
            }
        }

    }

    public String getPassportStatus(String no) {
        try {
            String query = "select PAS_STATUS from PASSPORT_DTL where PASSPOT_NO='" + no + "'";
            return template.query(query, new ResultSetExtractor<String>() {
                @Override
                public String extractData(ResultSet rs) throws SQLException, DataAccessException {
                    JSONObject jsonObj = new JSONObject();
                    if (rs.next()) {
                        System.out.println("Status" + rs.getString("PAS_STATUS"));
                        return rs.getString("PAS_STATUS").toString();
                    }
                    jsonObj.put("status", "Fail");
                    return "inactive";
                }

            });
        } catch (Exception ex) {
            return "Error" + ex.toString();
        }
    }

}
