<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<script src="${pageContext.request.contextPath}/resources/js/jquery-2.2.3.min.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/resources/js/plotly-latest.min.js" type="text/javascript"></script>
<style>
            .chartFont *
            {
                -webkit-font-smoothing: antialiased !important;
                -moz-osx-font-smoothing: grayscale !important;
                font-family: 'Source Sans Pro','Helvetica Neue',Helvetica,Arial,sans-serif !important;
                font-weight: 400 !important;

            }
            .lblOption {
                background-color: white;
                padding: 5px;
                font: 14px;
                width: 40%;
            }
            .modebar {
                display: none !important;
            }
        </style>
         <section class="content">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">Skill Information</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" id="btnRefereshTable">
                            <i class="fa fa-refresh"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="collapse">
                            <i class="fa fa-minus"></i>
                        </button>

                    </div>
                </div>

                <div class="box-body">
                    <div class="form-group">
                        <label>Select Skill:</label>
                        <select class="lblOption" id="AgentOpt">
                            <c:forEach var="users" items="${SkillList.result}">
                                <option value="${users.skillId}">${users.skillName}</option>
                            </c:forEach>
                        </select>
                    </div>
                    <div id="chartPieBox" class="text-center">
                        <div id="pieChart" class="chartFont"></div>
                        <div class="horizontalList col-md-12 col-sm-12 col-xs-12">
                            <ul id="horizontal-list"></ul>
                        </div>
                    </div>

                </div>
                <div class="box-body">
                    <table class="table table-bordered" id="realTimeCallInfo">
                        <thead id="realTimeCallInfoHead">

                            <tr>
                                <th>Label</th>
                                <th>Value</th>

                            </tr>
                        </thead>
                        <tbody id="realTimeCallInfoBody">
                            <tr>
                                <td>No Data Found</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </section>
    <content tag="local_script">
        <script>
            $(window).resize(function () {
                loadPieChart();

            });
            $(document).load(function () {
                // alert("Hello");
            });
            function loadCallInfo() {
                var selectedSkill = $("#AgentOpt option:selected").val();
                $.ajax({
                    type: "GET",
                    url: '${pageContext.request.servletContext.contextPath}/skill/skillOfagentinfo',
                    data: {skillID: selectedSkill},
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        $('.cssload-loader').hide();
                        $('.overlay').hide();
                        var list = response.result[0].idxs;
                        $('#realTimeCallInfoBody').empty();
                        var trHTML = '';
                        for (var i = 0; i < list.length; i++) {
                            trHTML += '<tr><td>' + list[i].id + '</td><td>' + list[i].val + '</td></tr>';
                        }
                        $('#realTimeCallInfoBody').append(trHTML);
                    },
                    error: function (xhr, status, error) {
                        //var err = eval("(" + xhr.responseText + ")");
                        console.log(status + "error \t" + error);
                    }
                });
            }
            function loadPieChart() {
                var selectedSkill = $("#AgentOpt option:selected").val();
                $.ajax({
                    type: "GET",
                    url: '../Request/AgentInfoSkillWise',
                    data: {skillID: selectedSkill},
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        $('.cssload-loader').hide();
                        $('.overlay').hide();
                        var list = response[0].idxs;
                        var dataSkillList = [];
                        var listH = "";
                        var flag = true;
                        for (i = 0; i < list.length; i++) {
                            if (list[i].val != "0") {
                                flag = false;
                            }
                            listH += "<li>" + list[i].id + "</li>"
                            var str = list[i].id.replace('Agents', '');
                            dataSkillList.push({

                                label: str,
                                value: parseInt(list[i].val)

                            });
                        }
                        $('#horizontal-list').empty();
                        $('#horizontal-list').append(listH);
                        if (flag) {
                            Notifier.warning('All values are zero');
                            $('#pieChart').empty();
                        } else
                        {
                            loadData(dataSkillList);
                        }

                    },
                    error: function (xhr, status, error) {
                        //var err = eval("(" + xhr.responseText + ")");
                        console.log(status);
                    }
                });
            }
            function loadData(dataSet) {

                $('#pieChart').empty();

                var fo = 0;
                var canvasWidth = 0;
                var titleFont = 0;
                if ($('#pieChart').width() < 380) {
                    if ($(window).width() < 325) {
                        fo = $(window).width() * 0.03;
                        titleFont = $(window).width() * 0.04;
                    } else {
                        fo = $(window).width() * 0.02;
                        titleFont = $(window).width() * 0.03;
                    }


                    var val = $(window).width() * 0.9;
                    canvasWidth = val;
                    //alert('a' + $('#pieChart').width());
                    //console.log(canvasWidth + ' ' + $(window).width());
                } else if ($('#pieChart').width() > 380 && $('#pieChart').width() < 780) {
                    titleFont = 16;
                    canvasWidth = $('#pieChart').width() / 1.2;
                    fo = 12;
                    //alert($('#pieChart').width());
                    //console.log(canvasWidth + ' s ' + $(window).width());
                } else {
                    titleFont = 20;
                    canvasWidth = $('#pieChart').width() / 1.2;
                    fo = 14;
                    // alert($('#pieChart').width());
                    //console.log(canvasWidth + ' s ' + $(window).width());
                }

                // pie.selectAll("svg > *").remove();
                var pie = new d3pie("pieChart", {
                    "header": {
                        "title": {
                            "text": "Agent Information",
                            "fontSize": titleFont,
                            "font": "open sans"
                        },
                        "subtitle": {
                            "color": "#999999",
                            "fontSize": 12,
                            "font": "open sans"
                        },
                        "titleSubtitlePadding": 9
                    },
                    "footer": {
                        "color": "#999999",
                        "fontSize": 10,
                        "font": "open sans",
                        "location": "bottom-left"
                    },
                    "size": {
                        "canvasWidth": canvasWidth,
                        "pieOuterRadius": "65%"
                    },
                    "data": {
                        "sortOrder": "value-desc",
                        "content": dataSet
                    },
                    "labels": {
                        "outer": {
                            "pieDistance": 10
                        },
                        "inner": {
                            "format": "value"
                        },
                        "mainLabel": {
                            "fontSize": fo
                        },
                        "percentage": {
                            "color": "#ffffff",
                            "decimalPlaces": 0
                        },
                        "value": {
                            "color": "#adadad",
                            "fontSize": fo
                        },
                        "lines": {
                            "enabled": true,
                            "style": "straight"
                        },
                        "truncation": {
                            "enabled": true
                        }
                    },
                    "tooltips": {
                        "enabled": true,
                        "type": "placeholder",
                        "string": "{label}: {value}"
                    },
                    "effects": {
                        "pullOutSegmentOnClick": {
                            "effect": "linear",
                            "speed": 400,
                            "size": 8
                        }
                    },
                    "misc": {
                        "gradient": {
                            "enabled": true,
                            "percentage": 100
                        },
                        "canvasPadding": {
                            "top": 3,
                            "right": 3,
                            "bottom": 3,
                            "left": 3
                        }
                    }
                });
            }
            $(document).ready(function () {

                var pie = null;
                loadPieChart();
                loadCallInfo();
                $('#btnRefereshTable').click(function () {
                    $('.cssload-loader').show();
                    $('.overlay').show();
                    loadPieChart();
                    loadCallInfo();
                });
                $("select#AgentOpt").change(function () {
                    $('.cssload-loader').show();
                    $('.overlay').show();
                    loadPieChart();
                    loadCallInfo();
                });
            });
        </script>
    </content>
