<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<script src="${pageContext.request.contextPath}/resources/js/jquery-2.2.3.min.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/resources/js/plotly-latest.min.js" type="text/javascript"></script>
<style>
    .chartFont *
    {
        -webkit-font-smoothing: antialiased !important;
        -moz-osx-font-smoothing: grayscale !important;
        font-family: 'Source Sans Pro','Helvetica Neue',Helvetica,Arial,sans-serif !important;
        font-weight: 400 !important;

    }
    .modebar {
        display: none !important;
    }
</style>
<section class="content">
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Incoming Call Stats</h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" id="btnRefereshTable">
                    <i class="fa fa-refresh"></i>
                </button>
<!--                <button type="button" class="btn btn-box-tool" data-widget="collapse">
                    <i class="fa fa-minus"></i>
                </button>-->

            </div>
        </div>
        <div class="box-body">
            <div id="myDiv" class="chartFont" >
                <canvas id="myChart2" class="col-lg-12 col-md-12"></canvas>
            </div>
            <div class="box-body">
                <table class="table table-bordered" id="realTimeCallInfo">
                    <thead id="realTimeCallInfoHead">
                        <tr>
                            <th>Label</th>
                            <th>Value</th>
                        </tr>
                    </thead>
                    <tbody id="realTimeCallInfoBody">
                        <tr>
                            <td>No Data Found</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- /.box-body -->
    </div>
</section>
<script>
    function loadCallDropRate() {
        $.ajax({
            type: "GET",
            url: '${pageContext.request.servletContext.contextPath}/vdn/HistoricalVDNDropRate',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                $('.cssload-loader').hide();
                $('.overlay').hide();
                var list = response.result[0].idxs;
                $('#realTimeCallInfoBody').empty();
                var trHTML = '';
                for (i = 0; i < list.length; i++) {
                    trHTML += '<tr><td>' + list[i].id + '</td><td>' + list[i].val + '</td></tr>';
                }
                $('#realTimeCallInfoBody').append(trHTML);
            },
            error: function (xhr, status, error) {
                console.log("HistoricalVDNDropRate \t" + status + "\t error: \t" + error);
            }
        });
    }
    function loadCallStatBar(lblData, dataSet) {
        $('#myDiv').empty();
        var data = [{
                x: lblData,
                y: dataSet,
                type: 'bar', width: 0.3
            }];
        var layout = {
            title: 'VDN Incoming Call Stats',
            font: {
                family: 'Raleway, snas-serif'
            },
            showlegend: false,
            xaxis: {
                fixedrange: true,
                tickangle: 0
            },
            yaxis: {
                fixedrange: true,
                zeroline: false,
                gridwidth: 0.5
            },
            bargap: 0.05
        };
        Plotly.newPlot('myDiv', data, layout);
        $(".main-svg").css("width", $(window).width() * 0.80 + "px");
        //$('#myChart2').empty();
        //var ctx = document.getElementById("myChart2");
        //var myChart = new Chart(ctx, {
        //    type: 'bar',
        //    data: {
        //        labels: lblData,
        //        datasets: [{
        //            label: 'Call Stats',
        //            data: dataSet,
        //            backgroundColor: "#004c91"

        //        }]
        //    },
        //    options: {
        //        scales: {
        //            yAxes: [{
        //                ticks: {
        //                    beginAtZero: true
        //                }
        //            }]
        //        },
        //        onResize: function () { }
        //    }

        //});


    }

    $(document).ready(function () {
        $(window).resize(function () {
            //alert($(window).width() * 0.80);
            //$(".svg-container").width($(window).width() * 0.90);
            $(".main-svg").css("width", $(window).width() * 0.80 + "px");
            Plotly.Plots.resize(myDiv);
        });
        loadCallStatsChart();
        loadCallDropRate();
        //loadCallStatsChart();
        $('#btnRefereshTable').click(function () {
            $('.cssload-loader').show();
            $('.overlay').show();
            loadCallStatsChart();
            loadCallDropRate();
        });

    });
    function loadCallStatsChart() {
        $.ajax({
            type: "GET",
            url: '${pageContext.request.servletContext.contextPath}/vdn/HistoricalVDNIncomingCallStats',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                $('.cssload-loader').hide();
                $('.overlay').hide();
                var list = response.result[0].idxs;
                var labelData = [];
                var val = [];
                var flag = true;
                for (var i = 0; i < list.length; i++) {
                    if (list[i].val != "0") {
                        flag = false;
                    }
                    var l = list[i].id;

                    labelData.push(list[i].id);
                    val.push(list[i].val);
                    //dataSkillList.push({

                    //    label: list[i].id,
                    //    value: parseInt(list[i].val)

                    //});
                }
                if (flag) {
                    $('#myDiv').empty();
                    Notifier.warning('No Data Found');
                } else {
                    loadCallStatBar(labelData, val);
                }

            },
            error: function (xhr, status, error) {
                //var err = eval("(" + xhr.responseText + ")");
                console.log(status);
            }
        });
    }
</script>

