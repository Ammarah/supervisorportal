<%@page import="Model.AgentInfo"%>
<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page errorPage="../pages/examples/500.jsp"%>
<jsp:include page="../parts/cachecontrol.jsp"></jsp:include>
    <html>
        <head>
            <title><sitemesh:write property='title' /></title>
        <sitemesh:write property='head' />
        <link rel="icon" href="${pageContext.request.contextPath}/resources/img/MRP.gif" />
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="${pageContext.request.contextPath}/resources/img/MRP.gif" />
    <title>Supervisor Portal</title>
    <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet" />
    <!-- Font Awesome -->
    <link href="${pageContext.request.contextPath}/resources/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <!-- Ionicons -->
    <link href="${pageContext.request.contextPath}/resources/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <!-- jvectormap -->
    <link href="${pageContext.request.contextPath}/resources/css/jvectormap/jquery-jvectormap-1.2.2.css" rel="stylesheet" />
    <link href="${pageContext.request.contextPath}/resources/css/progressBar/nprogress.css" rel="stylesheet" />
    <!-- Theme style -->
    <link href="${pageContext.request.contextPath}/resources/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link href="${pageContext.request.contextPath}/resources/css/_all-skins.min.css" rel="stylesheet" />
    <link href="${pageContext.request.contextPath}/resources/css/progressBar/progress1.css" rel="stylesheet" type="text/css"/>
    <link href="${pageContext.request.contextPath}/resources/js/plugins/datatables/jquery.dataTables.css" rel="stylesheet" type="text/css"/>
    <link href="${pageContext.request.contextPath}/resources/js/plugins/datatables/extensions/FixedHeader/css/dataTables.fixedHeader.css" rel="stylesheet" type="text/css"/>
    <!-- jQuery 2.2.3 -->
    <style>
        .overlay {
            background-color: #022158;
            display: block;
            position: fixed;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            opacity: 0.5;
            z-index: 3;
        }
        .btn
        {
            padding-right:6px;
        }
    </style>
    <%
        AgentInfo agent;
        agent = AgentInfo.getInstance();
        String AgentID = agent.getWorkID();
    %>
</head>
<body class="hold-transition skin-blue sidebar-mini">
    <div class="overlay" style="display:none;"></div>
    <div class="cssload-loader" style="display:none;" >
        <div class="cssload-side"></div>
        <div class="cssload-side"></div>
        <div class="cssload-side"></div>
        <div class="cssload-side"></div>
        <div class="cssload-side"></div>
        <div class="cssload-side"></div>
        <div class="cssload-side"></div>
        <div class="cssload-side"></div>
    </div>
    <div class="wrapper">
        <!-- Main Header -->
        <header class="main-header">
            <!-- Logo -->
            <a href="${pageContext.request.contextPath}/agent/dashboard" class="logo">
                <!-- mini logo for sidebar mini 50x50 pixels -->
                <span class="logo-mini"><b>SP</b></span>
                <!-- logo for regular state and mobile devices -->
                <span class="logo-lg">Supervisor Portal</span>
            </a>
            <!-- Header Navbar -->
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button-->
                <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                </a>
                <!-- Navbar Right Menu -->
                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        <!-- Messages: style can be found in dropdown.less-->

                        <!-- /.messages-menu -->
                        <!-- Notifications Menu -->

                        <!-- Tasks Menu -->

                        <!-- User Account Menu -->
                        <li class="dropdown user user-menu">
                            <!-- Menu Toggle Button -->
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <!-- The user image in the navbar-->
                                <img src="${pageContext.request.contextPath}/resources/img/profileimage.png" class="user-image" alt="User Image">
                                <!-- hidden-xs hides the username on small devices so only the image appears. -->
                                <span class="hidden-xs"><%= AgentID%></span>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- The user image in the menu -->
                                <li class="user-header">
                                    <img src="${pageContext.request.contextPath}/resources/img/profileimage.png" class="img-circle" alt="User Image">
                                    <p>Supervisor <%= AgentID%></p>
                                </li>
                                <!-- Menu Body -->
                                <!-- Menu Footer-->
                                <li class="user-footer">

                                    <div class="pull-right">
                                        <a id="btnLogout" class="btn btn-default btn-flat">Sign out</a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <!-- Control Sidebar Toggle Button -->

                    </ul>
                </div>
            </nav>
        </header>
        <!-- Left side column. contains the logo and sidebar -->
        <aside class="main-sidebar">
            <!-- sidebar: style can be found in sidebar.less -->
            <section class="sidebar">
                <!-- Sidebar user panel (optional) -->
                <div class="user-panel">
                    <div class="pull-left image">
                        <img src="${pageContext.request.contextPath}/resources/img/profileimage.png" class="img-circle" alt="User Image">
                    </div>
                    <div class="pull-left info">
                        <p>Supervisor <%= AgentID%></p>
                        <!-- Status -->
                        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                    </div>
                </div>
                <!-- search form (Optional) -->

                <!-- /.search form -->
                <!-- Sidebar Menu -->
                <ul class="sidebar-menu">
                    <!-- Optionally, you can add icons to the links -->

                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-group"></i> <span>Agent Management</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">

                            <li><a href="${pageContext.request.contextPath}/agent/dashboard"><i class="fa fa-circle-o"></i>Dashboard v1</a></li>
                            <li><a href="${pageContext.request.contextPath}/agent/dashboard_v2"><i class="fa fa-circle-o"></i>Dashboard v2</a></li>

                        </ul>
                    </li>
                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-tasks"></i> <span>Skills Queue</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="${pageContext.request.contextPath}/skill/agentinfo"><i class="fa fa-circle-o"></i>Agent Info</a></li>
                            <!--<li><a href="${pageContext.request.contextPath}/skill/agentinfo"><i class="fa fa-circle-o"></i>Skill Info</a></li>-->
                            <li><a href="${pageContext.request.contextPath}/skill/callstats"><i class="fa fa-circle-o"></i>Call Statistics</a></li>
                            <li><a href="${pageContext.request.contextPath}/skill/calldetails"><i class="fa fa-circle-o"></i>Call Details</a></li>

                        </ul>
                    </li>
                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-phone-square"></i><span>VDN</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="${pageContext.request.contextPath}/vdn/realtime" style="font-size: small;"><i class="fa fa-circle-o"></i>Real Time Agents Stats</a></li>
                            <li><a href="${pageContext.request.contextPath}/vdn/incoming"><i class="fa fa-circle-o"></i>Incoming Call Stats</a></li>
                            <li><a href="${pageContext.request.contextPath}/vdn/outgoing"><i class="fa fa-circle-o"></i>Outgoing Call Stats</a></li>
                        </ul>
                    </li>
                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-address-book" aria-hidden="true"></i> <span>Call Records</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">

                            <li><a href="${pageContext.request.contextPath}/CallAudioRecord/AgentReport"><i class="fa fa-circle-o"></i>Call Records</a></li>

                        </ul>
                    </li>
                      <li class="treeview">
                        <a href="#">
                            <i class="fa fa-bell" aria-hidden="true"></i> <span>Wallboard Notification</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">

                            <li><a href="${pageContext.request.contextPath}/NotificationsController/notification"><i class="fa fa-circle-o"></i>Wallboard Notification</a></li>

                        </ul>
                    </li>
                    <li style="display: none;">
                        <a href="#">
                            <i class="fa fa-dashboard"></i> <span>Campaign Management</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu" style="display:none;">
                            <li><a href="${pageContext.request.contextPath}/campaign_management/create"><i class="fa fa-circle-o"></i>Create</a></li>
                            <li><a href="${pageContext.request.contextPath}/campaign_management/upload"><i class="fa fa-circle-o"></i>Upload</a></li>
                            <li><a href="${pageContext.request.contextPath}/campaign_management/view"><i class="fa fa-circle-o"></i>View</a></li>
                        </ul>
                    </li>

                </ul>

            </section>

        </aside>
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->

            <!-- Main content -->
            <aside>
                <sitemesh:write property='body' />
            </aside>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
        <!-- Main Footer -->
        <footer class="main-footer">
            <!-- To the right -->
            <div class="pull-right hidden-xs">
                Supervisor Portal
            </div>
            <!-- Default to the left -->
            <strong>Copyright &copy; 2020 <a href="http://apollo.com.pk/">Apollo Telecom Pvt Ltd</a>.</strong> All rights reserved.
        </footer>
        <!-- Control Sidebar -->

        <!-- /.control-sidebar -->
        <!-- Add the sidebar's background. This div must be placed
             immediately after the control sidebar -->

    </div>


    <div id="logoutPopUp" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" id="btnClosePopUp">&times;</button>
                    <h4 class="modal-title">Agent Sign out</h4>
                </div>
                <div class="modal-body">
                    <p>You are forced to logout</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" id="btnOK">Ok</button>
                </div>
            </div>

        </div>
    </div>

<script src="${pageContext.request.contextPath}/resources/js/jquery-2.2.3.min.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/resources/js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/resources/js/plugins/datatables/extensions/FixedHeader/js/dataTables.fixedHeader.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/resources/js/plugins/timepicker/bootstrap-timepicker.js" type="text/javascript"></script>

<script src="${pageContext.request.contextPath}/resources/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/resources/js/moment.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>

<script src="${pageContext.request.contextPath}/resources/js/bootstrap-datetimepicker.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/tether.min.js"></script>

<script src="${pageContext.request.contextPath}/resources/js/AdminLTE/app.min.js" type="text/javascript"></script>

<script src="${pageContext.request.contextPath}/resources/js/d3.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/d3pie.js"></script>

<script src="${pageContext.request.contextPath}/resources/js/notifier.js"></script>

<script src="${pageContext.request.contextPath}/resources/js/AdminLTE/demo.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/resources/js/nprogress.js" type="text/javascript"></script>


<script src="${pageContext.request.contextPath}/resources/js/layoutAjax.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/resources/js/moment-duration-format.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/resources/js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/resources/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
<script>

// Show the progress bar
    NProgress.start();

// Increase randomly
    var interval = setInterval(function () {
        NProgress.inc();
    }, 500);

// Trigger finish when page fully loaded
    jQuery(window).load(function () {
        clearInterval(interval);
        NProgress.done();
    });

// Trigger bar when exiting the page
    jQuery(window).unload(function () {
        NProgress.start();
    });

</script>
</body>
</html>