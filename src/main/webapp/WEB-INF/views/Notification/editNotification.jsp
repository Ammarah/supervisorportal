<%-- 
    Document   : Wallboard-Notification
    Created on : Nov 7, 2019, 11:48:02 AM
    Author     : Ammarah
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
        <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="${pageContext.request.contextPath}/resources/css/timepicker/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">

        <style>
            .unstyled::-webkit-inner-spin-button,
            .unstyled::-webkit-calendar-picker-indicator {
                display: block;
                -webkit-appearance: none;
            }
        </style>
        <title>Wallboard Notifications</title>
    </head>
    <body>
        <section class="content">
            <div class="box box-success">
                <div class="box-header">
                    <h3 class="box-title">Edit Notification</h3>
                </div>
                <section class="content" id="edit_notification">

                    <div class="box-body">
                        <form action="../updateNotification" method="POST">
                            <div class="form-group row">

                                <input type="number" name="notificationID"  value="${notification.notificationID}"  hidden="hidden" />
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label" >Notification Text </label>
                                <div class="col-sm-3">
                                    <input type="text" name="text_info" class="form-control" value="${notification.notificationText}"  required="required"/>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Start Date Time</label>
                                <div class="input-group date form_datetime col-sm-3" data-date="2013-02-21T15:25:00Z" style="padding-left: 1.5%;padding-right: 1.5%;">
                                    <input size="16" type="text"  value="<fmt:formatDate pattern="yyyy-MMMM-dd HH:mm" value="${notification.startDateTime}" />" class="form-control" id="startDateTime" name="startDateTime" readonly>
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>

                                    </span>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">End Date Time</label>
                                <div class="input-group date form_datetime col-sm-3" data-date="2013-02-21T15:25:00Z" style="padding-left: 1.5%;padding-right: 1.5%;">
                                    <input size="16" type="text" value="<fmt:formatDate pattern="yyyy-MMMM-dd HH:mm" value="${notification.endDateTime}" />" id="endDateTime" name="endDateTime" class="form-control" readonly>
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>

                                    </span>
                                </div>
                                <div id="error" style="display: none; color: red;margin-left: 17%"></div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Select Location</label>
                                <div class="col-sm-3">
                                    <select id="location" name="location" class="form-control" required="required">
                                        <c:choose>
                                            <c:when test="${notification.location==1}">
                                                <option value=1>Lahore</option>  
                                                <option value=2>Karachi</option>
                                            </c:when>
                                            <c:when test="${notification.location==2}">
                                                <option value=2>Karachi</option>
                                                <option value=1>Lahore</option>  
                                            </c:when>
                                        </c:choose>

                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Select Skill</label>
                                <div class="col-sm-3">
                                    <select id="skillId" name="skillId" class="form-control" required="required">
                                        <c:choose>
                                            <c:when test="${notification.skillId=='BLB'}">
                                                <option value="BLB">BLB</option>  
                                                <option value="BISP">BISP</option>
                                            </c:when>
                                            <c:when test="${notification.skillId=='BISP'}">
                                                <option value="BISP">BISP</option>
                                                <option value="BLB">BLB</option>  
                                            </c:when>
                                        </c:choose>

                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Select Priority</label>
                                <div class="col-sm-3">
                                    <select id="priority" name="priority" class="form-control" required="required">
                                        <c:choose>
                                            <c:when test="${notification.priority=='low'}">
                                                <option value="low">Low</option>  
                                                <option value="medium">Medium</option>
                                                <option value="high">High</option>
                                            </c:when>
                                            <c:when test="${notification.priority=='medium'}">
                                                <option value="medium">Medium</option>
                                                <option value="low">Low</option>  
                                                <option value="high">High</option>
                                            </c:when>
                                            <c:when test="${notification.priority=='high'}">
                                                <option value="high">High</option>
                                                <option value="low">Low</option>  
                                                <option value="medium">Medium</option>
                                            </c:when>
                                        </c:choose>

                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">

                                <label class="col-sm-2 col-form-label">Notification Type</label>              
                                <div class="col-sm-3">
                                    <select id="type" name="type" class=" form-control " required="required">
                                        <c:choose>
                                            <c:when test="${notification.type=='info'}">
                                                <option value="info">Info</option>  
                                                <option value="warning">Warning</option>
                                                <option value="general">General</option>
                                            </c:when>
                                            <c:when test="${notification.type=='warning'}">
                                                <option value="warning">Warning</option>
                                                <option value="info">Info</option>  
                                                <option value="general">General</option>
                                            </c:when>
                                            <c:when test="${notification.type=='general'}">
                                                <option value="general">General</option>
                                                <option value="warning">Warning</option>
                                                <option value="info">Info</option>  
                                            </c:when>
                                        </c:choose>

                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label" >Status</label>
                                <div class="col-sm-3">
                                    <c:choose>
                                        <c:when test="${editable==true}">
                                            <select id="status" name="status" class=" form-control " required="required">
                                                <c:choose>
                                                    <c:when test="${notification.status=='Inactive'}">
                                                        <option value="Inactive">Inactive</option>  
                                                        <option value="Active">Active</option>
                                                    </c:when>
                                                    <c:when test="${notification.status=='Active'}">
                                                        <option value="Active">Active</option>
                                                        <option value="Inactive">Inactive</option>  
                                                    </c:when>

                                                </c:choose>

                                            </select>
                                        </c:when>
                                        <c:when test="${editable==false}">
                                            <input type="text" name="status" class="form-control" value="${notification.status}"  required="required" readonly/>
                                        </c:when>

                                    </c:choose>

                                </div>
                            </div>

                            <div class="col-sm-5 text-right" >
                                <button type="submit" class="btn btn-success" style="align-content: right;" >Update</button>
                                <a type="button" class="btn btn-danger" style="align-content: right;" href ="../notification">Cancel</a>
                            </div>  
                        </form>

                    </div>
                </section>

            </div>
        </section>  

        <script src="${pageContext.request.contextPath}/resources/js/jquery-2.2.3.min.js" type="text/javascript" charset="UTF-8"></script>
        <script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/resources/js/bootstrap-datetimepicker.js" type="text/javascript" charset="UTF-8"></script>
        <script src="${pageContext.request.contextPath}/resources/js/locales/bootstrap-datetimepicker.fr.js" type="text/javascript" charset="UTF-8"></script>

        <script language="javascript" type="text/javascript">
            $('.form_datetime').datetimepicker({
                //language:  'fr',
                weekStart: 1,
                todayBtn: 1,
                autoclose: 1,
                todayHighlight: 1,
                startView: 2,
                forceParse: 0,
                format: "yyyy-MM-dd hh:ii",
                autoclose: true,
                todayBtn: true,
                useCurrent: true,
                //startDate: "2013-02-14 10:00",
                startDate: new Date(),
                minuteStep: 5,
                showMeridian: 1
            });
            $("#endDateTime").change(function () {
                $("#error").hide();
                var startDate = document.getElementById("startDateTime").value;
                var endDate = document.getElementById("endDateTime").value;
                if (startDate != null && endDate != null) {
                    if (Date.parse(endDate) < Date.parse(startDate)) {
                        //alert("End date should be greater than Start date");
                        $("#error").html("<br> End Date Time should be greater than Start Date Time");
                        $("#error").show();
                        document.getElementById("endDateTime").value = "";
                    }
                }
            });
            $("#startDateTime").change(function () {
                $("#error").hide();
                var startDate = document.getElementById("startDateTime").value;
                var endDate = document.getElementById("endDateTime").value;
                if (startDate != null && endDate != null) {
                    if (Date.parse(startDate) > Date.parse(endDate)) {
                        //alert("End date should be greater than Start date");
                        $("#error").html("<br> Start Date Time should be smaller than End Date Time");
                        $("#error").show();
                        document.getElementById("startDateTime").value = "";
                    }
                }
            });
        </script>
    </body>

</html>
