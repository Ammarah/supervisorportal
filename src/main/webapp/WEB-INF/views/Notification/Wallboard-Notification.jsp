<%-- 
    Document   : Wallboard-Notification
    Created on : Nov 7, 2019, 11:48:02 AM
    Author     : Ammarah
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
        <script src="${pageContext.request.contextPath}/resources/js/jquery-2.2.3.min.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/resources/js/plugins/timepicker/bootstrap-timepicker.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery-2.2.3.min.js" type="text/javascript"></script>
        <style>
            .unstyled::-webkit-inner-spin-button,
            .unstyled::-webkit-calendar-picker-indicator {
                display: block;
                -webkit-appearance: none;
            }

        </style>
        <title>Wallboard Notifications</title>
    </head>
    <body>
        <section class="content">
            <div class="box box-success">
                <div class="box-header">
                    <h3 class="box-title">Notifications <i class="fa fa-bell" style="font-size:24px"></i></h3>
                    <div style="float: right;">
                        <a href="addNotification" class="btn btn-primary" id="add"> Add Notification
                        </a>
                    </div>
                </div>
                <!-- /.box-header -->
                <!-- view notifications-->
                <section class="content" id="list_notifications">

                    <div class="box-body">
                        <table class="table table table-bordered table-hover table-condensed" id="notification-table">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Text</th>
                                    <th>Start Date Time</th>
                                    <th>End Date Time</th>
                                    <th>Location</th>
                                    <th>Skill</th>
                                    <th>Priority</th>
                                    <th>Type</th>
                                    <th>Status</th>
                                    <th width="60px">Action</th>
                                </tr>
                            </thead>

                        </table>
                    </div>


                </section>

            </div>
        </section>
        <script language="javascript" type="text/javascript">

            $(document).ready(function () {
                datatables = $('#notification-table').dataTable({
                    "processing": true,
                    "paging": true,
                    "responsive": true,
                    "ajax": {
                        "url": "getNotifications",
                        "type": "GET",
                        "dataType": "json"
                    },
                    columns: [
                        //{"data": "notificationID"},
                        {
                            "render": function (data, type, row, meta) {
                                return meta.row + 1;
                            }
                        },
                        {"data": "notificationText"},
                        {"data": "startDateTime"},
                        {"data": "endDateTime"},
                        {
                            "render": function (data, type, row) {
                                if (row.location == 1) {
                                    return "Lahore";
                                } else if (row.location == 2) {
                                    return "Karachi";
                                } else
                                {
                                    return null;
                                }

                            }
                        },
                        {"data": "skillId"},
                        {"data": "priority"},
                        {"data": "type"},
                        {"data": "status"},
                        {
                            "render": function (data, type, row) {
                                var inner = '<a class="btn btn-sm btn-primary" href="editNotification/' + row.notificationID + '"><i class="fa fa-pencil" style="font-size:16px"></i></a>  <a class="btn btn-sm btn-danger"    onclick="javascript:return confirm(\'Are you sure you want to delete?\')";  href="deleteNotification/' + row.notificationID + '"><i class="fa fa-trash" style="font-size:16px"></i></a>';
                                return inner;

                            }
                        }

                    ]
                });

            });



        </script>
    </body>

</html>
