<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>Supervisor Portal | Dashboard</title>
<script src="${pageContext.request.contextPath}/resources/js/jquery-2.2.3.min.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/resources/js/plugins/timepicker/bootstrap-timepicker.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/resources/js/jquery-2.2.3.min.js" type="text/javascript"></script>
<style>
    .unstyled::-webkit-inner-spin-button,
    .unstyled::-webkit-calendar-picker-indicator {
        display: block;
        -webkit-appearance: none;
    }

</style>
<section class="content">
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Audio Information</h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" id="btnRefereshTable">
                    <i class="fa fa-refresh"></i>
                </button>
<!--                <button type="button" class="btn btn-box-tool" data-widget="collapse">
                    <i class="fa fa-minus"></i>
                </button>-->
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body" >
            <form class="form-horizontal" action="${pageContext.request.servletContext.contextPath}/CallAudioRecord/AjaxGetDataFromIPCC" method="POST">
                <!--<div class="row">-->
                <div class="col-sm-4">
                    <div class="form-group">
                        <label class="control-label col-sm-4" for="StartDateFilter">Start Date</label>
                        <div class="col-sm-8">
                            <input type="date"  class="form-control unstyled" id="StartDateFilter" name="StartDateFilter" required="required" autofocus="autofocus" >
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label class="control-label col-sm-4" for="EndDateFilter">End Date</label>
                        <div class="col-sm-8">
                            <input type="date" class="form-control unstyled" id="EndDateFilter" name="EndDateFilter" required="required" autofocus="autofocus" >
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary col-sm-6">Fetch</button>
                    </div>
                </div>
                <!--</div>-->

            </form>
        </div>
        <div class="box-body">
            <table   class="ReportingTables table table-bordered table-hover table-condensed">
                <thead>
                    <tr>
                        <th id="one">Callee No</th>
                        <th id="two">Caller No</th>
                        <th id="three">Agent Id</th>
                        <th id="four">Begin Time</th>
                        <th id="five">End Time</th>
                        <th id="six">Audio Recording</th>
                    </tr>
                </thead>
                <c:choose>
                    <c:when test = "${agentStats != '[{}]'}">
                        <tbody>
                            <c:forEach var="aStat" items="${agentStats}">
                                <jsp:useBean id="myDate" class="java.util.Date"/>
                                <c:set target="${myDate}" property="time" value="${aStat.get('BEGINTIME')}"/>
                                <jsp:useBean id="myDateEnd" class="java.util.Date"/>
                                <c:set target="${myDateEnd}" property="time" value="${aStat.get('ENDTIME')}"/>
                                <c:set var="string1" value="${aStat.get('BEGINTIME1')}" />
                                <fmt:parseDate value="${string1}" var="theDate"  pattern="yyyy-MM-dd HH:mm:ss " />
                                <fmt:formatDate value="${theDate}" pattern="dd MMM  yyyy" var="string2"/>
                                <tr>
                                    <td>${aStat.get("CALLEENO")}</td>
                                    <td>${aStat.get("CALLERNO")}</td>
                                    <td>${aStat.get("AGENTID")}</td>
                                    <td><c:out value="${myDate}" /></td>
                                    <td><c:out value="${myDateEnd}" /></td>
                                    <td><button class="btn btn-primary"  value="${aStat.get("FILENAME")}" onclick="CLICK(this)">Download</button></td>

                                </tr>
                            </c:forEach>
                        </tbody>
                    </c:when>
                    <c:otherwise>
                        <tbody></tbody>
                    </c:otherwise>
                </c:choose>
            </table>
        </div>
    </div>
</section>
<script>
    $(document).ready(function () {
//                alert('${agentStats}');
        //Setting Date at day -1 
        var todayless = new Date(Date.now());
        var todaylessStart = new Date(Date.now() - (24 * 60 * 60 * 1000));
        //  alert(todayless); 
        var dd = todayless.getDate();
        var mm = todayless.getMonth() + 1; //January is 0!
        var yyyy = todayless.getFullYear();
        if (dd < 10) {
            dd = '0' + dd
        }
        if (mm < 10) {
            mm = '0' + mm
        }
        var ddstart = todaylessStart.getDate();
        var mmstart = todaylessStart.getMonth() + 1; //January is 0!
        var yyyystart = todaylessStart.getFullYear();
        if (ddstart < 10) {
            ddstart = '0' + ddstart
        }
        if (mmstart < 10) {
            mmstart = '0' + mmstart
        }
        // Disable up and down keys.
        $('form').on('keydown', 'input[type=date]', function (e) {
            if (e.which == 38 || e.which == 40)
                e.preventDefault();
        });
        todayless = yyyy + '-' + mm + '-' + dd;
        todaylessStart = yyyystart + '-' + mmstart + '-' + ddstart;
        document.getElementById("StartDateFilter").setAttribute("max", todaylessStart);
        document.getElementById("EndDateFilter").setAttribute("max", todayless);
        //End Setting Date at day
        var startDate = document.getElementById("StartDateFilter").value;
        var endDate = document.getElementById("EndDateFilter").value;
        if ((Date.parse(startDate) <= Date.parse(endDate))) {
            alert("End date should be greater than Start date");
            document.getElementById("EndDateFilter").value = "";
        }
        var table = $(".table").DataTable({
            responsive: true,
            scrollX: true
        });
        $("#StartDateFilter").val('${FromDate}');
        $("#EndDateFilter").val('${ToDate}');
    });
    function CLICK(a) {
        var value = a.value;
        $.post("${pageContext.request.servletContext.contextPath}/CallAudioRecord/DownloadWidgetDataFile", {fileUrl: value}, function (data, status) {
            console.log("Data \t :" + data);
            var current_path = data.split('\\').pop();
            downloadURI(("http://" + data), current_path);
        });
    }
    function downloadURI(uri, name) {
        var link = document.createElement("a");
        link.download = name;
        link.href = uri;
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
        delete link;
    }
</script>