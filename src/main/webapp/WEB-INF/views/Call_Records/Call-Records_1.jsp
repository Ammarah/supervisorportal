<%-- 
    Document   : dashboard
    Created on : Jan 5, 2018, 3:13:36 PM
    Author     : Abdul Rehman
--%>
<%@page import="java.util.concurrent.TimeUnit"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.Date"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="windows-1252"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
        <title>Supervisor Portalss | Dashboard</title>
        <script src="${pageContext.request.contextPath}/resources/js/jquery-2.2.3.min.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/resources/js/plugins/timepicker/bootstrap-timepicker.js" type="text/javascript"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery-2.2.3.min.js" type="text/javascript"></script>
    </head>
    <body>
        <section class="content-header">
            <h1>
                Audio Call Recordings
            </h1>
        </section>
        <section class="content">
            <div class="center" >
                <table id="audioCallTable" class="display" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th id="one">Callee No</th>
                            <th id="two">Caller No</th>
                            <th id="three">Agent Id</th>
                            <th id="four">Begin Time</th>
                            <th id="five">End Time</th>
                            <th id="six">Audio Recording</th>
                        </tr>
                        <tr style="text-align:left;">
                            <th >Callee No</th>
                            <th>Caller No</th>
                            <th >Agent Id</th>
                            <th >Begin Time</th>
                            <th >End Time</th>
                            <th>Audio Recording</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th >Name</th>
                            <th >AgentID</th>
                            <th >Date</th>
                            <th >Action</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </section>
        <!-- /.Bootstrap Loader -->

        <div class="animationload" hidden="true" style="display: none;">
            <div class="osahanloading"></div>
        </div>
        <style>
            .animationload {
                background-color: #222d32c4;
                height: 100%;
                left: 0;
                position: fixed;
                top: 0;
                width: 100%;
                z-index: 10000;
            }
            .osahanloading {
                animation: 1.5s linear 0s normal none infinite running osahanloading;
                background: #fed37f none repeat scroll 0 0;
                border-radius: 50px;
                height: 50px;
                left: 50%;
                margin-left: -25px;
                margin-top: -25px;
                position: absolute;
                top: 50%;
                width: 50px;
            }
            .osahanloading::after {
                animation: 1.5s linear 0s normal none infinite running osahanloading_after;
                border-color: #85d6de transparent;
                border-radius: 80px;
                border-style: solid;
                border-width: 10px;
                content: "";
                height: 80px;
                left: -15px;
                position: absolute;
                top: -15px;
                width: 80px;
            }
            @keyframes osahanloading {
                0% {
                    transform: rotate(0deg);
                }
                50% {
                    background: #85d6de none repeat scroll 0 0;
                    transform: rotate(180deg);
                }
                100% {
                    transform: rotate(360deg);
                }
            }

        </style>
        <script language="javascript" type="text/javascript">
             var dataSet;
            $(document).ready(function () {
                var table = $('#audioCallTable').DataTable({
                    "processing": false, // control the processing indicator.
                    "serverSide": false, // recommended to use serverSide when data is more than 10000 rows for performance reasons
                    "info": false, // control table information display field
                    "stateSave": false,
                    "dataSrc": "",

                    "lengthMenu": [[10, 40, 100, -1], [10, 40, 100, "ALL"]],
                    "bPaginate": true, //restore table state on page reload,
                    "ajax": {
                        url: "${pageContext.request.contextPath}/CallAudioRecord/AjaxGetDataFromIPCC/",
                        type: "GET",
                        data: function (d) {
                            if ($("#frmDate").val() == undefined)
                            {
                                d.fromDate = moment().format('DD-MMM-YYYY'),
                                        d.toDate = moment().format('DD-MMM-YYYY')
                            } else
                            {
                                d.fromDate = $("#frmDate").val(),
                                        d.toDate = $("#toDate").val()
                            }



                        },
                        contentType: 'application/json; charset=utf-8',
                        success: function (obj, textstatus) {
                             dataSet = new Array;
                            if (!('error' in obj)) {
                                $.each(obj, function (index, value) {
                                    var tempArray = new Array;
                                    for (var o in value) {
                                        tempArray.push(value[o]);
                                    }
                                    dataSet.push(tempArray);
                                })
                            }
                        },
                        data: dataSet,
                        error: function (xhr, status, error) {
                            Notifier.error(xhr + 'File Server Not Responding' + status + error);
                            console.log(err.status);
                        }

                    },
                    "columns": [
                        {"data": "CALLEENO"},
                        {"data": "CALLERNO"},
                        {"data": "AGENTID"},
                        {
                            "data": "BEGINTIME"
                        },
                        {
                            "data": "ENDTIME"
                        },

                        {"data": "FILENAME"}
                    ],
                    "columnDefs": [{width: 30, targets: 0},
                        {width: 30, targets: 1},
                        {width: 30, targets: 2},

                        {
                            "targets": 5,
                            "data": "FILENAME",
                            "render": function (data, type, full, meta) {
                                alert(data);
                                return '<button class="btn btn-primary"  onclick="CLIC(\'' + data + '\')" >Download</button>';
                            }
                        }],
                    fixedColumns: true,
                    "order": [[0, "asc"]]

                });

                $(".dataTables_filter").css("visibility", "collapse");
                $("#audioCallTable_length").append('<div class="form-horizontal col-lg-6" style="margin-right:0px;"><div class="form-group"><label class="col-xs-1" style="margin-right: -10px;">From Date</label><div class="col-xs-4 date"><div class="input-group input-append date datePicker" ><input type="text" style=" padding:0px;" class="form-control input-group-addon" name="date" id="frmDate" /><span class="input-group-addon add-on"><span class="glyphicon glyphicon-calendar"></span></span></div></div><label class="col-xs-1 " style="margin-right: -10px; margin-left:-15px;">To Date</label><div class="col-xs-4 date"><div class="input-group input-append date datePicker2"><input style="padding:0px;" type="text" class="form-control input-group-addon" name="date2" id="toDate" /><span class="input-group-addon add-on"><span class="glyphicon glyphicon-calendar"></span></span></div></div><div class="col-xs-1 "><button type="button" class="btn btn-default" id="btnSearch">Search</button></div></div></div>');
                var d = $('#audioCallTable_length').children().first();
                $(".dataTables_filter").hide();
                d.css('margin', '10px');
                $('.datePicker').datetimepicker({
                    format: 'DD-MMM-YY'
                });
                $('.datePicker2').datetimepicker({
                    format: 'DD-MMM-YY'
                });
                //var table = $('#example').DataTable()

                var objDate = new Date(),
                        locale = "en-us",
                        month = objDate.toLocaleString(locale, {month: "long"});
                $("#frmDate").val(objDate.getDate() + '-' + objDate.toLocaleString(locale, {month: "short"}) + '-' + objDate.getFullYear().toString().substr(-2));
                $("#toDate").val(objDate.getDate() + '-' + objDate.toLocaleString(locale, {month: "short"}) + '-' + objDate.getFullYear().toString().substr(-2));
                // table.columns(1).search(this.value).draw();
                // table.columns(2).search(this.value).draw();
                $("#one").html('<input id="txtOne" type="text" style="width:80px; font-size:0.9em;" placeholder="CALLEE NO" />');
                $("#two").html('<input id="txtTwo" type="text" style="width:80px;font-size:0.9em;" placeholder="CALLER NO" />');
                $("#three").html('<input id="txtThree"  type="text" style="width:80px;font-size:0.9em;" placeholder="AGENT ID" />');
                $("#four").html('<input id="txtFour" type="text" id="dateTimePicker"  placeholder="BEGIN TIME" style="font-size:0.9em;visisbility:collapse;" />');
                $("#five").html('<input id="txtFive" type="text" placeholder="ENDTIME" style="font-size:0.9em;visisbility:collapse;" />');
                $("#six").html('<input id="txtSix" type="text" placeholder="FILE NAME" style="font-size:0.9em;visisbility:collapse;" />');


                $("#txtOne").keyup(function () {
                    var that = table.columns(0);
                    if (that.search() !== this.value) {
                        that.search(this.value).draw();
                    }
                });
                $("#txtTwo").keyup(function () {
                    var that = table.columns(1);
                    if (that.search() !== this.value) {
                        that.search(this.value).draw();
                    }
                });
                $("#txtThree").keyup(function () {
                    var that = table.columns(2);
                    if (that.search() !== this.value) {
                        that.search(this.value).draw();
                    }
                });
                $("#txtFour").keyup(function () {
                    var that = table.columns(3);
                    if (that.search() !== this.value) {
                        that.search(this.value).draw();
                    }
                });
                $("#txtFive").keyup(function () {
                    var that = table.columns(4);
                    if (that.search() !== this.value) {
                        that.search(this.value).draw();
                    }
                });
                $("#txtSix").keyup(function () {
                    var that = table.columns(5);
                    if (that.search() !== this.value) {
                        that.search(this.value).draw();
                    }
                });
                $("#txtFour").css('visibility', 'collapse');
                $("#txtFive").css('visibility', 'collapse');
                $("#txtSix").css('visibility', 'collapse');
                $('#btnSearch').click(function () {

                    table.ajax.reload();
                });
            });

            function CLIC(a) {
                // alert(a);
                window.location = '/SupervisorPortal/CallAudioRecord/DownloadWidgetDataFile?fileUrl=' + a;
            }
        </script>

    </body>
</html>

