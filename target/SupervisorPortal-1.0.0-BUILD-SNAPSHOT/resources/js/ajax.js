/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {
    $('#btnSignIn').click(function () {
        login();
    });
    var agentStatusTimer = setTimeout(reloadAgentStates, 5000);
    function checkAgentStatus() {
        $.ajax({
            type: "GET",
            url: '../agentpoolingstatus',
            contentType: "application/json",
            dataType: "json",
            crossDomain: true,
            cache: false,
            async: true,
            timeout: 15000,
            success: function (response) {
                clearTimeout(CheckAgentStatusTimer);
                console.log('Agent Status :' + response.retcode);

                if (response.retcode == "100-006")
                {
                    clearTimeout(CheckAgentStatusTimer);
                    redirectToLoginPage();

                } else if (response.retcode == "000-003")
                {
                    clearTimeout(CheckAgentStatusTimer);
                    redirectToLoginPage();

                } else if (response == "Error: The operation has timed out") {

                    CheckAgentStatusTimer = setTimeout(checkAgentStatus, 11000);

                } else {
                    CheckAgentStatusTimer = setTimeout(checkAgentStatus, 3000);
                }


            }, error: function (xhr, status, error) {
                console.log('Agent Status Error: ' + status);
                clearTimeout(CheckAgentStatusTimer);
                CheckAgentStatusTimer = setTimeout(checkAgentStatus, 11000);
            }
        });
    }
    function reloadAgentStates() {
        $.ajax({
            type: "GET",
            url: 'getallagentstatus',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            cache: false,
            timeout: 15000,
            success: function (response) {
                clearTimeout(agentStatusTimer);
                if (response.retcode == "0")
                {
                    var index = 0;
                    var t = response.result;
                    for (index = 0; index < t.length; ++index) {
                        var ob = $("#" + t[index].agentId);
                        var ti = moment().startOf('day')
                                .seconds(t[index].currentStateTime)
                                .format('HH:mm:ss');
                        $("#timeAgent" + t[index].agentId).text(ti);


                        var bg = $("#divbg" + t[index].agentId);
                        var btnIdle = $("#idle" + t[index].agentId);
                        var btnRest = $("#res" + t[index].agentId);
                        var btnBusy = $("#bus" + t[index].agentId);
                        var btnLogout = $("#log" + t[index].agentId);
                        var btnInfo = $("#stat" + t[index].agentId);
                        var btnAdjust = $("#skillAdj" + t[index].agentId);
                        var btnInsert = $("#Insert" + t[index].agentId);
                        var btnListen = $("#Listen" + t[index].agentId);
                        var btnStop = $("#stop" + t[index].agentId);
                        if (ob != undefined)
                        {
                            bg.removeClass();
                            if (t[index].status == 0)
                            {
                                //sign off
                                btnAdjust.addClass('disabled');
                                btnAdjust.prop('disabled', true);

                                btnStop.addClass('disabled');
                                btnStop.prop('disabled', true);

                                btnInsert.addClass('disabled');
                                btnInsert.prop('disabled', true);
                                btnListen.addClass('disabled');
                                btnListen.prop('disabled', true);

                                btnInfo.addClass('disabled');
                                btnInfo.prop('disabled', true);
                                btnIdle.addClass('disabled');
                                btnIdle.prop('disabled', true);
                                btnRest.addClass('disabled');
                                btnRest.prop('disabled', true);
                                btnBusy.addClass('disabled');
                                btnBusy.prop('disabled', true);
                                btnLogout.addClass('disabled');
                                btnLogout.prop('disabled', true);
                                bg.addClass('info-box bg-red-active');
                                ob.text('Sign off');
                            } else if (t[index].status == 2) {
                                //idle
                                btnAdjust.removeClass('disabled');
                                btnAdjust.prop('disabled', false);

                                btnStop.addClass('disabled');
                                btnStop.prop('disabled', true);

                                btnInsert.addClass('disabled');
                                btnInsert.prop('disabled', true);
                                btnListen.addClass('disabled');
                                btnListen.prop('disabled', true);

                                btnIdle.addClass('disabled');
                                btnIdle.prop('disabled', true);
                                btnInfo.removeClass('disabled');
                                btnInfo.prop('disabled', false);
                                btnRest.removeClass('disabled');
                                btnRest.prop('disabled', false);
                                btnBusy.removeClass('disabled');
                                btnBusy.prop('disabled', false);
                                btnLogout.removeClass('disabled');
                                btnLogout.prop('disabled', false);
                                bg.addClass('info-box bg-green-active');
                                ob.text('Ready');
                            } else
                            if (t[index].status == 3) {
                                //busy
                                btnAdjust.removeClass('disabled');
                                btnAdjust.prop('disabled', false);

                                btnStop.addClass('disabled');
                                btnStop.prop('disabled', true);

                                btnInsert.addClass('disabled');
                                btnInsert.prop('disabled', true);
                                btnListen.addClass('disabled');
                                btnListen.prop('disabled', true);

                                btnBusy.addClass('disabled');
                                btnBusy.prop('disabled', true);
                                btnInfo.removeClass('disabled');
                                btnInfo.prop('disabled', false);
                                btnRest.removeClass('disabled');
                                btnRest.prop('disabled', false);
                                btnIdle.removeClass('disabled');
                                btnIdle.prop('disabled', false);
                                btnLogout.removeClass('disabled');
                                btnLogout.prop('disabled', false);
                                bg.addClass('info-box bg-blue-active');
                                ob.text('Not Ready');
                            } else if (t[index].status == 6) {
                                //work
                                btnAdjust.removeClass('disabled');
                                btnAdjust.prop('disabled', false);

                                btnStop.addClass('disabled');
                                btnStop.prop('disabled', true);

                                btnInsert.addClass('disabled');
                                btnInsert.prop('disabled', true);
                                btnListen.addClass('disabled');
                                btnListen.prop('disabled', true);

                                btnRest.addClass('disabled');
                                btnRest.prop('disabled', true);
                                btnInfo.removeClass('disabled');
                                btnInfo.prop('disabled', false);
                                btnIdle.removeClass('disabled');
                                btnIdle.prop('disabled', false);
                                btnBusy.removeClass('disabled');
                                btnBusy.prop('disabled', false);
                                btnLogout.removeClass('disabled');
                                btnLogout.prop('disabled', false);
                                bg.addClass('info-box bg-olive-active');
                                ob.text('Work');
                            } else if (t[index].status == 4) {
                                //talking
                                btnAdjust.removeClass('disabled');
                                btnAdjust.prop('disabled', false);

                                btnStop.addClass('disabled');
                                btnStop.prop('disabled', true);

                                btnInsert.removeClass('disabled');
                                btnInsert.prop('disabled', false);
                                btnListen.removeClass('disabled');
                                btnListen.prop('disabled', false);

                                btnInfo.removeClass('disabled');
                                btnInfo.prop('disabled', false);
                                btnIdle.removeClass('disabled');
                                btnIdle.prop('disabled', false);
                                btnRest.removeClass('disabled');
                                btnRest.prop('disabled', false);
                                btnBusy.removeClass('disabled');
                                btnLogout.removeClass('disabled');
                                btnLogout.prop('disabled', false);
                                btnBusy.prop('disabled', false);
                                bg.addClass('info-box bg-orange-active');
                                ob.text('Talking');
                            } else if (t[index].status == 5) {
                                //Answering
                                btnAdjust.removeClass('disabled');
                                btnAdjust.prop('disabled', false);

                                btnStop.addClass('disabled');
                                btnStop.prop('disabled', true);

                                btnInsert.removeClass('disabled');
                                btnInsert.prop('disabled', false);
                                btnListen.removeClass('disabled');
                                btnListen.prop('disabled', false);

                                btnInfo.removeClass('disabled');
                                btnInfo.prop('disabled', false);
                                btnIdle.removeClass('disabled');
                                btnIdle.prop('disabled', false);
                                btnRest.removeClass('disabled');
                                btnRest.prop('disabled', false);
                                btnBusy.removeClass('disabled');
                                btnLogout.removeClass('disabled');
                                btnLogout.prop('disabled', false);
                                btnBusy.prop('disabled', false);
                                bg.addClass('info-box bg-orange');
                                ob.text('Answering');
                            } else if (t[index].status == 26) {
                                //rest/break
                                btnAdjust.removeClass('disabled');
                                btnAdjust.prop('disabled', false);

                                btnStop.addClass('disabled');
                                btnStop.prop('disabled', true);

                                btnInsert.addClass('disabled');
                                btnInsert.prop('disabled', true);
                                btnListen.addClass('disabled');
                                btnListen.prop('disabled', true);

                                btnInfo.removeClass('disabled');
                                btnInfo.prop('disabled', false);
                                btnIdle.removeClass('disabled');
                                btnIdle.prop('disabled', false);
                                btnRest.addClass('disabled');
                                btnRest.prop('disabled', true);
                                btnBusy.removeClass('disabled');
                                btnLogout.removeClass('disabled');
                                btnLogout.prop('disabled', false);
                                btnBusy.prop('disabled', false);
                                bg.addClass('info-box bg-yellow-active');
                                ob.text('Break');
                            }

                        }
                    }
                } else
                {
                    console.log("Req Fail: " + recData);
                }
                agentStatusTimer = setTimeout(reloadAgentStates, 5000);
            },
            error: function (xhr, status, error) {
                clearTimeout(agentStatusTimer);
                agentStatusTimer = setTimeout(reloadAgentStates, 5000);
            }
        });
    }
    function setAgentIdle(btnAgentIdle){
    var btnAgentIdleID = btnAgentIdle.id;
    var workID = btnAgentIdleID.substring(4, btnAgentIdleID.length);
    $.ajax({
        type: "GET",
        url: 'agentsetidle',
        data: { agentId: workID },
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (recData)
        {
            if (recData.retcode == "0")
            {
                Notifier.success('Set Agent Ready Successfully');
            }
            else
            {
                Notifier.error('Set Agent Ready Failed');
            }
        },
        error: function (xhr, status, error) {
            console.log('Set Agent Ready Error ' + status);
            Notifier.error('Set Agent Ready Failed');
        }
    });
}
function setAgentBusy(btnAgentBusy) {
    var a = btnAgentBusy.id;

    var newVal = a.substring(3, a.length);

    $.ajax({
        type: "GET",
        url: 'agentsetnotready',
        data: { agentId :newVal},
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (recData)
        {
            if (recData.retcode == "0")
            {
                Notifier.success('Set Agent Not Ready Successfully');
            }
            else
            {
                Notifier.error('Set Agent Not Ready Failed');
            }
        },
        error: function (xhr, status, error) {
            Notifier.error('Set State Not Ready failed');
            console.log('Start Not Ready Error ' + status);
        }
    });
}
    function login()
    {
        var workid = $("#workID").val();
        var pwd = $("#pwd").val();
        var phnNO = $("#sipNo").val();

        var param = {}
        param["password"] = pwd;
        param["phonenum"] = phnNO;
        param["workId"] = workid;
        $.ajax({
            url: "loginrequest",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(param), //Stringified Json Object
            async: false, //Cross-domain requests and dataType: "jsonp" requests do not support synchronous operation
            cache: false, //This will force requested pages not to be cached by the browser          
            processData: false, //To avoid making query String instead of JSON
            success: function (result) {
                // Success Message Handler
                if (result.retcode == "0")
                {
                    window.location.href = "agent/dashboard"
                } else
                {
                    alert(result.message);
                }

            }
        });
    }
});


