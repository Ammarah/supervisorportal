$(document).ready(function () {
    function checkAgentStatus() {
        $.ajax({
            type: "GET",
            url: "checkAgentStatus",
            contentType: "application/json",
            dataType: "json",
            crossDomain: true,
            cache: false,
            async: true,
            timeout: 15000,
            success: function (response) {
                clearTimeout(CheckAgentStatusTimer);
                console.log('Agent Status in checkAgentStatus :' + response.retcode);
                if (response.retcode == "100-006")
                {
                    clearTimeout(CheckAgentStatusTimer);
                    redirectToLoginPage();

                } else if (response.retcode == "000-003")
                {
                    clearTimeout(CheckAgentStatusTimer);
                    redirectToLoginPage();

                } else if (response == "Error: The operation has timed out") {

                    CheckAgentStatusTimer = setTimeout(checkAgentStatus, 11000);

                } else {
                    CheckAgentStatusTimer = setTimeout(checkAgentStatus, 3000);
                }


            }, error: function (xhr, status, error) {
                console.log('Agent Status Error: ' + status);
                clearTimeout(CheckAgentStatusTimer);
                CheckAgentStatusTimer = setTimeout(checkAgentStatus, 11000);
            }
        });
    }
    function redirectToLoginPage() {
        $("#logoutPopUp").modal('show');
        $("#btnOK").click();
    }

    var CheckAgentStatusTimer = setTimeout(checkAgentStatus, 5000);

    $("#btnLogout").click(function () {
        $('.cssload-loader').show();
        $('.overlay').show();
        logout();

    });
    $("#btnOK").click(function () {
        logout();
        //$("#logoutPopUp").modal('hide');
        // window.location.href = "/SupervisorPortal/Account/Login";

    });
    $("#btnClosePopUp").click(function () {
        //$("#logoutPopUp").modal('hide');
        // window.location.href = "/SupervisorPortal/Account/Login";

    });
    function logout()
    {
        if (window.location.href.includes("agent"))
        {

        }
        $.ajax({
            type: "GET",
            url: '../agent_logout',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            crossDomain: true,
            cache: false,
            success: function (recData)
            {
                $('.cssload-loader').hide();
                $('.overlay').hide();
                if (recData.retcode == "0")
                {
                    window.location.href = "../login";
                    Notifier.success(recData.message);

                } else
                {
                    Notifier.error(recData.message);
                }
            },
            error: function (xhr, status, error) {
                $('.cssload-loader').hide();
                $('.overlay').hide();
                window.location.href = "../login";
                Notifier.error("Request Failed");
                console.log(status);
            }
        });
    }

});