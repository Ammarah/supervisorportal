

$(document).ready(function () {
    setTimeout(function () {
        $('#agentWorkId').val('');
        $('#agentPwd').val('');
        $('#agentPhnNo').val('');
    }, 1000);
    $('[data-toggle="tooltip"]').tooltip();
    $('#agentWorkId').focus();
    // Disable Right Click
    $(document).bind("contextmenu", function (e) {
        return false;
    });
    $(document).keypress(function (e) {
        if (e.which == 13) {
            login();
        }
    });
    $('#agentWorkId').keypress(function (e) {
        if (e.which == 13) {
            login();
        }
    });
    $('#agentPwd').keypress(function (e) {
        if (e.which == 13) {
            console.log("Max Length Password \t" + $("#agentPwd").length);
            login();
        }
    });
    $('#agentPhnNo').keypress(function (e) {
        if (e.which == 13) {
            login();
        }
    });
    var mytooltip2 = $("#agentPhnNo").tooltip();
    var mytooltip1 = $("#agentPwd").tooltip();
    var mytooltip = $("#agentWorkId").tooltip();
    $("#agentPhnNo").keydown(function (e) {
        if (e.keyCode == 8)
        {
            return true;
        }
        if (
                (e.keyCode >= 35 && e.keyCode <= 39) || (e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {

            e.preventDefault();
        } else
        {
            return true;
        }
    });
    $("#agentWorkId").keydown(function (e) {
        if (e.keyCode == 8 || e.keyCode == 9 || e.keyCode == 13)
        {
            return true;
        }
        if ((e.keyCode == 86 && (e.ctrlKey === true || e.metaKey === true)) || (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true))) {
            $("#agentWorkId").attr('data-original-title', "Copy Paste not allowed").tooltip('fixTitle').tooltip('show');
            e.preventDefault();
        }
        if ($.inArray(e.keyCode, [44, 27, 13, 110, 190]) !== -1 ||
                // Allow: Ctrl/cmd+A
                        (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                        (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
                        // Allow: home, end, left, right
                                (e.keyCode >= 35 && e.keyCode <= 39) || (e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    $("#agentWorkId").attr('data-original-title', "Non numeric value are not allowed").tooltip('fixTitle').tooltip('show');

                    e.preventDefault();
                } else
                {
                    $("#agentWorkId").attr('data-original-title', "Enter Work Id").tooltip('fixTitle').tooltip('hide');
                }
            });
    $("#agentPwd").keydown(function (e) {
        if (e.keyCode == 8 || e.keyCode == 9 || e.keyCode == 13) {
            return true;
        }
        if ($.inArray(e.keyCode, [46, 27, 13, 110, 190]) !== -1 ||
                // Allow: Ctrl/cmd+A
                        (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                        // Allow: Ctrl/cmd+C
                                (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
                                // Allow: Ctrl/cmd+X
                                        (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
                                        // Allow: home, end, left, right
                                                (e.keyCode == 86 && (e.ctrlKey === true || e.metaKey === true)) ||
                                                // Allow Ctrl V
                                                        (e.keyCode >= 35 && e.keyCode <= 39)) {
                                            $("#agentPwd").attr('data-original-title', "Copy/Paste not allowed").tooltip('fixTitle').tooltip('show');
                                            // let it happen, don't do anything
                                            e.preventDefault();
                                        } else {
                                            $("#agentPwd").attr('data-original-title', "Enter Password").tooltip('fixTitle').tooltip('hide');
                                        }

                                    });
                            $('.cssload-loader').hide();
                            $('.overlay').hide();
                            function login()
                            {
                                $('.cssload-loader').show();
                                $('.overlay').show();
                                var workid = $("#agentWorkId").val().trim();
                                var pwd = $("#agentPwd").val().trim();
                                var phnNO = $("#agentPhnNo").val().trim();
                                if (workid == "") {
                                    Notifier.warning('Work Id is missing');
                                    $('.cssload-loader').hide();
                                    $('.overlay').hide();
                                    $("#btnLogin").attr('disabled', false);
                                    return;
                                }
                                if (pwd == "") {
                                    Notifier.warning('Password is missing');
                                    $('.cssload-loader').hide();
                                    $('.overlay').hide();
                                    $("#btnLogin").attr('disabled', false);
                                    return;
                                }


                                var params = {};
                                params["password"] = pwd;
                                params["phonenum"] = phnNO;
                                params["workId"] = workid;
                                $.ajax({
                                    type: "POST",
                                    url: "loginrequest",
                                    contentType: "application/json; charset=utf-8",
                                    data: JSON.stringify(params),
                                    dataType: "json",
                                    success: function (result) {
                                        $("#btnLogin").attr('disabled', true);
                                        $('.cssload-loader').hide();
                                        $('.overlay').hide();
                                        if (result.retcode == "0")
                                        {
                                            $("#btnLogin").attr('disabled', true);
                                            Notifier.success("Login Successfully");
                                            window.location.href = "agent/dashboard";
                                        } else
                                        {
                                            $("#btnLogin").attr('disabled', false);
                                            Notifier.error(result.message + " \t Retcode:" + result.retcode);

                                        }
                                    },
                                    error: function (xhr, status, error) {
                                        $("#btnLogin").attr('disabled', false);
                                        console.log(status);
                                    }
                                });
                            }
                            $("#btnLogin").click(function () {
                                $("#btnLogin").attr('disabled', true);
                                $('.cssload-loader').show();
                                $('.overlay').show();
                                login();

                            });

                        });
