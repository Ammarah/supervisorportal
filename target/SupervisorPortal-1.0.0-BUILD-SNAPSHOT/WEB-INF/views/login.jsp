<!DOCTYPE html>
<html class="bg-black">
    <head>
        <%
            if (session != null && session.getAttribute("LoginUser") != null) {
                response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
                response.setHeader("Pragma", "no-cache"); // HTTP 1.0.
                response.setDateHeader("Expires", 0);
                response.sendRedirect("redirect:/agent/dashboard");
            }
        %>
        <meta charset="UTF-8">
        <title>Supervisor Portal | Log in</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <link rel="icon" href="${pageContext.request.contextPath}/resources/Logo/BA_logo.png" />
        <!-- bootstrap 3.0.2 -->
        <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="${pageContext.request.contextPath}/resources/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="${pageContext.request.contextPath}/resources/css/AdminLTE.css" rel="stylesheet" type="text/css" />
        <style>
            .lblOption {
                background-color: white;
                padding: 5px;
                font: 14px;
                width: 40%;
            }
            .overlay {
                background-color: #022158;
                display: block;
                position: fixed;
                top: 0;
                right: 0;
                bottom: 0;
                left: 0;
                opacity: 0.5;
                z-index: 3;
            }
            .btn
            {
                padding-right:6px;
            }
        </style>
    </head>
    <body class="bg-black">
        <div class="overlay" style="display:none;"></div>
        <div class="cssload-loader" style="display:none;" >
            <div class="cssload-side"></div>
            <div class="cssload-side"></div>
            <div class="cssload-side"></div>
            <div class="cssload-side"></div>
            <div class="cssload-side"></div>
            <div class="cssload-side"></div>
            <div class="cssload-side"></div>
            <div class="cssload-side"></div>
        </div>
        <div class="form-box" id="login-box">
            <div class="header">Supervisor Portal <b>Sign In</b></div>
            <div>
                <div class="body bg-gray">
                    <div class="form-group">
                        <input type="text" maxlength="32" autocomplete="off" name="workid" data-toggle="tooltip" title="Enter Work id" id="agentWorkId" class="form-control" placeholder="Work ID"/>
                    </div>
                    <div class="form-group">
                        <input type="password" maxlength="32" name="password" id="agentPwd" data-toggle="tooltip" title="Enter password id" class="form-control" placeholder="Password"/>
                    </div>    
                    <div class="form-group">
                        <input type="text" min="0" onkeypress='return event.charCode >= 48 && event.charCode <= 57' maxlength="24" name="sipnum" id="agentPhnNo" data-toggle="tooltip" title="Enter Sip no" class="form-control" placeholder="Sip Number"/>
                    </div>    
                    <!--                    <div class="form-group">
                                            <input type="checkbox" name="remember_me"/> Remember me
                                        </div>-->
                </div>
                <div class="footer">                                                               
                    <button type="button" id="btnLogin"  class="btn bg-olive btn-block">Sign In</button>  

                    <!--                    <p><a href="#">I forgot my password</a></p>-->

                    <!--                    <a href="register" class="text-center">Register a new membership</a>-->
                </div>
            </div>

            <!--            <div class="margin text-center">
                            <span>Sign in using social networks</span>
                            <br/>
                            <button class="btn bg-light-blue btn-circle"><i class="fa fa-facebook"></i></button>
                            <button class="btn bg-aqua btn-circle"><i class="fa fa-twitter"></i></button>
                            <button class="btn bg-red btn-circle"><i class="fa fa-google-plus"></i></button>
            
                        </div>-->
        </div>


        <!-- jQuery 2.0.2 -->
        <script src="${pageContext.request.contextPath}/resources/js/jquery-2.2.3.min.js" type="text/javascript"></script>
        <!-- Bootstrap -->
        <script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js" type="text/javascript"></script> 
        <script src="${pageContext.request.contextPath}/resources/js/loginRequest.js" type="text/javascript"></script>

        <script src="${pageContext.request.contextPath}/resources/js/notifier.js" type="text/javascript"></script>
        <script>
                            var field = 'error';
                            var url = window.location.href;
                            if (url.indexOf('?' + field + '=') != -1) {
                                //Notifier.error("Invalid workid or password")
                            }
        </script>
    </body>
</html>