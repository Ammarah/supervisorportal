<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<script src="${pageContext.request.contextPath}/resources/js/jquery-2.2.3.min.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/resources/js/plotly-latest.min.js" type="text/javascript"></script>
<style>
    .chartFont *
    {
        -webkit-font-smoothing: antialiased !important;
        -moz-osx-font-smoothing: grayscale !important;
        font-family: 'Source Sans Pro','Helvetica Neue',Helvetica,Arial,sans-serif !important;
        font-weight: 400 !important;

    }
    .chart-container {
        width: 80%;
        height: 480px;
        margin: 0 auto;
    }

    .pie-chart-container {
        height: 360px;
        width: 360px;
        float: left;
    }

    .lblOption {
        background-color: white;
        padding: 5px;
        font: 14px;
        width: 40%;
        border-radius: 4px;
    }

    .horizontalList {
        display: table; /* Allow the centering to work */
        margin: 0 auto;
    }

    ul#horizontal-list {

        list-style-type: square;
        padding-top: 20px;
    }

    ul#horizontal-list li {
        float: left;
        margin-left: 25px;
    }
    .table {
        table-layout: fixed;
    }

</style>
<section class="content">
    <div class="col-lg-12">
        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title">Call Details</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" id="btnRefereshTable">
                        <i class="fa fa-refresh"></i>
                    </button>
<!--                    <button type="button" class="btn btn-box-tool" data-widget="collapse">
                        <i class="fa fa-minus"></i>
                    </button>-->
                </div>
            </div>
            <div class="box-body" >
                <div class="form-group">
                    <label>Select Skill:</label>
                    <select class="lblOption" id="AgentOpt">
                        <c:forEach var="users" items="${SkillList.result}">
                            <option value="${users.skillId}">${users.skillName}</option>
                        </c:forEach>
                    </select>
                </div>
            </div>
            <div class="box-body">
                <table class="table table-bordered" id="realTimeCallInfo">
                    <thead id="CallCompletionHead">
                        <tr>
                            <th>Label</th>
                            <th>Value</th>
                        </tr>
                    </thead>
                    <tbody id="CallCompletionBody">
                        <tr>
                            <td>No Data Found</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <h4 style="margin-left:5px;"> Average Call Info</h4>
            <div class="box-body">
                <table class="table table-bordered" id="realTimeCallInfo">
                    <thead id="AverageCallTimeHead">
                        <tr>
                            <th>Label</th>
                            <th>Value(s)</th>
                        </tr>
                    </thead>
                    <tbody id="AverageCallTimeBody">
                        <tr>
                            <td>No Data Found</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <!-- /.box-body -->
        </div>
    </div>
    <!-- /.box -->
</section>
<!-- /.content -->
<content tag="local_script">
    <script>
        function loadCallInfo() {
            var selectedSkill = $("#AgentOpt option:selected").val();
            $.ajax({
                type: "GET",
                url: '${pageContext.request.servletContext.contextPath}/skill/skillOfcallingrate',
                data: {skillID: selectedSkill},
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    $('.cssload-loader').hide();
                    $('.overlay').hide();
                    var list = response.result[0].idxs;
                    $('#CallCompletionBody').empty();
                    var trHTML = '';
                    for (i = 0; i < list.length; i++) {
                        trHTML += '<tr><td>' + list[i].id + '</td><td>' + list[i].val + '</td></tr>';
                    }
                    $('#CallCompletionBody').append(trHTML);
                },
                error: function (xhr, status, error) {
                    console.log(status);
                }
            });
        }
        function loadAverageCallInfo() {
            var selectedSkill = $("#AgentOpt option:selected").val();
            $.ajax({
                type: "GET",
                url: '${pageContext.request.servletContext.contextPath}/skill/skillOfcallavg',
                data: {skillID: selectedSkill},
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    $('.cssload-loader').hide();
                    $('.overlay').hide();
                    var list = response.result[0].idxs;
                    $('#AverageCallTimeBody').empty();
                    var trHTML = '';
                    for (i = 0; i < list.length; i++) {
                        trHTML += '<tr><td>' + list[i].id + '</td><td>' + list[i].val + '</td></tr>';
                    }
                    $('#AverageCallTimeBody').append(trHTML);
                },
                error: function (xhr, status, error) {
                    //var err = eval("(" + xhr.responseText + ")");
                    console.log(status);
                }
            });
        }
        $(document).ready(function () {
            var pie = null;
            loadCallInfo();
            loadAverageCallInfo();
            $('#btnRefereshTable').click(function () {
                $('.cssload-loader').show();
                $('.overlay').show();
                loadCallInfo();
                loadAverageCallInfo();
            });
            $("select#AgentOpt").change(function () {
                $('.cssload-loader').show();
                $('.overlay').show();
                loadCallInfo();
                loadAverageCallInfo();
            });
        });
    </script>
</content>