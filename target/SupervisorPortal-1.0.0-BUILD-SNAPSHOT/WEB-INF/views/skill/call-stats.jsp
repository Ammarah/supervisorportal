<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<script src="${pageContext.request.contextPath}/resources/js/jquery-2.2.3.min.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/resources/js/plotly-latest.min.js" type="text/javascript"></script>
<style>
    .chartFont *
    {
        -webkit-font-smoothing: antialiased !important;
        -moz-osx-font-smoothing: grayscale !important;
        font-family: 'Source Sans Pro','Helvetica Neue',Helvetica,Arial,sans-serif !important;
        font-weight: 400 !important;

    }
    .lblOption {
        background-color: white;
        padding: 5px;
        font: 14px;
        width: 40%;
    }
    .modebar {
        display: none !important;
    }
</style>

<section class="content">
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Call Statistics</h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" id="btnRefereshTable">
                    <i class="fa fa-refresh"></i>
                </button>
<!--                <button type="button" class="btn btn-box-tool" data-widget="collapse">
                    <i class="fa fa-minus"></i>
                </button>-->

            </div>
        </div>

        <div class="box-body">
            <div class="form-group">
                <label>Select Skill:</label>
                <select class="lblOption" id="AgentOpt">
                    <c:forEach var="users" items="${SkillList.result}">
                        <option value="${users.skillId}">${users.skillName}</option>
                    </c:forEach>
                </select>
            </div>
            <div id="myDiv" class="chartFont">
                <!--<canvas id="myChart2" class="col-lg-9 col-sm-12"></canvas>-->
            </div>
            <div id="tblData" style="width:100%;">
                <table class="table-striped" style="width:100%;">
                    <thead>
                        <tr>
                            <th>Lable</th>
                            <th>Value</th>
                        </tr>
                    </thead>
                    <tbody id="dataBody"></tbody>
                </table>
            </div>
        </div>
        <!-- /.box-body -->
    </div>
</section>
<script>
    function loadCallStatBar(lblData, dataSet) {
        $('#myDiv').empty();
        var data = [{
                x: lblData,
                y: dataSet,
                type: 'bar', width: 0.3
            }];
        var layout = {
            title: 'Skill Call Stats',
            font: {
                family: 'Raleway, snas-serif'
            },
            showlegend: false,
            xaxis: {
                fixedrange: true,
                tickangle: 0
            },
            yaxis: {
                fixedrange: true,
                zeroline: false,
                gridwidth: 0.5

            },
            bargap: 0.05
        };
        Plotly.newPlot('myDiv', data, layout);

        $(".main-svg").css("width", $(window).width() * 0.80 + "px");


    }
    function loadTable(lblData, dataSet)
    {
        $('#dataBody').empty();
        var trHTML = '';
        for (var i = 0; i < lblData.length; i++) {
            trHTML += '<tr><td>' + lblData[i] + '</td><td>' + dataSet[i] + '</td></tr>';
        }
        $('#dataBody').append(trHTML);
    }
    $(document).ready(function () {
        $(window).resize(function () {
            loadCallStatsChart();
        });
        $('#tblData').hide();
        loadCallStatsChart();
        $('#btnRefereshTable').click(function () {
            $('.cssload-loader').show();
            $('.overlay').show();
            loadCallStatsChart();
        });
        $("select#AgentOpt").change(function () {
            $('.cssload-loader').show();
            $('.overlay').show();
            loadCallStatsChart();
        });
    });
    function loadCallStatsChart() {

        var selectedSkill = $("#AgentOpt option:selected").val();
        $.ajax({
            type: "GET",
            url: '${pageContext.request.servletContext.contextPath}/skill/skillOfcallstat',
            data: {skillID: selectedSkill},
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                $('.cssload-loader').hide();
                $('.overlay').hide();
                var list = response.result[0].idxs;
                var labelData = [];
                var val = [];
                var flag = true;
                for (var i = 0; i < list.length; i++) {
                    if (list[i].val != "0") {
                        flag = false;
                    }
                    labelData.push(list[i].id);
                    val.push(list[i].val);
                }
                if (flag) {
                    $('#myDiv').empty();
                    Notifier.warning('Call statistics of selected skill are not available');
                } else
                {
                    if ($(window).width() < 940)
                    {
                        $('#myDiv').hide();
                        $('#tblData').show();
                        loadTable(labelData, val);
                    } else
                    {
                        $('#tblData').hide();
                        $('#myDiv').show();
                        loadCallStatBar(labelData, val);
                    }

                }

            },
            error: function (xhr, status, error) {
                //var err = eval("(" + xhr.responseText + ")");
                console.log(status);
            }
        });
    }
</script>
