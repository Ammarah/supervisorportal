<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<script src="${pageContext.request.contextPath}/resources/js/jquery-2.2.3.min.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/resources/js/plotly-latest.min.js" type="text/javascript"></script>
<style>
    .chartFont *
    {
        -webkit-font-smoothing: antialiased !important;
        -moz-osx-font-smoothing: grayscale !important;
        font-family: 'Source Sans Pro','Helvetica Neue',Helvetica,Arial,sans-serif !important;
        font-weight: 400 !important;

    }
    .horizontalList {
        display: table; /* Allow the centering to work */
        margin: 0 auto;
    }

    ul#horizontal-list {
        list-style-type: square;
        padding-top: 20px;
    }

    ul#horizontal-list li {
        float: left;
        margin-left: 25px;
    }
</style>
<section class="content">
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Real Time Agents Stats</h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" id="btnRefereshTable">
                    <i class="fa fa-refresh"></i>
                </button>
<!--                <button type="button" class="btn btn-box-tool" data-widget="collapse">
                    <i class="fa fa-minus"></i>
                </button>-->
            </div>
        </div>
        <div class="box-body text-center">
            <div id="pieChart" class="chartFont"></div>
            <div class="horizontalList">
                <ul id="horizontal-list"></ul>
            </div>
        </div>
    </div>
</section>
<script>
    function loadPieChart() {
        $.ajax({
            type: "GET",
            url: '${pageContext.request.servletContext.contextPath}/vdn/RealTimeVdnStats',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                $('.cssload-loader').hide();
                $('.overlay').hide();
                var list = response.result[0].idxs;
                var dataSkillList = [];
                var flag = true;
                var listH = "";
                for (var i = 0; i < list.length; i++)
                {
                    if (list[i].val != "0")
                    {
                        flag = false;
                    }
                    listH += "<li>" + list[i].id + "</li>"
                    dataSkillList.push({

                        label: list[i].id,
                        value: parseInt(list[i].val)

                    });
                }
                $('#horizontal-list').empty();
                $('#horizontal-list').append(listH);
                if (flag)
                {
                    $('#pieChart').empty();
                    Notifier.warning('All values are zero');
                } else
                {
                    loadData(dataSkillList);
                }

            },
            error: function (xhr, status, error) {
                //var err = eval("(" + xhr.responseText + ")");
                //console.log(status);
            }
        });
    }
    function loadData(dataSet) {

        $('#pieChart').empty();
        // alert($('#pieChart').width());
        var fo = 0;
        var canvasWidth = 0;
        var titleFont = 0;
        if ($('#pieChart').width() < 380) {
            if ($(window).width() < 325)
            {
                fo = $(window).width() * 0.03;
                titleFont = $(window).width() * 0.04;
            } else
            {
                fo = $(window).width() * 0.02;
                titleFont = $(window).width() * 0.03;
            }


            var val = $(window).width() * 0.9;
            canvasWidth = val;
            //alert('a' + $('#pieChart').width());
            //console.log(canvasWidth + ' ' + $(window).width());
        } else if ($('#pieChart').width() > 380 && $('#pieChart').width() < 780)
        {
            titleFont = 16;
            canvasWidth = $('#pieChart').width() / 1.2;
            fo = 12;
            //alert($('#pieChart').width());
            //console.log(canvasWidth + ' s ' + $(window).width());
        } else {
            titleFont = 20;
            canvasWidth = $('#pieChart').width() / 1.2;
            fo = 14;
            // alert($('#pieChart').width());
            //console.log(canvasWidth + ' s ' + $(window).width());
        }


        var pie = new d3pie("pieChart", {
            "header": {
                "title": {
                    "text": "Agent Information",
                    "fontSize": titleFont,
                    "font": "open sans"
                },
                "subtitle": {
                    "color": "#999999",
                    "fontSize": 12,
                    "font": "open sans"
                },
                "titleSubtitlePadding": 9
            },
            "footer": {
                "color": "#999999",
                "fontSize": 10,
                "font": "open sans",
                "location": "bottom-left"
            },
            "size": {
                "canvasWidth": canvasWidth,
                "pieOuterRadius": "65%"
            },
            "data": {
                "sortOrder": "value-desc",
                "content": dataSet
            },
            "labels": {
                "outer": {
                    "pieDistance": 32
                },
                "inner": {
                    "format": "value"
                },
                "mainLabel": {
                    "fontSize": fo
                },
                "percentage": {
                    "color": "#ffffff",
                    "decimalPlaces": 0
                },
                "value": {
                    "color": "#adadad",
                    "fontSize": fo
                },
                "lines": {
                    "enabled": true,
                    "style": "straight"
                },
                "truncation": {
                    "enabled": true
                }
            },
            "tooltips": {
                "enabled": true,
                "type": "placeholder",
                "string": "{label}: {value}"
            },
            "effects": {
                "pullOutSegmentOnClick": {
                    "effect": "linear",
                    "speed": 400,
                    "size": 8
                }
            },
            "misc": {
                "gradient": {
                    "enabled": true,
                    "percentage": 100
                },
                "canvasPadding": {
                    "top": 3,
                    "right": 3,
                    "bottom": 3,
                    "left": 3
                }
            }
        });
    }
    //document.getElementById("pieChart").onresize = function () { alert('sdf'); };
    //$('#pieChart').resize(function () {
    //    loadPieChart();

    //});


    $(window).resize(function () {
        //alert($('#pieChart').width());
        loadPieChart();
        //setTimeout(loadPieChart(), 3000);
        //loadPieChart();
    });
    $(document).ready(function () {
        $('#btnRefereshTable').click(function () {
            $('.cssload-loader').show();
            $('.overlay').show();
            loadPieChart();
        });
        loadPieChart();
    });
</script>