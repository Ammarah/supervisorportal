<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<script src="${pageContext.request.contextPath}/resources/js/jquery-2.2.3.min.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/resources/js/plotly-latest.min.js" type="text/javascript"></script>
<style>
    .chartFont *
    {
        -webkit-font-smoothing: antialiased !important;
        -moz-osx-font-smoothing: grayscale !important;
        font-family: 'Source Sans Pro','Helvetica Neue',Helvetica,Arial,sans-serif !important;
        font-weight: 400 !important;

    }
    .modebar {
        display: none !important;
    }
</style>
<section class="content">
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Outgoing Call Stats</h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" id="btnRefereshTable">
                    <i class="fa fa-refresh"></i>
                </button>
<!--                <button type="button" class="btn btn-box-tool" data-widget="collapse">
                    <i class="fa fa-minus"></i>
                </button>-->
            </div>
        </div>
        <div class="row text-center no-padding">
            <div id="myDiv" class="chartFont">
            </div>
        </div>
    </div>
</section>
<script>
    function loadCallStatBar(lblData, dataSet) {
        $('#myDiv').empty();
        var data = [{
                x: lblData,
                y: dataSet,
                type: 'bar'
//                type: 'bar', width: 0.3
            }];
        var layout = {
            title: 'VDN Outgoing Call Stats',
            font: {
                family: 'Raleway, snas-serif'
            },
            showlegend: false,
            xaxis: {
                fixedrange: true,
                tickangle: 0
            },
            yaxis: {
                yaxis: {range: [0, 100]},
                zeroline: false,
                fixedrange: true,
                gridwidth: 0.5

            },
            bargap: 0.05
        };
        Plotly.newPlot('myDiv', data, layout);
        $(".main-svg").css("width", $(window).width() * 0.80 + "px");
    }
    $(document).ready(function () {
        $(window).resize(function () {
            $(".main-svg").css("width", $(window).width() * 0.80 + "px");
            Plotly.Plots.resize(myDiv);
        });
        loadCallStatsChart();
        $('#btnRefereshTable').click(function () {
            $('.cssload-loader').show();
            $('.overlay').show();
            loadCallStatsChart();
        });

    });
    function loadCallStatsChart() {

        $.ajax({
            type: "GET",
            url: '${pageContext.request.servletContext.contextPath}/vdn/HistoricalVDNOutGoingCallStats',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                $('.cssload-loader').hide();
                $('.overlay').hide();
                var list = response.result[0].idxs;
                var labelData = [];
                var val = [];
                var flag = true;
                var maxVal = 0;
                for (i = 0; i < list.length; i++) {
                    if (list[i].val != "0") {
                        flag = false;
                    }
                    labelData.push(list[i].id);
                    if (list[i].val == "null")
                    {
                        val.push(0);
                    } else
                    {
                        val.push(parseInt(list[i].val));
                    }

                    //dataSkillList.push({

                    //    label: list[i].id,
                    //    value: parseInt(list[i].val)

                    //});
                }
                if (flag) {
                    $('#myChart2').empty();
                    Notifier.warning('No Data Found');
                } else {
                    loadCallStatBar(labelData, val);
                }

            },
            error: function (xhr, status, error) {
                //var err = eval("(" + xhr.responseText + ")");
                console.log(status);
            }
        });
    }
</script>