<%-- 
    Document   : dashboard
    Created on : Jan 5, 2018, 3:13:36 PM
    Author     : Abdul Rehman
--%>
<%@page import="java.util.concurrent.TimeUnit"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.Date"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="windows-1252"%>
<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
        <title>Supervisor Portal | Dashboard</title>
        <script src="${pageContext.request.contextPath}/resources/js/jquery-2.2.3.min.js" type="text/javascript"></script>
        <style>
            .btn.disabled, .btn[disabled], fieldset[disabled] .btn {
                opacity: 3.65 !important; 
            }
            .btn-default{
                opacity: 0.65 !important; 
            }

            .lblOption {
                background-color: white;
                padding: 5px;
                font: 14px;
                width: 40%;
            }
            .overlay1 {
                background-color: #022158;
                display: block;
                position: fixed;
                top: 0;
                right: 0;
                bottom: 0;
                left: 0;
                opacity: 0.5;
                z-index: 3;
            }
            input[type=number]::-webkit-inner-spin-button, 
            input[type=number]::-webkit-outer-spin-button { 
                -webkit-appearance: none; 
            }
            .btn
            {
                padding-right:6px;
            }
        </style>
    </head>
    <body>
        <div class="overlay1" style="display:none;"></div>
        <div class="cssload-loader1" style="display:none;" >
            <div class="cssload-side"></div>
            <div class="cssload-side"></div>
            <div class="cssload-side"></div>
            <div class="cssload-side"></div>
            <div class="cssload-side"></div>
            <div class="cssload-side"></div>
            <div class="cssload-side"></div>
            <div class="cssload-side"></div>
        </div>
        <section class="content-header">
            <h1>
                Dashboard
            </h1>        
            <div class="row" style="margin-top:2%;">

                <div class="col-md-12 col-sm-12 col-xs-12 form-inline">
                    <div class="dropdown col-md-2 col-sm-12 col-xs-12">
                        <select id="filterType" class="btn btn-primary" style="padding-left: 20%; padding-right: 20%;">
                            <option value="-">Filter Type</option>
                            <option value="AgentId">Agent ID</option>  
                            <option value="AgentName">Agent Name</option>
                            <option value="AgentStatus">Agent Status</option>
                        </select>
                    </div>
                    <div class="dropdown col-md-3 col-sm-12 col-xs-12" id="search-div" style="display: none;" >
                        <span class="fa fa-search form-control-feedback" style="margin-right:83%;"></span>                           
                        <input id="srchAgent" type="text" class="form-control" placeholder="  Search" style="padding-left:10%; width:100%;" />
                    </div>
                    <div id="status-div" class="dropdown col-md-3 col-sm-12 col-xs-12" style="display: none;" >
                        <select id="agentState" class="btn btn-primary" style="padding-left: 25%; padding-right: 25%;">
                            <option value=-1>All Agents</option>
                            <option value=0>Sign Off</option>
                            <option value=2>Ready</option>
                            <option value=3>Not Ready</option>
                            <option value=27>Work</option>
                            <option value=5>Answering</option>
                            <option value=4>Talking</option>
                            <option value=26>Break</option>
                            <option value=13>Transferred to IVR</option>
                            <option value=30>On Hold</option>

                        </select>
                    </div>
                    <div id="submit-div" class="dropdown col-md-4 col-sm-12 col-xs-12" style="margin-left:25%;display: none;">
                        <button type="button" title="Submit" class="btn btn-primary" id="submit-search" onclick="submitSearch();" style="padding-left: 15%; padding-right: 15%; margin-right: 3%;"> Submit</button>
                        <button type="button" title="Reset" class="btn btn-primary" id="reset" onclick="reset();"  style="padding-left: 15%; padding-right: 15%;"> Reset</button>
                    </div>
                </div>



            </div>               
        </section>
        <section class="content" id="divs_agents">
            <c:if test="${not empty agentList}">
                <ul>
                    <c:forEach var="agent" items="${agentList}">
                        <c:set var="te" value="${agent.currentStateTime}" />
                        <%
                            Date now = new Date(Long.parseLong(pageContext.getAttribute("te").toString()));
                            SimpleDateFormat formate1 = new SimpleDateFormat("HH:mm:ss");
                            formate1.format(now);
                            pageContext.setAttribute("formate", formate1.format(now));

                        %>
                        <c:if test = "${agent.status == 0}">
                            <div class="col-md-6 col-sm-12 col-xs-12">
                                <div class="info-box bg-red-active" id="divbg${agent.agentId}">
                                    <span class="info-box-icon">
                                        <!--User-Image.png-->
                                        <img src="<c:url value="/resources/img/profileimage.png"/>" class="img-circle" style="padding:10px;" alt="User Image">
                                    </span>
                                    <div class="info-box-content">
                                        <div style="text-align:right;width:100%">
                                            <span style="float:left" class="info-box-text">${agent.agentId}</span>
                                            ${agent.agentName}                   
                                        </div>
                                        <span class="info-box-text pull-right" id="timeAgent${agent.agentId}">
                                            <%--<c:out value="${formate}"/>--%>
                                            <c:out value="00:00:00"/>
                                        </span>
                                        <span class="info-box-number" id="${agent.agentId}">Sign Off</span>
                                        <div class="btn-group">
                                            <button type="button" title="Skill Adjustment" id="skillAdj${agent.agentId}" onclick="AdjustAgentSkill(this);" class="btn btn-default disabled" disabled="disabled"><i class="fa fa-wrench  "></i></button>
                                            <button type="button" title="Statistics" id="stat${agent.agentId}" onclick="getAgentStats(this);" class="btn btn-default disabled" disabled="disabled" ><i class="fa fa-info  "></i></button>
                                            <button type="button" title="Ready" id="idle${agent.agentId}" onclick="setAgentIdle(this);" class="btn btn-default disabled" disabled="disabled"><i class="fa fa-user-circle fa-align-center"></i></button>
                                            <button type="button" title="Break" id="res${agent.agentId}" onclick="setAgentRest(this);" class="btn btn-default disabled"><i class="fa fa-hourglass  "></i></button>
                                            <button type="button" title="Not Ready" id="bus${agent.agentId}" onclick="setAgentBusy(this);" class="btn btn-default disabled"><i class="fa fa-minus-circle  "></i></button>
                                            <button type="button" title="Listening" id="Listen${agent.agentId}" onclick="setAgentListening(this);" class="btn btn-default disabled" disabled="disabled"><i class="fa fa-headphones"></i></button>
                                            <button type="button" title="Shadowing" id="Insert${agent.agentId}" onclick="setAgentInserting(this);" class="btn btn-default disabled" disabled="disabled"><i class="fa fa-tty"></i></button>
                                            <button type="button" title="Stop Listening/Shadowing" id="stop${agent.agentId}" onclick="setAgentStopListeningInserting(this);" class="btn btn-default disabled" disabled="disabled"><i class="fa fa-stop-circle-o  "></i></button>
                                            <button type="button" title="Logout" id="log${agent.agentId}" onclick="setAgentLogout(this);" class="btn btn-default disabled"><i class="fa  fa-sign-out  "></i></button>
                                            <!--</div>-->
                                        </div>
                                    </div>
                                    <!-- /.info-box-content -->
                                </div>
                                <!-- /.info-box -->
                            </div>
                        </c:if>
                        <c:if test = "${agent.status == 2}">

                            <div class="col-md-6 col-sm-12 col-xs-12">
                                <div class="info-box bg-green-active" id="divbg${agent.agentId}">
                                    <span class="info-box-icon"><img src="${pageContext.request.contextPath}/resources/img/profileimage.png" class="img-circle" style="padding:10px;" alt="User Image"></span>
                                    <div class="info-box-content">
                                        <div style="text-align:right;width:100%">
                                            <span style="float:left" class="info-box-text">${agent.agentId}</span>
                                            ${agent.agentName}                   
                                        </div>                                        <span class="info-box-text pull-right" id="timeAgent${agent.agentId}">
                                            <%--<c:out value="${formate}"/>--%>                                            <c:out value="00:00:00"/>
                                        </span>
                                        <span class="info-box-number" id="${agent.agentId}">Ready</span>
                                        <div class="btn-group">
                                            <button type="button" title="Skill Adjustment" id="skillAdj${agent.agentId}" onclick="AdjustAgentSkill(this);" class="btn btn-default" ><i class="fa fa-wrench  "></i></button>
                                            <button type="button" title="Statistics" id="stat${agent.agentId}" onclick="getAgentStats(this);" class="btn btn-default"><i class="fa fa-info  "></i></button>
                                            <button type="button" title="Ready" id="idle${agent.agentId}" onclick="setAgentIdle(this);" class="btn btn-default disabled"><i class="fa fa-user-circle fa-align-center"></i></button>
                                            <button type="button" title="Break" id="res${agent.agentId}" onclick="setAgentRest(this);" class="btn btn-default "><i class="fa fa-hourglass"></i></button>
                                            <button type="button" title="Not Ready" id="bus${agent.agentId}" onclick="setAgentBusy(this);" class="btn btn-default "><i class="fa fa-minus-circle  "></i></button>
                                            <button type="button" title="Listening" id="Listen${agent.agentId}" onclick="setAgentListening(this);" class="btn btn-default disabled" disabled="disabled"><i class="fa fa-headphones"></i></button>
                                            <button type="button" title="Shadowing" id="Insert${agent.agentId}" onclick="setAgentInserting(this);" class="btn btn-default disabled" disabled="disabled"><i class="fa  fa-tty"></i></button>
                                            <button type="button" title="Stop Listening/Shadowing" id="stop${agent.agentId}" onclick="setAgentStopListeningInserting(this);" class="btn btn-default disabled" disabled="disabled"><i class="fa fa-stop-circle-o  "></i></button>
                                            <button type="button" title="Logout" id="log${agent.agentId}" onclick="setAgentLogout(this);" class="btn btn-default "><i class="fa  fa-sign-out  "></i></button>
                                        </div>

                                    </div>
                                    <!-- /.info-box-content -->
                                </div>
                                <!-- /.info-box -->
                            </div>
                        </c:if>
                        <c:if test = "${agent.status == 3}">
                            <div class="col-md-6 col-sm-12 col-xs-12">
                                <div class="info-box bg-blue-active" id="divbg${agent.agentId}">
                                    <span class="info-box-icon"><img src="${pageContext.request.contextPath}/resources/img/profileimage.png" class="img-circle" style="padding:10px;" alt="User Image"></span>
                                    <div class="info-box-content">
                                        <div style="text-align:right;width:100%">
                                            <span style="float:left" class="info-box-text">${agent.agentId}</span>
                                            ${agent.agentName}                   
                                        </div>       
                                        <span class="info-box-text pull-right" id="timeAgent${agent.agentId}">
                                            <%--<c:out value="${formate}"/>--%>                                            <c:out value="00:00:00"/>
                                        </span>

                                        <span class="info-box-number" id="${agent.agentId}">Not Ready</span>
                                        <!--<span class="info-box-number" id="@d.agentId">Not Ready</span>-->
                                        <div class="btn-group">
                                            <button type="button" title="Skill Adjustment" id="skillAdj${agent.agentId}" onclick="AdjustAgentSkill(this);" class="btn btn-default"><i class="fa fa-wrench  "></i></button>
                                            <button type="button" title="Statistics" id="stat${agent.agentId}" onclick="getAgentStats(this);" class="btn btn-default"><i class="fa fa-info  "></i></button>
                                            <button type="button" title="Ready" id="idle${agent.agentId}" onclick="setAgentIdle(this);" class="btn btn-default "><i class="fa fa-user-circle fa-align-center"></i></button>
                                            <button type="button" title="Break" id="res${agent.agentId}" onclick="setAgentRest(this);" class="btn btn-default "><i class="fa fa-hourglass  "></i></button>
                                            <button type="button" title="Not Ready" id="bus${agent.agentId}" onclick="setAgentBusy(this);" class="btn btn-default disabled"><i class="fa fa-minus-circle  "></i></button>
                                            <button type="button" title="Listening" id="Listen${agent.agentId}" onclick="setAgentListening(this);" class="btn btn-default disabled" disabled="disabled"><i class="fa fa-headphones"></i></button>
                                            <button type="button" title="Shadowing" id="Insert${agent.agentId}" onclick="setAgentInserting(this);" class="btn btn-default disabled" disabled="disabled"><i class="fa  fa-tty"></i></button>
                                            <button type="button" title="Stop Listening/Shadowing" id="stop${agent.agentId}" onclick="setAgentStopListeningInserting(this);" class="btn btn-default disabled" disabled="disabled"><i class="fa fa-stop-circle-o  "></i></button>
                                            <button type="button" title="Logout" id="log${agent.agentId}" onclick="setAgentLogout(this);" class="btn btn-default "><i class="fa  fa-sign-out  "></i></button></div>

                                    </div>
                                    <!-- /.info-box-content -->
                                </div>
                                <!-- /.info-box -->
                            </div>
                        </c:if>
                        <c:if test = "${agent.status == 27}">
                            <div class="col-md-6 col-sm-12 col-xs-12">
                                <div class="info-box bg-olive-active" id="divbg${agent.agentId}">
                                    <span class="info-box-icon"><img src="${pageContext.request.contextPath}/resources/img/profileimage.png" class="img-circle" style="padding:10px;" alt="User Image"></span>
                                    <div class="info-box-content">
                                        <div style="text-align:right;width:100%">
                                            <span style="float:left" class="info-box-text">${agent.agentId}</span>
                                            ${agent.agentName}                   
                                        </div>
                                        <span class="info-box-text pull-right" id="timeAgent${agent.agentId}">
                                            <%--<c:out value="${formate}"/>--%>                                            <c:out value="00:00:00"/>
                                        </span>
                                        <span class="info-box-number" id="${agent.agentId}">Work</span>
                                        <div class="btn-group">
                                            <button type="button" title="Skill Adjustment" id="skillAdj${agent.agentId}" onclick="AdjustAgentSkill(this);" class="btn btn-default"><i class="fa fa-wrench  "></i></button>
                                            <button type="button" title="Statistics" id="stat${agent.agentId}" onclick="getAgentStats(this);" class="btn btn-default"><i class="fa fa-info  "></i></button>
                                            <button type="button" title="Ready" id="idle${agent.agentId}" onclick="setAgentIdle(this);" class="btn btn-default "><i class="fa fa-user-circle fa-align-center"></i></button>
                                            <button type="button" title="Break" id="res${agent.agentId}" onclick="setAgentRest(this);" class="btn btn-default disabled"><i class="fa fa-hourglass  "></i></button>
                                            <button type="button" title="Not Ready" id="bus${agent.agentId}" onclick="setAgentBusy(this);" class="btn btn-default "><i class="fa fa-minus-circle  "></i></button>
                                            <button type="button" title="Listening" id="Listen${agent.agentId}" onclick="setAgentListening(this);" class="btn btn-default disabled" disabled="disabled"><i class="fa fa-headphones"></i></button>
                                            <button type="button" title="Shadowing" id="Insert${agent.agentId}" onclick="setAgentInserting(this);" class="btn btn-default disabled" disabled="disabled"><i class="fa fa-tty"></i></button>
                                            <button type="button" title="Stop Listening/Shadowing" id="stop${agent.agentId}" onclick="setAgentStopListeningInserting(this);" class="btn btn-default disabled" disabled="disabled"><i class="fa fa-stop-circle-o  "></i></button>
                                            <button type="button" title="Logout" id="log${agent.agentId}" onclick="setAgentLogout(this);" class="btn btn-default "><i class="fa  fa-sign-out  "></i></button>
                                        </div>

                                    </div>
                                    <!-- /.info-box-content -->
                                </div>
                                <!-- /.info-box -->
                            </div>
                        </c:if>
                        <c:if test = "${agent.status == 5}">
                            <div class="col-md-6 col-sm-12 col-xs-12">
                                <div class="info-box bg-orange" id="divbg${agent.agentId}">
                                    <span class="info-box-icon"><img src="${pageContext.request.contextPath}/resources/img/profileimage.png" class="img-circle" style="padding:10px;" alt="User Image"></span>
                                    <div class="info-box-content">
                                        <div style="text-align:right;width:100%">
                                            <span style="float:left" class="info-box-text">${agent.agentId}</span>
                                            ${agent.agentName}                   
                                        </div>
                                        <span class="info-box-text pull-right" id="timeAgent${agent.agentId}">
                                            <%--<c:out value="${formate}"/>--%>                                            <c:out value="00:00:00"/>
                                        </span>
                                        <span class="info-box-number" id="${agent.agentId}">Answering</span>
                                        <div class="btn-group">
                                            <button type="button" title="Skill Adjustment" id="skillAdj${agent.agentId}" onclick="AdjustAgentSkill(this);" class="btn btn-default"><i class="fa fa-wrench  "></i></button>
                                            <button type="button" title="Statistics" id="stat${agent.agentId}" onclick="getAgentStats(this);" class="btn btn-default"><i class="fa fa-info  "></i></button>
                                            <button type="button" title="Ready" id="idle${agent.agentId}" onclick="setAgentIdle(this);" class="btn btn-default "><i class="fa fa-user-circle fa-align-center"></i></button>
                                            <button type="button" title="Break" id="res${agent.agentId}" onclick="setAgentRest(this);" class="btn btn-default "><i class="fa fa-hourglass  "></i></button>
                                            <button type="button" title="Not Ready" id="bus${agent.agentId}" onclick="setAgentBusy(this);" class="btn btn-default "><i class="fa fa-minus-circle  "></i></button>
                                            <button type="button" title="Listening" id="Listen${agent.agentId}" onclick="setAgentListening(this);" class="btn btn-default"><i class="fa fa-headphones"></i></button>
                                            <button type="button" title="Shadowing" id="Insert${agent.agentId}" onclick="setAgentInserting(this);" class="btn btn-default"><i class="fa fa-tty"></i></button>
                                            <button type="button" title="Stop Listening/Shadowing" id="stop${agent.agentId}" onclick="setAgentStopListeningInserting(this);" class="btn btn-default disabled" disabled="disabled"><i class="fa fa-stop-circle-o  "></i></button>
                                            <button type="button" title="Logout" id="log${agent.agentId}" onclick="setAgentLogout(this);" class="btn btn-default "><i class="fa  fa-sign-out  "></i></button>
                                        </div>

                                    </div>
                                    <!-- /.info-box-content -->
                                </div>
                                <!-- /.info-box -->
                            </div>
                        </c:if>
                        <c:if test = "${agent.status == 4}">

                            <div class="col-md-6 col-sm-12 col-xs-12">
                                <div class="info-box bg-orange-active" id="divbg${agent.agentId}">
                                    <span class="info-box-icon"><img src="${pageContext.request.contextPath}/resources/img/profileimage.png" class="img-circle" style="padding:10px;" alt="User Image"></span>
                                    <div class="info-box-content">
                                        <div style="text-align:right;width:100%">
                                            <span style="float:left" class="info-box-text">${agent.agentId}</span>
                                            ${agent.agentName}                   
                                        </div>
                                        <span class="info-box-text pull-right" id="timeAgent${agent.agentId}">
                                            <%--<c:out value="${formate}"/>--%>                                            <c:out value="00:00:00"/>
                                        </span>
                                        <span class="info-box-number" id="${agent.agentId}">Talking</span>
                                        <div class="btn-group">
                                            <button type="button" title="Skill Adjustment" id="skillAdj${agent.agentId}" onclick="AdjustAgentSkill(this);" class="btn btn-default"><i class="fa fa-wrench  "></i></button>
                                            <button type="button" title="Statistics" id="stat${agent.agentId}" onclick="getAgentStats(this);" class="btn btn-default"><i class="fa fa-info  "></i></button>
                                            <button type="button" title="Ready" id="idle${agent.agentId}" onclick="setAgentIdle(this);" class="btn btn-default "><i class="fa fa-user-circle fa-align-center"></i></button>
                                            <button type="button" title="Break" id="res${agent.agentId}" onclick="setAgentRest(this);" class="btn btn-default "><i class="fa fa-hourglass  "></i></button>
                                            <button type="button" title="Not Ready" id="bus${agent.agentId}" onclick="setAgentBusy(this);" class="btn btn-default "><i class="fa fa-minus-circle  "></i></button>
                                            <button type="button" title="Listening" id="Listen${agent.agentId}" onclick="setAgentListening(this);" class="btn btn-default"><i class="fa fa-headphones"></i></button>
                                            <button type="button" title="Shadowing" id="Insert${agent.agentId}" onclick="setAgentInserting(this);" class="btn btn-default"><i class="fa fa-tty"></i></button>
                                            <button type="button" title="Stop Listening/Shadowing" id="stop${agent.agentId}" onclick="setAgentStopListeningInserting(this);" class="btn btn-default disabled" disabled="disabled"><i class="fa fa-stop-circle-o  "></i></button>
                                            <button type="button" title="Logout" id="log${agent.agentId}" onclick="setAgentLogout(this);" class="btn btn-default "><i class="fa  fa-sign-out  "></i></button>
                                        </div>

                                    </div>
                                    <!-- /.info-box-content -->
                                </div>
                                <!-- /.info-box -->
                            </div>
                        </c:if>
                        <c:if test = "${agent.status == 26}">

                            <div class="col-md-6 col-sm-12 col-xs-12">
                                <div class="info-box bg-yellow-active" id="divbg${agent.agentId}">
                                    <span class="info-box-icon">
                                        <img src="${pageContext.request.contextPath}/resources/img/profileimage.png" class="img-circle" style="padding:10px;" alt="User Image"></span>
                                    <div class="info-box-content">
                                        <div style="text-align:right;width:100%">
                                            <span style="float:left" class="info-box-text">${agent.agentId}</span>
                                            ${agent.agentName}                   
                                        </div>
                                        <span class="info-box-text pull-right" id="timeAgent${agent.agentId}">
                                            <%--<c:out value="${formate}"/>--%>                                            <c:out value="00:00:00"/>
                                        </span>
                                        <span class="info-box-number" id="${agent.agentId}">Break</span>
                                        <div class="btn-group">
                                            <button type="button" title="Skill Adjustment" id="skillAdj${agent.agentId}" onclick="AdjustAgentSkill(this);" class="btn btn-default"><i class="fa fa-wrench  "></i></button>
                                            <button type="button" title="Statistics" id="stat${agent.agentId}" onclick="getAgentStats(this);" class="btn btn-default"><i class="fa fa-info  "></i></button>
                                            <button type="button" title="Ready" id="idle${agent.agentId}" onclick="setAgentIdle(this);" class="btn btn-default "><i class="fa fa-user-circle fa-align-center"></i></button>
                                            <button type="button" title="Break" id="res${agent.agentId}" onclick="setAgentRest(this);" class="btn btn-default disabled" disabled="disabled"><i class="fa fa-hourglass  "></i></button>
                                            <button type="button" title="Not Ready" id="bus${agent.agentId}" onclick="setAgentBusy(this);" class="btn btn-default "><i class="fa fa-minus-circle  "></i></button>
                                            <button type="button" title="Listening" id="Listen${agent.agentId}" onclick="setAgentListening(this);" class="btn btn-default disabled" disabled="disabled"><i class="fa fa-headphones"></i></button>
                                            <button type="button" title="Shadowing" id="Insert${agent.agentId}" onclick="setAgentInserting(this);" class="btn btn-default disabled" disabled="disabled"><i class="fa fa-tty"></i></button>
                                            <button type="button" title="Stop Listening/Shadowing" id="stop${agent.agentId}" onclick="setAgentStopListeningInserting(this);" class="btn btn-default disabled" disabled="disabled"><i class="fa fa-stop-circle-o  "></i></button>
                                            <button type="button" title="Logout" id="log${agent.agentId}" onclick="setAgentLogout(this);" class="btn btn-default "><i class="fa  fa-sign-out  "></i></button>
                                        </div>
                                    </div>
                                    <!-- /.info-box-content -->
                                </div>
                                <!-- /.info-box -->
                            </div>
                        </c:if>
                        <c:if test = "${agent.status == 13}">

                            <div class="col-md-6 col-sm-12 col-xs-12">
                                <div class="info-box bg-aqua-active" id="divbg${agent.agentId}">
                                    <span class="info-box-icon">
                                        <img src="${pageContext.request.contextPath}/resources/img/profileimage.png" class="img-circle" style="padding:10px;" alt="User Image"></span>
                                    <div class="info-box-content">
                                        <div style="text-align:right;width:100%">
                                            <span style="float:left" class="info-box-text">${agent.agentId}</span>
                                            ${agent.agentName}                   
                                        </div>
                                        <span class="info-box-text pull-right" id="timeAgent${agent.agentId}">
                                            <%--<c:out value="${formate}"/>--%>                                            <c:out value="00:00:00"/>
                                        </span>
                                        <span class="info-box-number" id="${agent.agentId}">Transferred to IVR</span>
                                        <div class="btn-group">
                                            <button type="button" title="Skill Adjustment" id="skillAdj${agent.agentId}" onclick="AdjustAgentSkill(this);" class="btn btn-default"><i class="fa fa-wrench  "></i></button>
                                            <button type="button" title="Statistics" id="stat${agent.agentId}" onclick="getAgentStats(this);" class="btn btn-default"><i class="fa fa-info  "></i></button>
                                            <button type="button" title="Ready" id="idle${agent.agentId}" onclick="setAgentIdle(this);" class="btn btn-default "><i class="fa fa-user-circle fa-align-center"></i></button>
                                            <button type="button" title="Break" id="res${agent.agentId}" onclick="setAgentRest(this);" class="btn btn-default disabled" disabled="disabled"><i class="fa fa-hourglass  "></i></button>
                                            <button type="button" title="Not Ready" id="bus${agent.agentId}" onclick="setAgentBusy(this);" class="btn btn-default "><i class="fa fa-minus-circle  "></i></button>
                                            <button type="button" title="Listening" id="Listen${agent.agentId}" onclick="setAgentListening(this);" class="btn btn-default disabled" disabled="disabled"><i class="fa fa-headphones"></i></button>
                                            <button type="button" title="Shadowing" id="Insert${agent.agentId}" onclick="setAgentInserting(this);" class="btn btn-default disabled" disabled="disabled"><i class="fa fa-tty"></i></button>
                                            <button type="button" title="Stop Listening/Shadowing" id="stop${agent.agentId}" onclick="setAgentStopListeningInserting(this);" class="btn btn-default disabled" disabled="disabled"><i class="fa fa-stop-circle-o  "></i></button>
                                            <button type="button" title="Logout" id="log${agent.agentId}" onclick="setAgentLogout(this);" class="btn btn-default "><i class="fa  fa-sign-out  "></i></button>
                                        </div>
                                    </div>
                                    <!-- /.info-box-content -->
                                </div>
                                <!-- /.info-box -->
                            </div>
                        </c:if>
                        <c:if test = "${agent.status == 30}">

                            <div class="col-md-6 col-sm-12 col-xs-12">
                                <div class="info-box bg-purple-active" id="divbg${agent.agentId}">
                                    <span class="info-box-icon">
                                        <img src="${pageContext.request.contextPath}/resources/img/profileimage.png" class="img-circle" style="padding:10px;" alt="User Image"></span>
                                    <div class="info-box-content">
                                        <div style="text-align:right;width:100%">
                                            <span style="float:left" class="info-box-text">${agent.agentId}</span>
                                            ${agent.agentName}                   
                                        </div>
                                        <span class="info-box-text pull-right" id="timeAgent${agent.agentId}">
                                            <%--<c:out value="${formate}"/>--%>                                            <c:out value="00:00:00"/>
                                        </span>
                                        <span class="info-box-number" id="${agent.agentId}">On Hold</span>
                                        <div class="btn-group">
                                            <button type="button" title="Skill Adjustment" id="skillAdj${agent.agentId}" onclick="AdjustAgentSkill(this);" class="btn btn-default"><i class="fa fa-wrench  "></i></button>
                                            <button type="button" title="Statistics" id="stat${agent.agentId}" onclick="getAgentStats(this);" class="btn btn-default"><i class="fa fa-info  "></i></button>
                                            <button type="button" title="Ready" id="idle${agent.agentId}" onclick="setAgentIdle(this);" class="btn btn-default "><i class="fa fa-user-circle fa-align-center"></i></button>
                                            <button type="button" title="Break" id="res${agent.agentId}" onclick="setAgentRest(this);" class="btn btn-default disabled" disabled="disabled"><i class="fa fa-hourglass  "></i></button>
                                            <button type="button" title="Not Ready" id="bus${agent.agentId}" onclick="setAgentBusy(this);" class="btn btn-default "><i class="fa fa-minus-circle  "></i></button>
                                            <button type="button" title="Listening" id="Listen${agent.agentId}" onclick="setAgentListening(this);" class="btn btn-default disabled" disabled="disabled"><i class="fa fa-headphones"></i></button>
                                            <button type="button" title="Shadowing" id="Insert${agent.agentId}" onclick="setAgentInserting(this);" class="btn btn-default disabled" disabled="disabled"><i class="fa fa-tty"></i></button>
                                            <button type="button" title="Stop Listening/Shadowing" id="stop${agent.agentId}" onclick="setAgentStopListeningInserting(this);" class="btn btn-default disabled" disabled="disabled"><i class="fa fa-stop-circle-o  "></i></button>
                                            <button type="button" title="Logout" id="log${agent.agentId}" onclick="setAgentLogout(this);" class="btn btn-default "><i class="fa  fa-sign-out  "></i></button>
                                        </div>
                                    </div>
                                    <!-- /.info-box-content -->
                                </div>
                                <!-- /.info-box -->
                            </div>
                        </c:if>
                    </c:forEach>
                </ul>

            </c:if>



        </section>
        <div class="modal fade"  role="dialog" id="statPopUp">
            <div class="modal-dialog">
                <div class="modal-content" style="border-radius:3px; -webkit-border-radius:3px; -moz-border-radius:3px;">

                    <div class="modal-body">
                        <div class="box" style="margin-bottom:5px;">
                            <div class="box-header with-border">
                                <h3 class="box-title">Agent Statistics</h3>
                                <div class="box-tools pull-right">
                                    <button type="button" id="btnRefreshStats" class="btn btn-box-tool">
                                        <i class="fa fa-refresh"></i>
                                    </button>
                                    <button type="button" class="btn btn-box-tool" data-dismiss="modal" aria-label="Close">
                                        <i class="fa fa-times"></i>
                                    </button>
                                </div>

                            </div>

                            <!-- /.box-header -->
                            <div class="box-body no-padding" style="height:70vh; overflow:scroll;">
                                <table class="table table-striped">
                                    <thead id="agentStatsTableHead">
                                        <tr>
                                            <th >Title</th>
                                            <th>Value</th>
                                        </tr>
                                    </thead>
                                    <tbody id="agentStatsTableBody">
                                        <tr>
                                            <td>No data found</td>

                                        </tr>

                                    </tbody>
                                </table>
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>
                    <!--                    <div class="modal-footer" style="padding:5px;">
                                            <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
                                            @*<button type="button" class="btn btn-primary">Save changes</button>*@
                                        </div>-->
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <div class="modal fade" role="dialog" id="skillAdjustPopUp">
            <div class="modal-dialog">
                <div class="modal-content" style="border-radius:3px; -webkit-border-radius:3px; -moz-border-radius:3px;">

                    <div class="modal-body">
                        <div class="box" style="margin-bottom:5px;">
                            <div class="box-header with-border">
                                <h3 class="box-title">Agent Skill Adjustment</h3>
                                <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-box-tool" data-dismiss="modal" aria-label="Close">
                                        <i class="fa fa-times"></i>
                                    </button>
                                </div>
                            </div>
                            <!-- /.box-header -->
                            <div class="row" id="popUpData">
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>
                    <div class="modal-footer" style="padding:5px;">
                        <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>&nbsp;
                        <button type="button" class="btn btn-primary" id="btnSaveAgentAdjustSkills"> Save</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <div class="modal fade" role="dialog" id="setBreakTimePopup">
            <div class="modal-dialog">
                <div class="modal-content" style="border-radius:3px; -webkit-border-radius:3px; -moz-border-radius:3px;">
                    <div class="modal-body">
                        <div class="box" style="margin-bottom:5px;">
                            <div class="box-header with-border">
                                <h3 class="box-title">Set Break Time</h3>
                                <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-box-tool" data-dismiss="modal" aria-label="Close">
                                        <i class="fa fa-times"></i>
                                    </button>
                                </div>
                            </div>
                            <!-- /.box-header -->
                            <div class="modal-body">
                                <div class="row" id="popBreak">
                                    <div class="form-group">
                                        <label for="restBrkOption">Select Break Reason</label>
                                        <select  class="form-control" id="restBrkOption">
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="breakTime">Break Time (Min.)</label>
                                        <input type="number" min="1"  class="form-control" placeholder="Break Time in Mintues" class="form-control" id="breakTime">
                                    </div>


                                </div>
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>
                    <div class="modal-footer" style="padding:5px;">
                        <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>&nbsp;
                        <button type="button" class="btn btn-primary" id="btnSetBreak"> OK</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.Bootstrap Loader -->

        <!--        <div class="animationload">
                    <div class="osahanloading"></div>
                </div>-->
        <!--        <style>
                    .animationload {
                        background-color: #222d32c4;
                        height: 100%;
                        left: 0;
                        position: fixed;
                        top: 0;
                        width: 100%;
                        z-index: 10000;
                    }
                    .osahanloading {
                        animation: 1.5s linear 0s normal none infinite running osahanloading;
                        background: #fed37f none repeat scroll 0 0;
                        border-radius: 50px;
                        height: 50px;
                        left: 50%;
                        margin-left: -25px;
                        margin-top: -25px;
                        position: absolute;
                        top: 50%;
                        width: 50px;
                    }
                    .osahanloading::after {
                        animation: 1.5s linear 0s normal none infinite running osahanloading_after;
                        border-color: #85d6de transparent;
                        border-radius: 80px;
                        border-style: solid;
                        border-width: 10px;
                        content: "";
                        height: 80px;
                        left: -15px;
                        position: absolute;
                        top: -15px;
                        width: 80px;
                    }
                    @keyframes osahanloading {
                        0% {
                            transform: rotate(0deg);
                        }
                        50% {
                            background: #85d6de none repeat scroll 0 0;
                            transform: rotate(180deg);
                        }
                        100% {
                            transform: rotate(360deg);
                        }
                    }
        
                </style>-->
        <script language="javascript" type="text/javascript">
            $('#filterType').change(function () {
                if ($(this).val() == "AgentId") {
                    $('#status-div').hide();
                    $('#search-div').show();
                    $('#srchAgent').attr("type", "number");
                    $('#srchAgent').val("");
                    $('#submit-div').show();
                } else if ($(this).val() == "AgentName") {
                    $('#status-div').hide();
                    $('#search-div').show();
                    $('#srchAgent').attr("type", "text");
                    $('#srchAgent').val("");
                    $('#submit-div').show();
                } else if ($(this).val() == "AgentStatus") {
                    $('#search-div').hide();
                    $('#status-div').show();
                    $('#submit-div').show();
                } else {
                    $('#status-div').hide();
                    $('#search-div').hide();
                    $('#submit-div').hide();
                    reset();
                }
                reloadAgentStates();
            });
            function reset() {
                clearTimeout(submitSearchTimer);
                $('#status-div').hide();
                $('#search-div').hide();
                $('#submit-div').hide();
                $('#srchAgent').val("");
                $('#filterType').prop('selectedIndex', 0);
                $('#agentState').prop('selectedIndex', 0);
//                agentStatusTimer = setTimeout(reloadAgentStates, 2000);
                reloadAgentStates();
            }
            var submitSearchTimer;
            function submitSearch() {
                var searchType = $('#filterType').children("option:selected").val();
                var searchValue = null;
                if (searchType === "AgentStatus") {
                    searchValue = $('#agentState').children("option:selected").val();
                } else if (searchType === "AgentName") {
                    searchValue = $('#srchAgent').val();
                    if (searchValue == "") {
                        alert("Please enter a valid Agent Name to search");
                        return;
                    }
                } else {
                    searchValue = $('#srchAgent').val();
                    if (searchValue == "") {
                        alert("Please enter a valid Agent ID to search");
                        return;
                    }
                    searchValue = searchValue.toString();
                }

                $.ajax({
                    type: "GET",
                    url: 'filterAgentsDashboardV1/' + searchType + '/' + searchValue,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    timeout: 15000,
                    success: function (response) {
                        clearTimeout(agentStatusTimer);
                        clearTimeout(submitSearchTimer);
                        if (response.retcode == "0") {
                            document.getElementById("divs_agents").innerHTML = "";
                            var index = 0;
                            var t = response.result;
                            if (t.length == 0) {
                                var ediv = document.createElement("div");
                                ediv.id = "ediv";
                                ediv.setAttribute("class", "col-md-8 col-sm-12 col-xs-12 info-box bg-white-active");
                                ediv.innerHTML = '<h3>No Record Found!</h3>';
                                document.getElementById("divs_agents").appendChild(ediv);
                            } else {
                                for (index = 0; index < t.length; ++index) {
                                    var ob = $("#" + t[index].agentId).empty().text('');
                                    var ti = moment().startOf('day').seconds(t[index].currentStateTime).format('HH:mm:ss');
                                    var agentId = t[index].agentId;
                                    var agentName = t[index].agentName;

                                    if (ob != undefined)
                                    {
                                        $('.cssload-loader1').hide();
                                        $('.overlay1').hide();
                                        //  bg.removeClass();
                                        if (t[index].status == 0)
                                        {//sign off

                                            var maindiv = document.createElement("div");
                                            maindiv.id = "maindiv" + agentId;
                                            maindiv.setAttribute("class", "col-md-6 col-sm-12 col-xs-12");
                                            document.getElementById("divs_agents").appendChild(maindiv);

                                            var agent_div = document.createElement("div");
                                            agent_div.id = "divbg" + agentId;
                                            agent_div.setAttribute("class", "info-box bg-red-active");
                                            document.getElementById("maindiv" + agentId).appendChild(agent_div);

                                            var span_icon = document.createElement("span");
                                            span_icon.id = "span_icon" + agentId;
                                            span_icon.setAttribute("class", "info-box-icon");
                                            span_icon.innerHTML = '<img src="${pageContext.request.contextPath}/resources/img/profileimage.png" class="img-circle" style="padding:10px;" alt="User Image">';
                                            document.getElementById("divbg" + agentId).appendChild(span_icon);

                                            var div_content = document.createElement("div");
                                            div_content.id = "div_content" + agentId;
                                            div_content.setAttribute("class", "info-box-content");
                                            document.getElementById("divbg" + agentId).appendChild(div_content);

                                            var div_agentID = document.createElement("div");
                                            div_agentID.id = "div_agentID" + agentId;
                                            div_agentID.setAttribute("style", "text-align:right;width:100%");
                                            div_agentID.innerHTML = agentName;
                                            document.getElementById("div_content" + agentId).appendChild(div_agentID);

                                            var span_info = document.createElement("span");
                                            span_info.id = "span_info" + agentId;
                                            span_info.setAttribute("class", "info-box-text");
                                            span_info.setAttribute("style", "float:left");
                                            span_info.innerHTML = agentId;
                                            document.getElementById("div_agentID" + agentId).appendChild(span_info);

                                            var span_time = document.createElement("span");
                                            span_time.id = "timeAgent" + agentId;
                                            span_time.setAttribute("class", "info-box-text pull-right");
                                            span_time.innerHTML = ti;
                                            document.getElementById("div_content" + agentId).appendChild(span_time);

                                            var span_boxno = document.createElement("span");
                                            span_boxno.id = agentId;
                                            span_boxno.setAttribute("class", "info-box-number");
                                            span_boxno.innerHTML = "Sign Off";
                                            document.getElementById("div_content" + agentId).appendChild(span_boxno);

                                            var div_btns = document.createElement("div");
                                            div_btns.id = "div_btns" + agentId;
                                            div_btns.setAttribute("class", "btn-group");
                                            document.getElementById("div_content" + agentId).appendChild(div_btns);

                                            var btnAdjust = document.createElement("button");
                                            btnAdjust.id = "skillAdj" + agentId;
                                            btnAdjust.title = "Skill Adjustment";
                                            btnAdjust.setAttribute("type", "button");
                                            btnAdjust.setAttribute("class", "btn btn-default disabled");
                                            btnAdjust.disabled = true;
                                            btnAdjust.setAttribute("onclick", "AdjustAgentSkill(this);");
                                            btnAdjust.innerHTML = '<i class="fa fa-wrench  "></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnAdjust);

                                            var btnInfo = document.createElement("button");
                                            btnInfo.id = "stat" + agentId;
                                            btnInfo.title = "Statistics";
                                            btnInfo.setAttribute("type", "button");
                                            btnInfo.setAttribute("class", "btn btn-default disabled");
                                            btnInfo.disabled = true;
                                            btnInfo.setAttribute("onclick", "getAgentStats(this);");
                                            btnInfo.innerHTML = '<i class="fa fa-info  "></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnInfo);

                                            var btnIdle = document.createElement("button");
                                            btnIdle.id = "idle" + agentId;
                                            btnIdle.title = "Ready";
                                            btnIdle.setAttribute("type", "button");
                                            btnIdle.setAttribute("class", "btn btn-default disabled");
                                            btnIdle.disabled = true;
                                            btnIdle.setAttribute("onclick", "setAgentIdle(this);");
                                            btnIdle.innerHTML = '<i class="fa-user-circle fa-align-center"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnIdle);

                                            var btnRest = document.createElement("button");
                                            btnRest.id = "res" + agentId;
                                            btnRest.title = "Break";
                                            btnRest.setAttribute("type", "button");
                                            btnRest.setAttribute("class", "btn btn-default disabled");
                                            btnRest.disabled = true;
                                            btnRest.setAttribute("onclick", "setAgentRest(this);");
                                            btnRest.innerHTML = '<i class="fa fa-hourglass"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnRest);

                                            var btnBusy = document.createElement("button");
                                            btnBusy.id = "bus" + agentId;
                                            btnBusy.title = "Not Ready";
                                            btnBusy.setAttribute("type", "button");
                                            btnBusy.setAttribute("class", "btn btn-default disabled");
                                            btnBusy.disabled = true;
                                            btnBusy.setAttribute("onclick", "setAgentBusy(this);");
                                            btnBusy.innerHTML = '<i class="fa fa-minus-circle"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnBusy);

                                            var btnListen = document.createElement("button");
                                            btnListen.id = "Listen" + agentId;
                                            btnListen.title = "Listening";
                                            btnListen.setAttribute("type", "button");
                                            btnListen.setAttribute("class", "btn btn-default disabled");
                                            btnListen.disabled = true;
                                            btnListen.setAttribute("onclick", "setAgentListening(this);");
                                            btnListen.innerHTML = '<i class="fa fa-headphones"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnListen);

                                            var btnInsert = document.createElement("button");
                                            btnInsert.id = "Insert" + agentId;
                                            btnInsert.title = "Shadowing";
                                            btnInsert.setAttribute("type", "button");
                                            btnInsert.setAttribute("class", "btn btn-default disabled");
                                            btnInsert.disabled = true;
                                            btnInsert.setAttribute("onclick", "setAgentInserting(this);");
                                            btnInsert.innerHTML = '<i class="fa fa-tty"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnInsert);

                                            var btnStop = document.createElement("button");
                                            btnStop.id = "stop" + agentId;
                                            btnStop.title = "Stop Listening/Shadowing";
                                            btnStop.setAttribute("type", "button");
                                            btnStop.setAttribute("class", "btn btn-default disabled");
                                            btnStop.disabled = true;
                                            btnStop.setAttribute("onclick", "setAgentStopListeningInserting(this);");
                                            btnStop.innerHTML = '<i class="fa fa-stop-circle-o"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnStop);

                                            var btnLogout = document.createElement("button");
                                            btnLogout.id = "log" + agentId;
                                            btnLogout.title = "Logout";
                                            btnLogout.setAttribute("type", "button");
                                            btnLogout.setAttribute("class", "btn btn-default disabled");
                                            btnLogout.disabled = true;
                                            btnLogout.setAttribute("onclick", "setAgentLogout(this);");
                                            btnLogout.innerHTML = '<i class="fa fa-sign-out"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnLogout);

                                        } else if (t[index].status == 2) {
                                            //idle

                                            var maindiv = document.createElement("div");
                                            maindiv.id = "maindiv" + agentId;
                                            maindiv.setAttribute("class", "col-md-6 col-sm-12 col-xs-12");
                                            document.getElementById("divs_agents").appendChild(maindiv);

                                            var agent_div = document.createElement("div");
                                            agent_div.id = "divbg" + agentId;
                                            agent_div.setAttribute("class", "info-box bg-green-active");
                                            document.getElementById("maindiv" + agentId).appendChild(agent_div);

                                            var span_icon = document.createElement("span");
                                            span_icon.id = "span_icon" + agentId;
                                            span_icon.setAttribute("class", "info-box-icon");
                                            span_icon.innerHTML = '<img src="${pageContext.request.contextPath}/resources/img/profileimage.png" class="img-circle" style="padding:10px;" alt="User Image">';
                                            document.getElementById("divbg" + agentId).appendChild(span_icon);

                                            var div_content = document.createElement("div");
                                            div_content.id = "div_content" + agentId;
                                            div_content.setAttribute("class", "info-box-content");
                                            document.getElementById("divbg" + agentId).appendChild(div_content);

                                            var div_agentID = document.createElement("div");
                                            div_agentID.id = "div_agentID" + agentId;
                                            div_agentID.setAttribute("style", "text-align:right;width:100%");
                                            div_agentID.innerHTML = agentName;
                                            document.getElementById("div_content" + agentId).appendChild(div_agentID);

                                            var span_info = document.createElement("span");
                                            span_info.id = "span_info" + agentId;
                                            span_info.setAttribute("class", "info-box-text");
                                            span_info.setAttribute("style", "float:left");
                                            span_info.innerHTML = agentId;
                                            document.getElementById("div_agentID" + agentId).appendChild(span_info);

                                            var span_time = document.createElement("span");
                                            span_time.id = "timeAgent" + agentId;
                                            span_time.setAttribute("class", "info-box-text pull-right");
                                            span_time.innerHTML = ti;
                                            document.getElementById("div_content" + agentId).appendChild(span_time);

                                            var span_boxno = document.createElement("span");
                                            span_boxno.id = agentId;
                                            span_boxno.setAttribute("class", "info-box-number");
                                            span_boxno.innerHTML = "Ready";
                                            document.getElementById("div_content" + agentId).appendChild(span_boxno);

                                            var div_btns = document.createElement("div");
                                            div_btns.id = "div_btns" + agentId;
                                            div_btns.setAttribute("class", "btn-group");
                                            document.getElementById("div_content" + agentId).appendChild(div_btns);

                                            var btnAdjust = document.createElement("button");
                                            btnAdjust.id = "skillAdj" + agentId;
                                            btnAdjust.title = "Skill Adjustment";
                                            btnAdjust.setAttribute("type", "button");
                                            btnAdjust.setAttribute("class", "btn btn-default");
                                            btnAdjust.disabled = false;
                                            btnAdjust.setAttribute("onclick", "AdjustAgentSkill(this);");
                                            btnAdjust.innerHTML = '<i class="fa fa-wrench  "></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnAdjust);

                                            var btnInfo = document.createElement("button");
                                            btnInfo.id = "stat" + agentId;
                                            btnInfo.title = "Statistics";
                                            btnInfo.setAttribute("type", "button");
                                            btnInfo.setAttribute("class", "btn btn-default");
                                            btnInfo.disabled = false;
                                            btnInfo.setAttribute("onclick", "getAgentStats(this);");
                                            btnInfo.innerHTML = '<i class="fa fa-info  "></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnInfo);

                                            var btnIdle = document.createElement("button");
                                            btnIdle.id = "idle" + agentId;
                                            btnIdle.title = "Ready";
                                            btnIdle.setAttribute("type", "button");
                                            btnIdle.setAttribute("class", "btn btn-default disabled");
                                            btnIdle.disabled = true;
                                            btnIdle.setAttribute("onclick", "setAgentIdle(this);");
                                            btnIdle.innerHTML = '<i class="fa-user-circle fa-align-center"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnIdle);

                                            var btnRest = document.createElement("button");
                                            btnRest.id = "res" + agentId;
                                            btnRest.title = "Break";
                                            btnRest.setAttribute("type", "button");
                                            btnRest.setAttribute("class", "btn btn-default");
                                            btnRest.disabled = false;
                                            btnRest.setAttribute("onclick", "setAgentRest(this);");
                                            btnRest.innerHTML = '<i class="fa fa-hourglass"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnRest);

                                            var btnBusy = document.createElement("button");
                                            btnBusy.id = "bus" + agentId;
                                            btnBusy.title = "Not Ready";
                                            btnBusy.setAttribute("type", "button");
                                            btnBusy.setAttribute("class", "btn btn-default");
                                            btnBusy.disabled = false;
                                            btnBusy.setAttribute("onclick", "setAgentBusy(this);");
                                            btnBusy.innerHTML = '<i class="fa fa-minus-circle"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnBusy);

                                            var btnListen = document.createElement("button");
                                            btnListen.id = "Listen" + agentId;
                                            btnListen.title = "Listening";
                                            btnListen.setAttribute("type", "button");
                                            btnListen.setAttribute("class", "btn btn-default disabled");
                                            btnListen.disabled = true;
                                            btnListen.setAttribute("onclick", "setAgentListening(this);");
                                            btnListen.innerHTML = '<i class="fa fa-headphones"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnListen);

                                            var btnInsert = document.createElement("button");
                                            btnInsert.id = "Insert" + agentId;
                                            btnInsert.title = "Shadowing";
                                            btnInsert.setAttribute("type", "button");
                                            btnInsert.setAttribute("class", "btn btn-default disabled");
                                            btnInsert.disabled = true;
                                            btnInsert.setAttribute("onclick", "setAgentInserting(this);");
                                            btnInsert.innerHTML = '<i class="fa fa-tty"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnInsert);

                                            var btnStop = document.createElement("button");
                                            btnStop.id = "stop" + agentId;
                                            btnStop.title = "Stop Listening/Shadowing";
                                            btnStop.setAttribute("type", "button");
                                            btnStop.setAttribute("class", "btn btn-default disabled");
                                            btnStop.disabled = true;
                                            btnStop.setAttribute("onclick", "setAgentStopListeningInserting(this);");
                                            btnStop.innerHTML = '<i class="fa fa-stop-circle-o"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnStop);

                                            var btnLogout = document.createElement("button");
                                            btnLogout.id = "log" + agentId;
                                            btnLogout.title = "Logout";
                                            btnLogout.setAttribute("type", "button");
                                            btnLogout.setAttribute("class", "btn btn-default");
                                            btnLogout.disabled = false;
                                            btnLogout.setAttribute("onclick", "setAgentLogout(this);");
                                            btnLogout.innerHTML = '<i class="fa fa-sign-out"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnLogout);


                                        } else if (t[index].status == 3) {
                                            //busy

                                            var maindiv = document.createElement("div");
                                            maindiv.id = "maindiv" + agentId;
                                            maindiv.setAttribute("class", "col-md-6 col-sm-12 col-xs-12");
                                            document.getElementById("divs_agents").appendChild(maindiv);

                                            var agent_div = document.createElement("div");
                                            agent_div.id = "divbg" + agentId;
                                            agent_div.setAttribute("class", "info-box bg-blue-active");
                                            document.getElementById("maindiv" + agentId).appendChild(agent_div);

                                            var span_icon = document.createElement("span");
                                            span_icon.id = "span_icon" + agentId;
                                            span_icon.setAttribute("class", "info-box-icon");
                                            span_icon.innerHTML = '<img src="${pageContext.request.contextPath}/resources/img/profileimage.png" class="img-circle" style="padding:10px;" alt="User Image">';
                                            document.getElementById("divbg" + agentId).appendChild(span_icon);

                                            var div_content = document.createElement("div");
                                            div_content.id = "div_content" + agentId;
                                            div_content.setAttribute("class", "info-box-content");
                                            document.getElementById("divbg" + agentId).appendChild(div_content);

                                            var div_agentID = document.createElement("div");
                                            div_agentID.id = "div_agentID" + agentId;
                                            div_agentID.setAttribute("style", "text-align:right;width:100%");
                                            div_agentID.innerHTML = agentName;
                                            document.getElementById("div_content" + agentId).appendChild(div_agentID);

                                            var span_info = document.createElement("span");
                                            span_info.id = "span_info" + agentId;
                                            span_info.setAttribute("class", "info-box-text");
                                            span_info.setAttribute("style", "float:left");
                                            span_info.innerHTML = agentId;
                                            document.getElementById("div_agentID" + agentId).appendChild(span_info);

                                            var span_time = document.createElement("span");
                                            span_time.id = "timeAgent" + agentId;
                                            span_time.setAttribute("class", "info-box-text pull-right");
                                            span_time.innerHTML = ti;
                                            document.getElementById("div_content" + agentId).appendChild(span_time);

                                            var span_boxno = document.createElement("span");
                                            span_boxno.id = agentId;
                                            span_boxno.setAttribute("class", "info-box-number");
                                            span_boxno.innerHTML = "Not Ready";
                                            document.getElementById("div_content" + agentId).appendChild(span_boxno);

                                            var div_btns = document.createElement("div");
                                            div_btns.id = "div_btns" + agentId;
                                            div_btns.setAttribute("class", "btn-group");
                                            document.getElementById("div_content" + agentId).appendChild(div_btns);

                                            var btnAdjust = document.createElement("button");
                                            btnAdjust.id = "skillAdj" + agentId;
                                            btnAdjust.title = "Skill Adjustment";
                                            btnAdjust.setAttribute("type", "button");
                                            btnAdjust.setAttribute("class", "btn btn-default");
                                            btnAdjust.disabled = false;
                                            btnAdjust.setAttribute("onclick", "AdjustAgentSkill(this);");
                                            btnAdjust.innerHTML = '<i class="fa fa-wrench  "></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnAdjust);

                                            var btnInfo = document.createElement("button");
                                            btnInfo.id = "stat" + agentId;
                                            btnInfo.title = "Statistics";
                                            btnInfo.setAttribute("type", "button");
                                            btnInfo.setAttribute("class", "btn btn-default");
                                            btnInfo.disabled = false;
                                            btnInfo.setAttribute("onclick", "getAgentStats(this);");
                                            btnInfo.innerHTML = '<i class="fa fa-info  "></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnInfo);

                                            var btnIdle = document.createElement("button");
                                            btnIdle.id = "idle" + agentId;
                                            btnIdle.title = "Ready";
                                            btnIdle.setAttribute("type", "button");
                                            btnIdle.setAttribute("class", "btn btn-default");
                                            btnIdle.disabled = false;
                                            btnIdle.setAttribute("onclick", "setAgentIdle(this);");
                                            btnIdle.innerHTML = '<i class="fa-user-circle fa-align-center"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnIdle);

                                            var btnRest = document.createElement("button");
                                            btnRest.id = "res" + agentId;
                                            btnRest.title = "Break";
                                            btnRest.setAttribute("type", "button");
                                            btnRest.setAttribute("class", "btn btn-default");
                                            btnRest.disabled = false;
                                            btnRest.setAttribute("onclick", "setAgentRest(this);");
                                            btnRest.innerHTML = '<i class="fa fa-hourglass"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnRest);

                                            var btnBusy = document.createElement("button");
                                            btnBusy.id = "bus" + agentId;
                                            btnBusy.title = "Not Ready";
                                            btnBusy.setAttribute("type", "button");
                                            btnBusy.setAttribute("class", "btn btn-default disabled");
                                            btnBusy.disabled = true;
                                            btnBusy.setAttribute("onclick", "setAgentBusy(this);");
                                            btnBusy.innerHTML = '<i class="fa fa-minus-circle"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnBusy);

                                            var btnListen = document.createElement("button");
                                            btnListen.id = "Listen" + agentId;
                                            btnListen.title = "Listening";
                                            btnListen.setAttribute("type", "button");
                                            btnListen.setAttribute("class", "btn btn-default disabled");
                                            btnListen.disabled = true;
                                            btnListen.setAttribute("onclick", "setAgentListening(this);");
                                            btnListen.innerHTML = '<i class="fa fa-headphones"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnListen);

                                            var btnInsert = document.createElement("button");
                                            btnInsert.id = "Insert" + agentId;
                                            btnInsert.title = "Shadowing";
                                            btnInsert.setAttribute("type", "button");
                                            btnInsert.setAttribute("class", "btn btn-default disabled");
                                            btnInsert.disabled = true;
                                            btnInsert.setAttribute("onclick", "setAgentInserting(this);");
                                            btnInsert.innerHTML = '<i class="fa fa-tty"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnInsert);

                                            var btnStop = document.createElement("button");
                                            btnStop.id = "stop" + agentId;
                                            btnStop.title = "Stop Listening/Shadowing";
                                            btnStop.setAttribute("type", "button");
                                            btnStop.setAttribute("class", "btn btn-default disabled");
                                            btnStop.disabled = true;
                                            btnStop.setAttribute("onclick", "setAgentStopListeningInserting(this);");
                                            btnStop.innerHTML = '<i class="fa fa-stop-circle-o"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnStop);

                                            var btnLogout = document.createElement("button");
                                            btnLogout.id = "log" + agentId;
                                            btnLogout.title = "Logout";
                                            btnLogout.setAttribute("type", "button");
                                            btnLogout.setAttribute("class", "btn btn-default");
                                            btnLogout.disabled = false;
                                            btnLogout.setAttribute("onclick", "setAgentLogout(this);");
                                            btnLogout.innerHTML = '<i class="fa fa-sign-out"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnLogout);

                                        } else if (t[index].status == 27) {
                                            //work
                                            var maindiv = document.createElement("div");
                                            maindiv.id = "maindiv" + agentId;
                                            maindiv.setAttribute("class", "col-md-6 col-sm-12 col-xs-12");
                                            document.getElementById("divs_agents").appendChild(maindiv);

                                            var agent_div = document.createElement("div");
                                            agent_div.id = "divbg" + agentId;
                                            agent_div.setAttribute("class", "info-box bg-olive-active");
                                            document.getElementById("maindiv" + agentId).appendChild(agent_div);

                                            var span_icon = document.createElement("span");
                                            span_icon.id = "span_icon" + agentId;
                                            span_icon.setAttribute("class", "info-box-icon");
                                            span_icon.innerHTML = '<img src="${pageContext.request.contextPath}/resources/img/profileimage.png" class="img-circle" style="padding:10px;" alt="User Image">';
                                            document.getElementById("divbg" + agentId).appendChild(span_icon);

                                            var div_content = document.createElement("div");
                                            div_content.id = "div_content" + agentId;
                                            div_content.setAttribute("class", "info-box-content");
                                            document.getElementById("divbg" + agentId).appendChild(div_content);

                                            var div_agentID = document.createElement("div");
                                            div_agentID.id = "div_agentID" + agentId;
                                            div_agentID.setAttribute("style", "text-align:right;width:100%");
                                            div_agentID.innerHTML = agentName;
                                            document.getElementById("div_content" + agentId).appendChild(div_agentID);

                                            var span_info = document.createElement("span");
                                            span_info.id = "span_info" + agentId;
                                            span_info.setAttribute("class", "info-box-text");
                                            span_info.setAttribute("style", "float:left");
                                            span_info.innerHTML = agentId;
                                            document.getElementById("div_agentID" + agentId).appendChild(span_info);

                                            var span_time = document.createElement("span");
                                            span_time.id = "timeAgent" + agentId;
                                            span_time.setAttribute("class", "info-box-text pull-right");
                                            span_time.innerHTML = ti;
                                            document.getElementById("div_content" + agentId).appendChild(span_time);

                                            var span_boxno = document.createElement("span");
                                            span_boxno.id = agentId;
                                            span_boxno.setAttribute("class", "info-box-number");
                                            span_boxno.innerHTML = "Work";
                                            document.getElementById("div_content" + agentId).appendChild(span_boxno);

                                            var div_btns = document.createElement("div");
                                            div_btns.id = "div_btns" + agentId;
                                            div_btns.setAttribute("class", "btn-group");
                                            document.getElementById("div_content" + agentId).appendChild(div_btns);

                                            var btnAdjust = document.createElement("button");
                                            btnAdjust.id = "skillAdj" + agentId;
                                            btnAdjust.title = "Skill Adjustment";
                                            btnAdjust.setAttribute("type", "button");
                                            btnAdjust.setAttribute("class", "btn btn-default");
                                            btnAdjust.disabled = false;
                                            btnAdjust.setAttribute("onclick", "AdjustAgentSkill(this);");
                                            btnAdjust.innerHTML = '<i class="fa fa-wrench  "></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnAdjust);

                                            var btnInfo = document.createElement("button");
                                            btnInfo.id = "stat" + agentId;
                                            btnInfo.title = "Statistics";
                                            btnInfo.setAttribute("type", "button");
                                            btnInfo.setAttribute("class", "btn btn-default");
                                            btnInfo.disabled = false;
                                            btnInfo.setAttribute("onclick", "getAgentStats(this);");
                                            btnInfo.innerHTML = '<i class="fa fa-info  "></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnInfo);

                                            var btnIdle = document.createElement("button");
                                            btnIdle.id = "idle" + agentId;
                                            btnIdle.title = "Ready";
                                            btnIdle.setAttribute("type", "button");
                                            btnIdle.setAttribute("class", "btn btn-default disabled");
                                            btnIdle.disabled = true;
                                            btnIdle.setAttribute("onclick", "setAgentIdle(this);");
                                            btnIdle.innerHTML = '<i class="fa-user-circle fa-align-center"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnIdle);

                                            var btnRest = document.createElement("button");
                                            btnRest.id = "res" + agentId;
                                            btnRest.title = "Break";
                                            btnRest.setAttribute("type", "button");
                                            btnRest.setAttribute("class", "btn btn-default");
                                            btnRest.disabled = false;
                                            btnRest.setAttribute("onclick", "setAgentRest(this);");
                                            btnRest.innerHTML = '<i class="fa fa-hourglass"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnRest);

                                            var btnBusy = document.createElement("button");
                                            btnBusy.id = "bus" + agentId;
                                            btnBusy.title = "Not Ready";
                                            btnBusy.setAttribute("type", "button");
                                            btnBusy.setAttribute("class", "btn btn-default");
                                            btnBusy.disabled = false;
                                            btnBusy.setAttribute("onclick", "setAgentBusy(this);");
                                            btnBusy.innerHTML = '<i class="fa fa-minus-circle"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnBusy);

                                            var btnListen = document.createElement("button");
                                            btnListen.id = "Listen" + agentId;
                                            btnListen.title = "Listening";
                                            btnListen.setAttribute("type", "button");
                                            btnListen.setAttribute("class", "btn btn-default disabled");
                                            btnListen.disabled = true;
                                            btnListen.setAttribute("onclick", "setAgentListening(this);");
                                            btnListen.innerHTML = '<i class="fa fa-headphones"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnListen);

                                            var btnInsert = document.createElement("button");
                                            btnInsert.id = "Insert" + agentId;
                                            btnInsert.title = "Shadowing";
                                            btnInsert.setAttribute("type", "button");
                                            btnInsert.setAttribute("class", "btn btn-default disabled");
                                            btnInsert.disabled = true;
                                            btnInsert.setAttribute("onclick", "setAgentInserting(this);");
                                            btnInsert.innerHTML = '<i class="fa fa-tty"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnInsert);

                                            var btnStop = document.createElement("button");
                                            btnStop.id = "stop" + agentId;
                                            btnStop.title = "Stop Listening/Shadowing";
                                            btnStop.setAttribute("type", "button");
                                            btnStop.setAttribute("class", "btn btn-default disabled");
                                            btnStop.disabled = true;
                                            btnStop.setAttribute("onclick", "setAgentStopListeningInserting(this);");
                                            btnStop.innerHTML = '<i class="fa fa-stop-circle-o"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnStop);

                                            var btnLogout = document.createElement("button");
                                            btnLogout.id = "log" + agentId;
                                            btnLogout.title = "Logout";
                                            btnLogout.setAttribute("type", "button");
                                            btnLogout.setAttribute("class", "btn btn-default");
                                            btnLogout.disabled = false;
                                            btnLogout.setAttribute("onclick", "setAgentLogout(this);");
                                            btnLogout.innerHTML = '<i class="fa fa-sign-out"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnLogout);

                                        } else if (t[index].status == 4) {
                                            //talking

                                            var maindiv = document.createElement("div");
                                            maindiv.id = "maindiv" + agentId;
                                            maindiv.setAttribute("class", "col-md-6 col-sm-12 col-xs-12");
                                            document.getElementById("divs_agents").appendChild(maindiv);

                                            var agent_div = document.createElement("div");
                                            agent_div.id = "divbg" + agentId;
                                            agent_div.setAttribute("class", "info-box bg-orange-active");
                                            document.getElementById("maindiv" + agentId).appendChild(agent_div);

                                            var span_icon = document.createElement("span");
                                            span_icon.id = "span_icon" + agentId;
                                            span_icon.setAttribute("class", "info-box-icon");
                                            span_icon.innerHTML = '<img src="${pageContext.request.contextPath}/resources/img/profileimage.png" class="img-circle" style="padding:10px;" alt="User Image">';
                                            document.getElementById("divbg" + agentId).appendChild(span_icon);

                                            var div_content = document.createElement("div");
                                            div_content.id = "div_content" + agentId;
                                            div_content.setAttribute("class", "info-box-content");
                                            document.getElementById("divbg" + agentId).appendChild(div_content);

                                            var div_agentID = document.createElement("div");
                                            div_agentID.id = "div_agentID" + agentId;
                                            div_agentID.setAttribute("style", "text-align:right;width:100%");
                                            div_agentID.innerHTML = agentName;
                                            document.getElementById("div_content" + agentId).appendChild(div_agentID);

                                            var span_info = document.createElement("span");
                                            span_info.id = "span_info" + agentId;
                                            span_info.setAttribute("class", "info-box-text");
                                            span_info.setAttribute("style", "float:left");
                                            span_info.innerHTML = agentId;
                                            document.getElementById("div_agentID" + agentId).appendChild(span_info);

                                            var span_time = document.createElement("span");
                                            span_time.id = "timeAgent$" + agentId;
                                            span_time.setAttribute("class", "info-box-text pull-right");
                                            span_time.innerHTML = ti;
                                            document.getElementById("div_content" + agentId).appendChild(span_time);

                                            var span_boxno = document.createElement("span");
                                            span_boxno.id = agentId;
                                            span_boxno.setAttribute("class", "info-box-number");
                                            span_boxno.innerHTML = "Talking";
                                            document.getElementById("div_content" + agentId).appendChild(span_boxno);

                                            var div_btns = document.createElement("div");
                                            div_btns.id = "div_btns" + agentId;
                                            div_btns.setAttribute("class", "btn-group");
                                            document.getElementById("div_content" + agentId).appendChild(div_btns);

                                            var btnAdjust = document.createElement("button");
                                            btnAdjust.id = "skillAdj" + agentId;
                                            btnAdjust.title = "Skill Adjustment";
                                            btnAdjust.setAttribute("type", "button");
                                            btnAdjust.setAttribute("class", "btn btn-default");
                                            btnAdjust.disabled = false;
                                            btnAdjust.setAttribute("onclick", "AdjustAgentSkill(this);");
                                            btnAdjust.innerHTML = '<i class="fa fa-wrench  "></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnAdjust);

                                            var btnInfo = document.createElement("button");
                                            btnInfo.id = "stat" + agentId;
                                            btnInfo.title = "Statistics";
                                            btnInfo.setAttribute("type", "button");
                                            btnInfo.setAttribute("class", "btn btn-default");
                                            btnInfo.disabled = false;
                                            btnInfo.setAttribute("onclick", "getAgentStats(this);");
                                            btnInfo.innerHTML = '<i class="fa fa-info  "></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnInfo);

                                            var btnIdle = document.createElement("button");
                                            btnIdle.id = "idle" + agentId;
                                            btnIdle.title = "Ready";
                                            btnIdle.setAttribute("type", "button");
                                            btnIdle.setAttribute("class", "btn btn-default");
                                            btnIdle.disabled = false;
                                            btnIdle.setAttribute("onclick", "setAgentIdle(this);");
                                            btnIdle.innerHTML = '<i class="fa-user-circle fa-align-center"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnIdle);

                                            var btnRest = document.createElement("button");
                                            btnRest.id = "res" + agentId;
                                            btnRest.title = "Break";
                                            btnRest.setAttribute("type", "button");
                                            btnRest.setAttribute("class", "btn btn-default");
                                            btnRest.disabled = false;
                                            btnRest.setAttribute("onclick", "setAgentRest(this);");
                                            btnRest.innerHTML = '<i class="fa fa-hourglass"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnRest);

                                            var btnBusy = document.createElement("button");
                                            btnBusy.id = "bus" + agentId;
                                            btnBusy.title = "Not Ready";
                                            btnBusy.setAttribute("type", "button");
                                            btnBusy.setAttribute("class", "btn btn-default");
                                            btnBusy.disabled = false;
                                            btnBusy.setAttribute("onclick", "setAgentBusy(this);");
                                            btnBusy.innerHTML = '<i class="fa fa-minus-circle"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnBusy);

                                            var btnListen = document.createElement("button");
                                            btnListen.id = "Listen" + agentId;
                                            btnListen.title = "Listening";
                                            btnListen.setAttribute("type", "button");
                                            btnListen.setAttribute("class", "btn btn-default");
                                            btnListen.disabled = false;
                                            btnListen.setAttribute("onclick", "setAgentListening(this);");
                                            btnListen.innerHTML = '<i class="fa fa-headphones"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnListen);

                                            var btnInsert = document.createElement("button");
                                            btnInsert.id = "Insert" + agentId;
                                            btnInsert.title = "Shadowing";
                                            btnInsert.setAttribute("type", "button");
                                            btnInsert.setAttribute("class", "btn btn-default");
                                            btnInsert.disabled = false;
                                            btnInsert.setAttribute("onclick", "setAgentInserting(this);");
                                            btnInsert.innerHTML = '<i class="fa fa-tty"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnInsert);

                                            var btnStop = document.createElement("button");
                                            btnStop.id = "stop" + agentId;
                                            btnStop.title = "Stop Listening/Shadowing";
                                            btnStop.setAttribute("type", "button");
                                            btnStop.setAttribute("class", "btn btn-default disabled");
                                            btnStop.disabled = true;
                                            btnStop.setAttribute("onclick", "setAgentStopListeningInserting(this);");
                                            btnStop.innerHTML = '<i class="fa fa-stop-circle-o"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnStop);

                                            var btnLogout = document.createElement("button");
                                            btnLogout.id = "log" + agentId;
                                            btnLogout.title = "Logout";
                                            btnLogout.setAttribute("type", "button");
                                            btnLogout.setAttribute("class", "btn btn-default");
                                            btnLogout.disabled = false;
                                            btnLogout.setAttribute("onclick", "setAgentLogout(this);");
                                            btnLogout.innerHTML = '<i class="fa fa-sign-out"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnLogout);

                                        } else if (t[index].status == 5) {
                                            //Answering

                                            var maindiv = document.createElement("div");
                                            maindiv.id = "maindiv" + agentId;
                                            maindiv.setAttribute("class", "col-md-6 col-sm-12 col-xs-12");
                                            document.getElementById("divs_agents").appendChild(maindiv);

                                            var agent_div = document.createElement("div");
                                            agent_div.id = "divbg" + agentId;
                                            agent_div.setAttribute("class", "info-box bg-orange");
                                            document.getElementById("maindiv" + agentId).appendChild(agent_div);

                                            var span_icon = document.createElement("span");
                                            span_icon.id = "span_icon" + agentId;
                                            span_icon.setAttribute("class", "info-box-icon");
                                            span_icon.innerHTML = '<img src="${pageContext.request.contextPath}/resources/img/profileimage.png" class="img-circle" style="padding:10px;" alt="User Image">';
                                            document.getElementById("divbg" + agentId).appendChild(span_icon);

                                            var div_content = document.createElement("div");
                                            div_content.id = "div_content" + agentId;
                                            div_content.setAttribute("class", "info-box-content");
                                            document.getElementById("divbg" + agentId).appendChild(div_content);

                                            var div_agentID = document.createElement("div");
                                            div_agentID.id = "div_agentID" + agentId;
                                            div_agentID.setAttribute("style", "text-align:right;width:100%");
                                            div_agentID.innerHTML = agentName;
                                            document.getElementById("div_content" + agentId).appendChild(div_agentID);

                                            var span_info = document.createElement("span");
                                            span_info.id = "span_info" + agentId;
                                            span_info.setAttribute("class", "info-box-text");
                                            span_info.setAttribute("style", "float:left");
                                            span_info.innerHTML = agentId;
                                            document.getElementById("div_agentID" + agentId).appendChild(span_info);

                                            var span_time = document.createElement("span");
                                            span_time.id = "timeAgent$" + agentId;
                                            span_time.setAttribute("class", "info-box-text pull-right");
                                            span_time.innerHTML = ti;
                                            document.getElementById("div_content" + agentId).appendChild(span_time);

                                            var span_boxno = document.createElement("span");
                                            span_boxno.id = agentId;
                                            span_boxno.setAttribute("class", "info-box-number");
                                            span_boxno.innerHTML = "Answering";
                                            document.getElementById("div_content" + agentId).appendChild(span_boxno);

                                            var div_btns = document.createElement("div");
                                            div_btns.id = "div_btns" + agentId;
                                            div_btns.setAttribute("class", "btn-group");
                                            document.getElementById("div_content" + agentId).appendChild(div_btns);

                                            var btnAdjust = document.createElement("button");
                                            btnAdjust.id = "skillAdj" + agentId;
                                            btnAdjust.title = "Skill Adjustment";
                                            btnAdjust.setAttribute("type", "button");
                                            btnAdjust.setAttribute("class", "btn btn-default");
                                            btnAdjust.disabled = false;
                                            btnAdjust.setAttribute("onclick", "AdjustAgentSkill(this);");
                                            btnAdjust.innerHTML = '<i class="fa fa-wrench  "></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnAdjust);

                                            var btnInfo = document.createElement("button");
                                            btnInfo.id = "stat" + agentId;
                                            btnInfo.title = "Statistics";
                                            btnInfo.setAttribute("type", "button");
                                            btnInfo.setAttribute("class", "btn btn-default");
                                            btnInfo.disabled = false;
                                            btnInfo.setAttribute("onclick", "getAgentStats(this);");
                                            btnInfo.innerHTML = '<i class="fa fa-info  "></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnInfo);

                                            var btnIdle = document.createElement("button");
                                            btnIdle.id = "idle" + agentId;
                                            btnIdle.title = "Ready";
                                            btnIdle.setAttribute("type", "button");
                                            btnIdle.setAttribute("class", "btn btn-default");
                                            btnIdle.disabled = false;
                                            btnIdle.setAttribute("onclick", "setAgentIdle(this);");
                                            btnIdle.innerHTML = '<i class="fa-user-circle fa-align-center"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnIdle);

                                            var btnRest = document.createElement("button");
                                            btnRest.id = "res" + agentId;
                                            btnRest.title = "Break";
                                            btnRest.setAttribute("type", "button");
                                            btnRest.setAttribute("class", "btn btn-default");
                                            btnRest.disabled = false;
                                            btnRest.setAttribute("onclick", "setAgentRest(this);");
                                            btnRest.innerHTML = '<i class="fa fa-hourglass"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnRest);

                                            var btnBusy = document.createElement("button");
                                            btnBusy.id = "bus" + agentId;
                                            btnBusy.title = "Not Ready";
                                            btnBusy.setAttribute("type", "button");
                                            btnBusy.setAttribute("class", "btn btn-default");
                                            btnBusy.disabled = false;
                                            btnBusy.setAttribute("onclick", "setAgentBusy(this);");
                                            btnBusy.innerHTML = '<i class="fa fa-minus-circle"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnBusy);

                                            var btnListen = document.createElement("button");
                                            btnListen.id = "Listen" + agentId;
                                            btnListen.title = "Listening";
                                            btnListen.setAttribute("type", "button");
                                            btnListen.setAttribute("class", "btn btn-default");
                                            btnListen.disabled = false;
                                            btnListen.setAttribute("onclick", "setAgentListening(this);");
                                            btnListen.innerHTML = '<i class="fa fa-headphones"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnListen);

                                            var btnInsert = document.createElement("button");
                                            btnInsert.id = "Insert" + agentId;
                                            btnInsert.title = "Shadowing";
                                            btnInsert.setAttribute("type", "button");
                                            btnInsert.setAttribute("class", "btn btn-default");
                                            btnInsert.disabled = false;
                                            btnInsert.setAttribute("onclick", "setAgentInserting(this);");
                                            btnInsert.innerHTML = '<i class="fa fa-tty"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnInsert);

                                            var btnStop = document.createElement("button");
                                            btnStop.id = "stop" + agentId;
                                            btnStop.title = "Stop Listening/Shadowing";
                                            btnStop.setAttribute("type", "button");
                                            btnStop.setAttribute("class", "btn btn-default disabled");
                                            btnStop.disabled = true;
                                            btnStop.setAttribute("onclick", "setAgentStopListeningInserting(this);");
                                            btnStop.innerHTML = '<i class="fa fa-stop-circle-o"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnStop);

                                            var btnLogout = document.createElement("button");
                                            btnLogout.id = "log" + agentId;
                                            btnLogout.title = "Logout";
                                            btnLogout.setAttribute("type", "button");
                                            btnLogout.setAttribute("class", "btn btn-default");
                                            btnLogout.disabled = false;
                                            btnLogout.setAttribute("onclick", "setAgentLogout(this);");
                                            btnLogout.innerHTML = '<i class="fa fa-sign-out"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnLogout);

                                        } else if (t[index].status == 26) {
                                            //rest/break

                                            var maindiv = document.createElement("div");
                                            maindiv.id = "maindiv" + agentId;
                                            maindiv.setAttribute("class", "col-md-6 col-sm-12 col-xs-12");
                                            document.getElementById("divs_agents").appendChild(maindiv);

                                            var agent_div = document.createElement("div");
                                            agent_div.id = "divbg" + agentId;
                                            agent_div.setAttribute("class", "info-box bg-yellow-active");
                                            document.getElementById("maindiv" + agentId).appendChild(agent_div);

                                            var span_icon = document.createElement("span");
                                            span_icon.id = "span_icon" + agentId;
                                            span_icon.setAttribute("class", "info-box-icon");
                                            span_icon.innerHTML = '<img src="${pageContext.request.contextPath}/resources/img/profileimage.png" class="img-circle" style="padding:10px;" alt="User Image">';
                                            document.getElementById("divbg" + agentId).appendChild(span_icon);

                                            var div_content = document.createElement("div");
                                            div_content.id = "div_content" + agentId;
                                            div_content.setAttribute("class", "info-box-content");
                                            document.getElementById("divbg" + agentId).appendChild(div_content);

                                            var div_agentID = document.createElement("div");
                                            div_agentID.id = "div_agentID" + agentId;
                                            div_agentID.setAttribute("style", "text-align:right;width:100%");
                                            div_agentID.innerHTML = agentName;
                                            document.getElementById("div_content" + agentId).appendChild(div_agentID);

                                            var span_info = document.createElement("span");
                                            span_info.id = "span_info" + agentId;
                                            span_info.setAttribute("class", "info-box-text");
                                            span_info.setAttribute("style", "float:left");
                                            span_info.innerHTML = agentId;
                                            document.getElementById("div_agentID" + agentId).appendChild(span_info);

                                            var span_time = document.createElement("span");
                                            span_time.id = "timeAgent$" + agentId;
                                            span_time.setAttribute("class", "info-box-text pull-right");
                                            span_time.innerHTML = ti;
                                            document.getElementById("div_content" + agentId).appendChild(span_time);

                                            var span_boxno = document.createElement("span");
                                            span_boxno.id = agentId;
                                            span_boxno.setAttribute("class", "info-box-number");
                                            span_boxno.innerHTML = "Break";
                                            document.getElementById("div_content" + agentId).appendChild(span_boxno);

                                            var div_btns = document.createElement("div");
                                            div_btns.id = "div_btns" + agentId;
                                            div_btns.setAttribute("class", "btn-group");
                                            document.getElementById("div_content" + agentId).appendChild(div_btns);

                                            var btnAdjust = document.createElement("button");
                                            btnAdjust.id = "skillAdj" + agentId;
                                            btnAdjust.title = "Skill Adjustment";
                                            btnAdjust.setAttribute("type", "button");
                                            btnAdjust.setAttribute("class", "btn btn-default");
                                            btnAdjust.disabled = false;
                                            btnAdjust.setAttribute("onclick", "AdjustAgentSkill(this);");
                                            btnAdjust.innerHTML = '<i class="fa fa-wrench  "></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnAdjust);

                                            var btnInfo = document.createElement("button");
                                            btnInfo.id = "stat" + agentId;
                                            btnInfo.title = "Statistics";
                                            btnInfo.setAttribute("type", "button");
                                            btnInfo.setAttribute("class", "btn btn-default");
                                            btnInfo.disabled = false;
                                            btnInfo.setAttribute("onclick", "getAgentStats(this);");
                                            btnInfo.innerHTML = '<i class="fa fa-info  "></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnInfo);

                                            var btnIdle = document.createElement("button");
                                            btnIdle.id = "idle" + agentId;
                                            btnIdle.title = "Ready";
                                            btnIdle.setAttribute("type", "button");
                                            btnIdle.setAttribute("class", "btn btn-default");
                                            btnIdle.disabled = false;
                                            btnIdle.setAttribute("onclick", "setAgentIdle(this);");
                                            btnIdle.innerHTML = '<i class="fa-user-circle fa-align-center"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnIdle);

                                            var btnRest = document.createElement("button");
                                            btnRest.id = "res" + agentId;
                                            btnRest.title = "Break";
                                            btnRest.setAttribute("type", "button");
                                            btnRest.setAttribute("class", "btn btn-default disabled");
                                            btnRest.disabled = true;
                                            btnRest.setAttribute("onclick", "setAgentRest(this);");
                                            btnRest.innerHTML = '<i class="fa fa-hourglass"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnRest);

                                            var btnBusy = document.createElement("button");
                                            btnBusy.id = "bus" + agentId;
                                            btnBusy.title = "Not Ready";
                                            btnBusy.setAttribute("type", "button");
                                            btnBusy.setAttribute("class", "btn btn-default");
                                            btnBusy.disabled = false;
                                            btnBusy.setAttribute("onclick", "setAgentBusy(this);");
                                            btnBusy.innerHTML = '<i class="fa fa-minus-circle"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnBusy);

                                            var btnListen = document.createElement("button");
                                            btnListen.id = "Listen" + agentId;
                                            btnListen.title = "Listening";
                                            btnListen.setAttribute("type", "button");
                                            btnListen.setAttribute("class", "btn btn-default disabled");
                                            btnListen.disabled = true;
                                            btnListen.setAttribute("onclick", "setAgentListening(this);");
                                            btnListen.innerHTML = '<i class="fa fa-headphones"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnListen);

                                            var btnInsert = document.createElement("button");
                                            btnInsert.id = "Insert" + agentId;
                                            btnInsert.title = "Shadowing";
                                            btnInsert.setAttribute("type", "button");
                                            btnInsert.setAttribute("class", "btn btn-default disabled");
                                            btnInsert.disabled = true;
                                            btnInsert.setAttribute("onclick", "setAgentInserting(this);");
                                            btnInsert.innerHTML = '<i class="fa fa-tty"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnInsert);

                                            var btnStop = document.createElement("button");
                                            btnStop.id = "stop" + agentId;
                                            btnStop.title = "Stop Listening/Shadowing";
                                            btnStop.setAttribute("type", "button");
                                            btnStop.setAttribute("class", "btn btn-default disabled");
                                            btnStop.disabled = true;
                                            btnStop.setAttribute("onclick", "setAgentStopListeningInserting(this);");
                                            btnStop.innerHTML = '<i class="fa fa-stop-circle-o"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnStop);

                                            var btnLogout = document.createElement("button");
                                            btnLogout.id = "log" + agentId;
                                            btnLogout.title = "Logout";
                                            btnLogout.setAttribute("type", "button");
                                            btnLogout.setAttribute("class", "btn btn-default");
                                            btnLogout.disabled = false;
                                            btnLogout.setAttribute("onclick", "setAgentLogout(this);");
                                            btnLogout.innerHTML = '<i class="fa fa-sign-out"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnLogout);

                                        } else if (t[index].status == 13) {
                                            //Transferred to IVR

                                            var maindiv = document.createElement("div");
                                            maindiv.id = "maindiv" + agentId;
                                            maindiv.setAttribute("class", "col-md-6 col-sm-12 col-xs-12");
                                            document.getElementById("divs_agents").appendChild(maindiv);

                                            var agent_div = document.createElement("div");
                                            agent_div.id = "divbg" + agentId;
                                            agent_div.setAttribute("class", "info-box bg-aqua-active");
                                            document.getElementById("maindiv" + agentId).appendChild(agent_div);

                                            var span_icon = document.createElement("span");
                                            span_icon.id = "span_icon" + agentId;
                                            span_icon.setAttribute("class", "info-box-icon");
                                            span_icon.innerHTML = '<img src="${pageContext.request.contextPath}/resources/img/profileimage.png" class="img-circle" style="padding:10px;" alt="User Image">';
                                            document.getElementById("divbg" + agentId).appendChild(span_icon);

                                            var div_content = document.createElement("div");
                                            div_content.id = "div_content" + agentId;
                                            div_content.setAttribute("class", "info-box-content");
                                            document.getElementById("divbg" + agentId).appendChild(div_content);

                                            var div_agentID = document.createElement("div");
                                            div_agentID.id = "div_agentID" + agentId;
                                            div_agentID.setAttribute("style", "text-align:right;width:100%");
                                            div_agentID.innerHTML = agentName;
                                            document.getElementById("div_content" + agentId).appendChild(div_agentID);

                                            var span_info = document.createElement("span");
                                            span_info.id = "span_info" + agentId;
                                            span_info.setAttribute("class", "info-box-text");
                                            span_info.setAttribute("style", "float:left");
                                            span_info.innerHTML = agentId;
                                            document.getElementById("div_agentID" + agentId).appendChild(span_info);

                                            var span_time = document.createElement("span");
                                            span_time.id = "timeAgent$" + agentId;
                                            span_time.setAttribute("class", "info-box-text pull-right");
                                            span_time.innerHTML = ti;
                                            document.getElementById("div_content" + agentId).appendChild(span_time);

                                            var span_boxno = document.createElement("span");
                                            span_boxno.id = agentId;
                                            span_boxno.setAttribute("class", "info-box-number");
                                            span_boxno.innerHTML = "Transferred to IVR";
                                            document.getElementById("div_content" + agentId).appendChild(span_boxno);

                                            var div_btns = document.createElement("div");
                                            div_btns.id = "div_btns" + agentId;
                                            div_btns.setAttribute("class", "btn-group");
                                            document.getElementById("div_content" + agentId).appendChild(div_btns);

                                            var btnAdjust = document.createElement("button");
                                            btnAdjust.id = "skillAdj" + agentId;
                                            btnAdjust.title = "Skill Adjustment";
                                            btnAdjust.setAttribute("type", "button");
                                            btnAdjust.setAttribute("class", "btn btn-default");
                                            btnAdjust.disabled = true;
                                            btnAdjust.setAttribute("onclick", "AdjustAgentSkill(this);");
                                            btnAdjust.innerHTML = '<i class="fa fa-wrench  "></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnAdjust);

                                            var btnInfo = document.createElement("button");
                                            btnInfo.id = "stat" + agentId;
                                            btnInfo.title = "Statistics";
                                            btnInfo.setAttribute("type", "button");
                                            btnInfo.setAttribute("class", "btn btn-default");
                                            btnInfo.disabled = false;
                                            btnInfo.setAttribute("onclick", "getAgentStats(this);");
                                            btnInfo.innerHTML = '<i class="fa fa-info  "></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnInfo);

                                            var btnIdle = document.createElement("button");
                                            btnIdle.id = "idle" + agentId;
                                            btnIdle.title = "Ready";
                                            btnIdle.setAttribute("type", "button");
                                            btnIdle.setAttribute("class", "btn btn-default");
                                            btnIdle.disabled = true;
                                            btnIdle.setAttribute("onclick", "setAgentIdle(this);");
                                            btnIdle.innerHTML = '<i class="fa-user-circle fa-align-center"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnIdle);

                                            var btnRest = document.createElement("button");
                                            btnRest.id = "res" + agentId;
                                            btnRest.title = "Break";
                                            btnRest.setAttribute("type", "button");
                                            btnRest.setAttribute("class", "btn btn-default disabled");
                                            btnRest.disabled = true;
                                            btnRest.setAttribute("onclick", "setAgentRest(this);");
                                            btnRest.innerHTML = '<i class="fa fa-hourglass"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnRest);

                                            var btnBusy = document.createElement("button");
                                            btnBusy.id = "bus" + agentId;
                                            btnBusy.title = "Not Ready";
                                            btnBusy.setAttribute("type", "button");
                                            btnBusy.setAttribute("class", "btn btn-default");
                                            btnBusy.disabled = true;
                                            btnBusy.setAttribute("onclick", "setAgentBusy(this);");
                                            btnBusy.innerHTML = '<i class="fa fa-minus-circle"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnBusy);

                                            var btnListen = document.createElement("button");
                                            btnListen.id = "Listen" + agentId;
                                            btnListen.title = "Listening";
                                            btnListen.setAttribute("type", "button");
                                            btnListen.setAttribute("class", "btn btn-default disabled");
                                            btnListen.disabled = true;
                                            btnListen.setAttribute("onclick", "setAgentListening(this);");
                                            btnListen.innerHTML = '<i class="fa fa-headphones"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnListen);

                                            var btnInsert = document.createElement("button");
                                            btnInsert.id = "Insert" + agentId;
                                            btnInsert.title = "Shadowing";
                                            btnInsert.setAttribute("type", "button");
                                            btnInsert.setAttribute("class", "btn btn-default disabled");
                                            btnInsert.disabled = true;
                                            btnInsert.setAttribute("onclick", "setAgentInserting(this);");
                                            btnInsert.innerHTML = '<i class="fa fa-tty"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnInsert);

                                            var btnStop = document.createElement("button");
                                            btnStop.id = "stop" + agentId;
                                            btnStop.title = "Stop Listening/Shadowing";
                                            btnStop.setAttribute("type", "button");
                                            btnStop.setAttribute("class", "btn btn-default disabled");
                                            btnStop.disabled = true;
                                            btnStop.setAttribute("onclick", "setAgentStopListeningInserting(this);");
                                            btnStop.innerHTML = '<i class="fa fa-stop-circle-o"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnStop);

                                            var btnLogout = document.createElement("button");
                                            btnLogout.id = "log" + agentId;
                                            btnLogout.title = "Logout";
                                            btnLogout.setAttribute("type", "button");
                                            btnLogout.setAttribute("class", "btn btn-default");
                                            btnLogout.disabled = true;
                                            btnLogout.setAttribute("onclick", "setAgentLogout(this);");
                                            btnLogout.innerHTML = '<i class="fa fa-sign-out"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnLogout);

                                        } else if (t[index].status == 30) {
                                            //On Hold

                                            var maindiv = document.createElement("div");
                                            maindiv.id = "maindiv" + agentId;
                                            maindiv.setAttribute("class", "col-md-6 col-sm-12 col-xs-12");
                                            document.getElementById("divs_agents").appendChild(maindiv);

                                            var agent_div = document.createElement("div");
                                            agent_div.id = "divbg" + agentId;
                                            agent_div.setAttribute("class", "info-box bg-purple-active");
                                            document.getElementById("maindiv" + agentId).appendChild(agent_div);

                                            var span_icon = document.createElement("span");
                                            span_icon.id = "span_icon" + agentId;
                                            span_icon.setAttribute("class", "info-box-icon");
                                            span_icon.innerHTML = '<img src="${pageContext.request.contextPath}/resources/img/profileimage.png" class="img-circle" style="padding:10px;" alt="User Image">';
                                            document.getElementById("divbg" + agentId).appendChild(span_icon);

                                            var div_content = document.createElement("div");
                                            div_content.id = "div_content" + agentId;
                                            div_content.setAttribute("class", "info-box-content");
                                            document.getElementById("divbg" + agentId).appendChild(div_content);

                                            var div_agentID = document.createElement("div");
                                            div_agentID.id = "div_agentID" + agentId;
                                            div_agentID.setAttribute("style", "text-align:right;width:100%");
                                            div_agentID.innerHTML = agentName;
                                            document.getElementById("div_content" + agentId).appendChild(div_agentID);

                                            var span_info = document.createElement("span");
                                            span_info.id = "span_info" + agentId;
                                            span_info.setAttribute("class", "info-box-text");
                                            span_info.setAttribute("style", "float:left");
                                            span_info.innerHTML = agentId;
                                            document.getElementById("div_agentID" + agentId).appendChild(span_info);

                                            var span_time = document.createElement("span");
                                            span_time.id = "timeAgent$" + agentId;
                                            span_time.setAttribute("class", "info-box-text pull-right");
                                            span_time.innerHTML = ti;
                                            document.getElementById("div_content" + agentId).appendChild(span_time);

                                            var span_boxno = document.createElement("span");
                                            span_boxno.id = agentId;
                                            span_boxno.setAttribute("class", "info-box-number");
                                            span_boxno.innerHTML = "On Hold";
                                            document.getElementById("div_content" + agentId).appendChild(span_boxno);

                                            var div_btns = document.createElement("div");
                                            div_btns.id = "div_btns" + agentId;
                                            div_btns.setAttribute("class", "btn-group");
                                            document.getElementById("div_content" + agentId).appendChild(div_btns);

                                            var btnAdjust = document.createElement("button");
                                            btnAdjust.id = "skillAdj" + agentId;
                                            btnAdjust.title = "Skill Adjustment";
                                            btnAdjust.setAttribute("type", "button");
                                            btnAdjust.setAttribute("class", "btn btn-default");
                                            btnAdjust.disabled = true;
                                            btnAdjust.setAttribute("onclick", "AdjustAgentSkill(this);");
                                            btnAdjust.innerHTML = '<i class="fa fa-wrench  "></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnAdjust);

                                            var btnInfo = document.createElement("button");
                                            btnInfo.id = "stat" + agentId;
                                            btnInfo.title = "Statistics";
                                            btnInfo.setAttribute("type", "button");
                                            btnInfo.setAttribute("class", "btn btn-default");
                                            btnInfo.disabled = false;
                                            btnInfo.setAttribute("onclick", "getAgentStats(this);");
                                            btnInfo.innerHTML = '<i class="fa fa-info  "></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnInfo);

                                            var btnIdle = document.createElement("button");
                                            btnIdle.id = "idle" + agentId;
                                            btnIdle.title = "Ready";
                                            btnIdle.setAttribute("type", "button");
                                            btnIdle.setAttribute("class", "btn btn-default");
                                            btnIdle.disabled = true;
                                            btnIdle.setAttribute("onclick", "setAgentIdle(this);");
                                            btnIdle.innerHTML = '<i class="fa-user-circle fa-align-center"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnIdle);

                                            var btnRest = document.createElement("button");
                                            btnRest.id = "res" + agentId;
                                            btnRest.title = "Break";
                                            btnRest.setAttribute("type", "button");
                                            btnRest.setAttribute("class", "btn btn-default disabled");
                                            btnRest.disabled = true;
                                            btnRest.setAttribute("onclick", "setAgentRest(this);");
                                            btnRest.innerHTML = '<i class="fa fa-hourglass"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnRest);

                                            var btnBusy = document.createElement("button");
                                            btnBusy.id = "bus" + agentId;
                                            btnBusy.title = "Not Ready";
                                            btnBusy.setAttribute("type", "button");
                                            btnBusy.setAttribute("class", "btn btn-default");
                                            btnBusy.disabled = true;
                                            btnBusy.setAttribute("onclick", "setAgentBusy(this);");
                                            btnBusy.innerHTML = '<i class="fa fa-minus-circle"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnBusy);

                                            var btnListen = document.createElement("button");
                                            btnListen.id = "Listen" + agentId;
                                            btnListen.title = "Listening";
                                            btnListen.setAttribute("type", "button");
                                            btnListen.setAttribute("class", "btn btn-default disabled");
                                            btnListen.disabled = true;
                                            btnListen.setAttribute("onclick", "setAgentListening(this);");
                                            btnListen.innerHTML = '<i class="fa fa-headphones"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnListen);

                                            var btnInsert = document.createElement("button");
                                            btnInsert.id = "Insert" + agentId;
                                            btnInsert.title = "Shadowing";
                                            btnInsert.setAttribute("type", "button");
                                            btnInsert.setAttribute("class", "btn btn-default disabled");
                                            btnInsert.disabled = true;
                                            btnInsert.setAttribute("onclick", "setAgentInserting(this);");
                                            btnInsert.innerHTML = '<i class="fa fa-tty"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnInsert);

                                            var btnStop = document.createElement("button");
                                            btnStop.id = "stop" + agentId;
                                            btnStop.title = "Stop Listening/Shadowing";
                                            btnStop.setAttribute("type", "button");
                                            btnStop.setAttribute("class", "btn btn-default disabled");
                                            btnStop.disabled = true;
                                            btnStop.setAttribute("onclick", "setAgentStopListeningInserting(this);");
                                            btnStop.innerHTML = '<i class="fa fa-stop-circle-o"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnStop);

                                            var btnLogout = document.createElement("button");
                                            btnLogout.id = "log" + agentId;
                                            btnLogout.title = "Logout";
                                            btnLogout.setAttribute("type", "button");
                                            btnLogout.setAttribute("class", "btn btn-default");
                                            btnLogout.disabled = true;
                                            btnLogout.setAttribute("onclick", "setAgentLogout(this);");
                                            btnLogout.innerHTML = '<i class="fa fa-sign-out"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnLogout);
                                        }

                                    }
                                }
                            }
                        } else {
                            console.log("Req Reload Agents Fail: " + recData);
                        }
                        if (searchType == "AgentStatus") {
                            submitSearchTimer = setTimeout(submitSearch, 5000);
                        }
                    },
                    error: function (xhr, status, error) {
                        clearTimeout(agentStatusTimer);
                        submitSearchTimer = setTimeout(submitSearch, 5000);
                    }
                });


            }


            var agentStatusTimer = setTimeout(reloadAgentStates, 2000);
            function reloadAgentStates() {
                $.ajax({
                    type: "GET",
                    url: 'GetAllAgentsDashboardV1',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    cache: false,
                    timeout: 15000,
                    success: function (response) {
                        clearTimeout(agentStatusTimer);
                        if (response.retcode == "0") {
                            document.getElementById("divs_agents").innerHTML = "";
                            var index = 0;
                            var t = response.result;
                            if (t.length == 0) {
                                var ediv = document.createElement("div");
                                ediv.id = "ediv";
                                ediv.setAttribute("class", "col-md-8 col-sm-12 col-xs-12 info-box bg-white-active");
                                ediv.innerHTML = '<h3>No Record Found!</h3>';
                                document.getElementById("divs_agents").appendChild(ediv);
                            } else {
                                for (index = 0; index < t.length; ++index) {
                                    var ob = $("#" + t[index].agentId).empty().text('');
                                    var ti = moment().startOf('day').seconds(t[index].currentStateTime).format('HH:mm:ss');
                                    var agentId = t[index].agentId;
                                    var agentName = t[index].agentName;

                                    if (ob != undefined)
                                    {
                                        $('.cssload-loader1').hide();
                                        $('.overlay1').hide();
                                        //  bg.removeClass();
                                        if (t[index].status == 0)
                                        {//sign off

                                            var maindiv = document.createElement("div");
                                            maindiv.id = "maindiv" + agentId;
                                            maindiv.setAttribute("class", "col-md-6 col-sm-12 col-xs-12");
                                            document.getElementById("divs_agents").appendChild(maindiv);

                                            var agent_div = document.createElement("div");
                                            agent_div.id = "divbg" + agentId;
                                            agent_div.setAttribute("class", "info-box bg-red-active");
                                            document.getElementById("maindiv" + agentId).appendChild(agent_div);

                                            var span_icon = document.createElement("span");
                                            span_icon.id = "span_icon" + agentId;
                                            span_icon.setAttribute("class", "info-box-icon");
                                            span_icon.innerHTML = '<img src="${pageContext.request.contextPath}/resources/img/profileimage.png" class="img-circle" style="padding:10px;" alt="User Image">';
                                            document.getElementById("divbg" + agentId).appendChild(span_icon);

                                            var div_content = document.createElement("div");
                                            div_content.id = "div_content" + agentId;
                                            div_content.setAttribute("class", "info-box-content");
                                            document.getElementById("divbg" + agentId).appendChild(div_content);

                                            var div_agentID = document.createElement("div");
                                            div_agentID.id = "div_agentID" + agentId;
                                            div_agentID.setAttribute("style", "text-align:right;width:100%");
                                            div_agentID.innerHTML = agentName;
                                            document.getElementById("div_content" + agentId).appendChild(div_agentID);

                                            var span_info = document.createElement("span");
                                            span_info.id = "span_info" + agentId;
                                            span_info.setAttribute("class", "info-box-text");
                                            span_info.setAttribute("style", "float:left");
                                            span_info.innerHTML = agentId;
                                            document.getElementById("div_agentID" + agentId).appendChild(span_info);

                                            var span_time = document.createElement("span");
                                            span_time.id = "timeAgent" + agentId;
                                            span_time.setAttribute("class", "info-box-text pull-right");
                                            span_time.innerHTML = ti;
                                            document.getElementById("div_content" + agentId).appendChild(span_time);

                                            var span_boxno = document.createElement("span");
                                            span_boxno.id = agentId;
                                            span_boxno.setAttribute("class", "info-box-number");
                                            span_boxno.innerHTML = "Sign Off";
                                            document.getElementById("div_content" + agentId).appendChild(span_boxno);

                                            var div_btns = document.createElement("div");
                                            div_btns.id = "div_btns" + agentId;
                                            div_btns.setAttribute("class", "btn-group");
                                            document.getElementById("div_content" + agentId).appendChild(div_btns);

                                            var btnAdjust = document.createElement("button");
                                            btnAdjust.id = "skillAdj" + agentId;
                                            btnAdjust.title = "Skill Adjustment";
                                            btnAdjust.setAttribute("type", "button");
                                            btnAdjust.setAttribute("class", "btn btn-default disabled");
                                            btnAdjust.disabled = true;
                                            btnAdjust.setAttribute("onclick", "AdjustAgentSkill(this);");
                                            btnAdjust.innerHTML = '<i class="fa fa-wrench  "></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnAdjust);

                                            var btnInfo = document.createElement("button");
                                            btnInfo.id = "stat" + agentId;
                                            btnInfo.title = "Statistics";
                                            btnInfo.setAttribute("type", "button");
                                            btnInfo.setAttribute("class", "btn btn-default disabled");
                                            btnInfo.disabled = true;
                                            btnInfo.setAttribute("onclick", "getAgentStats(this);");
                                            btnInfo.innerHTML = '<i class="fa fa-info  "></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnInfo);

                                            var btnIdle = document.createElement("button");
                                            btnIdle.id = "idle" + agentId;
                                            btnIdle.title = "Ready";
                                            btnIdle.setAttribute("type", "button");
                                            btnIdle.setAttribute("class", "btn btn-default disabled");
                                            btnIdle.disabled = true;
                                            btnIdle.setAttribute("onclick", "setAgentIdle(this);");
                                            btnIdle.innerHTML = '<i class="fa-user-circle fa-align-center"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnIdle);

                                            var btnRest = document.createElement("button");
                                            btnRest.id = "res" + agentId;
                                            btnRest.title = "Break";
                                            btnRest.setAttribute("type", "button");
                                            btnRest.setAttribute("class", "btn btn-default disabled");
                                            btnRest.disabled = true;
                                            btnRest.setAttribute("onclick", "setAgentRest(this);");
                                            btnRest.innerHTML = '<i class="fa fa-hourglass"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnRest);

                                            var btnBusy = document.createElement("button");
                                            btnBusy.id = "bus" + agentId;
                                            btnBusy.title = "Not Ready";
                                            btnBusy.setAttribute("type", "button");
                                            btnBusy.setAttribute("class", "btn btn-default disabled");
                                            btnBusy.disabled = true;
                                            btnBusy.setAttribute("onclick", "setAgentBusy(this);");
                                            btnBusy.innerHTML = '<i class="fa fa-minus-circle"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnBusy);

                                            var btnListen = document.createElement("button");
                                            btnListen.id = "Listen" + agentId;
                                            btnListen.title = "Listening";
                                            btnListen.setAttribute("type", "button");
                                            btnListen.setAttribute("class", "btn btn-default disabled");
                                            btnListen.disabled = true;
                                            btnListen.setAttribute("onclick", "setAgentListening(this);");
                                            btnListen.innerHTML = '<i class="fa fa-headphones"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnListen);

                                            var btnInsert = document.createElement("button");
                                            btnInsert.id = "Insert" + agentId;
                                            btnInsert.title = "Shadowing";
                                            btnInsert.setAttribute("type", "button");
                                            btnInsert.setAttribute("class", "btn btn-default disabled");
                                            btnInsert.disabled = true;
                                            btnInsert.setAttribute("onclick", "setAgentInserting(this);");
                                            btnInsert.innerHTML = '<i class="fa fa-tty"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnInsert);

                                            var btnStop = document.createElement("button");
                                            btnStop.id = "stop" + agentId;
                                            btnStop.title = "Stop Listening/Shadowing";
                                            btnStop.setAttribute("type", "button");
                                            btnStop.setAttribute("class", "btn btn-default disabled");
                                            btnStop.disabled = true;
                                            btnStop.setAttribute("onclick", "setAgentStopListeningInserting(this);");
                                            btnStop.innerHTML = '<i class="fa fa-stop-circle-o"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnStop);

                                            var btnLogout = document.createElement("button");
                                            btnLogout.id = "log" + agentId;
                                            btnLogout.title = "Logout";
                                            btnLogout.setAttribute("type", "button");
                                            btnLogout.setAttribute("class", "btn btn-default disabled");
                                            btnLogout.disabled = true;
                                            btnLogout.setAttribute("onclick", "setAgentLogout(this);");
                                            btnLogout.innerHTML = '<i class="fa fa-sign-out"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnLogout);

                                        } else if (t[index].status == 2) {
                                            //idle

                                            var maindiv = document.createElement("div");
                                            maindiv.id = "maindiv" + agentId;
                                            maindiv.setAttribute("class", "col-md-6 col-sm-12 col-xs-12");
                                            document.getElementById("divs_agents").appendChild(maindiv);

                                            var agent_div = document.createElement("div");
                                            agent_div.id = "divbg" + agentId;
                                            agent_div.setAttribute("class", "info-box bg-green-active");
                                            document.getElementById("maindiv" + agentId).appendChild(agent_div);

                                            var span_icon = document.createElement("span");
                                            span_icon.id = "span_icon" + agentId;
                                            span_icon.setAttribute("class", "info-box-icon");
                                            span_icon.innerHTML = '<img src="${pageContext.request.contextPath}/resources/img/profileimage.png" class="img-circle" style="padding:10px;" alt="User Image">';
                                            document.getElementById("divbg" + agentId).appendChild(span_icon);

                                            var div_content = document.createElement("div");
                                            div_content.id = "div_content" + agentId;
                                            div_content.setAttribute("class", "info-box-content");
                                            document.getElementById("divbg" + agentId).appendChild(div_content);

                                            var div_agentID = document.createElement("div");
                                            div_agentID.id = "div_agentID" + agentId;
                                            div_agentID.setAttribute("style", "text-align:right;width:100%");
                                            div_agentID.innerHTML = agentName;
                                            document.getElementById("div_content" + agentId).appendChild(div_agentID);

                                            var span_info = document.createElement("span");
                                            span_info.id = "span_info" + agentId;
                                            span_info.setAttribute("class", "info-box-text");
                                            span_info.setAttribute("style", "float:left");
                                            span_info.innerHTML = agentId;
                                            document.getElementById("div_agentID" + agentId).appendChild(span_info);

                                            var span_time = document.createElement("span");
                                            span_time.id = "timeAgent" + agentId;
                                            span_time.setAttribute("class", "info-box-text pull-right");
                                            span_time.innerHTML = ti;
                                            document.getElementById("div_content" + agentId).appendChild(span_time);

                                            var span_boxno = document.createElement("span");
                                            span_boxno.id = agentId;
                                            span_boxno.setAttribute("class", "info-box-number");
                                            span_boxno.innerHTML = "Ready";
                                            document.getElementById("div_content" + agentId).appendChild(span_boxno);

                                            var div_btns = document.createElement("div");
                                            div_btns.id = "div_btns" + agentId;
                                            div_btns.setAttribute("class", "btn-group");
                                            document.getElementById("div_content" + agentId).appendChild(div_btns);

                                            var btnAdjust = document.createElement("button");
                                            btnAdjust.id = "skillAdj" + agentId;
                                            btnAdjust.title = "Skill Adjustment";
                                            btnAdjust.setAttribute("type", "button");
                                            btnAdjust.setAttribute("class", "btn btn-default");
                                            btnAdjust.disabled = false;
                                            btnAdjust.setAttribute("onclick", "AdjustAgentSkill(this);");
                                            btnAdjust.innerHTML = '<i class="fa fa-wrench  "></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnAdjust);

                                            var btnInfo = document.createElement("button");
                                            btnInfo.id = "stat" + agentId;
                                            btnInfo.title = "Statistics";
                                            btnInfo.setAttribute("type", "button");
                                            btnInfo.setAttribute("class", "btn btn-default");
                                            btnInfo.disabled = false;
                                            btnInfo.setAttribute("onclick", "getAgentStats(this);");
                                            btnInfo.innerHTML = '<i class="fa fa-info  "></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnInfo);

                                            var btnIdle = document.createElement("button");
                                            btnIdle.id = "idle" + agentId;
                                            btnIdle.title = "Ready";
                                            btnIdle.setAttribute("type", "button");
                                            btnIdle.setAttribute("class", "btn btn-default disabled");
                                            btnIdle.disabled = true;
                                            btnIdle.setAttribute("onclick", "setAgentIdle(this);");
                                            btnIdle.innerHTML = '<i class="fa-user-circle fa-align-center"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnIdle);

                                            var btnRest = document.createElement("button");
                                            btnRest.id = "res" + agentId;
                                            btnRest.title = "Break";
                                            btnRest.setAttribute("type", "button");
                                            btnRest.setAttribute("class", "btn btn-default");
                                            btnRest.disabled = false;
                                            btnRest.setAttribute("onclick", "setAgentRest(this);");
                                            btnRest.innerHTML = '<i class="fa fa-hourglass"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnRest);

                                            var btnBusy = document.createElement("button");
                                            btnBusy.id = "bus" + agentId;
                                            btnBusy.title = "Not Ready";
                                            btnBusy.setAttribute("type", "button");
                                            btnBusy.setAttribute("class", "btn btn-default");
                                            btnBusy.disabled = false;
                                            btnBusy.setAttribute("onclick", "setAgentBusy(this);");
                                            btnBusy.innerHTML = '<i class="fa fa-minus-circle"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnBusy);

                                            var btnListen = document.createElement("button");
                                            btnListen.id = "Listen" + agentId;
                                            btnListen.title = "Listening";
                                            btnListen.setAttribute("type", "button");
                                            btnListen.setAttribute("class", "btn btn-default disabled");
                                            btnListen.disabled = true;
                                            btnListen.setAttribute("onclick", "setAgentListening(this);");
                                            btnListen.innerHTML = '<i class="fa fa-headphones"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnListen);

                                            var btnInsert = document.createElement("button");
                                            btnInsert.id = "Insert" + agentId;
                                            btnInsert.title = "Shadowing";
                                            btnInsert.setAttribute("type", "button");
                                            btnInsert.setAttribute("class", "btn btn-default disabled");
                                            btnInsert.disabled = true;
                                            btnInsert.setAttribute("onclick", "setAgentInserting(this);");
                                            btnInsert.innerHTML = '<i class="fa fa-tty"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnInsert);

                                            var btnStop = document.createElement("button");
                                            btnStop.id = "stop" + agentId;
                                            btnStop.title = "Stop Listening/Shadowing";
                                            btnStop.setAttribute("type", "button");
                                            btnStop.setAttribute("class", "btn btn-default disabled");
                                            btnStop.disabled = true;
                                            btnStop.setAttribute("onclick", "setAgentStopListeningInserting(this);");
                                            btnStop.innerHTML = '<i class="fa fa-stop-circle-o"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnStop);

                                            var btnLogout = document.createElement("button");
                                            btnLogout.id = "log" + agentId;
                                            btnLogout.title = "Logout";
                                            btnLogout.setAttribute("type", "button");
                                            btnLogout.setAttribute("class", "btn btn-default");
                                            btnLogout.disabled = false;
                                            btnLogout.setAttribute("onclick", "setAgentLogout(this);");
                                            btnLogout.innerHTML = '<i class="fa fa-sign-out"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnLogout);


                                        } else if (t[index].status == 3) {
                                            //busy

                                            var maindiv = document.createElement("div");
                                            maindiv.id = "maindiv" + agentId;
                                            maindiv.setAttribute("class", "col-md-6 col-sm-12 col-xs-12");
                                            document.getElementById("divs_agents").appendChild(maindiv);

                                            var agent_div = document.createElement("div");
                                            agent_div.id = "divbg" + agentId;
                                            agent_div.setAttribute("class", "info-box bg-blue-active");
                                            document.getElementById("maindiv" + agentId).appendChild(agent_div);

                                            var span_icon = document.createElement("span");
                                            span_icon.id = "span_icon" + agentId;
                                            span_icon.setAttribute("class", "info-box-icon");
                                            span_icon.innerHTML = '<img src="${pageContext.request.contextPath}/resources/img/profileimage.png" class="img-circle" style="padding:10px;" alt="User Image">';
                                            document.getElementById("divbg" + agentId).appendChild(span_icon);

                                            var div_content = document.createElement("div");
                                            div_content.id = "div_content" + agentId;
                                            div_content.setAttribute("class", "info-box-content");
                                            document.getElementById("divbg" + agentId).appendChild(div_content);

                                            var div_agentID = document.createElement("div");
                                            div_agentID.id = "div_agentID" + agentId;
                                            div_agentID.setAttribute("style", "text-align:right;width:100%");
                                            div_agentID.innerHTML = agentName;
                                            document.getElementById("div_content" + agentId).appendChild(div_agentID);

                                            var span_info = document.createElement("span");
                                            span_info.id = "span_info" + agentId;
                                            span_info.setAttribute("class", "info-box-text");
                                            span_info.setAttribute("style", "float:left");
                                            span_info.innerHTML = agentId;
                                            document.getElementById("div_agentID" + agentId).appendChild(span_info);

                                            var span_time = document.createElement("span");
                                            span_time.id = "timeAgent" + agentId;
                                            span_time.setAttribute("class", "info-box-text pull-right");
                                            span_time.innerHTML = ti;
                                            document.getElementById("div_content" + agentId).appendChild(span_time);

                                            var span_boxno = document.createElement("span");
                                            span_boxno.id = agentId;
                                            span_boxno.setAttribute("class", "info-box-number");
                                            span_boxno.innerHTML = "Not Ready";
                                            document.getElementById("div_content" + agentId).appendChild(span_boxno);

                                            var div_btns = document.createElement("div");
                                            div_btns.id = "div_btns" + agentId;
                                            div_btns.setAttribute("class", "btn-group");
                                            document.getElementById("div_content" + agentId).appendChild(div_btns);

                                            var btnAdjust = document.createElement("button");
                                            btnAdjust.id = "skillAdj" + agentId;
                                            btnAdjust.title = "Skill Adjustment";
                                            btnAdjust.setAttribute("type", "button");
                                            btnAdjust.setAttribute("class", "btn btn-default");
                                            btnAdjust.disabled = false;
                                            btnAdjust.setAttribute("onclick", "AdjustAgentSkill(this);");
                                            btnAdjust.innerHTML = '<i class="fa fa-wrench  "></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnAdjust);

                                            var btnInfo = document.createElement("button");
                                            btnInfo.id = "stat" + agentId;
                                            btnInfo.title = "Statistics";
                                            btnInfo.setAttribute("type", "button");
                                            btnInfo.setAttribute("class", "btn btn-default");
                                            btnInfo.disabled = false;
                                            btnInfo.setAttribute("onclick", "getAgentStats(this);");
                                            btnInfo.innerHTML = '<i class="fa fa-info  "></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnInfo);

                                            var btnIdle = document.createElement("button");
                                            btnIdle.id = "idle" + agentId;
                                            btnIdle.title = "Ready";
                                            btnIdle.setAttribute("type", "button");
                                            btnIdle.setAttribute("class", "btn btn-default");
                                            btnIdle.disabled = false;
                                            btnIdle.setAttribute("onclick", "setAgentIdle(this);");
                                            btnIdle.innerHTML = '<i class="fa-user-circle fa-align-center"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnIdle);

                                            var btnRest = document.createElement("button");
                                            btnRest.id = "res" + agentId;
                                            btnRest.title = "Break";
                                            btnRest.setAttribute("type", "button");
                                            btnRest.setAttribute("class", "btn btn-default");
                                            btnRest.disabled = false;
                                            btnRest.setAttribute("onclick", "setAgentRest(this);");
                                            btnRest.innerHTML = '<i class="fa fa-hourglass"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnRest);

                                            var btnBusy = document.createElement("button");
                                            btnBusy.id = "bus" + agentId;
                                            btnBusy.title = "Not Ready";
                                            btnBusy.setAttribute("type", "button");
                                            btnBusy.setAttribute("class", "btn btn-default disabled");
                                            btnBusy.disabled = true;
                                            btnBusy.setAttribute("onclick", "setAgentBusy(this);");
                                            btnBusy.innerHTML = '<i class="fa fa-minus-circle"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnBusy);

                                            var btnListen = document.createElement("button");
                                            btnListen.id = "Listen" + agentId;
                                            btnListen.title = "Listening";
                                            btnListen.setAttribute("type", "button");
                                            btnListen.setAttribute("class", "btn btn-default disabled");
                                            btnListen.disabled = true;
                                            btnListen.setAttribute("onclick", "setAgentListening(this);");
                                            btnListen.innerHTML = '<i class="fa fa-headphones"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnListen);

                                            var btnInsert = document.createElement("button");
                                            btnInsert.id = "Insert" + agentId;
                                            btnInsert.title = "Shadowing";
                                            btnInsert.setAttribute("type", "button");
                                            btnInsert.setAttribute("class", "btn btn-default disabled");
                                            btnInsert.disabled = true;
                                            btnInsert.setAttribute("onclick", "setAgentInserting(this);");
                                            btnInsert.innerHTML = '<i class="fa fa-tty"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnInsert);

                                            var btnStop = document.createElement("button");
                                            btnStop.id = "stop" + agentId;
                                            btnStop.title = "Stop Listening/Shadowing";
                                            btnStop.setAttribute("type", "button");
                                            btnStop.setAttribute("class", "btn btn-default disabled");
                                            btnStop.disabled = true;
                                            btnStop.setAttribute("onclick", "setAgentStopListeningInserting(this);");
                                            btnStop.innerHTML = '<i class="fa fa-stop-circle-o"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnStop);

                                            var btnLogout = document.createElement("button");
                                            btnLogout.id = "log" + agentId;
                                            btnLogout.title = "Logout";
                                            btnLogout.setAttribute("type", "button");
                                            btnLogout.setAttribute("class", "btn btn-default");
                                            btnLogout.disabled = false;
                                            btnLogout.setAttribute("onclick", "setAgentLogout(this);");
                                            btnLogout.innerHTML = '<i class="fa fa-sign-out"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnLogout);

                                        } else if (t[index].status == 27) {
                                            //work
                                            var maindiv = document.createElement("div");
                                            maindiv.id = "maindiv" + agentId;
                                            maindiv.setAttribute("class", "col-md-6 col-sm-12 col-xs-12");
                                            document.getElementById("divs_agents").appendChild(maindiv);

                                            var agent_div = document.createElement("div");
                                            agent_div.id = "divbg" + agentId;
                                            agent_div.setAttribute("class", "info-box bg-olive-active");
                                            document.getElementById("maindiv" + agentId).appendChild(agent_div);

                                            var span_icon = document.createElement("span");
                                            span_icon.id = "span_icon" + agentId;
                                            span_icon.setAttribute("class", "info-box-icon");
                                            span_icon.innerHTML = '<img src="${pageContext.request.contextPath}/resources/img/profileimage.png" class="img-circle" style="padding:10px;" alt="User Image">';
                                            document.getElementById("divbg" + agentId).appendChild(span_icon);

                                            var div_content = document.createElement("div");
                                            div_content.id = "div_content" + agentId;
                                            div_content.setAttribute("class", "info-box-content");
                                            document.getElementById("divbg" + agentId).appendChild(div_content);

                                            var div_agentID = document.createElement("div");
                                            div_agentID.id = "div_agentID" + agentId;
                                            div_agentID.setAttribute("style", "text-align:right;width:100%");
                                            div_agentID.innerHTML = agentName;
                                            document.getElementById("div_content" + agentId).appendChild(div_agentID);

                                            var span_info = document.createElement("span");
                                            span_info.id = "span_info" + agentId;
                                            span_info.setAttribute("class", "info-box-text");
                                            span_info.setAttribute("style", "float:left");
                                            span_info.innerHTML = agentId;
                                            document.getElementById("div_agentID" + agentId).appendChild(span_info);

                                            var span_time = document.createElement("span");
                                            span_time.id = "timeAgent" + agentId;
                                            span_time.setAttribute("class", "info-box-text pull-right");
                                            span_time.innerHTML = ti;
                                            document.getElementById("div_content" + agentId).appendChild(span_time);

                                            var span_boxno = document.createElement("span");
                                            span_boxno.id = agentId;
                                            span_boxno.setAttribute("class", "info-box-number");
                                            span_boxno.innerHTML = "Work";
                                            document.getElementById("div_content" + agentId).appendChild(span_boxno);

                                            var div_btns = document.createElement("div");
                                            div_btns.id = "div_btns" + agentId;
                                            div_btns.setAttribute("class", "btn-group");
                                            document.getElementById("div_content" + agentId).appendChild(div_btns);

                                            var btnAdjust = document.createElement("button");
                                            btnAdjust.id = "skillAdj" + agentId;
                                            btnAdjust.title = "Skill Adjustment";
                                            btnAdjust.setAttribute("type", "button");
                                            btnAdjust.setAttribute("class", "btn btn-default");
                                            btnAdjust.disabled = false;
                                            btnAdjust.setAttribute("onclick", "AdjustAgentSkill(this);");
                                            btnAdjust.innerHTML = '<i class="fa fa-wrench  "></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnAdjust);

                                            var btnInfo = document.createElement("button");
                                            btnInfo.id = "stat" + agentId;
                                            btnInfo.title = "Statistics";
                                            btnInfo.setAttribute("type", "button");
                                            btnInfo.setAttribute("class", "btn btn-default");
                                            btnInfo.disabled = false;
                                            btnInfo.setAttribute("onclick", "getAgentStats(this);");
                                            btnInfo.innerHTML = '<i class="fa fa-info  "></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnInfo);

                                            var btnIdle = document.createElement("button");
                                            btnIdle.id = "idle" + agentId;
                                            btnIdle.title = "Ready";
                                            btnIdle.setAttribute("type", "button");
                                            btnIdle.setAttribute("class", "btn btn-default disabled");
                                            btnIdle.disabled = true;
                                            btnIdle.setAttribute("onclick", "setAgentIdle(this);");
                                            btnIdle.innerHTML = '<i class="fa-user-circle fa-align-center"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnIdle);

                                            var btnRest = document.createElement("button");
                                            btnRest.id = "res" + agentId;
                                            btnRest.title = "Break";
                                            btnRest.setAttribute("type", "button");
                                            btnRest.setAttribute("class", "btn btn-default");
                                            btnRest.disabled = false;
                                            btnRest.setAttribute("onclick", "setAgentRest(this);");
                                            btnRest.innerHTML = '<i class="fa fa-hourglass"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnRest);

                                            var btnBusy = document.createElement("button");
                                            btnBusy.id = "bus" + agentId;
                                            btnBusy.title = "Not Ready";
                                            btnBusy.setAttribute("type", "button");
                                            btnBusy.setAttribute("class", "btn btn-default");
                                            btnBusy.disabled = false;
                                            btnBusy.setAttribute("onclick", "setAgentBusy(this);");
                                            btnBusy.innerHTML = '<i class="fa fa-minus-circle"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnBusy);

                                            var btnListen = document.createElement("button");
                                            btnListen.id = "Listen" + agentId;
                                            btnListen.title = "Listening";
                                            btnListen.setAttribute("type", "button");
                                            btnListen.setAttribute("class", "btn btn-default disabled");
                                            btnListen.disabled = true;
                                            btnListen.setAttribute("onclick", "setAgentListening(this);");
                                            btnListen.innerHTML = '<i class="fa fa-headphones"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnListen);

                                            var btnInsert = document.createElement("button");
                                            btnInsert.id = "Insert" + agentId;
                                            btnInsert.title = "Shadowing";
                                            btnInsert.setAttribute("type", "button");
                                            btnInsert.setAttribute("class", "btn btn-default disabled");
                                            btnInsert.disabled = true;
                                            btnInsert.setAttribute("onclick", "setAgentInserting(this);");
                                            btnInsert.innerHTML = '<i class="fa fa-tty"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnInsert);

                                            var btnStop = document.createElement("button");
                                            btnStop.id = "stop" + agentId;
                                            btnStop.title = "Stop Listening/Shadowing";
                                            btnStop.setAttribute("type", "button");
                                            btnStop.setAttribute("class", "btn btn-default disabled");
                                            btnStop.disabled = true;
                                            btnStop.setAttribute("onclick", "setAgentStopListeningInserting(this);");
                                            btnStop.innerHTML = '<i class="fa fa-stop-circle-o"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnStop);

                                            var btnLogout = document.createElement("button");
                                            btnLogout.id = "log" + agentId;
                                            btnLogout.title = "Logout";
                                            btnLogout.setAttribute("type", "button");
                                            btnLogout.setAttribute("class", "btn btn-default");
                                            btnLogout.disabled = false;
                                            btnLogout.setAttribute("onclick", "setAgentLogout(this);");
                                            btnLogout.innerHTML = '<i class="fa fa-sign-out"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnLogout);

                                        } else if (t[index].status == 4) {
                                            //talking

                                            var maindiv = document.createElement("div");
                                            maindiv.id = "maindiv" + agentId;
                                            maindiv.setAttribute("class", "col-md-6 col-sm-12 col-xs-12");
                                            document.getElementById("divs_agents").appendChild(maindiv);

                                            var agent_div = document.createElement("div");
                                            agent_div.id = "divbg" + agentId;
                                            agent_div.setAttribute("class", "info-box bg-orange-active");
                                            document.getElementById("maindiv" + agentId).appendChild(agent_div);

                                            var span_icon = document.createElement("span");
                                            span_icon.id = "span_icon" + agentId;
                                            span_icon.setAttribute("class", "info-box-icon");
                                            span_icon.innerHTML = '<img src="${pageContext.request.contextPath}/resources/img/profileimage.png" class="img-circle" style="padding:10px;" alt="User Image">';
                                            document.getElementById("divbg" + agentId).appendChild(span_icon);

                                            var div_content = document.createElement("div");
                                            div_content.id = "div_content" + agentId;
                                            div_content.setAttribute("class", "info-box-content");
                                            document.getElementById("divbg" + agentId).appendChild(div_content);

                                            var div_agentID = document.createElement("div");
                                            div_agentID.id = "div_agentID" + agentId;
                                            div_agentID.setAttribute("style", "text-align:right;width:100%");
                                            div_agentID.innerHTML = agentName;
                                            document.getElementById("div_content" + agentId).appendChild(div_agentID);

                                            var span_info = document.createElement("span");
                                            span_info.id = "span_info" + agentId;
                                            span_info.setAttribute("class", "info-box-text");
                                            span_info.setAttribute("style", "float:left");
                                            span_info.innerHTML = agentId;
                                            document.getElementById("div_agentID" + agentId).appendChild(span_info);

                                            var span_time = document.createElement("span");
                                            span_time.id = "timeAgent$" + agentId;
                                            span_time.setAttribute("class", "info-box-text pull-right");
                                            span_time.innerHTML = ti;
                                            document.getElementById("div_content" + agentId).appendChild(span_time);

                                            var span_boxno = document.createElement("span");
                                            span_boxno.id = agentId;
                                            span_boxno.setAttribute("class", "info-box-number");
                                            span_boxno.innerHTML = "Talking";
                                            document.getElementById("div_content" + agentId).appendChild(span_boxno);

                                            var div_btns = document.createElement("div");
                                            div_btns.id = "div_btns" + agentId;
                                            div_btns.setAttribute("class", "btn-group");
                                            document.getElementById("div_content" + agentId).appendChild(div_btns);

                                            var btnAdjust = document.createElement("button");
                                            btnAdjust.id = "skillAdj" + agentId;
                                            btnAdjust.title = "Skill Adjustment";
                                            btnAdjust.setAttribute("type", "button");
                                            btnAdjust.setAttribute("class", "btn btn-default");
                                            btnAdjust.disabled = false;
                                            btnAdjust.setAttribute("onclick", "AdjustAgentSkill(this);");
                                            btnAdjust.innerHTML = '<i class="fa fa-wrench  "></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnAdjust);

                                            var btnInfo = document.createElement("button");
                                            btnInfo.id = "stat" + agentId;
                                            btnInfo.title = "Statistics";
                                            btnInfo.setAttribute("type", "button");
                                            btnInfo.setAttribute("class", "btn btn-default");
                                            btnInfo.disabled = false;
                                            btnInfo.setAttribute("onclick", "getAgentStats(this);");
                                            btnInfo.innerHTML = '<i class="fa fa-info  "></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnInfo);

                                            var btnIdle = document.createElement("button");
                                            btnIdle.id = "idle" + agentId;
                                            btnIdle.title = "Ready";
                                            btnIdle.setAttribute("type", "button");
                                            btnIdle.setAttribute("class", "btn btn-default");
                                            btnIdle.disabled = false;
                                            btnIdle.setAttribute("onclick", "setAgentIdle(this);");
                                            btnIdle.innerHTML = '<i class="fa-user-circle fa-align-center"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnIdle);

                                            var btnRest = document.createElement("button");
                                            btnRest.id = "res" + agentId;
                                            btnRest.title = "Break";
                                            btnRest.setAttribute("type", "button");
                                            btnRest.setAttribute("class", "btn btn-default");
                                            btnRest.disabled = false;
                                            btnRest.setAttribute("onclick", "setAgentRest(this);");
                                            btnRest.innerHTML = '<i class="fa fa-hourglass"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnRest);

                                            var btnBusy = document.createElement("button");
                                            btnBusy.id = "bus" + agentId;
                                            btnBusy.title = "Not Ready";
                                            btnBusy.setAttribute("type", "button");
                                            btnBusy.setAttribute("class", "btn btn-default");
                                            btnBusy.disabled = false;
                                            btnBusy.setAttribute("onclick", "setAgentBusy(this);");
                                            btnBusy.innerHTML = '<i class="fa fa-minus-circle"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnBusy);

                                            var btnListen = document.createElement("button");
                                            btnListen.id = "Listen" + agentId;
                                            btnListen.title = "Listening";
                                            btnListen.setAttribute("type", "button");
                                            btnListen.setAttribute("class", "btn btn-default");
                                            btnListen.disabled = false;
                                            btnListen.setAttribute("onclick", "setAgentListening(this);");
                                            btnListen.innerHTML = '<i class="fa fa-headphones"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnListen);

                                            var btnInsert = document.createElement("button");
                                            btnInsert.id = "Insert" + agentId;
                                            btnInsert.title = "Shadowing";
                                            btnInsert.setAttribute("type", "button");
                                            btnInsert.setAttribute("class", "btn btn-default");
                                            btnInsert.disabled = false;
                                            btnInsert.setAttribute("onclick", "setAgentInserting(this);");
                                            btnInsert.innerHTML = '<i class="fa fa-tty"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnInsert);

                                            var btnStop = document.createElement("button");
                                            btnStop.id = "stop" + agentId;
                                            btnStop.title = "Stop Listening/Shadowing";
                                            btnStop.setAttribute("type", "button");
                                            btnStop.setAttribute("class", "btn btn-default disabled");
                                            btnStop.disabled = true;
                                            btnStop.setAttribute("onclick", "setAgentStopListeningInserting(this);");
                                            btnStop.innerHTML = '<i class="fa fa-stop-circle-o"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnStop);

                                            var btnLogout = document.createElement("button");
                                            btnLogout.id = "log" + agentId;
                                            btnLogout.title = "Logout";
                                            btnLogout.setAttribute("type", "button");
                                            btnLogout.setAttribute("class", "btn btn-default");
                                            btnLogout.disabled = false;
                                            btnLogout.setAttribute("onclick", "setAgentLogout(this);");
                                            btnLogout.innerHTML = '<i class="fa fa-sign-out"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnLogout);

                                        } else if (t[index].status == 5) {
                                            //Answering

                                            var maindiv = document.createElement("div");
                                            maindiv.id = "maindiv" + agentId;
                                            maindiv.setAttribute("class", "col-md-6 col-sm-12 col-xs-12");
                                            document.getElementById("divs_agents").appendChild(maindiv);

                                            var agent_div = document.createElement("div");
                                            agent_div.id = "divbg" + agentId;
                                            agent_div.setAttribute("class", "info-box bg-orange");
                                            document.getElementById("maindiv" + agentId).appendChild(agent_div);

                                            var span_icon = document.createElement("span");
                                            span_icon.id = "span_icon" + agentId;
                                            span_icon.setAttribute("class", "info-box-icon");
                                            span_icon.innerHTML = '<img src="${pageContext.request.contextPath}/resources/img/profileimage.png" class="img-circle" style="padding:10px;" alt="User Image">';
                                            document.getElementById("divbg" + agentId).appendChild(span_icon);

                                            var div_content = document.createElement("div");
                                            div_content.id = "div_content" + agentId;
                                            div_content.setAttribute("class", "info-box-content");
                                            document.getElementById("divbg" + agentId).appendChild(div_content);

                                            var div_agentID = document.createElement("div");
                                            div_agentID.id = "div_agentID" + agentId;
                                            div_agentID.setAttribute("style", "text-align:right;width:100%");
                                            div_agentID.innerHTML = agentName;
                                            document.getElementById("div_content" + agentId).appendChild(div_agentID);

                                            var span_info = document.createElement("span");
                                            span_info.id = "span_info" + agentId;
                                            span_info.setAttribute("class", "info-box-text");
                                            span_info.setAttribute("style", "float:left");
                                            span_info.innerHTML = agentId;
                                            document.getElementById("div_agentID" + agentId).appendChild(span_info);

                                            var span_time = document.createElement("span");
                                            span_time.id = "timeAgent$" + agentId;
                                            span_time.setAttribute("class", "info-box-text pull-right");
                                            span_time.innerHTML = ti;
                                            document.getElementById("div_content" + agentId).appendChild(span_time);

                                            var span_boxno = document.createElement("span");
                                            span_boxno.id = agentId;
                                            span_boxno.setAttribute("class", "info-box-number");
                                            span_boxno.innerHTML = "Answering";
                                            document.getElementById("div_content" + agentId).appendChild(span_boxno);

                                            var div_btns = document.createElement("div");
                                            div_btns.id = "div_btns" + agentId;
                                            div_btns.setAttribute("class", "btn-group");
                                            document.getElementById("div_content" + agentId).appendChild(div_btns);

                                            var btnAdjust = document.createElement("button");
                                            btnAdjust.id = "skillAdj" + agentId;
                                            btnAdjust.title = "Skill Adjustment";
                                            btnAdjust.setAttribute("type", "button");
                                            btnAdjust.setAttribute("class", "btn btn-default");
                                            btnAdjust.disabled = false;
                                            btnAdjust.setAttribute("onclick", "AdjustAgentSkill(this);");
                                            btnAdjust.innerHTML = '<i class="fa fa-wrench  "></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnAdjust);

                                            var btnInfo = document.createElement("button");
                                            btnInfo.id = "stat" + agentId;
                                            btnInfo.title = "Statistics";
                                            btnInfo.setAttribute("type", "button");
                                            btnInfo.setAttribute("class", "btn btn-default");
                                            btnInfo.disabled = false;
                                            btnInfo.setAttribute("onclick", "getAgentStats(this);");
                                            btnInfo.innerHTML = '<i class="fa fa-info  "></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnInfo);

                                            var btnIdle = document.createElement("button");
                                            btnIdle.id = "idle" + agentId;
                                            btnIdle.title = "Ready";
                                            btnIdle.setAttribute("type", "button");
                                            btnIdle.setAttribute("class", "btn btn-default");
                                            btnIdle.disabled = false;
                                            btnIdle.setAttribute("onclick", "setAgentIdle(this);");
                                            btnIdle.innerHTML = '<i class="fa-user-circle fa-align-center"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnIdle);

                                            var btnRest = document.createElement("button");
                                            btnRest.id = "res" + agentId;
                                            btnRest.title = "Break";
                                            btnRest.setAttribute("type", "button");
                                            btnRest.setAttribute("class", "btn btn-default");
                                            btnRest.disabled = false;
                                            btnRest.setAttribute("onclick", "setAgentRest(this);");
                                            btnRest.innerHTML = '<i class="fa fa-hourglass"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnRest);

                                            var btnBusy = document.createElement("button");
                                            btnBusy.id = "bus" + agentId;
                                            btnBusy.title = "Not Ready";
                                            btnBusy.setAttribute("type", "button");
                                            btnBusy.setAttribute("class", "btn btn-default");
                                            btnBusy.disabled = false;
                                            btnBusy.setAttribute("onclick", "setAgentBusy(this);");
                                            btnBusy.innerHTML = '<i class="fa fa-minus-circle"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnBusy);

                                            var btnListen = document.createElement("button");
                                            btnListen.id = "Listen" + agentId;
                                            btnListen.title = "Listening";
                                            btnListen.setAttribute("type", "button");
                                            btnListen.setAttribute("class", "btn btn-default");
                                            btnListen.disabled = false;
                                            btnListen.setAttribute("onclick", "setAgentListening(this);");
                                            btnListen.innerHTML = '<i class="fa fa-headphones"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnListen);

                                            var btnInsert = document.createElement("button");
                                            btnInsert.id = "Insert" + agentId;
                                            btnInsert.title = "Shadowing";
                                            btnInsert.setAttribute("type", "button");
                                            btnInsert.setAttribute("class", "btn btn-default");
                                            btnInsert.disabled = false;
                                            btnInsert.setAttribute("onclick", "setAgentInserting(this);");
                                            btnInsert.innerHTML = '<i class="fa fa-tty"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnInsert);

                                            var btnStop = document.createElement("button");
                                            btnStop.id = "stop" + agentId;
                                            btnStop.title = "Stop Listening/Shadowing";
                                            btnStop.setAttribute("type", "button");
                                            btnStop.setAttribute("class", "btn btn-default disabled");
                                            btnStop.disabled = true;
                                            btnStop.setAttribute("onclick", "setAgentStopListeningInserting(this);");
                                            btnStop.innerHTML = '<i class="fa fa-stop-circle-o"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnStop);

                                            var btnLogout = document.createElement("button");
                                            btnLogout.id = "log" + agentId;
                                            btnLogout.title = "Logout";
                                            btnLogout.setAttribute("type", "button");
                                            btnLogout.setAttribute("class", "btn btn-default");
                                            btnLogout.disabled = false;
                                            btnLogout.setAttribute("onclick", "setAgentLogout(this);");
                                            btnLogout.innerHTML = '<i class="fa fa-sign-out"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnLogout);

                                        } else if (t[index].status == 26) {
                                            //rest/break

                                            var maindiv = document.createElement("div");
                                            maindiv.id = "maindiv" + agentId;
                                            maindiv.setAttribute("class", "col-md-6 col-sm-12 col-xs-12");
                                            document.getElementById("divs_agents").appendChild(maindiv);

                                            var agent_div = document.createElement("div");
                                            agent_div.id = "divbg" + agentId;
                                            agent_div.setAttribute("class", "info-box bg-yellow-active");
                                            document.getElementById("maindiv" + agentId).appendChild(agent_div);

                                            var span_icon = document.createElement("span");
                                            span_icon.id = "span_icon" + agentId;
                                            span_icon.setAttribute("class", "info-box-icon");
                                            span_icon.innerHTML = '<img src="${pageContext.request.contextPath}/resources/img/profileimage.png" class="img-circle" style="padding:10px;" alt="User Image">';
                                            document.getElementById("divbg" + agentId).appendChild(span_icon);

                                            var div_content = document.createElement("div");
                                            div_content.id = "div_content" + agentId;
                                            div_content.setAttribute("class", "info-box-content");
                                            document.getElementById("divbg" + agentId).appendChild(div_content);

                                            var div_agentID = document.createElement("div");
                                            div_agentID.id = "div_agentID" + agentId;
                                            div_agentID.setAttribute("style", "text-align:right;width:100%");
                                            div_agentID.innerHTML = agentName;
                                            document.getElementById("div_content" + agentId).appendChild(div_agentID);

                                            var span_info = document.createElement("span");
                                            span_info.id = "span_info" + agentId;
                                            span_info.setAttribute("class", "info-box-text");
                                            span_info.setAttribute("style", "float:left");
                                            span_info.innerHTML = agentId;
                                            document.getElementById("div_agentID" + agentId).appendChild(span_info);

                                            var span_time = document.createElement("span");
                                            span_time.id = "timeAgent$" + agentId;
                                            span_time.setAttribute("class", "info-box-text pull-right");
                                            span_time.innerHTML = ti;
                                            document.getElementById("div_content" + agentId).appendChild(span_time);

                                            var span_boxno = document.createElement("span");
                                            span_boxno.id = agentId;
                                            span_boxno.setAttribute("class", "info-box-number");
                                            span_boxno.innerHTML = "Break";
                                            document.getElementById("div_content" + agentId).appendChild(span_boxno);

                                            var div_btns = document.createElement("div");
                                            div_btns.id = "div_btns" + agentId;
                                            div_btns.setAttribute("class", "btn-group");
                                            document.getElementById("div_content" + agentId).appendChild(div_btns);

                                            var btnAdjust = document.createElement("button");
                                            btnAdjust.id = "skillAdj" + agentId;
                                            btnAdjust.title = "Skill Adjustment";
                                            btnAdjust.setAttribute("type", "button");
                                            btnAdjust.setAttribute("class", "btn btn-default");
                                            btnAdjust.disabled = false;
                                            btnAdjust.setAttribute("onclick", "AdjustAgentSkill(this);");
                                            btnAdjust.innerHTML = '<i class="fa fa-wrench  "></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnAdjust);

                                            var btnInfo = document.createElement("button");
                                            btnInfo.id = "stat" + agentId;
                                            btnInfo.title = "Statistics";
                                            btnInfo.setAttribute("type", "button");
                                            btnInfo.setAttribute("class", "btn btn-default");
                                            btnInfo.disabled = false;
                                            btnInfo.setAttribute("onclick", "getAgentStats(this);");
                                            btnInfo.innerHTML = '<i class="fa fa-info  "></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnInfo);

                                            var btnIdle = document.createElement("button");
                                            btnIdle.id = "idle" + agentId;
                                            btnIdle.title = "Ready";
                                            btnIdle.setAttribute("type", "button");
                                            btnIdle.setAttribute("class", "btn btn-default");
                                            btnIdle.disabled = false;
                                            btnIdle.setAttribute("onclick", "setAgentIdle(this);");
                                            btnIdle.innerHTML = '<i class="fa-user-circle fa-align-center"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnIdle);

                                            var btnRest = document.createElement("button");
                                            btnRest.id = "res" + agentId;
                                            btnRest.title = "Break";
                                            btnRest.setAttribute("type", "button");
                                            btnRest.setAttribute("class", "btn btn-default disabled");
                                            btnRest.disabled = true;
                                            btnRest.setAttribute("onclick", "setAgentRest(this);");
                                            btnRest.innerHTML = '<i class="fa fa-hourglass"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnRest);

                                            var btnBusy = document.createElement("button");
                                            btnBusy.id = "bus" + agentId;
                                            btnBusy.title = "Not Ready";
                                            btnBusy.setAttribute("type", "button");
                                            btnBusy.setAttribute("class", "btn btn-default");
                                            btnBusy.disabled = false;
                                            btnBusy.setAttribute("onclick", "setAgentBusy(this);");
                                            btnBusy.innerHTML = '<i class="fa fa-minus-circle"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnBusy);

                                            var btnListen = document.createElement("button");
                                            btnListen.id = "Listen" + agentId;
                                            btnListen.title = "Listening";
                                            btnListen.setAttribute("type", "button");
                                            btnListen.setAttribute("class", "btn btn-default disabled");
                                            btnListen.disabled = true;
                                            btnListen.setAttribute("onclick", "setAgentListening(this);");
                                            btnListen.innerHTML = '<i class="fa fa-headphones"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnListen);

                                            var btnInsert = document.createElement("button");
                                            btnInsert.id = "Insert" + agentId;
                                            btnInsert.title = "Shadowing";
                                            btnInsert.setAttribute("type", "button");
                                            btnInsert.setAttribute("class", "btn btn-default disabled");
                                            btnInsert.disabled = true;
                                            btnInsert.setAttribute("onclick", "setAgentInserting(this);");
                                            btnInsert.innerHTML = '<i class="fa fa-tty"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnInsert);

                                            var btnStop = document.createElement("button");
                                            btnStop.id = "stop" + agentId;
                                            btnStop.title = "Stop Listening/Shadowing";
                                            btnStop.setAttribute("type", "button");
                                            btnStop.setAttribute("class", "btn btn-default disabled");
                                            btnStop.disabled = true;
                                            btnStop.setAttribute("onclick", "setAgentStopListeningInserting(this);");
                                            btnStop.innerHTML = '<i class="fa fa-stop-circle-o"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnStop);

                                            var btnLogout = document.createElement("button");
                                            btnLogout.id = "log" + agentId;
                                            btnLogout.title = "Logout";
                                            btnLogout.setAttribute("type", "button");
                                            btnLogout.setAttribute("class", "btn btn-default");
                                            btnLogout.disabled = false;
                                            btnLogout.setAttribute("onclick", "setAgentLogout(this);");
                                            btnLogout.innerHTML = '<i class="fa fa-sign-out"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnLogout);

                                        } else if (t[index].status == 13) {
                                            //Transferred to IVR
                                            
                                            var maindiv = document.createElement("div");
                                            maindiv.id = "maindiv" + agentId;
                                            maindiv.setAttribute("class", "col-md-6 col-sm-12 col-xs-12");
                                            document.getElementById("divs_agents").appendChild(maindiv);

                                            var agent_div = document.createElement("div");
                                            agent_div.id = "divbg" + agentId;
                                            agent_div.setAttribute("class", "info-box bg-aqua-active");
                                            document.getElementById("maindiv" + agentId).appendChild(agent_div);

                                            var span_icon = document.createElement("span");
                                            span_icon.id = "span_icon" + agentId;
                                            span_icon.setAttribute("class", "info-box-icon");
                                            span_icon.innerHTML = '<img src="${pageContext.request.contextPath}/resources/img/profileimage.png" class="img-circle" style="padding:10px;" alt="User Image">';
                                            document.getElementById("divbg" + agentId).appendChild(span_icon);

                                            var div_content = document.createElement("div");
                                            div_content.id = "div_content" + agentId;
                                            div_content.setAttribute("class", "info-box-content");
                                            document.getElementById("divbg" + agentId).appendChild(div_content);

                                            var div_agentID = document.createElement("div");
                                            div_agentID.id = "div_agentID" + agentId;
                                            div_agentID.setAttribute("style", "text-align:right;width:100%");
                                            div_agentID.innerHTML = agentName;
                                            document.getElementById("div_content" + agentId).appendChild(div_agentID);

                                            var span_info = document.createElement("span");
                                            span_info.id = "span_info" + agentId;
                                            span_info.setAttribute("class", "info-box-text");
                                            span_info.setAttribute("style", "float:left");
                                            span_info.innerHTML = agentId;
                                            document.getElementById("div_agentID" + agentId).appendChild(span_info);

                                            var span_time = document.createElement("span");
                                            span_time.id = "timeAgent$" + agentId;
                                            span_time.setAttribute("class", "info-box-text pull-right");
                                            span_time.innerHTML = ti;
                                            document.getElementById("div_content" + agentId).appendChild(span_time);

                                            var span_boxno = document.createElement("span");
                                            span_boxno.id = agentId;
                                            span_boxno.setAttribute("class", "info-box-number");
                                            span_boxno.innerHTML = "Transferred to IVR";
                                            document.getElementById("div_content" + agentId).appendChild(span_boxno);

                                            var div_btns = document.createElement("div");
                                            div_btns.id = "div_btns" + agentId;
                                            div_btns.setAttribute("class", "btn-group");
                                            document.getElementById("div_content" + agentId).appendChild(div_btns);

                                            var btnAdjust = document.createElement("button");
                                            btnAdjust.id = "skillAdj" + agentId;
                                            btnAdjust.title = "Skill Adjustment";
                                            btnAdjust.setAttribute("type", "button");
                                            btnAdjust.setAttribute("class", "btn btn-default");
                                            btnAdjust.disabled = true;
                                            btnAdjust.setAttribute("onclick", "AdjustAgentSkill(this);");
                                            btnAdjust.innerHTML = '<i class="fa fa-wrench  "></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnAdjust);

                                            var btnInfo = document.createElement("button");
                                            btnInfo.id = "stat" + agentId;
                                            btnInfo.title = "Statistics";
                                            btnInfo.setAttribute("type", "button");
                                            btnInfo.setAttribute("class", "btn btn-default");
                                            btnInfo.disabled = false;
                                            btnInfo.setAttribute("onclick", "getAgentStats(this);");
                                            btnInfo.innerHTML = '<i class="fa fa-info  "></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnInfo);

                                            var btnIdle = document.createElement("button");
                                            btnIdle.id = "idle" + agentId;
                                            btnIdle.title = "Ready";
                                            btnIdle.setAttribute("type", "button");
                                            btnIdle.setAttribute("class", "btn btn-default");
                                            btnIdle.disabled = true;
                                            btnIdle.setAttribute("onclick", "setAgentIdle(this);");
                                            btnIdle.innerHTML = '<i class="fa-user-circle fa-align-center"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnIdle);

                                            var btnRest = document.createElement("button");
                                            btnRest.id = "res" + agentId;
                                            btnRest.title = "Break";
                                            btnRest.setAttribute("type", "button");
                                            btnRest.setAttribute("class", "btn btn-default disabled");
                                            btnRest.disabled = true;
                                            btnRest.setAttribute("onclick", "setAgentRest(this);");
                                            btnRest.innerHTML = '<i class="fa fa-hourglass"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnRest);

                                            var btnBusy = document.createElement("button");
                                            btnBusy.id = "bus" + agentId;
                                            btnBusy.title = "Not Ready";
                                            btnBusy.setAttribute("type", "button");
                                            btnBusy.setAttribute("class", "btn btn-default");
                                            btnBusy.disabled = true;
                                            btnBusy.setAttribute("onclick", "setAgentBusy(this);");
                                            btnBusy.innerHTML = '<i class="fa fa-minus-circle"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnBusy);

                                            var btnListen = document.createElement("button");
                                            btnListen.id = "Listen" + agentId;
                                            btnListen.title = "Listening";
                                            btnListen.setAttribute("type", "button");
                                            btnListen.setAttribute("class", "btn btn-default disabled");
                                            btnListen.disabled = true;
                                            btnListen.setAttribute("onclick", "setAgentListening(this);");
                                            btnListen.innerHTML = '<i class="fa fa-headphones"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnListen);

                                            var btnInsert = document.createElement("button");
                                            btnInsert.id = "Insert" + agentId;
                                            btnInsert.title = "Shadowing";
                                            btnInsert.setAttribute("type", "button");
                                            btnInsert.setAttribute("class", "btn btn-default disabled");
                                            btnInsert.disabled = true;
                                            btnInsert.setAttribute("onclick", "setAgentInserting(this);");
                                            btnInsert.innerHTML = '<i class="fa fa-tty"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnInsert);

                                            var btnStop = document.createElement("button");
                                            btnStop.id = "stop" + agentId;
                                            btnStop.title = "Stop Listening/Shadowing";
                                            btnStop.setAttribute("type", "button");
                                            btnStop.setAttribute("class", "btn btn-default disabled");
                                            btnStop.disabled = true;
                                            btnStop.setAttribute("onclick", "setAgentStopListeningInserting(this);");
                                            btnStop.innerHTML = '<i class="fa fa-stop-circle-o"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnStop);

                                            var btnLogout = document.createElement("button");
                                            btnLogout.id = "log" + agentId;
                                            btnLogout.title = "Logout";
                                            btnLogout.setAttribute("type", "button");
                                            btnLogout.setAttribute("class", "btn btn-default");
                                            btnLogout.disabled = true;
                                            btnLogout.setAttribute("onclick", "setAgentLogout(this);");
                                            btnLogout.innerHTML = '<i class="fa fa-sign-out"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnLogout);

                                        } else if (t[index].status == 30) {
                                            //On Hold

                                            var maindiv = document.createElement("div");
                                            maindiv.id = "maindiv" + agentId;
                                            maindiv.setAttribute("class", "col-md-6 col-sm-12 col-xs-12");
                                            document.getElementById("divs_agents").appendChild(maindiv);

                                            var agent_div = document.createElement("div");
                                            agent_div.id = "divbg" + agentId;
                                            agent_div.setAttribute("class", "info-box bg-purple-active");
                                            document.getElementById("maindiv" + agentId).appendChild(agent_div);

                                            var span_icon = document.createElement("span");
                                            span_icon.id = "span_icon" + agentId;
                                            span_icon.setAttribute("class", "info-box-icon");
                                            span_icon.innerHTML = '<img src="${pageContext.request.contextPath}/resources/img/profileimage.png" class="img-circle" style="padding:10px;" alt="User Image">';
                                            document.getElementById("divbg" + agentId).appendChild(span_icon);

                                            var div_content = document.createElement("div");
                                            div_content.id = "div_content" + agentId;
                                            div_content.setAttribute("class", "info-box-content");
                                            document.getElementById("divbg" + agentId).appendChild(div_content);

                                            var div_agentID = document.createElement("div");
                                            div_agentID.id = "div_agentID" + agentId;
                                            div_agentID.setAttribute("style", "text-align:right;width:100%");
                                            div_agentID.innerHTML = agentName;
                                            document.getElementById("div_content" + agentId).appendChild(div_agentID);

                                            var span_info = document.createElement("span");
                                            span_info.id = "span_info" + agentId;
                                            span_info.setAttribute("class", "info-box-text");
                                            span_info.setAttribute("style", "float:left");
                                            span_info.innerHTML = agentId;
                                            document.getElementById("div_agentID" + agentId).appendChild(span_info);

                                            var span_time = document.createElement("span");
                                            span_time.id = "timeAgent$" + agentId;
                                            span_time.setAttribute("class", "info-box-text pull-right");
                                            span_time.innerHTML = ti;
                                            document.getElementById("div_content" + agentId).appendChild(span_time);

                                            var span_boxno = document.createElement("span");
                                            span_boxno.id = agentId;
                                            span_boxno.setAttribute("class", "info-box-number");
                                            span_boxno.innerHTML = "On Hold";
                                            document.getElementById("div_content" + agentId).appendChild(span_boxno);

                                            var div_btns = document.createElement("div");
                                            div_btns.id = "div_btns" + agentId;
                                            div_btns.setAttribute("class", "btn-group");
                                            document.getElementById("div_content" + agentId).appendChild(div_btns);

                                            var btnAdjust = document.createElement("button");
                                            btnAdjust.id = "skillAdj" + agentId;
                                            btnAdjust.title = "Skill Adjustment";
                                            btnAdjust.setAttribute("type", "button");
                                            btnAdjust.setAttribute("class", "btn btn-default");
                                            btnAdjust.disabled = true;
                                            btnAdjust.setAttribute("onclick", "AdjustAgentSkill(this);");
                                            btnAdjust.innerHTML = '<i class="fa fa-wrench  "></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnAdjust);

                                            var btnInfo = document.createElement("button");
                                            btnInfo.id = "stat" + agentId;
                                            btnInfo.title = "Statistics";
                                            btnInfo.setAttribute("type", "button");
                                            btnInfo.setAttribute("class", "btn btn-default");
                                            btnInfo.disabled = false;
                                            btnInfo.setAttribute("onclick", "getAgentStats(this);");
                                            btnInfo.innerHTML = '<i class="fa fa-info  "></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnInfo);

                                            var btnIdle = document.createElement("button");
                                            btnIdle.id = "idle" + agentId;
                                            btnIdle.title = "Ready";
                                            btnIdle.setAttribute("type", "button");
                                            btnIdle.setAttribute("class", "btn btn-default");
                                            btnIdle.disabled = true;
                                            btnIdle.setAttribute("onclick", "setAgentIdle(this);");
                                            btnIdle.innerHTML = '<i class="fa-user-circle fa-align-center"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnIdle);

                                            var btnRest = document.createElement("button");
                                            btnRest.id = "res" + agentId;
                                            btnRest.title = "Break";
                                            btnRest.setAttribute("type", "button");
                                            btnRest.setAttribute("class", "btn btn-default disabled");
                                            btnRest.disabled = true;
                                            btnRest.setAttribute("onclick", "setAgentRest(this);");
                                            btnRest.innerHTML = '<i class="fa fa-hourglass"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnRest);

                                            var btnBusy = document.createElement("button");
                                            btnBusy.id = "bus" + agentId;
                                            btnBusy.title = "Not Ready";
                                            btnBusy.setAttribute("type", "button");
                                            btnBusy.setAttribute("class", "btn btn-default");
                                            btnBusy.disabled = true;
                                            btnBusy.setAttribute("onclick", "setAgentBusy(this);");
                                            btnBusy.innerHTML = '<i class="fa fa-minus-circle"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnBusy);

                                            var btnListen = document.createElement("button");
                                            btnListen.id = "Listen" + agentId;
                                            btnListen.title = "Listening";
                                            btnListen.setAttribute("type", "button");
                                            btnListen.setAttribute("class", "btn btn-default disabled");
                                            btnListen.disabled = true;
                                            btnListen.setAttribute("onclick", "setAgentListening(this);");
                                            btnListen.innerHTML = '<i class="fa fa-headphones"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnListen);

                                            var btnInsert = document.createElement("button");
                                            btnInsert.id = "Insert" + agentId;
                                            btnInsert.title = "Shadowing";
                                            btnInsert.setAttribute("type", "button");
                                            btnInsert.setAttribute("class", "btn btn-default disabled");
                                            btnInsert.disabled = true;
                                            btnInsert.setAttribute("onclick", "setAgentInserting(this);");
                                            btnInsert.innerHTML = '<i class="fa fa-tty"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnInsert);

                                            var btnStop = document.createElement("button");
                                            btnStop.id = "stop" + agentId;
                                            btnStop.title = "Stop Listening/Shadowing";
                                            btnStop.setAttribute("type", "button");
                                            btnStop.setAttribute("class", "btn btn-default disabled");
                                            btnStop.disabled = true;
                                            btnStop.setAttribute("onclick", "setAgentStopListeningInserting(this);");
                                            btnStop.innerHTML = '<i class="fa fa-stop-circle-o"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnStop);

                                            var btnLogout = document.createElement("button");
                                            btnLogout.id = "log" + agentId;
                                            btnLogout.title = "Logout";
                                            btnLogout.setAttribute("type", "button");
                                            btnLogout.setAttribute("class", "btn btn-default");
                                            btnLogout.disabled = true;
                                            btnLogout.setAttribute("onclick", "setAgentLogout(this);");
                                            btnLogout.innerHTML = '<i class="fa fa-sign-out"></i>';
                                            document.getElementById("div_btns" + agentId).appendChild(btnLogout);
                                        }

                                    }
                                }
                            }
                        } else {
                            console.log("Req Reload Agents Fail: " + recData);
                        }
                        agentStatusTimer = setTimeout(reloadAgentStates, 5000);
                    },
                    error: function (xhr, status, error) {
                        clearTimeout(agentStatusTimer);
                        agentStatusTimer = setTimeout(reloadAgentStates, 5000);
                    }
                });
            }

            function setAgentIdle(btnAgentIdle)
            {
                var btnAgentIdleID = btnAgentIdle.id;
                var workID = btnAgentIdleID.substring(4, btnAgentIdleID.length);
                $.ajax({
                    type: "GET",
                    url: 'agentsetidle',
                    data: {agentId: workID},
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (recData)
                    {
                        if (recData.retcode == "0")
                        {
                            $('.cssload-loader1').show();
                            $('.overlay1').show();
//                            reloadAgentStates();
                            Notifier.success('Set Agent Ready Successfully');
                            setTimeout(function ()
                            {
                                $.get("${pageContext.request.servletContext.contextPath}/agent/dashboard");
//                                $('.cssload-loader1').show();
//                                $('.overlay1').show();
                            }, 1000);
                        } else
                        {
                            Notifier.error('Set Agent Ready Failed');
                        }
                    },
                    error: function (xhr, status, error) {
                        console.log('Set Agent Ready Error ' + status);
                        Notifier.error('Set Agent Ready Failed');
                    }
                });
            }
            function setAgentBusy(btnAgentBusy) {
                var a = btnAgentBusy.id;
                var newVal = a.substring(3, a.length);
                $.ajax({
                    type: "GET",
                    url: 'agentsetnotready',
                    data: {agentId: newVal},
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (recData)
                    {
                        if (recData.retcode == "0")
                        {
                            $('.cssload-loader1').show();
                            $('.overlay1').show();
//                            reloadAgentStates();
                            Notifier.success('Set Agent Not Ready Successfully');
                            setTimeout(function ()
                            {
                                $.get("${pageContext.request.servletContext.contextPath}/agent/dashboard");
                                $('.cssload-loader1').show();
                                $('.overlay1').show();
                            }, 1000);
                        } else
                        {
                            Notifier.error('Set Agent Not Ready Failed');
                        }
                    },
                    error: function (xhr, status, error) {
                        Notifier.error('Set State Not Ready failed');
                        console.log('Start Not Ready Error ' + status);
                    }
                });
            }
            function setAgentRest(btnAgentRest) {
                $.ajax({
                    type: "GET",
                    url: 'agentgetbreaks',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (recData)
                    {
                        if (recData.retcode == "0")
                        {
                            $.each(recData.result, function (i, item) {
                                $("#restBrkOption").append("<option id= " + item.restReasonId + ">" + item.restReason + "</option>");
                            });
                        } else
                        {
                            Notifier.error(' Agent Breaks  load Failed');
                        }
                    },
                    error: function (xhr, status, error) {
                        Notifier.error(' Agent Breaks  load Failed');
                        console.log('Agent Break load Error ' + status);
                    }
                });
                var a = btnAgentRest.id;
                var newVal = a.substring(3, a.length);
                $('#btnSetBreak').attr('onClick', 'SetBreakFunc(' + newVal + ')');
                $("#setBreakTimePopup").modal('show');
            }
            function SetBreakFunc(data2) {

                var btime = (parseInt($('#breakTime').val())) * 60;
                var breakReasonID = $("#restBrkOption option:selected").attr("id");
                $.ajax({
                    type: "GET",
                    url: 'agentsetbreak',
                    data: {agentId: data2, time: btime, ReasonId: breakReasonID},
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (recData)
                    {
                        if (recData.retcode == "0")
                        {
                            $('.cssload-loader1').show();
                            $('.overlay1').show();
                            Notifier.success('Set Agent Break Successfully');
                            $("#setBreakTimePopup").modal('hide');
                            setTimeout(function ()
                            {
                                $.get("${pageContext.request.servletContext.contextPath}/agent/dashboard");
//                                $('.cssload-loader1').show();
//                                $('.overlay1').show();
                            }, 1000);
                        } else
                        {
                            Notifier.error('Set Agent Break Failed');
                        }
                    },
                    error: function (xhr, status, error) {
                        Notifier.success('Set State Break failed');
                        console.log('Start Break Error ' + status);
                    }
                });
            }
            function setAgentInserting(btnAgentInsert) {
                var a = btnAgentInsert.id;
                $('#' + a).addClass('disabled');
                $('#' + a).prop('disabled', true);
                var newVal = a.substring(6, a.length);
                var btnStop = $("#stop" + newVal);
                btnStop.removeClass('disabled');
                btnStop.prop('disabled', false);
                var btnLis = $("#Listen" + newVal);
                btnLis.addClass('disabled');
                btnLis.prop('disabled', true);
                $.ajax({
                    type: "GET",
                    url: 'agentsetinsert',
                    data: {agentId: newVal},
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (recData)
                    {
                        if (recData.retcode == "0")
                        {
                            setTimeout(function ()
                            {
                                $.get("${pageContext.request.servletContext.contextPath}/agent/dashboard");
                            }, 1000);
                            Notifier.success('Insert start successfully');
                        } else {
                            Notifier.error('Insert start failed');
                        }
                    },
                    error: function (xhr, status, error) {
                        Notifier.error('Insert start failed');
                        console.log('Start Shadow Error ' + status);
                    }
                });
            }
            function setAgentListening(btnAgentListen) {
                var a = btnAgentListen.id;
                $("#" + a).addClass("disabled");
                $("#" + a).prop("disabled", true);
                var newVal = a.substring(6, a.length);
                var btnStop = $("#stop" + newVal);
                btnStop.removeClass("disabled");
                btnStop.prop("disabled", false);
                btnStop.removeAttr("disabled");
                var btnIns = $("#Insert" + newVal);
                btnIns.addClass("disabled");
                btnIns.prop("disabled", true);
                $.ajax({
                    type: "GET",
                    url: 'agentsetlisten',
                    data: {agentId: newVal},
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (recData)
                    {
                        if (recData.retcode == "0")
                        {
                            setTimeout(function ()
                            {
                                $.get("${pageContext.request.servletContext.contextPath}/agent/dashboard");
//                                $('.cssload-loader1').show();
//                                $('.overlay1').show();
                            }, 1000);
                            Notifier.success('Listen start successfully');
                        } else
                        {
                            Notifier.error('Listen start failed');
                        }
                    },
                    error: function (xhr, status, error) {
                        Notifier.error('Listen start failed');
                        console.log('Start Listen Error ' + status);
                    }
                });
            }
            function setAgentStopListeningInserting(btnAgentListenInsertStop) {
                var a = btnAgentListenInsertStop.id;
                var newVal = a.substring(4, a.length);
                var btnIns = $("#Insert" + newVal);
                var btnLis = $("#Listen" + newVal);
                btnIns.removeClass('disabled');
                btnIns.prop('disabled', false);
                btnLis.removeClass('disabled');
                btnLis.prop('disabled', false);
                $("#" + a).addClass('disabled');
                $("#" + a).prop('disabled', true);
                $.ajax({
                    type: "GET",
                    url: 'agentstoplisteninsert',
                    data: {agentId: newVal},
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (recData)
                    {
                        if (recData.retcode == "0")
                        {
                            setTimeout(function ()
                            {
                                $.get("${pageContext.request.servletContext.contextPath}/agent/dashboard");
//                                $('.cssload-loader1').show();
//                                $('.overlay1').show();
                            }, 1000);
                            Notifier.success('Stop Listen/Insert');
                        } else
                        {
                            Notifier.error('Failed to Stop Listen/Insert');
                        }
                    },
                    error: function (xhr, status, error) {
                        Notifier.error('Failed to Stop Listen/Insert');
                        console.log('Stop Shadow/Listen Error ' + status);
                    }
                });
            }
            function setAgentLogout(btnAgentLogout) {
                var a = btnAgentLogout.id;
                var newVal = a.substring(3, a.length);
                $.ajax({
                    type: "GET",
                    url: 'agentsetforcelogout',
                    data: {agentId: newVal},
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (recData)
                    {
                        if (recData.retcode == "0")
                        {
                            setTimeout(function ()
                            {
                                $.get("${pageContext.request.servletContext.contextPath}/agent/dashboard");
//                                $('.cssload-loader1').show();
//                                $('.overlay1').show();
                            }, 1000);
                            Notifier.success('Logout Agent ' + newVal + ' Successfully');
                        } else
                        {
                            Notifier.error('Failed to agent ' + newVal + ' to logout');
                        }
                    },
                    error: function (xhr, status, error) {
                        Notifier.error('Failed to agent ' + newVal + ' to logout');
                    }
                });
            }
            function getAgentStats(btnAgentStats) {
                var a = btnAgentStats.id;
                var newVal = a.substring(4, a.length);
                debugger;
                console.log("bn4 ajax request is called now");
                $.ajax({
                    type: "GET",
                    url: 'agentgetstatistics',
                    data: {agentId: newVal},
                    crossDomain: true,
                    cache: false,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        console.log("StatList:\t" + data.res.retcode);
                        console.log("AgentDetailData:\t" + data.agentbyworkno.retcode);
                        if (data.res.retcode == "0")
                        {
                            var totalSignInDuration = 0;
                            var agentDetail = data.agentbyworkno.result;
                            var skills = "";
                            console.log('Agent Detail of ' + agentDetail.workno + ': \n' + agentDetail.phonenumber + '\n' + agentDetail.mediatype + '\n');
                            $.each(agentDetail.skilllist, function (i, item) {
                                console.log(item.name + '\n');
                                skills += item.name + ' ,';
                            });
                            debugger;
                            skills = skills.substring(0, skills.length - 1);
                            var da = data.res.result[0].idxs;
                            var da1 = data.agentdurationresp.result[0].idxs[0].val;
//                            $.each(data.agentdurationresp.result[0].idxs[0].val, function (i, item) {
//                                totalSignInDuration = totalSignInDuration + parseInt(item.duration);
//                            });
                            data = data.res.result;
                            $("#agentStatsTableBody").empty();
                            var trHTML = '';
//                            var duration = moment.duration(totalSignInDuration, 'seconds');
//                            var formatted = duration.format("hh:mm:ss");
//                            var m = Math.floor(totalSignInDuration / 60);
//                            var s = Math.floor(totalSignInDuration % 60);
//                            console.log(totalSignInDuration);
                            trHTML += '<tr><td>Agent WorkId</td><td>' + agentDetail.workno + '</td></tr>';
                            trHTML += '<tr><td>Agent Phone No</td><td>' + agentDetail.phonenumber + '</td></tr>';
//                            trHTML += '<tr><td>Agent Media Type</td><td>' + agentDetail.mediatype + '</td></tr>';
                            trHTML += '<tr><td>Agent Group Name</td><td>' + agentDetail.groupname + '</td></tr>';
                            trHTML += '<tr><td>Agent Skills</td><td>' + skills + '</td></tr>';
                           // trHTML += '<tr><td>Agent Sign In Duration</td><td>' + formatted + '</td></tr>';
                            //$('#btnRefreshStats').addClass(agentDetail.workno);
                            // $("#btnRefreshStats").on("click", function () { ReloadAgentStats(); });
                            $("#btnRefreshStats").off('click').on('click', function () {
                                // function body
                            });
                            $('#btnRefreshStats').attr('onClick', 'ReloadAgentStats(' + agentDetail.workno + ')');
                            //$("#btnRefreshStats").click(function () { ReloadAgentStats(); });
                            // $('#btnRefreshStats').attr('id', 'btnRefreshStats' + agentDetail.workno);
                            $.each(data[0].idxs, function (i, item) {
                                if (item.id.includes("Duration")) {
                                    var seconds = parseInt(item.val);
                                    var duration = moment.duration(seconds, 'seconds');
                                    var format = duration.format("hh:mm:ss");
                                    trHTML += '<tr><td>' + item.id + '</td><td>' + format + '</td></tr>';
                                } else {
                                    trHTML += '<tr><td>' + item.id + '</td><td>' + item.val + '</td></tr>';
                                }
                            });
                            $('#agentStatsTableBody').append(trHTML);
                            console.log('Geting Agent Statistics Showing PopUp');
                            $("#statPopUp").modal('show');
                        } else {
                            Notifier.error('No Data Found');
                        }
                    },
                    error: function (xhr, status, error) {
                        Notifier.error('No Data Found' + status);
                    }
                });
            }
            function toTitleCase(str) {
                return str.replace(/(?:^|\s)\w/g, function (match) {
                    return match.toUpperCase();
                });
            }
            function AdjustAgentSkill(btnAdjustSkill)
            {
                var a = btnAdjustSkill.id;
                var newVal = a.substring(8, a.length);
                $.ajax({
                    type: "GET",
                    url: 'getallskills',
                    data: {agentId: newVal},
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (recData)
                    {
                        console.log("All Skills \t" + recData.res.retcode);
                        console.log("Specific Skill \t" + recData.resUniskill.retcode);
                        if (recData.res.retcode == "0")
                        {
                            var agentDetail = recData.res.message;
                            var skills = "";
                            var workno = agentDetail.split("|")[0];
                            var phonenumber = agentDetail.split("|")[1];
                            var popDiv = $('#popUpData');
                            popDiv.empty();
                            var ulVal = '<ul style="list-style:none;" id="agentSkillList">'
                            ulVal += '<li id="skillWorkId">WorkID : ' + newVal + '</li>';
//                            ulVal += '<li >Phone Num : ' + phonenumber + '</li>';
                            recData.res.result.forEach(function (item) {
                                var flag = false;
                                if (recData.resUniskill.result != null) {
                                    recData.resUniskill.result.forEach(function (item1) {
                                        if (parseInt(item.skillId) == parseInt(item1.id)) {
                                            ulVal += '<li><div class="checkbox checkbox-success"><input id="' + item.skillId + '" type="checkbox" checked value="' + item.skillName + '" ><label for="' + item.skillId + '">' + toTitleCase(item.skillName) + '</label></div></li>';
                                            flag = true;
                                            return false;
                                        }
                                    });
                                }
                                if (flag == false) {
                                    ulVal += '<li><div class="checkbox checkbox-success"><input id="' + item.skillId + '" type="checkbox" value="' + item.skillName + '" ><label for="' + item.skillId + '">' + toTitleCase(item.skillName) + '</label></div></li>';
                                }
                            });
                            ulVal += '</ul>';
                            popDiv.append(ulVal);
                            $("#skillAdjustPopUp").modal('show');
                        } else
                        {
                            Notifier.error("Failed to get agent details");
                        }



                    },
                    error: function (xhr, status, error) {

                        console.log("Adjust Error " + status);
                    }
                });
            }
            function ReloadAgentStats(data2) {


                $.ajax({
                    type: "GET",
                    url: '../Home/GetAgentStatistics',
                    data: {agentId: data2},
                    crossDomain: true,
                    cache: false,
                    contentType: "application/json",
                    dataType: "json",
                    success: function (response) {
                        if (response.StatList.retcode == "0")
                        {
                            var totalSignInDuration = 0;
                            var agentDetail = response.AgentDetailData.result;
                            var skills = "";
                            console.log('Agent Detail of ' + agentDetail.workno + ': \n' + agentDetail.phonenumber + '\n' + agentDetail.mediatype + '\n');
                            $.each(agentDetail.skilllist, function (i, item) {
                                console.log(item.name + '\n');
                                skills += item.name + ' ,';
                            });
                            skills = skills.substring(0, skills.length - 1);
                            console.log('Geting Agent Statistics' + response.StatList.message);
                            var da = response.StatList.result[0].idxs;
                            var da1 = response.SignInSignOut.result[0].idxs[0].val;
                            $.each(response.SignInSignOut.result[0].idxs[0].val, function (i, item) {
                                totalSignInDuration = totalSignInDuration + parseInt(item.duration);
                            });
                            response = response.StatList.result;
                            $("#agentStatsTableBody").empty();
                            var trHTML = '';
                            var m = Math.floor(totalSignInDuration / 60);
                            var s = Math.floor(totalSignInDuration % 60);
                            var duration = moment.duration(totalSignInDuration, 'seconds');
                            var formatted = duration.format("hh:mm:ss");
                            console.log(totalSignInDuration);
                            trHTML += '<tr><td>Agent WorkId</td><td>' + agentDetail.workno + '</td></tr>';
                            trHTML += '<tr><td>Agent Phone No</td><td>' + agentDetail.phonenumber + '</td></tr>';
//                            trHTML += '<tr><td>Agent Media Type</td><td>' + agentDetail.mediatype + '</td></tr>';
                            trHTML += '<tr><td>Agent Group Name</td><td>' + agentDetail.groupname + '</td></tr>';
                            trHTML += '<tr><td>Agent Skills</td><td>' + skills + '</td></tr>';
                            trHTML += '<tr><td>Agent Sign In Duration</td><td>' + formatted + '</td></tr>';
                            $.each(response[0].idxs, function (i, item) {
                                trHTML += '<tr><td>' + item.id + '</td><td>' + item.val + '</td></tr>';
                            });
                            $('#agentStatsTableBody').append(trHTML);
                            console.log('Geting Agent Statistics Showing PopUp');
                        } else
                        {
                            Notifier.error("Failed retreive again");
                        }


                    },
                    error: function (xhr, status, error) {
                        var err = eval("(" + xhr.responseText + ")");
                        console.log(err.Message);
                    }
                });
            }
            $("#btnRefreshStats").click(function () {
                ReloadAgentStats();
            });
            $("#btnSaveAgentAdjustSkills").click(function ()
            {
                var workid = $('#skillWorkId').text();
                workid = workid.split(":")[1].trim();
                var skill = [];
                var i = 0;
                $('#agentSkillList :checkbox').each(function () {
                    if ($(this).prop('checked') == true)
                    {
//                        skill[i] = $(this).val();
                        skill[i] = $(this).attr("id");
                        i++;
                    }

                });
                var skillDataParam = new Object();
                skillDataParam.workNo = workid;
                skillDataParam.skills = skill;
                $.ajax({
                    type: "POST",
                    url: "agentadjustskill",
                    contentType: "application/json; charset=utf-8",
                    data: JSON.stringify(skillDataParam),
                    dataType: "json",
                    success: function (recData)
                    {
                        if (recData.retcode == "0")
                        {
                            $('.cssload-loader1').show();
                            $('.overlay1').show();
//                            reloadAgentStates();
                            Notifier.success('Skills Adjusted Successfully');
                            $("#skillAdjustPopUp").modal('hide');
                        } else
                        {
                            Notifier.error('Skills Adjustment Failed');
                        }
                    },
                    error: function (xhr, status, error) {
                        console.log(err.status);
                    }
                });
            });
        </script>

    </body>
</html>
