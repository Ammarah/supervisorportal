<%-- 
    Document   : dashboard view 2
    Created on : Aug 21, 2019, 10:52:36 AM
    Author     : Ammarah Urooj
--%>
<%@page import="java.util.concurrent.TimeUnit"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.Date"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="windows-1252"%>
<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
        <title>Supervisor Portal | Dashboard</title>
        <script src="${pageContext.request.contextPath}/resources/js/jquery-2.2.3.min.js" type="text/javascript"></script>
        <style>
            .btn.disabled, .btn[disabled], fieldset[disabled] .btn {
                opacity: 3.65 !important; 
            }
            .btn-default{
                /*opacity: 0.65 !important;*/ 
                font-family: Raleway-SemiBold;
                font-size: 13px;
                color: rgba(108, 88, 179, 0.75);
                letter-spacing: 1px;
                line-height: 15px;
                border: 2px solid rgba(108, 89, 179, 0.75);
                border-radius: 8px;
                background: transparent;
                transition: all 0.3s ease 0s; 
            }
            .btn-default:hover {
                color: #FFF;
                background: rgba(108, 88, 179, 0.75);
                border: 2px solid rgba(108, 89, 179, 0.75);
            }

            .lblOption {
                background-color: white;
                padding: 5px;
                font: 14px;
                width: 40%;
            }
            .overlay1 {
                background-color: #022158;
                display: block;
                position: fixed;
                top: 0;
                right: 0;
                bottom: 0;
                left: 0;
                opacity: 0.5;
                z-index: 3;
            }

            .btn
            {
                padding-right:6px;
            }
            input[type=number]::-webkit-inner-spin-button, 
            input[type=number]::-webkit-outer-spin-button { 
                -webkit-appearance: none; 
            }
        </style>
    </head>
    <body>
        <div class="overlay1" style="display:none;"></div>
        <div class="cssload-loader1" style="display:none;" >
            <div class="cssload-side"></div>
            <div class="cssload-side"></div>
            <div class="cssload-side"></div>
            <div class="cssload-side"></div>
            <div class="cssload-side"></div>
            <div class="cssload-side"></div>
            <div class="cssload-side"></div>
            <div class="cssload-side"></div>
        </div>
        <section class="content-header">
            <h1>
                Dashboard
            </h1>       
            <div class="row" style="margin-top:2%;">

                <div class="col-md-12 col-sm-12 col-xs-12 form-inline">
                    <div class="dropdown col-md-2 col-sm-12 col-xs-12">
                        <select id="filterType" class="btn btn-primary" style="padding-left: 20%; padding-right: 20%;">
                            <option value="-">Filter Type</option>
                            <option value="AgentId">Agent ID</option>  
                            <option value="AgentName">Agent Name</option>
                            <option value="AgentStatus">Agent Status</option>
                        </select>
                    </div>
                    <div class="dropdown col-md-3 col-sm-12 col-xs-12" id="search-div" style="display: none;" >
                        <span class="fa fa-search form-control-feedback" style="margin-right:83%;"></span>                           
                        <input id="srchAgent" type="text" class="form-control" placeholder="  Search" style="padding-left:10%; width:100%;" />
                    </div>
                    <div id="status-div" class="dropdown col-md-3 col-sm-12 col-xs-12" style="display: none;" >
                        <select id="agentState" class="btn btn-primary" style="padding-left: 25%; padding-right: 25%;">
                            <option value=-1>All Agents</option>
                            <option value=0>Sign Off</option>
                            <option value=2>Ready</option>
                            <option value=3>Not Ready</option>
                            <option value=27>Work</option>
                            <option value=5>Answering</option>
                            <option value=4>Talking</option>
                            <option value=26>Break</option>
                        </select>
                    </div>
                    <div id="submit-div" class="dropdown col-md-4 col-sm-12 col-xs-12" style="margin-left:25%;display: none;">
                        <button type="button" title="Submit" class="btn btn-primary" id="submit-search" onclick="submitSearch();" style="padding-left: 15%; padding-right: 15%; margin-right: 3%;"> Submit</button>
                        <button type="button" title="Reset" class="btn btn-primary" id="reset" onclick="reset();"  style="padding-left: 15%; padding-right: 15%;"> Reset</button>
                    </div>
                </div>
            </div>               
            <br>
        </section>
        <!--buttons section -->
        <section>
            <div class="w3-bar bg-gray"style="margin-left:1.5%; margin-right:2%;" >
                <button type="button" id="skillAdj" onclick="AdjustAgentSkill(this);" class="btn btn-box-tool " disabled="disabled">
                    <i class="fa fa-wrench" style="font-size:15px;color:black"></i> Skill Adjustment
                </button> 
                <!--                <button type="button" id="stat" onclick="getAgentStats(this);" class="btn btn-box-tool " disabled="disabled">
                                    <i class="fa fa-info" style="font-size:15px;color:black"></i> Statistics
                                </button> -->
                <button type="button" id="idle" onclick="setAgentIdle(this);"class="btn btn-box-tool " disabled="disabled">
                    <i class="fa fa fa-smile-o" style="font-size:15px;color:green"></i> Ready
                </button>
                <button type="button" id="res" onclick="setAgentRest(this);"class="btn btn-box-tool " disabled="disabled">
                    <i class="fa fa-hourglass" style="color:yellow"></i> Break
                </button> 
                <button type="button" id="bus" onclick="setAgentBusy(this);"class="btn btn-box-tool " disabled="disabled">
                    <i class="fa  fa-minus-circle" style="font-size:15px;color:blue"></i> Not Ready
                </button> 
                <button type="button" id="Listen" onclick="setAgentListening(this);"class="btn btn-box-tool " disabled="disabled">
                    <i class="fa  fa-headphones" style="font-size:15px;color:orange"></i> Listening
                </button> 

                <button type="button" id="Insert" onclick="setAgentInserting(this);"class="btn btn-box-tool " disabled="disabled">
                    <i class="fa  fa-tty" style="font-size:15px;color:green"></i> Shadowing
                </button>
                <button type="button" id="stop" onclick="setAgentStopListeningInserting(this);"class="btn btn-box-tool " disabled="disabled">
                    <i class="fa fa-stop-circle-o " style="font-size:15px;color:red"></i> Stop Listening
                </button>
                <button type="button" id="log" onclick="setAgentLogout(this);"class="btn btn-box-tool" disabled="disabled">
                    <i class="fa fa-sign-out " style="font-size:15px;color:green"></i> Logout
                </button>
            </div>
        </section>
        <!--list agents section -->

        <section class="content" id="list_agents">

            <div class="bg-gray"  style="display: inline;">
                <table class="table table-bordered table-striped table-hover table-condensed dataTable" style="float: left;" id="agent-table">
                    <thead class="bg-gray thead-dark">
                        <tr>
                            <th></th>
                            <th>Agent ID</th>
                            <th>Agent Name</th>
                            <th>Agent Status</th>
                            <th>Duration(s)</th>
                            <th>Details
                                <button type="button" id="btnDetails" class="btn btn-box-tool">
                                    <i class="fa fa-gear" style="color:black"></i> 
                                </button>
                            </th>
                        </tr>
                    </thead>
                </table>
            </div>


        </section>
        <div class="modal fade"  role="dialog" id="statPopUp">
            <div class="modal-dialog">
                <div class="modal-content" style="border-radius:3px; -webkit-border-radius:3px; -moz-border-radius:3px;">

                    <div class="modal-body">
                        <div class="box" style="margin-bottom:5px;">
                            <div class="box-header with-border bg-gray">
                                Agent Info Details
                                <div class="box-tools pull-right">
                                    <button type="button" id="btnRefreshStats" class="btn btn-box-tool">
                                        <i class="fa fa-refresh"></i>
                                    </button>
                                    <button type="button" class="btn btn-box-tool" data-dismiss="modal" aria-label="Close">
                                        <i class="fa fa-times"></i>
                                    </button>
                                </div>

                            </div>
                            <!-- /.box-header -->

                            <div class="box-body no-padding" style="height:70vh; overflow:scroll;">

                                <table class="table table-striped">
                                    <thead id="agentStatsTableHead">

                                    </thead>
                                    <tbody id="agentStatsTableBody">
                                        <tr>
                                            <td>No data found</td>

                                        </tr>

                                    </tbody>
                                </table>

                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>
                    <!--                    <div class="modal-footer" style="padding:5px;">
                                            <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
                                            @*<button type="button" class="btn btn-primary">Save changes</button>*@
                                        </div>-->
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <div class="modal fade" role="dialog" id="skillAdjustPopUp">
            <div class="modal-dialog">
                <div class="modal-content" style="border-radius:3px; -webkit-border-radius:3px; -moz-border-radius:3px;">

                    <div class="modal-body">
                        <div class="box" style="margin-bottom:5px;">
                            <div class="box-header with-border">
                                <h3 class="box-title">Agent Skill Adjustment</h3>
                                <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-box-tool" data-dismiss="modal" aria-label="Close">
                                        <i class="fa fa-times"></i>
                                    </button>
                                </div>
                            </div>
                            <!-- /.box-header -->
                            <div class="row" id="popUpData">
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>
                    <div class="modal-footer" style="padding:5px;">
                        <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>&nbsp;
                        <button type="button" class="btn btn-primary" id="btnSaveAgentAdjustSkills"> Save</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <div class="modal fade" role="dialog" id="setBreakTimePopup">
            <div class="modal-dialog">
                <div class="modal-content" style="border-radius:3px; -webkit-border-radius:3px; -moz-border-radius:3px;">
                    <div class="modal-body">
                        <div class="box" style="margin-bottom:5px;">
                            <div class="box-header with-border">
                                <h3 class="box-title">Set Break Time</h3>
                                <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-box-tool" data-dismiss="modal" aria-label="Close">
                                        <i class="fa fa-times"></i>
                                    </button>
                                </div>
                            </div>
                            <!-- /.box-header -->
                            <div class="modal-body">
                                <div class="row" id="popBreak">
                                    <div class="form-group">
                                        <label for="restBrkOption">Select Break Reason</label>
                                        <select  class="form-control" id="restBrkOption">
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="breakTime">Break Time (Min.)</label>
                                        <input type="number" min="1"  class="form-control" placeholder="Break Time in Mintues" class="form-control" id="breakTime">
                                    </div>


                                </div>
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>
                    <div class="modal-footer" style="padding:5px;">
                        <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Close</button>&nbsp;
                        <button type="button" class="btn btn-primary" id="btnSetBreak"> OK</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <script language="javascript" type="text/javascript">
            $('#filterType').change(function () {
                if ($(this).val() == "AgentId") {
                    $('#status-div').hide();
                    $('#search-div').show();
                    $('#srchAgent').attr("type", "number");
                    $('#srchAgent').val("");
                    $('#submit-div').show();
                } else if ($(this).val() == "AgentName") {
                    $('#status-div').hide();
                    $('#search-div').show();
                    $('#srchAgent').attr("type", "text");
                    $('#srchAgent').val("");
                    $('#submit-div').show();
                } else if ($(this).val() == "AgentStatus") {
                    $('#search-div').hide();
                    $('#status-div').show();
                    $('#submit-div').show();
                } else {
                    $('#status-div').hide();
                    $('#search-div').hide();
                    $('#submit-div').hide();
                    reset();
                }
                if (submitSearchTimer)
                    clearInterval(submitSearchTimer);
                setDefault();
                datatables.ajax.reload();
                // setTimeout(reset, 10000);
            });
            var submitSearchTimer = null;
            var FilterAgents = null;
            var datarefreshTimer = 0;
            var datatables = null;
            var slct = null;
            var status = -1;
            var agent = -1;
            var func = "status";
            function reset() {
                if (submitSearchTimer)
                    clearInterval(submitSearchTimer);
                if (datarefreshTimer)
                    clearInterval(datarefreshTimer);
                $('#status-div').hide();
                $('#search-div').hide();
                $('#submit-div').hide();
                $('#srchAgent').val("");
                $('#filterType').prop('selectedIndex', 0);
                $('#agentState').prop('selectedIndex', 0);
                setDefault();
                if ($.fn.DataTable.isDataTable(FilterAgents)) {
                    FilterAgents.DataTable().clear().destroy();
                }
                datatables.ajax.reload();
                datarefreshTimer = setInterval(datatables.ajax.reload, 3000);
            }

            function setDefault() {
                document.getElementById("skillAdj").value = "";
                document.getElementById("idle").value = "";
                document.getElementById("res").value = "";
                document.getElementById("bus").value = "";
                document.getElementById("Listen").value = "";
                document.getElementById("Insert").value = "";
                document.getElementById("stop").value = "";
                document.getElementById("log").value = "";
                //set disabled
                document.getElementById("skillAdj").disabled = true;
                document.getElementById("idle").disabled = true;
                document.getElementById("res").disabled = true;
                document.getElementById("bus").disabled = true;
                document.getElementById("Listen").disabled = true;
                document.getElementById("Insert").disabled = true;
                document.getElementById("stop").disabled = true;
                document.getElementById("log").disabled = true;
                //reset css
                $("#skillAdj").removeClass('btn-default').addClass('btn');
                $("#idle").removeClass('btn-default').addClass('btn');
                $("#res").removeClass('btn-default').addClass('btn');
                $("#bus").removeClass('btn-default').addClass('btn');
                $("#Listen").removeClass('btn-default').addClass('btn');
                $("#Insert").removeClass('btn-default').addClass('btn');
                $("#stop").removeClass('btn-default').addClass('btn');
                $("#log").removeClass('btn-default').addClass('btn');
            }

            function submitSearch() {
                var searchType = $('#filterType').children("option:selected").val();
                var searchValue = null;
                if (searchType === "AgentStatus") {
                    searchValue = $('#agentState').children("option:selected").val();
                } else if (searchType === "AgentName") {
                    searchValue = $('#srchAgent').val();
                    if (searchValue == "") {
                        alert("Please enter a valid Agent Name to search");
                        return;
                    }
                } else {
                    searchValue = $('#srchAgent').val();
                    if (searchValue == "") {
                        alert("Please enter a valid Agent ID to search");
                        return;
                    }
                    searchValue = searchValue.toString();
                }
                if ($.fn.DataTable.isDataTable(datatables))
                    datatables.DataTable().clear().destroy();
                if (datarefreshTimer)
                    clearInterval(datarefreshTimer);
                if (submitSearchTimer)
                    clearInterval(submitSearchTimer);
                FilterAgents = $('#agent-table').DataTable({
                    "ordering": true,
                    responsive: true,
                    dom: 'rt<"bottom"flpi><"clear">',
                    scrollX: true,
                    "searching": false,
                    "ajax": {
                        url: 'filterAgentsDashboardV2/' + searchType + '/' + searchValue,
                        "dataSrc": "data.result",
                        "type": "GET",
                        "dataType": 'json'
                    },
                    "columns": [
                        {

                            "render": function (data, type, row) {
                                var inner = '<input type=\"radio\" name=\"agentSelected\" id=\"agentSelected\" value="' + row.agentId + '-' + row.status + '"/>';
                                return inner;
                            }
                        },
                        {'data': 'agentId'},
                        {'data': 'agentName'},
                        {
//Agent Status
                            "render": function (data, type, row) {
                                if (row.status == "0") {
                                    return "Sign Off";
                                } else if (row.status == "2") {
                                    return "Ready"
                                } else if (row.status == "3") {
                                    return "Not Ready"
                                } else if (row.status == "27") {
                                    return "Work"
                                } else if (row.status == "5") {
                                    return "Answering"
                                } else if (row.status == "4") {
                                    return "Talking"
                                } else if (row.status == "26") {
                                    return "Break"
                                } else if (row.status == "13") {
                                    return "Transferred to IVR"
                                } else if (row.status == "30") {
                                    return "On Hold"
                                } else {
                                    setTimeOut(datatables.ajax.reload, 2000);
                                    return "request of status failed"
                                }
                            }
                        }, {

                            "render": function (data, type, row) {
                                var inner = moment().startOf('day').seconds(row.currentStateTime).format('HH:mm:ss');
                                return inner;
                            }
                        },
                        //owned skills
//                        {'data': 'agentId'},
                        {
                            "render": function (data, type, row) {
                                var inner = "<button type=\"button\" id=\"stat" + row.agentId + "\"onclick = \"getAgentStats(this);\"class=\"btn btn-box-tool\"><i class=\"fa fa-eye\" style=\"color:black\"></i></button>";
                                //var inner = "<button type=\"button\" id=\"btnDetails\"class=\"btn btn-box-tool\"><i class=\"fa fa-eye\" style=\"color:black\"></i></button>";
                                return inner;
                            }
                        }
                    ],
                    "rowCallback": function (row, data) {
                        if (data.status == "0") {
                            $('td:eq(3)', row).css('color', 'red');
                        }
                        if (data.status == "2") {
                            $('td:eq(3)', row).css('color', 'green');
                        }
                        if (data.status == "3") {
                            $('td:eq(3)', row).css('color', 'blue');
                        }
                        if (data.status == "27") {
                            $('td:eq(3)', row).css('color', 'olive');
                        }
                        if (data.status == "5") {
                            $('td:eq(3)', row).css('color', 'orange');
                        }
                        if (data.status == "4") {
                            $('td:eq(3)', row).css('color', 'orange');
                        }
                        if (data.status == "26") {
                            $('td:eq(3)', row).css('color', 'yellow');
                        }
                        if (data.status == "13") {
                            $('td:eq(3)', row).css('color', 'aqua');
                        }
                        if (data.status == "30") {
                            $('td:eq(3)', row).css('color', 'purple');
                        }

                    },
                    "destroy": true

                });
                if (searchType === "AgentStatus") {
                    submitSearchTimer = setInterval(FilterAgents.ajax.reload, 5000);
                }
            }

            $(document).on('change', 'input[type="radio"]', function (event) {
                if (datarefreshTimer)
                    clearInterval(datarefreshTimer);
                if (submitSearchTimer)
                    clearInterval(submitSearchTimer);
                var value = (event.target).value;
                var ar = value.split("-");
                document.getElementById("skillAdj").value = "skillAdj" + ar[0];
                //document.getElementById("stat").value = "stat" + ar[0];
                document.getElementById("idle").value = "idle" + ar[0];
                document.getElementById("res").value = "res" + ar[0];
                document.getElementById("bus").value = "bus" + ar[0];
                document.getElementById("Listen").value = "Listen" + ar[0];
                document.getElementById("Insert").value = "Insert" + ar[0];
                document.getElementById("stop").value = "stop" + ar[0];
                document.getElementById("log").value = "log" + ar[0];
                if (ar[1] == "0") {
                    //alert("sign off");
                    document.getElementById("skillAdj").disabled = true;
                    // document.getElementById("stat").disabled = true;
                    document.getElementById("idle").disabled = true;
                    document.getElementById("res").disabled = true;
                    document.getElementById("bus").disabled = true;
                    document.getElementById("Listen").disabled = true;
                    document.getElementById("Insert").disabled = true;
                    document.getElementById("stop").disabled = true;
                    document.getElementById("log").disabled = true;
                } else if (ar[1] == "2") {
                    //ready
                    document.getElementById("skillAdj").disabled = false;
                    //document.getElementById("stat").disabled = false;
                    document.getElementById("idle").disabled = true;
                    document.getElementById("res").disabled = false;
                    document.getElementById("bus").disabled = false;
                    document.getElementById("Listen").disabled = true;
                    document.getElementById("Insert").disabled = true;
                    document.getElementById("stop").disabled = true;
                    document.getElementById("log").disabled = false;
                    //set css
                    $("#skillAdj").removeClass('btn').addClass('btn-default');
                    $("#res").removeClass('btn').addClass('btn-default');
                    $("#bus").removeClass('btn').addClass('btn-default');
                    $("#log").removeClass('btn').addClass('btn-default');
                } else if (ar[1] == "3") {
                    //not ready
                    document.getElementById("skillAdj").disabled = false;
                    //  document.getElementById("stat").disabled = false;
                    document.getElementById("idle").disabled = false;
                    document.getElementById("res").disabled = false;
                    document.getElementById("bus").disabled = true;
                    document.getElementById("Listen").disabled = true;
                    document.getElementById("Insert").disabled = true;
                    document.getElementById("stop").disabled = true;
                    document.getElementById("log").disabled = false;
                    //set css
                    $("#skillAdj").removeClass('btn').addClass('btn-default');
                    $("#res").removeClass('btn').addClass('btn-default');
                    $("#idle").removeClass('btn').addClass('btn-default');
                    $("#log").removeClass('btn').addClass('btn-default');
                } else if (ar[1] == "27") {
                    //work
                    document.getElementById("skillAdj").disabled = false;
                    //  document.getElementById("stat").disabled = false;
                    document.getElementById("idle").disabled = true;
                    document.getElementById("res").disabled = false;
                    document.getElementById("bus").disabled = false;
                    document.getElementById("Listen").disabled = true;
                    document.getElementById("Insert").disabled = true;
                    document.getElementById("stop").disabled = true;
                    document.getElementById("log").disabled = false;
                    //set css
                    $("#skillAdj").removeClass('btn').addClass('btn-default');
                    $("#res").removeClass('btn').addClass('btn-default');
                    $("#bus").removeClass('btn').addClass('btn-default');
                    $("#log").removeClass('btn').addClass('btn-default');
                } else if (ar[1] == "4") {
                    //talking
                    document.getElementById("skillAdj").disabled = false;
                    //  document.getElementById("stat").disabled = false;
                    document.getElementById("idle").disabled = false;
                    document.getElementById("res").disabled = false;
                    document.getElementById("bus").disabled = false;
                    document.getElementById("Listen").disabled = false;
                    document.getElementById("Insert").disabled = false;
                    document.getElementById("stop").disabled = true;
                    document.getElementById("log").disabled = false;
                    //set css
                    $("#skillAdj").removeClass('btn').addClass('btn-default');
                    $("#idle").removeClass('btn').addClass('btn-default');
                    $("#res").removeClass('btn').addClass('btn-default');
                    $("#bus").removeClass('btn').addClass('btn-default');
                    $("#Listen").removeClass('btn').addClass('btn-default');
                    $("#Insert").removeClass('btn').addClass('btn-default');
                    $("#log").removeClass('btn').addClass('btn-default');
                } else if (ar[1] == "5") {
                    //answering
                    document.getElementById("skillAdj").disabled = false;
                    // document.getElementById("stat").disabled = false;
                    document.getElementById("idle").disabled = false;
                    document.getElementById("res").disabled = false;
                    document.getElementById("bus").disabled = false;
                    document.getElementById("Listen").disabled = false;
                    document.getElementById("Insert").disabled = false;
                    document.getElementById("stop").disabled = true;
                    document.getElementById("log").disabled = false;
                    //set css
                    $("#skillAdj").removeClass('btn').addClass('btn-default');
                    $("#idle").removeClass('btn').addClass('btn-default');
                    $("#res").removeClass('btn').addClass('btn-default');
                    $("#bus").removeClass('btn').addClass('btn-default');
                    $("#Listen").removeClass('btn').addClass('btn-default');
                    $("#Insert").removeClass('btn').addClass('btn-default');
                    $("#log").removeClass('btn').addClass('btn-default');
                } else if (ar[1] == "26") {
                    //break
                    document.getElementById("skillAdj").disabled = false;
                    //  document.getElementById("stat").disabled = false;
                    document.getElementById("idle").disabled = false;
                    document.getElementById("res").disabled = true;
                    document.getElementById("bus").disabled = false;
                    document.getElementById("Listen").disabled = true;
                    document.getElementById("Insert").disabled = true;
                    document.getElementById("stop").disabled = true;
                    document.getElementById("log").disabled = false;
                    //set css
                    $("#skillAdj").removeClass('btn').addClass('btn-default');
                    $("#idle").removeClass('btn').addClass('btn-default');
                    $("#bus").removeClass('btn').addClass('btn-default');
                    $("#log").removeClass('btn').addClass('btn-default');
                } else if (ar[1] == "13") {
                    //Transferred to Ivr
                    document.getElementById("skillAdj").disabled = true;
                    //  document.getElementById("stat").disabled = false;
                    document.getElementById("idle").disabled = true;
                    document.getElementById("res").disabled = true;
                    document.getElementById("bus").disabled = true;
                    document.getElementById("Listen").disabled = true;
                    document.getElementById("Insert").disabled = true;
                    document.getElementById("stop").disabled = true;
                    document.getElementById("log").disabled = true;
                    //set css
//            $("#skillAdj").removeClass('btn').addClass('btn-default');
//            $("#idle").removeClass('btn').addClass('btn-default');
//            $("#bus").removeClass('btn').addClass('btn-default');
//            $("#log").removeClass('btn').addClass('btn-default');
                } else if (ar[1] == "30") {
                    //On Hold
                    document.getElementById("skillAdj").disabled = true;
                    //  document.getElementById("stat").disabled = false;
                    document.getElementById("idle").disabled = true;
                    document.getElementById("res").disabled = true;
                    document.getElementById("bus").disabled = true;
                    document.getElementById("Listen").disabled = true;
                    document.getElementById("Insert").disabled = true;
                    document.getElementById("stop").disabled = true;
                    document.getElementById("log").disabled = true;
                    //set css
//            $("#skillAdj").removeClass('btn').addClass('btn-default');
//            $("#idle").removeClass('btn').addClass('btn-default');
//            $("#bus").removeClass('btn').addClass('btn-default');
//            $("#log").removeClass('btn').addClass('btn-default');
                } else {
                    console.log("Req Reload Agents Fails");
                    setTimeOut(reset, 2000);
                }
                setTimeout(reset, 60000);
            });
            $(document).ready(function () {
                datatables = $('#agent-table').DataTable({
                    "ordering": true,
                    responsive: true,
                    dom: 'rt<"bottom"flpi><"clear">',
                    scrollX: true,
                    "searching": false,
                    "ajax": {
                        url: 'GetAllAgentsDashboardV2',
                        "dataSrc": "data.result",
                        "type": "GET",
                        "dataType": 'json'
                    },
                    "columns": [
                        {

                            "render": function (data, type, row) {
                                var inner = '<input type=\"radio\" name=\"agentSelected\" id=\"agentSelected\" value="' + row.agentId + '-' + row.status + '"/>';
                                return inner;
                            }
                        },
                        {'data': 'agentId'},
                        {'data': 'agentName'},
                        {
//Agent Status
                            "render": function (data, type, row) {
                                if (row.status == "0") {
                                    return"Sign Off"

                                } else if (row.status == "2") {
                                    return "Ready"
                                } else if (row.status == "3") {
                                    return "Not Ready"
                                } else if (row.status == "27") {
                                    return "Work"
                                } else if (row.status == "5") {
                                    return "Answering"
                                } else if (row.status == "4") {
                                    return "Talking"
                                } else if (row.status == "26") {
                                    return "Break"
                                } else if (row.status == "13") {
                                    return "Transferred to IVR"
                                } else if (row.status == "30") {
                                    return "On Hold"
                                } else {
                                    setTimeOut(datatables.ajax.reload, 2000);
                                    return "request of status failed"
                                }
                            }
                        }, {

                            "render": function (data, type, row) {
                                var inner = moment().startOf('day').seconds(row.currentStateTime).format('HH:mm:ss');
                                return inner;
                            }
                        },
                        //owned skills
//                        {'data': 'agentId'},
                        {
                            "render": function (data, type, row) {
                                var inner = "<button type=\"button\" id=\"stat" + row.agentId + "\"onclick = \"getAgentStats(this);\"class=\"btn btn-box-tool\"><i class=\"fa fa-eye\" style=\"color:black\"></i></button>";
                                //var inner = "<button type=\"button\" id=\"btnDetails\"class=\"btn btn-box-tool\"><i class=\"fa fa-eye\" style=\"color:black\"></i></button>";
                                return inner;
                            }
                        }
                    ],
                    "rowCallback": function (row, data) {
                        if (data.status == "0") {
                            // $('td:eq(3)', row).css('color', 'white').css('background-color', 'red');
                            // $(row).css('color', 'white').css('background-color', 'red');
                            //$(row).css('color', 'red');
                            $('td:eq(3)', row).css('color', 'red');
                        }
                        if (data.status == "2") {
                            //  $('td:eq(3)', row).css('color', 'white').css('background-color', 'green');
                            //   $(row).css('color', 'white').css('background-color', 'green');
                            //    $(row).css('color', 'green');
                            $('td:eq(3)', row).css('color', 'green');
                        }
                        if (data.status == "3") {
                            $('td:eq(3)', row).css('color', 'blue');
                        }
                        if (data.status == "27") {
                            $('td:eq(3)', row).css('color', 'olive');
                        }
                        if (data.status == "5") {
                            $('td:eq(3)', row).css('color', 'orange');
                        }
                        if (data.status == "4") {
                            $('td:eq(3)', row).css('color', 'orange');
                        }
                        if (data.status == "26") {
                            $('td:eq(3)', row).css('color', 'yellow');
                        }
                        if (data.status == "13") {
                            $('td:eq(3)', row).css('color', 'aqua');
                        }
                        if (data.status == "30") {
                            $('td:eq(3)', row).css('color', 'purple');
                        }
                    },
                    "destroy": true

                });
                datarefreshTimer = setInterval(datatables.ajax.reload, 3000);
                // refreshIntervalId = setInterval(setDefault, 2000);
            });
            function setAgentIdle(btnAgentIdle)
            {
                $('.cssload-loader1').show();
                $('.overlay1').show();
                var btnAgentIdleID = btnAgentIdle.value;
                var workID = btnAgentIdleID.substring(4, btnAgentIdleID.length);
                $.ajax({
                    type: "GET",
                    url: 'agentsetidle',
                    data: {agentId: workID},
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (recData)
                    {
                        if (recData.retcode == "0")
                        {
                            $('.cssload-loader1').hide();
                            $('.overlay1').hide();
//                            reloadAgentStates();
                            Notifier.success('Set Agent Ready Successfully');
                            setTimeout(function ()
                            {
                                $.get("${pageContext.request.servletContext.contextPath}/agent/dashboard_v2");
//                                $('.cssload-loader1').show();
//                                $('.overlay1').show();
                            }, 1000);
                        } else
                        {
                            $('.cssload-loader1').hide();
                            $('.overlay1').hide();
                            Notifier.error('Set Agent Ready Failed');
                        }
                    },
                    error: function (xhr, status, error) {
                        $('.cssload-loader1').hide();
                        $('.overlay1').hide();
                        console.log('Set Agent Ready Error ' + status);
                        Notifier.error('Set Agent Ready Failed');
                    }
                });
            }
            function setAgentBusy(btnAgentBusy) {
                $('.cssload-loader1').show();
                $('.overlay1').show();
                var a = btnAgentBusy.value;
                var newVal = a.substring(3, a.length);
                $.ajax({
                    type: "GET",
                    url: 'agentsetnotready',
                    data: {agentId: newVal},
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (recData)
                    {
                        if (recData.retcode == "0")
                        {

                            $('.cssload-loader1').hide();
                            $('.overlay1').hide();
//                            reloadAgentStates();
                            Notifier.success('Set Agent Not Ready Successfully');
                            setTimeout(function ()
                            {
                                $.get("${pageContext.request.servletContext.contextPath}/agent/dashboard_v2");
                            }, 1000);
                        } else
                        {

                            $('.cssload-loader1').hide();
                            $('.overlay1').hide();
                            Notifier.error('Set Agent Not Ready Failed');
                        }
                    },
                    error: function (xhr, status, error) {

                        $('.cssload-loader1').hide();
                        $('.overlay1').hide();
                        Notifier.error('Set State Not Ready failed');
                        console.log('Start Not Ready Error ' + status);
                    }
                });
            }
            function setAgentRest(btnAgentRest) {

                $('.cssload-loader1').show();
                $('.overlay1').show();
                $.ajax({
                    type: "GET",
                    url: 'agentgetbreaks',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (recData)
                    {
                        if (recData.retcode == "0")
                        {
                            $('.cssload-loader1').hide();
                            $('.overlay1').hide();
                            $.each(recData.result, function (i, item) {
                                $("#restBrkOption").append("<option id= " + item.restReasonId + ">" + item.restReason + "</option>");
                            });
                        } else
                        {

                            $('.cssload-loader1').hide();
                            $('.overlay1').hide();
                            Notifier.error(' Agent Breaks  load Failed');
                        }
                    },
                    error: function (xhr, status, error) {

                        $('.cssload-loader1').hide();
                        $('.overlay1').hide();
                        Notifier.error(' Agent Breaks  load Failed');
                        console.log('Agent Break load Error ' + status);
                    }
                });
                var a = btnAgentRest.value;
                var newVal = a.substring(3, a.length);
                $('#btnSetBreak').attr('onClick', 'SetBreakFunc(' + newVal + ')');
                $("#setBreakTimePopup").modal('show');
            }
            function SetBreakFunc(data2) {
                $('.cssload-loader1').show();
                $('.overlay1').show();
                var btime = (parseInt($('#breakTime').val())) * 60;
                var breakReasonID = $("#restBrkOption option:selected").attr("id");
                $.ajax({
                    type: "GET",
                    url: 'agentsetbreak',
                    data: {agentId: data2, time: btime, ReasonId: breakReasonID},
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (recData)
                    {
                        if (recData.retcode == "0")
                        {
                            $('.cssload-loader1').hide();
                            $('.overlay1').hide();
                            Notifier.success('Set Agent Break Successfully');
                            $("#setBreakTimePopup").modal('hide');
                            setTimeout(function ()
                            {
                                $.get("${pageContext.request.servletContext.contextPath}/agent/dashboard");
//                                $('.cssload-loader1').show();
//                                $('.overlay1').show();
                            }, 1000);
                        } else
                        {
                            $('.cssload-loader1').hide();
                            $('.overlay1').hide();
                            Notifier.error('Set Agent Break Failed');
                        }
                    },
                    error: function (xhr, status, error) {
                        $('.cssload-loader1').hide();
                        $('.overlay1').hide();
                        Notifier.success('Set State Break failed');
                        console.log('Start Break Error ' + status);
                    }
                });
            }
            function setAgentInserting(btnAgentInsert) {
                var a = btnAgentInsert.value;
                $('#' + a).addClass('disabled');
                $('#' + a).prop('disabled', true);
                var newVal = a.substring(6, a.length);
                var btnStop = $("#stop");
                btnStop.removeClass('disabled');
                btnStop.prop('disabled', false);
                var btnLis = $("#Listen");
                btnLis.addClass('disabled');
                btnLis.prop('disabled', true);
                $.ajax({
                    type: "GET",
                    url: 'agentsetinsert',
                    data: {agentId: newVal},
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (recData)
                    {
                        if (recData.retcode == "0")
                        {
                            setTimeout(function ()
                            {
                                $.get("${pageContext.request.servletContext.contextPath}/agent/dashboard_v2");
                            }, 1000);
                            Notifier.success('Insert start successfully');
                        } else {
                            Notifier.error('Insert start failed');
                        }
                    },
                    error: function (xhr, status, error) {
                        Notifier.error('Insert start failed');
                        console.log('Start Shadow Error ' + status);
                    }
                });
            }
            function setAgentListening(btnAgentListen) {
                var a = btnAgentListen.value;
                $("#" + a).addClass("disabled");
                $("#" + a).prop("disabled", true);
                var newVal = a.substring(6, a.length);
                var btnStop = $("#stop");
                btnStop.removeClass("disabled");
                btnStop.prop("disabled", false);
                btnStop.removeAttr("disabled");
                var btnIns = $("#Insert");
                btnIns.addClass("disabled");
                btnIns.prop("disabled", true);
                $.ajax({
                    type: "GET",
                    url: 'agentsetlisten',
                    data: {agentId: newVal},
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (recData)
                    {
                        if (recData.retcode == "0")
                        {
                            setTimeout(function ()
                            {
                                $.get("${pageContext.request.servletContext.contextPath}/agent/dashboard_v2");
//                                $('.cssload-loader1').show();
//                                $('.overlay1').show();
                            }, 1000);
                            Notifier.success('Listen start successfully');
                        } else
                        {
                            Notifier.error('Listen start failed');
                        }
                    },
                    error: function (xhr, status, error) {
                        Notifier.error('Listen start failed');
                        console.log('Start Listen Error ' + status);
                    }
                });
            }
            function setAgentStopListeningInserting(btnAgentListenInsertStop) {
                var a = btnAgentListenInsertStop.value;
                var newVal = a.substring(4, a.length);
                var btnIns = $("#Insert");
                var btnLis = $("#Listen");
                btnIns.removeClass('disabled');
                btnIns.prop('disabled', false);
                btnLis.removeClass('disabled');
                btnLis.prop('disabled', false);
                $("#" + a).addClass('disabled');
                $("#" + a).prop('disabled', true);
                $.ajax({
                    type: "GET",
                    url: 'agentstoplisteninsert',
                    data: {agentId: newVal},
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (recData)
                    {
                        if (recData.retcode == "0")
                        {
                            setTimeout(function ()
                            {
                                $.get("${pageContext.request.servletContext.contextPath}/agent/dashboard_v2");
//                                $('.cssload-loader1').show();
//                                $('.overlay1').show();
                            }, 1000);
                            Notifier.success('Stop Listen/Insert');
                        } else
                        {
                            Notifier.error('Failed to Stop Listen/Insert');
                        }
                    },
                    error: function (xhr, status, error) {
                        Notifier.error('Failed to Stop Listen/Insert');
                        console.log('Stop Shadow/Listen Error ' + status);
                    }
                });
            }
            function setAgentLogout(btnAgentLogout) {
                var a = btnAgentLogout.value;
                var newVal = a.substring(3, a.length);
                $.ajax({
                    type: "GET",
                    url: 'agentsetforcelogout',
                    data: {agentId: newVal},
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (recData)
                    {
                        if (recData.retcode == "0")
                        {
                            setTimeout(function ()
                            {
                                $.get("${pageContext.request.servletContext.contextPath}/agent/dashboard_v2");
//                                $('.cssload-loader1').show();
//                                $('.overlay1').show();
                            }, 1000);
                            Notifier.success('Logout Agent ' + newVal + ' Successfully');
                        } else
                        {
                            Notifier.error('Failed to agent ' + newVal + ' to logout');
                        }
                    },
                    error: function (xhr, status, error) {
                        Notifier.error('Failed to agent ' + newVal + ' to logout');
                    }
                });
            }
            function getAgentStats(btnAgentStats) {
                var a = btnAgentStats.id;
                var newVal = a.substring(4, a.length);
                debugger;
                console.log("bn4 ajax request is called now");
                $.ajax({
                    type: "GET",
                    url: 'agentgetstatistics',
                    data: {agentId: newVal},
                    crossDomain: true,
                    cache: false,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        console.log("StatList:\t" + data.res.retcode);
                        console.log("AgentDetailData:\t" + data.agentbyworkno.retcode);
                        if (data.res.retcode == "0")
                        {
                            var totalSignInDuration = 0;
                            var agentDetail = data.agentbyworkno.result;
                            var skills = "";
                            console.log('Agent Detail of ' + agentDetail.workno + ': \n' + agentDetail.phonenumber + '\n' + agentDetail.mediatype + '\n');
                            $.each(agentDetail.skilllist, function (i, item) {
                                console.log(item.name + '\n');
                                skills += item.name + ' ,';
                            });
                            debugger;
                            skills = skills.substring(0, skills.length - 1);
                            var da = data.res.result[0].idxs;
                            var da1 = data.agentdurationresp.result[0].idxs[0].val;
//                            $.each(data.agentdurationresp.result[0].idxs[0].val, function (i, item) {
//                                totalSignInDuration = totalSignInDuration + parseInt(item.duration);
//                            });
                            data = data.res.result;
                            $("#agentStatsTableBody").empty();
                            var trHTML = '';
//                            var duration = moment.duration(totalSignInDuration, 'seconds');
//                            var formatted = duration.format("hh:mm:ss");
//                            var m = Math.floor(totalSignInDuration / 60);
//                            var s = Math.floor(totalSignInDuration % 60);
//                            console.log(totalSignInDuration);
                            trHTML += '<tr><td>Agent WorkId</td><td>' + agentDetail.workno + '</td></tr>';
                            trHTML += '<tr><td>Agent Phone No</td><td>' + agentDetail.phonenumber + '</td></tr>';
//                            trHTML += '<tr><td>Agent Media Type</td><td>' + agentDetail.mediatype + '</td></tr>';
                            trHTML += '<tr><td>Agent Group Name</td><td>' + agentDetail.groupname + '</td></tr>';
                            trHTML += '<tr><td>Agent Skills</td><td>' + skills + '</td></tr>';
                            // trHTML += '<tr><td>Agent Sign In Duration</td><td>' + formatted + '</td></tr>';
                            //$('#btnRefreshStats').addClass(agentDetail.workno);
                            // $("#btnRefreshStats").on("click", function () { ReloadAgentStats(); });
                            $("#btnRefreshStats").off('click').on('click', function () {
                                // function body
                            });
                            $('#btnRefreshStats').attr('onClick', 'ReloadAgentStats(' + agentDetail.workno + ')');
                            //$("#btnRefreshStats").click(function () { ReloadAgentStats(); });
                            // $('#btnRefreshStats').attr('id', 'btnRefreshStats' + agentDetail.workno);
                            $.each(data[0].idxs, function (i, item) {
                                if (item.id.includes("Duration")) {
                                    var seconds = parseInt(item.val);
                                    var duration = moment.duration(seconds, 'seconds');
                                    var format = duration.format("hh:mm:ss");
                                    trHTML += '<tr><td>' + item.id + '</td><td>' + format + '</td></tr>';
                                } else {
                                    trHTML += '<tr><td>' + item.id + '</td><td>' + item.val + '</td></tr>';
                                }
                            });
                            $('#agentStatsTableBody').append(trHTML);
                            console.log('Geting Agent Statistics Showing PopUp');
                            $("#statPopUp").modal('show');
                        } else {
                            Notifier.error('No Data Found');
                        }
                    },
                    error: function (xhr, status, error) {
                        Notifier.error('No Data Found' + status);
                    }
                });
            }

            function toTitleCase(str) {
                return str.replace(/(?:^|\s)\w/g, function (match) {
                    return match.toUpperCase();
                });
            }
            function AdjustAgentSkill(btnAdjustSkill)
            {
                var a = btnAdjustSkill.value;
                var newVal = a.substring(8, a.length);
                $.ajax({
                    type: "GET",
                    url: 'getallskills',
                    data: {agentId: newVal},
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (recData)
                    {
                        console.log("All Skills \t" + recData.res.retcode);
                        console.log("Specific Skill \t" + recData.resUniskill.retcode);
                        if (recData.res.retcode == "0")
                        {
                            var agentDetail = recData.res.message;
                            var skills = "";
                            var workno = agentDetail.split("|")[0];
                            var phonenumber = agentDetail.split("|")[1];
                            var popDiv = $('#popUpData');
                            popDiv.empty();
                            var ulVal = '<ul style="list-style:none;" id="agentSkillList">'
                            ulVal += '<li id="skillWorkId">WorkID : ' + newVal + '</li>';
//                            ulVal += '<li >Phone Num : ' + phonenumber + '</li>';
                            recData.res.result.forEach(function (item) {
                                var flag = false;
                                if (recData.resUniskill.result != null) {
                                    recData.resUniskill.result.forEach(function (item1) {
                                        if (parseInt(item.skillId) == parseInt(item1.id)) {
                                            ulVal += '<li><div class="checkbox checkbox-success"><input id="' + item.skillId + '" type="checkbox" checked value="' + item.skillName + '" ><label for="' + item.skillId + '">' + toTitleCase(item.skillName) + '</label></div></li>';
                                            flag = true;
                                            return false;
                                        }
                                    });
                                }
                                if (flag == false) {
                                    ulVal += '<li><div class="checkbox checkbox-success"><input id="' + item.skillId + '" type="checkbox" value="' + item.skillName + '" ><label for="' + item.skillId + '">' + toTitleCase(item.skillName) + '</label></div></li>';
                                }
                            });
                            ulVal += '</ul>';
                            popDiv.append(ulVal);
                            $("#skillAdjustPopUp").modal('show');
                        } else
                        {
                            Notifier.error("Failed to get agent details");
                        }



                    },
                    error: function (xhr, status, error) {

                        console.log("Adjust Error " + status);
                    }
                });
            }
            function ReloadAgentStats(data2) {


                $.ajax({
                    type: "GET",
                    url: '../Home/GetAgentStatistics',
                    data: {agentId: data2},
                    crossDomain: true,
                    cache: false,
                    contentType: "application/json",
                    dataType: "json",
                    success: function (response) {
                        if (response.StatList.retcode == "0")
                        {
                            var totalSignInDuration = 0;
                            var agentDetail = response.AgentDetailData.result;
                            var skills = "";
                            console.log('Agent Detail of ' + agentDetail.workno + ': \n' + agentDetail.phonenumber + '\n' + agentDetail.mediatype + '\n');
                            $.each(agentDetail.skilllist, function (i, item) {
                                console.log(item.name + '\n');
                                skills += item.name + ' ,';
                            });
                            skills = skills.substring(0, skills.length - 1);
                            console.log('Geting Agent Statistics' + response.StatList.message);
                            var da = response.StatList.result[0].idxs;
                            var da1 = response.SignInSignOut.result[0].idxs[0].val;
                            $.each(response.SignInSignOut.result[0].idxs[0].val, function (i, item) {
                                totalSignInDuration = totalSignInDuration + parseInt(item.duration);
                            });
                            response = response.StatList.result;
                            $("#agentStatsTableBody").empty();
                            var trHTML = '';
                            var m = Math.floor(totalSignInDuration / 60);
                            var s = Math.floor(totalSignInDuration % 60);
                            var duration = moment.duration(totalSignInDuration, 'seconds');
                            var formatted = duration.format("hh:mm:ss");
                            console.log(totalSignInDuration);
                            trHTML += '<tr><td>Agent WorkId</td><td>' + agentDetail.workno + '</td></tr>';
                            trHTML += '<tr><td>Agent Phone No</td><td>' + agentDetail.phonenumber + '</td></tr>';
//                            trHTML += '<tr><td>Agent Media Type</td><td>' + agentDetail.mediatype + '</td></tr>';
                            trHTML += '<tr><td>Agent Group Name</td><td>' + agentDetail.groupname + '</td></tr>';
                            trHTML += '<tr><td>Agent Skills</td><td>' + skills + '</td></tr>';
                            trHTML += '<tr><td>Agent Sign In Duration</td><td>' + formatted + '</td></tr>';
                            $.each(response[0].idxs, function (i, item) {
                                trHTML += '<tr><td>' + item.id + '</td><td>' + item.val + '</td></tr>';
                            });
                            $('#agentStatsTableBody').append(trHTML);
                            console.log('Geting Agent Statistics Showing PopUp');
                        } else
                        {
                            Notifier.error("Failed retreive again");
                        }


                    },
                    error: function (xhr, status, error) {
                        var err = eval("(" + xhr.responseText + ")");
                        console.log(err.Message);
                    }
                });
            }
            $("#btnRefreshStats").click(function () {
                reset();
            });
            $("#btnSaveAgentAdjustSkills").click(function ()
            {
                $('.cssload-loader1').show();
                $('.overlay1').show();
                var workid = $('#skillWorkId').text();
                workid = workid.split(":")[1].trim();
                var skill = [];
                var i = 0;
                $('#agentSkillList :checkbox').each(function () {
                    if ($(this).prop('checked') == true)
                    {
//                        skill[i] = $(this).val();
                        skill[i] = $(this).attr("id");
                        i++;
                    }

                });
                var skillDataParam = new Object();
                skillDataParam.workNo = workid;
                skillDataParam.skills = skill;
                $.ajax({
                    type: "POST",
                    url: "agentadjustskill",
                    contentType: "application/json; charset=utf-8",
                    data: JSON.stringify(skillDataParam),
                    dataType: "json",
                    success: function (recData)
                    {
                        if (recData.retcode == "0")
                        {
                            $('.cssload-loader1').hide();
                            $('.overlay1').hide();
//                            reloadAgentStates();
                            Notifier.success('Skills Adjusted Successfully');
                            $("#skillAdjustPopUp").modal('hide');
                        } else
                        {
                            $('.cssload-loader1').hide();
                            $('.overlay1').hide();
                            Notifier.error('Skills Adjustment Failed');
                        }
                    },
                    error: function (xhr, status, error) {
                        $('.cssload-loader1').hide();
                        $('.overlay1').hide();
                        console.log(err.status);
                    }
                });
            });
        </script>

    </body>
</html>
